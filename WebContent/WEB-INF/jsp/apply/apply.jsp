<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<sec:csrfMetaTags />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>中银经营贷</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/apply/apply.css"/>
	</head>
	<body>
		<!--中银经营贷-->
		<div class="margin_top">
			<div class="weui-cells weui-cells_form">
			  	<div class="weui-cell">
				    <div class="weui-cell__hd"><label class="weui-label">产品代码：</label></div>
				    <div class="weui-cell__bd">
				      	<input class="weui-input productCode" placeholder="000539" disabled="disabled">
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form">
			  	<div class="weui-cell">
				    <div class="weui-cell__hd"><label class="weui-label">产品名称：</label></div>
				    <div class="weui-cell__bd">
				      	<input class="weui-input productName" placeholder="中银经营贷" disabled="disabled">
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form">
			  	<div class="weui-cell weui-cells_form">
				    <div class="weui-cell__hd"><label class="weui-label">年利率：</label></div>
				    <div class="weui-cell__bd">
				      	<input class="weui-input interestRate" placeholder="8.1%" disabled="disabled">
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form expirationTime">
			  	<div class="weui-cell">
				    <div class="weui-cell__hd"><label class="weui-label">截止日期：</label></div>
				    <div class="weui-cell__bd">
				      	<input class="weui-input expiration" placeholder="" disabled="disabled">
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form">
			  	<div class="weui-cell textarea">
				    <div class="weui-cell__hd"><label class="weui-label">产品简介：</label></div>
				    <div class="weui-cell__bd">
				      	<textarea name="" rows="" cols="" class="weui-textarea productIntroduction" disabled="disabled">根据小微企业所提供的工商物业抵押价值，向小微客户发放的，用于其经营中涉及物业装修、维护、改造等用途的融资产品。</textarea>
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form expirationTime">
			  	<div class="weui-cell">
				    <div class="weui-cell__hd"><label class="weui-label">预约开始时间：</label></div>
				    <div class="weui-cell__bd">
				      	<input class="weui-input" type="text" id='datetime-picker'/>
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form expirationTime">
			  	<div class="weui-cell">
				    <div class="weui-cell__hd"><label class="weui-label">预约结束时间：</label></div>
				    <div class="weui-cell__bd">
				      	<input class="weui-input" type="text" id='datetime-picke'/>
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form">
			  	<div class="weui-cell">
				    <div class="weui-cell__hd"><label class="weui-label">市县区域：</label></div>
				    <div class="weui-cell__bd region">
				      	<select class="weui_select district" name="select1">
		 					<option selected="请选择区域" value="0">请选择区域</option>
		 				</select>
				    </div>
			  	</div>
			</div>
			<div class="weui-cells weui-cells_form">
			  	<div class="weui-cell">
				    <div class="weui-cell__hd"><label class="weui-label">受理网点：</label></div>
				    <div class="weui-cell__bd">
				      	<select class="weui_select mechanism" name="select2">
		 					<option selected="请选择机构" value="0">请选择机构</option>
		 				</select>
				    </div>
			  	</div>
			</div>
			<button class="weui_btn weui_btn_primary appointment">预约申请</button>
			<div class="tips">
				<div class="tipstext">
					<span class="Written">提示语</span>
					<button class="weui_btn weui_btn_primary Sure">确定</button>
				</div>
			</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script>
	  	$("#datetime-picker").datetimePicker();
	  	$("#datetime-picke").datetimePicker();
	</script>
	</script>
	<script type="text/jscript">
		var csrfParameter = $("meta[name='_csrf_parameter']").attr('content');
		var csrfHeader = $("meta[name='_csrf_header']").attr('content');
		var csrfToken = $("meta[name='_csrf']").attr('content');
	</script>
	<script src="/static_resources/js/apply/apply.js"></script>
	</body>
</html>