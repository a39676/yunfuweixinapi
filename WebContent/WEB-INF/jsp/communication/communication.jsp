<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>云浮市普惠金融服务平台</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/communication/communication.css"/>
	</head>
	<body>
		<div class="weui_tab">
		    <ul class="weui_tabbar">
		    	<li href="#" class="weui_tabbar_item weui_bar_item_on">
		        	<p class="weui_tabbar_label"><img src="/static_resources/images/u3033_img.png" alt="" /></p>
		    	</li>
		    	<li href="#" class="weui_tabbar_item">
		        	<p class="weui_tabbar_label base">金融服务</p>
		        	<ul class="navigation">
			    		<li><a href="../docking/docking.jsp" class="navigation_zi">融资对接</a></li>
			    		<li><a href="../financing/financing.jsp" class="navigation_zi">投资理财</a></li>
			    		<li><a href="../exchange/exchange.jsp" class="navigation_zi">零钱兑换</a></li>
			    		<li><a href="../insurance/insurance.jsp" class="navigation_zi">保险服务</a></li>
			    		<li><a href="../securities/securities.jsp" class="navigation_zi">证券服务</a></li>
			    		<li><a href="../areas/areas.jsp" class="navigation_zi">互联网+信用三农</a></li>
			    		<li><a href="../Complaint/Complaint.jsp" class="navigation_zi">举报投诉</a></li>
			    	</ul>
		    	</li>
	    		<li href="#" class="weui_tabbar_item">
		        	<p class="weui_tabbar_label base">知识学习</p>
		        	<ul class="navigation">
			    		<li><a href="../knowledge/knowledge.jsp" class="navigation_zi">金融知识</a></li>
			    		<li><a href="../answer/answer.jsp" class="navigation_zi">有奖问答</a></li>
			    	</ul>
		    	</li>
		    	<li href="#" class="weui_tabbar_item">
		        	<p class="weui_tabbar_label base">快捷功能</p>
		        	<ul class="navigation">
			    		<li><a href="../notice/notice.jsp" class="navigation_zi">公告</a></li>
			    		<li><a href="../link/link.jsp" class="navigation_zi">链接</a></li>
			    		<li><a href="../sign/sign.jsp" class="navigation_zi">注册登录</a></li>
			    		<li>
			    			<a href="#" class="navigation_zi activeMay">我的申请</a>
			    			<ul class="navigationOne">
					    		<li><a href="../normal/normal.jsp" class="navigation_zi">正常申请受理状态</a></li>
					    		<li><a href="../aberration/aberration.jsp" class="navigation_zi">履约失常状态</a></li>
					    		<li><a href="../overtime/overtime.jsp" class="navigation_zi">受理超时状态</a></li>
					    	</ul>
			    		</li>
			    	</ul>
		    	</li>
		    </ul>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script src="/static_resources/js/communication/communication.js"></script>
	</body>
</html>