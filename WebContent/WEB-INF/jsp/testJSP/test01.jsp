<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<sec:csrfMetaTags />
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.1.1/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.3/js/tether.js"></script>
<script type="text/javascript" src="https://cdn.bootcss.com/bootstrap/4.1.0/js/bootstrap.js"></script>



test jsp<br>
<label id="csrfHeader">csrfHeader</label><br>
<label id="csrfToken">csrfToken</label><br>

<button name="testB">testB</button>
<input type="text" name="areaId" id="areaId">

<script type="text/javascript">
  var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
  var csrfHeader = $("meta[name='_csrf_header']").attr("content");
  var csrfToken = $("meta[name='_csrf']").attr("content");

  $("#csrfHeader").empty();
  $("#csrfHeader").append(csrfHeader);
  $("#csrfToken").empty();
  $("#csrfToken").append(csrfToken);


  $("button[name='testB']").click(function () {
    var url = "/geographical/findByBelongId";
    var jsonOutput = {
      areaParentId:$("#areaId").val()
    };
    $.ajax({  
      type : "POST",  
      async : true,
      url : url,  
      data: JSON.stringify(jsonOutput),
      cache : false,
      contentType: "application/json",
      dataType: "json",
      timeout:50000,  
      beforeSend: function(xhr) {
        xhr.setRequestHeader(csrfHeader, csrfToken);
      },
      success:function(datas){
        console.log(datas);
      },  
      error: function(datas) {              
      }  
    });  
  });
</script>