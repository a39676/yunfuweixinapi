<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>零钱兑换</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/exchange/exchange.css"/>
	</head>
	<body>
		<!--零钱兑换-->
		<div class="weui_tab">
			<div class="margin_top">
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <a class="weui-cell__hd" href="/reservation/applicationExchangeDib"><img src="/static_resources/images/u3123_img.png" alt="" /></a>
					    <div class="weui-cell__bd">零钱兑换</div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <a class="weui-cell__hd" href="/reservation/applicationExchangeDib"><img src="/static_resources/images/u3122_img.png" alt="" /></a>
					    <div class="weui-cell__bd">残钞兑换</div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <a class="weui-cell__hd" href="/reservation/applicationExchangeDib"><img src="/static_resources/images/u3125_img.png" alt="" /></a>
					    <div class="weui-cell__bd">硬/纸币兑换</div>
				  	</div>
				</div>
			</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	</body>
</html>