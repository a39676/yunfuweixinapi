<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>金融知识</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/knowledge/knowledge.css"/>
	</head>
	<body>
		<!--金融知识-->
		<section class="weui_body">
			<div class="weui-row weui-no-gutter">
				<div class="weui-col-20">理财常识</div>
				<div class="weui-col-20">保险常识</div>
				<div class="weui-col-20">证券知识</div>
				<div class="weui-col-20">投资知识</div>
				<div class="weui-col-20">贷款常识</div>
				<div class="weui-col-20">金融政策</div>
			</div>
		</section>
		<div class="weui_panel weui_panel_access">
	 		<div class="weui_panel_bd">
	 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
	 				<div class="weui_media_hd">
		 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
		 			</div>
		 			<div class="weui_media_bd">
	 					<p class="weui_media_desc">二维码付款需做好遮挡，注意安全！</p>
					</div>
					<div class="weui-cell__ft">2018-08-24</div>
	 			</a>
	 		</div>
	 		<div class="weui_panel_bd">
	 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
	 				<div class="weui_media_hd">
		 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
		 			</div>
		 			<div class="weui_media_bd">
	 					<p class="weui_media_desc">注意小细节 让你的银行卡更安全</p>
					</div>
					<div class="weui-cell__ft">2018-07-10</div>
	 			</a>
	 		</div>
	 		<div class="weui_panel_bd">
	 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
	 				<div class="weui_media_hd">
		 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
		 			</div>
		 			<div class="weui_media_bd">
	 					<p class="weui_media_desc">一图教你增强风险意识、了解基本权利</p>
					</div>
					<div class="weui-cell__ft">2018-06-29</div>
	 			</a>
	 		</div>
	 		<div class="weui_panel_bd">
	 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
	 				<div class="weui_media_hd">
		 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
		 			</div>
		 			<div class="weui_media_bd">
	 					<p class="weui_media_desc">做到这六个“不”，才能收守住“钱袋子”</p>
					</div>
					<div class="weui-cell__ft">2018-05-15</div>
	 			</a>
	 		</div>
	 	</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	</body>
</html>