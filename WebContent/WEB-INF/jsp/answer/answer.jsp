<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>有奖问答</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/answer/answer.css"/>
	</head>
	<body>
		<!--有奖问答-->
		<div class="weui_tab">
		    <div class="">
		    	<div class="weui-flex active">
				  	<div class="weui_panel_hd">1. 防范电信诈骗的措施包括（）</div>
				  	<div class="weui_cells weui_cells_checkbox">
				  		<label class="weui_cell weui_check_label" for="s11">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" class="weui_check" name="checkbox1" id="s11" checked="checked">
					            <i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>不透露</p>
					        </div>
					    </label>
					    <label class="weui_cell weui_check_label" for="s12">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" name="checkbox1" class="weui_check" id="s12"> 
				        		<i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>不转账</p>
					        </div>
					    </label>
					    <label class="weui_cell weui_check_label" for="s13">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" name="checkbox1" class="weui_check" id="s13"> 
				        		<i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>不轻信</p>
					        </div>
					    </label>
					    <label class="weui_cell weui_check_label" for="s14">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" name="checkbox1" class="weui_check" id="s14"> 
				        		<i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>要及时报案</p>
					        </div>
					    </label>
				  	</div>
				</div>
				<div class="weui-flex active_two">
				  	<div class="weui_panel_hd">2. 社会公众可以通过（）方式举报洗钱犯罪及其上游犯罪，所有举报信息将严格保密。</div>
				  	<div class="weui_cells weui_cells_checkbox">
				  		<label class="weui_cell weui_check_label" for="s15">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" class="weui_check" name="checkbox1" id="s15" checked="checked">
					            <i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>传真</p>
					        </div>
					    </label>
					    <label class="weui_cell weui_check_label" for="s16">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" name="checkbox1" class="weui_check" id="s16"> 
				        		<i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>电话</p>
					        </div>
					    </label>
					    <label class="weui_cell weui_check_label" for="s17">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" name="checkbox1" class="weui_check" id="s17"> 
				        		<i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>电子邮件</p>
					        </div>
					    </label>
					    <label class="weui_cell weui_check_label" for="s18">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" name="checkbox1" class="weui_check" id="s18"> 
				        		<i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>信件</p>
					        </div>
					    </label>
				  	</div>
				</div>
				<div class="weui-flex active_three">
				  	<div class="weui_panel_hd">3. 任何单位和个人在与金融机构建立业务关系时，都应当提供真实有效的身份证件或者其他身份证明文件。</div>
				  	<div class="weui_cells weui_cells_checkbox">
				  		<label class="weui_cell weui_check_label" for="s19">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" class="weui_check" name="checkbox1" id="s19" checked="checked">
					            <i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>正确</p>
					        </div>
					    </label>
					    <label class="weui_cell weui_check_label" for="s20">
					        <div class="weui_cell_hd">
					        	<input type="checkbox" name="checkbox1" class="weui_check" id="s20"> 
				        		<i class="weui_icon_checked"></i>
					        </div>
					        <div class="weui_cell_bd weui_cell_primary">
					        	<p>错误</p>
					        </div>
					    </label>
				  	</div>
				</div>
				<div class="weui-flex active_four">
				  	<div class="weui_panel_hd">恭喜您！您是第99位正确答题者，中奖啦！</div>
				</div>
		    </div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script src="/static_resources/js/answer/answer.js"></script>
	</body>
</html>