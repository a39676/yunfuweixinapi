<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<sec:csrfMetaTags />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		
		
		<title>小微企业贷款</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/loan/loan.css"/>
	</head>
	<body>
		<!--小微企业贷款-->
		<div class="weui_tab">
			<div class="margin_top">
				<div class="weui-tab">
				  	<div class="weui-navbar">
					    <a class="weui-navbar__item weui-bar__item--on tab1" href="#tab1">产品</a>
					    <a class="weui-navbar__item tab2" href="#tab2">更多</a>
				  	</div>
				  	<div class="weui-tab__bd" style="clear: both;">
					    <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
					      	
					    </div>
					    <div id="tab2" class="weui-tab__bd-item">
							<div class="weui-cells">
							  	<div class="weui-cell">
							    	<p class="weui-cell_top"><span class="data"></span></p>
							    	<p class="weui-cell_top color_top">8.1%</p>
							    	<p class="weui-cell_top">年利率</p>
							  	</div>
							  	<div class="weui-cell__bd">
						      		<a href="../product/product.html" class="weui_btn weui_btn_primary">详细介绍</a>
						      		<a href="../apply/apply.html" class="weui_btn weui_btn_primary">我要申请</a>
						    	</div>
							</div>
					    </div>
				  	</div>
				</div>
			</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script type="text/jscript">
		var csrfParameter = $("meta[name='_csrf_parameter']").attr('content');
		var csrfHeader = $("meta[name='_csrf_header']").attr('content');
		var csrfToken = $("meta[name='_csrf']").attr('content');
	</script>
	<script src="/static_resources/js/loan/loan.js"></script>
	</body>
</html>