<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<sec:csrfMetaTags />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>金融知识</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/normal/normal.css"/>
	</head>
	<body>
		<!--正常受理状态-->
		<div class="weui-cells weui-cells_form margintop">
		  	<div class="weui-cell">
			    <div class="weui-cell__hd"><label class="weui-label">申请状态：</label></div>
			    <div class="weui-cell__bd">
			      	<select class="weui_select ReservationStatusType" name="select1">
	 					<option selected="" value="1">申请中</option>
	 					<option value="2">已受理</option>
	 					<option value="3">已拒绝</option>
	 					<option value="4">已履约</option>
	 					<option value="5">已爽约</option>
	 				</select>
			    </div>
		  	</div>
		</div>
		<div class="weui_tab">
			<div class="margin_top">
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label color_lable">购车贷款</label></div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理号：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="10563">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理结果：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="通过">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理网点：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="云城区支行">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理时间：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="2018-09-20">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理人：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="李经理">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">联系电话：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="13898764765">
					    </div>
				  	</div>
				</div>
			</div>
			<div class="margin_top">
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label color_lable">购房贷款</label></div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理号：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="10987">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理结果：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="拒绝">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理原因：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" placeholder="资质不通过">
					    </div>
				  	</div>
				</div>
			</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script type="text/jscript">
		var csrfParameter = $("meta[name='_csrf_parameter']").attr('content');
		var csrfHeader = $("meta[name='_csrf_header']").attr('content');
		var csrfToken = $("meta[name='_csrf']").attr('content');
	</script>
	<script src="/static_resources/js/normal/normal.js"></script>
	</body>
</html>