<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<sec:csrfMetaTags />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>产品详情</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/product/product.css"/>
	</head>
	<body>
		<!--中银经营贷-->
		<div class="">
			<div class="margin_top">
				<div class="weui-tab">
				  	<header class="demos-header">
				      	<p class="product">中银经营贷(000539)</p>
				    </header>
				    <div class="section">
				    	<section>
				    		<p>产品介绍</p>
				    		<p>工商物业经营贷是根据小微企业所提供的工商物业抵押价值，向小微客户发放的，用于其经营中涉及物业装修、维护、改造等用途的融资产品。</p>
				    	</section>
				    	<section>
				    		<p>产品特点</p>
				    		<p>一、贷款期限长。贷款期限最长可至10年，有效支持物业持有人进行物业改造升级。</p>
				    		<p>二、贷款用途多。有效满足持有人在经营期间的装修、维护、改造等多方面资金需求。</p>
				    		<p>三、还款压力小。按月还本付息，单期还款压力小，为企业灵活安排经营资金留出充足空间。</p>
				    	</section>
				    	<section>
				    		<p>办理指引</p>
				    		<p>一、业务申请。办理业务时，借款人应向农业银行提交借款申请书。</p>
				    		<p>二、业务受理。农业银行受理后，须要求借款人提供以下资料：</p>
				    		<p>（一）企业基本资料、企业财务报表、公司章程、董事会决议、抵押担保。</p>
				    		<p>三、业务审批。农业银行进行贷款调查、审查和审批。</p>
				    		<p>四、合同签订。农业银行与借款人签订借款合同、担保合同。</p>
				    		<p>五、贷款发放</p>
				    	</section>
				    </div>
				</div>
			</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script type="text/jscript">
		var csrfParameter = $("meta[name='_csrf_parameter']").attr('content');
		var csrfHeader = $("meta[name='_csrf_header']").attr('content');
		var csrfToken = $("meta[name='_csrf']").attr('content');
	</script>
	<script src="/static_resources/js/product/product.js"></script>
	</body>
</html>