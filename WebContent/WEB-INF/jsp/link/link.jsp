<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>链接</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/link/link.css"/>
	</head>
	<body>
		<!--链接-->
		<div class="weui_tab">
		    <div class="weui_panel weui_panel_access">
		 		<a class="weui_cell" href=""> 中征应收账款融资服务平台</a>
		 	</div>
		 	<div class="weui_panel weui_panel_access">
		 		<a class="weui_cell" href=""> 中征存款融资服务平台</a>
		 	</div>
		 	<div class="weui_panel weui_panel_access">
		 		<a class="weui_cell" href=""> 云浮市中小微企业信用信息和融资对接平台</a>
		 	</div>
		 	<div class="weui_panel weui_panel_access">
		 		<a class="weui_cell" href=""> 会员单位公众服务号</a>
		 	</div>
		 	<div class="weui_panel weui_panel_access">
		 		<a class="weui_cell" href=""> 会员单位网站</a>
		 	</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script src="/static_resources/js/answer/answer.js"></script>
	</body>
</html>