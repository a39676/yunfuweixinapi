<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<sec:csrfMetaTags />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		<title>公告</title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/notice/notice.css"/>
	</head>
	<body>
		<!--公告-->
		<div class="weui_tab">
		    <div class="weui_panel weui_panel_access">
		 		<div class="weui_panel_bd">
		 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
		 				<div class="weui_media_hd">
			 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
			 			</div>
			 			<div class="weui_media_bd">
		 					<p class="weui_media_desc">中国人民银行公告（2018）第11号</p>
						</div>
						<div class="weui-cell__ft">2018-08-24</div>
		 			</a>
		 		</div>
		 		<div class="weui_panel_bd">
		 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
		 				<div class="weui_media_hd">
			 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
			 			</div>
			 			<div class="weui_media_bd">
		 					<p class="weui_media_desc">中国人民银行公告（2018）第11号</p>
						</div>
						<div class="weui-cell__ft">2018-07-10</div>
		 			</a>
		 		</div>
		 		<div class="weui_panel_bd">
		 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
		 				<div class="weui_media_hd">
			 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
			 			</div>
			 			<div class="weui_media_bd">
		 					<p class="weui_media_desc">“金融消费者权益日”宣传手册</p>
						</div>
						<div class="weui-cell__ft">2018-06-29</div>
		 			</a>
		 		</div>
		 		<div class="weui_panel_bd">
		 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
		 				<div class="weui_media_hd">
			 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
			 			</div>
			 			<div class="weui_media_bd">
		 					<p class="weui_media_desc">区政府发布加快资本市场发展的意见</p>
						</div>
						<div class="weui-cell__ft">2018-05-15</div>
		 			</a>
		 		</div>
		 		<div class="weui_panel_bd">
		 			<a href="javascript:void(0);" class="weui_media_box weui_media_appmsg">
		 				<div class="weui_media_hd">
			 				<img class="weui_media_appmsg_thumb" src="/static_resources/images/u2814_img.jpg" alt="">
			 			</div>
			 			<div class="weui_media_bd">
		 					<p class="weui_media_desc">市政府发布科技创新扶持政策项目管理方法</p>
						</div>
						<div class="weui-cell__ft">2018-05-15</div>
		 			</a>
		 		</div>
		 	</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script type="text/jscript">
		var csrfParameter = $("meta[name='_csrf_parameter']").attr('content');
		var csrfHeader = $("meta[name='_csrf_header']").attr('content');
		var csrfToken = $("meta[name='_csrf']").attr('content');
	</script>
	<script src="/static_resources/js/notice/notice.js"></script>
	</body>
</html>