<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<sec:csrfMetaTags />
		<script type="text/javascript">
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 +"px";
					var deviceWidth = document.documentElement.clientWidth;
					if (deviceWidth > 640) {
						deviceWidth = 640;
					}
			document.documentElement.style.fontSize = deviceWidth / 6.4 + "px";
		</script>
		
		
		<title></title>
		<link rel="stylesheet" href="/static_resources/lib/css/weui.min.css">
		<link rel="stylesheet" href="/static_resources/lib/css/jquery-weui.css">
		<link rel="stylesheet" href="/static_resources/lib/css/demos.css">
		<link rel="stylesheet" type="text/css" href="/static_resources/css/loan/loan.css"/>
		<style type="text/css">* { touch-action: pan-y; }</style>
	</head>
	<body>
		<!--小微企业贷款-->
		<div class="weui_tab">
			<div class="margin_top">
				<div class="weui-pull-to-refresh-layer">
			 		<div class="down">下拉刷新</div>
			 		<div class="up">释放刷新</div>
			 		<div class="refresh">正在刷新</div>
			 	</div>
				<div class="weui-tab">
				  	<div class="weui-navbar">
					    <a class="weui-navbar__item weui-bar__item--on tab1" href="#tab1">产品</a>
					    <a class="weui-navbar__item tab2" href="#tab2">更多</a>
				  	</div>
				  	<div class="weui-tab__bd" style="clear: both;">
					    <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
					    </div>
					    <div id="tab2" class="weui-tab__bd-item">
							
					    </div>
				  	</div>
				</div>
			</div>
		</div>
	<script src="/static_resources/lib/js/jquery-2.1.4.js"></script>
	<script src="/static_resources/lib/js/fastclick.js"></script>
	<script src="/static_resources/js/jquery-weui.js"></script>
	<script>
		  $(function() {
			  $(document.body).pullToRefresh({
				  onRefresh: function () { /* 当下拉刷新触发的时候执行的回调 */ },
				  onPull: function (percent) { /* 用户下拉过程中会触发，接收一个百分比表示用户下拉的比例 */ },
				  distance: 50 /* 下拉刷新的触发距离， 注意，如果你重新定义了这个值，那么你需要重载一部分CSS才可以，请参考下面的自定义样式部分 */
			  });
			  $(document.body).pullToRefreshDone();
		  });
	</script>
	<script type="text/jscript">
		var csrfParameter = $("meta[name='_csrf_parameter']").attr('content');
		var csrfHeader = $("meta[name='_csrf_header']").attr('content');
		var csrfToken = $("meta[name='_csrf']").attr('content');
	</script>
	<script src="/static_resources/js/loan/loan.js"></script>
	</body>
</html>