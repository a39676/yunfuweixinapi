<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><sec:csrfMetaTags />
<meta name="Keywords" content="系统管理">
<meta name="Description" content="系统管理">
<title>系统管理</title>
<link href="/admin_resources/lib/iconfont/iconfont.css" type="text/css" rel="stylesheet">
<link href="/admin_resources/lib/layui/css/layui.css" type="text/css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  	<script src="/admin_resources/lib/js/html5shiv.js"></script>
  	<script src="/admin_resources/lib/js/respond.js"></script>
<![endif]-->
<link href="/admin_resources/lib/zTree/metroStyle/metroStyle.css" type="text/css" rel="stylesheet">
<link href="/admin_resources/lib/css/animate.min.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetStyle.css" />
<link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetUi.css" />
<link rel="stylesheet" type="text/css" href="/admin_resources/css/app.css" />
</head>
<body>
<!-- loading -->
<div id="loading">
    <div id="loading-center">
        <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
        </div>
    </div>
</div>
<!-- 后台框架布局 -->
<div class="layui-layout layui-layout-admin" id="layui-layout-admin" style="display: none;">
    <!-- 头部部分 -->
    <div class="layui-header">
        <ul class="layui-nav layui-layout-left" style="left: 0;color: #000;">
            <li class="layui-nav-item">
                <span class="nav-adjustable" id="nav-adjustable">
                    <i class="icon iconfont icon-weibiaoti2zhuanhuan transition-all" style="display: inline-block;"></i>
                </span>
                <b class="layui-header-title" id="systemTitle"></b>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right layui-nav-set" style="right: 0;">
            <li class="layui-nav-item" style="margin: 0 10px;">
                <a href="javascript:void(0);"><i class="icon iconfont icon-user" style="margin-right: 5px;font-size: 16px;position: relative;top: -1px;"></i><span id="userName"></span></a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:void(0);" id="personCenter">个人中心</a></dd>
                    <dd><a href="javascript:void(0);" id="logout">安全退出</a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <!-- 侧边导航 -->
    <div class="layui-side">
        <div class="layui-logo">
            <a href="javascript:void(0);"><img src="/admin_resources/images/logo.png" /></a>
        </div>
        <div class="layui-side-scroll" id="layui-side-scroll">
            <div id="menuTree" class="ztree"></div>
        </div>
    </div>
    <!-- 主体部分 -->
    <div class="layui-body" id="layui-body">
        <div class="layui-content" id="layui-content">
            <iframe src="view/home/home" frameborder='0' width='100%' height='100%'></iframe>
        </div>
    </div>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/jquery-1.12.2.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/layui/layui.js"></script>
<script type="text/javascript" src="/admin_resources/lib/zTree/jquery.ztree.all.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/echarts/echarts.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/json2.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jsrender-min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/easy.ajax.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/require.js" data-main="/admin_resources/js/app"></script>
</body>
</html>