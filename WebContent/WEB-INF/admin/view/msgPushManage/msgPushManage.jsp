<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><sec:csrfMetaTags />
    <meta name="Keywords" content="系统管理">
    <meta name="Description" content="系统管理">
    <title>系统管理</title>
    <link href="/admin_resources/lib/iconfont/iconfont.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/layui/css/layui.css" type="text/css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/admin_resources/lib/js/html5shiv.js"></script>
    <script src="/admin_resources/lib/js/respond.js"></script>
    <![endif]-->
    <link href="/admin_resources/lib/zTree/metroStyle/metroStyle.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/css/animate.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetStyle.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetUi.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/css/msgPushManage/msgPushManage.css" />
</head>
<body>
<div class="wrap">
    <blockquote class="layui-elem-quote">推送时间</blockquote>
    <div class="layui-form" style="margin-bottom: 5px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">起始时间:</label>
                <div class="layui-input-inline">
                    <input type="text" placeholder="" class="layui-input" id="startTime">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">截止时间:</label>
                <div class="layui-input-inline">
                    <input type="text" placeholder="" class="layui-input" id="endTime">
                </div>
            </div>
        </div>
    </div>
    <blockquote class="layui-elem-quote">推送至机构（金融服务）</blockquote>
    <div class="layui-form" style="margin-bottom: 5px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">融资产品申请信息:</label>
                <div class="layui-input-inline">
                    <select>
                        <option value=""></option>
                        <option value="10001">选项一</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">理财产品申请信息:</label>
                <div class="layui-input-inline">
                    <select>
                        <option value=""></option>
                        <option value="10001">选项一</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">保险产品申请信息:</label>
                <div class="layui-input-inline">
                    <select>
                        <option value=""></option>
                        <option value="10001">选项一</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">证券产品申请信息:</label>
                <div class="layui-input-inline">
                    <select>
                        <option value=""></option>
                        <option value="10001">选项一</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">零钞兑换申请信息:</label>
                <div class="layui-input-inline">
                    <select>
                        <option value=""></option>
                        <option value="10001">选项一</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <blockquote class="layui-elem-quote">推送至机构（其他服务）</blockquote>
    <p style="line-height: 40px;text-indent: 15px;">投诉举报信息:</p>
    <table id="dataTable" lay-filter="dataTable"></table>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/jquery-1.12.2.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/layui/layui.js"></script>
<script type="text/javascript" src="/admin_resources/lib/zTree/jquery.ztree.all.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/echarts/echarts.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/json2.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jsrender-min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/easy.ajax.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/require.js" data-main="/admin_resources/js/msgPushManage/msgPushManage"></script>
</body>
</html>