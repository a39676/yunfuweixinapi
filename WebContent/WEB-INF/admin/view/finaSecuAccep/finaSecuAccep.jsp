<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><sec:csrfMetaTags />
    <meta name="Keywords" content="系统管理">
    <meta name="Description" content="系统管理">
    <title>系统管理</title>
    <link href="/admin_resources/lib/iconfont/iconfont.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/layui/css/layui.css" type="text/css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/admin_resources/lib/js/html5shiv.js"></script>
    <script src="/admin_resources/lib/js/respond.js"></script>
    <![endif]-->
    <link href="/admin_resources/lib/zTree/metroStyle/metroStyle.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/css/animate.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetStyle.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetUi.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/css/finaSecuAccep/finaSecuAccep.css" />
</head>
<body>
	<!--证券产品受理-->
<div class="wrap">
    <!-- 查询条件 -->
    <div class="layui-form" style="margin-bottom: 5px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">受理号:</label>
                <div class="layui-input-inline">
                    <input type="text" placeholder="" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">受理结果:</label>
                <div class="layui-input-inline">
                    <select>
                        <option value=""></option>
                        <option value="10001">全部</option>
                        <option value="10002">通过</option>
                        <option value="10003">拒绝</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">受理网点:</label>
                <div class="layui-input-inline">
                    <select>
                        <option value=""></option>
                        <option value="10001">全部</option>
                        <option value="10002">云城区支行</option>
                        <option value="10003">罗定区支行</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">受理时间:</label>
                <div class="layui-input-inline">
                    <input type="text" placeholder="" class="layui-input" id="presentationTime">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">受理人:</label>
                <div class="layui-input-inline">
                    <input type="text" placeholder="" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">联系电话:</label>
                <div class="layui-input-inline">
                    <input type="text" placeholder="" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">拒收理由:</label>
                <div class="layui-input-inline">
                    <input type="text" placeholder="" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <button class="layui-btn layui-btn-custom-blue" lay-submit lay-filter="searchBtn" id="searchBtn">查询</button>
            </div>
        </div>
    </div>
    <!-- 数据渲染 -->
    <table id="dataTable" lay-filter="dataTable"></table>
    <!-- 表格分页 -->
    <div class="custom-page-box" id="dataPage"></div>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/jquery-1.12.2.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/layui/layui.js"></script>
<script type="text/javascript" src="/admin_resources/lib/zTree/jquery.ztree.all.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/echarts/echarts.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/json2.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jsrender-min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/easy.ajax.js"></script>
<!-- 表头toolbar模板 -->
<div id="dataTableToolBarTpl" style="display: none;">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="edit">提交</a>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/require.js" data-main="/admin_resources/js/finaSecuAccep/finaSecuAccep"></script>
</body>
</html>