<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><sec:csrfMetaTags />
    <meta name="Keywords" content="系统管理">
    <meta name="Description" content="系统管理">
    <title>系统管理</title>
    <link href="/admin_resources/lib/iconfont/iconfont.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/layui/css/layui.css" type="text/css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/admin_resources/lib/js/html5shiv.js"></script>
    <script src="/admin_resources/lib/js/respond.js"></script>
    <![endif]-->
    <link href="/admin_resources/lib/zTree/metroStyle/metroStyle.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/css/animate.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetStyle.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetUi.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/css/home/home.css" />
</head>
<body>
	<!--零钱兑换限额管理-->
<div class="wrap">
    <!-- 查询条件 -->
    <div class="layui-form-item layui-layer-padding ">
    	<div class="layui-col-xs12 demo border">
	      	<span class="grid-demo grid-demo-bg2">账号整体情况</span>
	    </div>
    	<div class="layui-col-xs12 border" style="padding: 8px;">
    		<div class="layui-col-xs6 user">
		      	<div class="user_top">新用户</div>
		      	<div class="user_bottom">32</div>
		    </div>
		    <div class="layui-col-xs6 user">
		      	<div class="user_top">总用户数</div>
		      	<div class="user_bottom">888</div>
		    </div>
    	</div>
    </div>
    <div class="layui-form-item layui-layer-padding margin_top">
    	<div class="layui-col-xs12 demo border">
	      	<span class="grid-demo grid-demo-bg2">待处理事务</span>
	    </div>
    	<div class="layui-col-xs12 border" style="padding: 8px;color: #666;">
    		<div class="layui-col-xs4">
		      	<div class="affair layui-col-xs10" style="">
		      		<div class="left layui-col-xs8">融资产品受理</div>
		      		<div class="right"><span>(</span><span class="Number">10</span><span>)</span></div>
		      	</div>
		    </div>
		    <div class="layui-col-xs4">
		      	<div class="affair layui-col-xs10" style="">
		      		<div class="left layui-col-xs8">理财产品受理</div>
		      		<div class="right"><span>(</span><span class="Number">10</span><span>)</span></div>
		      	</div>
		    </div>
		    <div class="layui-col-xs4">
		      	<div class="affair layui-col-xs10" style="">
		      		<div class="left layui-col-xs8">保险产品受理</div>
		      		<div class="right"><span>(</span><span class="Number">10</span><span>)</span></div>
		      	</div>
		    </div>
		    <div class="layui-col-xs4">
		      	<div class="affair layui-col-xs10" style="">
		      		<div class="left layui-col-xs8">证券产品受理</div>
		      		<div class="right"><span>(</span><span class="Number">10</span><span>)</span></div>
		      	</div>
		    </div>
		    <div class="layui-col-xs4">
		      	<div class="affair layui-col-xs10" style="">
		      		<div class="left layui-col-xs8">零钞兑换受理</div>
		      		<div class="right"><span>(</span><span class="Number">10</span><span>)</span></div>
		      	</div>
		    </div>
		    <div class="layui-col-xs4">
		      	<div class="affair layui-col-xs10" style="">
		      		<div class="left layui-col-xs8">投诉举报受理</div>
		      		<div class="right"><span>(</span><span class="Number">10</span><span>)</span></div>
		      	</div>
		    </div>
		    <div class="layui-col-xs4">
		      	<div class="affair layui-col-xs10" style="">
		      		<div class="left layui-col-xs8">互联网+信用三农产品受理</div>
		      		<div class="right"><span>(</span><span class="Number">10</span><span>)</span></div>
		      	</div>
		    </div>
    	</div>
    </div>
    <div class="layui-form-item layui-layer-padding margin_top">
    	<div class="layui-col-xs12 demo border">
	      	<span class="grid-demo grid-demo-bg2">服务快捷入口</span>
	    </div>
    	<div class="layui-col-xs12 border" style="padding: 8px;">
		    <div class="layui-col-xs12">
		      	<a href="#" class="Quick">
		      		<img src="/admin_resources/images/mechanism.png" alt="" />
		      		<div class="Entrance">
		      			机构人员管理
		      		</div>
		      	</a>
		      	<a href="#" class="Quick">
		      		<img src="/admin_resources/images/information.png" alt="" />
		      		<div class="Entrance">
		      			信息推送管理
		      		</div>
		      	</a>
		      	<a href="#" class="Quick">
		      		<img src="/admin_resources/images/financing.png" alt="" />
		      		<div class="Entrance">
		      			融资产品受理
		      		</div>
		      	</a>
		    </div>
    	</div>
    </div>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/jquery-1.12.2.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/layui/layui.js"></script>
<script type="text/javascript" src="/admin_resources/lib/zTree/jquery.ztree.all.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/echarts/echarts.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/json2.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jsrender-min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/easy.ajax.js"></script>
<!-- 表头toolbar模板 -->
<div id="dataTableToolBarTpl" style="display: none;">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="edit">提交</a>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/require.js" data-main="/admin_resources/js/finaExchaAdmin/finaExchaAdmin"></script>
</body>
</html>