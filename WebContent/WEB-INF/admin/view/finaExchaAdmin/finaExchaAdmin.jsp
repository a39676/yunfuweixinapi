<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><sec:csrfMetaTags />
    <meta name="Keywords" content="系统管理">
    <meta name="Description" content="系统管理">
    <title>系统管理</title>
    <link href="/admin_resources/lib/iconfont/iconfont.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/layui/css/layui.css" type="text/css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/admin_resources/lib/js/html5shiv.js"></script>
    <script src="/admin_resources/lib/js/respond.js"></script>
    <![endif]-->
    <link href="/admin_resources/lib/zTree/metroStyle/metroStyle.css" type="text/css" rel="stylesheet">
    <link href="/admin_resources/lib/css/animate.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetStyle.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetUi.css" />
    <link rel="stylesheet" type="text/css" href="/admin_resources/css/finaExchaAdmin/finaExchaAdmin.css" />
</head>
<body>
	<!--零钱兑换限额管理-->
<div class="wrap">
    <!-- 查询条件 -->
    <div class="layui-form layui-layer-padding ">
    	<div class="layui-col-xs12 demo border">
	      	<span class="grid-demo grid-demo-bg2">零钞兑换限额管理</span>
	      	<button class="layui-btn layui-btn-primary right">恢复默认设置</button>
	    </div>
    	<div class="layui-col-xs12 border" style="padding: 8px;">
    		<div class="layui-inline layui-col-xs3">
	            <label class="layui-form-label" style="float:none;text-align: left;">
	                	用户月限额:
	            </label>
	            <div class="layui-input-inline" style="width: 100%;position: relative;">
	                <input type="text" placeholder="" class="layui-input" lay-verify="required">
	                <span id="" style="position: absolute;right: -20px;top: 6px;">
	                	元
	                </span>
	            </div>
	        </div>
    	</div>
    	<div class="layui-form-item layui-col-xs12 border" style="padding: 8px;">
    		<div class="layui-form">
		        <div class="layui-form-item layui-row layui-col-space20" style="margin: 0;">
		            <div class="layui-inline layui-col-xs3" style="margin-bottom: -10px;">
		                <label class="layui-form-label" style="float:none;text-align: left;">
		                    机构限额:
		                </label>
		                <div class="layui-input-inline" style="width: 100%;">
		                    <select lay-verify="required">
		                        <option value=""></option>
		                        <option value="10001">云浮市建设银行</option>
		                        <option value="10002">云浮市农业银行</option>
		                        <option value="10003">云浮市农业银行云城区支行</option>
		                        <option value="10004">云浮市农业银行云城区支行河口街道网点</option>
		                    </select>
		                </div>
		            </div>
		        </div>
		        <div class="layui-form-item layui-row layui-col-space20" style="margin: 0;">
		            <div class="layui-inline layui-col-xs4">
		                <label class="layui-form-label width">1元</label>
		                <div class="layui-input-inline">
		                    <input type="text" placeholder="" class="layui-input">
		                </div>
		            </div>
		            <div class="layui-inline layui-col-xs4">
		                <label class="layui-form-label width">5元</label>
		                <div class="layui-input-inline">
		                    <input type="text" placeholder="" class="layui-input">
		                </div>
		            </div>
		            <div class="layui-inline layui-col-xs4">
		                <label class="layui-form-label width">10元</label>
		                <div class="layui-input-inline">
		                    <input type="text" placeholder="" class="layui-input">
		                </div>
		            </div>
		            <div class="layui-inline layui-col-xs4">
		                <label class="layui-form-label width">20元</label>
		                <div class="layui-input-inline">
		                    <input type="text" placeholder="" class="layui-input">
		                </div>
		            </div>
		            <div class="layui-inline layui-col-xs4">
		                <label class="layui-form-label width">50元</label>
		                <div class="layui-input-inline">
		                    <input type="text" placeholder="" class="layui-input">
		                </div>
		            </div>
		            <div class="layui-inline layui-col-xs4">
		                <label class="layui-form-label width">100元</label>
		                <div class="layui-input-inline">
		                    <input type="text" placeholder="" class="layui-input">
		                </div>
		            </div>
		        </div>
		    </div>
    	</div>
    </div>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/jquery-1.12.2.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/layui/layui.js"></script>
<script type="text/javascript" src="/admin_resources/lib/zTree/jquery.ztree.all.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/echarts/echarts.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/json2.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jsrender-min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/easy.ajax.js"></script>
<!-- 表头toolbar模板 -->
<div id="dataTableToolBarTpl" style="display: none;">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="edit">提交</a>
</div>
<script type="text/javascript" src="/admin_resources/lib/js/require.js" data-main="/admin_resources/js/finaExchaAdmin/finaExchaAdmin"></script>
</body>
</html>