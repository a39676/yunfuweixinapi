<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><sec:csrfMetaTags />
<meta name="Keywords" content="系统管理">
<meta name="Description" content="系统管理">
<title>系统管理</title>
<link href="/admin_resources/lib/iconfont/iconfont.css" type="text/css" rel="stylesheet">
<link href="/admin_resources/lib/layui/css/layui.css" type="text/css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="/admin_resources/lib/js/html5shiv.js"></script>
	<script src="/admin_resources/lib/js/respond.js"></script>
<![endif]-->
<link href="/admin_resources/lib/css/animate.min.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetStyle.css" />
<link rel="stylesheet" type="text/css" href="/admin_resources/lib/css/resetUi.css" />
<link rel="stylesheet" type="text/css" href="/admin_resources/css/personCenter.css" />
</head>
<body>
	<!-- 个人中心界面布局 -->
	<div class="wrap-box">
		<!-- 选项卡 -->
		<div class="layui-tab layui-tab-brief">
			<ul class="layui-tab-title">
				<li class="layui-this">个人设置</li>
				<li>修改密码</li>
			</ul>
			<div class="layui-tab-content">
				<!-- 个人设置 -->
				<div class="layui-tab-item layui-show">
					<form class="layui-form">
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label" style="width: 100px;">表格每页条数</label>
								<div class="layui-input-inline">
									<input type="number" name="pageSize" lay-verify="required|integerPageSize" placeholder="5~50以内的整数" class="layui-input" id="pageSize">
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label" style="width: 100px;"></label>
								<div class="layui-input-inline">
									<button class="layui-btn layui-btn-custom-blue" lay-submit lay-filter="saveBaseSet">保存</button>
									<button class="layui-btn layui-btn-custom-blue" type="reset" id="resetBaseSet">重置</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- 修改密码 -->
				<div class="layui-tab-item">
					<form class="layui-form">
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label" style="width: 100px;">旧密码</label>
								<div class="layui-input-inline">
									<input type="password" name="oldPassword" lay-verify="required" placeholder="" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label" style="width: 100px;">新密码</label>
								<div class="layui-input-inline">
									<input type="password" name="pwd" lay-verify="required|charAndInteger" placeholder="" class="layui-input" id="equalPassword">
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label" style="width: 100px;">确认新密码</label>
								<div class="layui-input-inline">
									<input type="password" name="confirm" lay-verify="required|equalPassword" placeholder="" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label" style="width: 100px;"></label>
								<div class="layui-input-inline">
									<button class="layui-btn layui-btn-custom-blue" lay-submit lay-filter="saveModifyPassword">保存</button>
									<button class="layui-btn layui-btn-custom-blue" type="reset" id="resetModifyPassword">重置</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="/admin_resources/lib/js/jquery-1.12.2.min.js"></script>
<script type="text/javascript" src="/admin_resources/lib/layui/layui.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/json2.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/md5.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/easy.ajax.js"></script>
<script type="text/javascript" src="/admin_resources/lib/js/require.js" data-main="/admin_resources/js/personCenter"></script>
</body>
</html>