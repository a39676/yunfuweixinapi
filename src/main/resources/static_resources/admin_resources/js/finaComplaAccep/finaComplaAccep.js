/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();//每个页面都有
        form.render();//每个页面都有
        laydate.render({
            elem: '#presentationTime'
            ,theme: '#588fd0'
            ,format: 'yyyy/MM/dd HH:mm:ss'
        });
        laydate.render({
            elem: '#EndTime'
            ,theme: '#588fd0'
            ,format: 'yyyy/MM/dd HH:mm:ss'
        });
        /**
         * 页面布局使用
         * 数据表格中计算高度部分的固定高度
         */
        var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
        /**
         * 全局变量
         */
        var modelIndex = null; //标记模态框

        /****************************************静态数据开始****************************************/
        function dodata(page,data) {
          console.log(data)
          var _list = formatList(data)
          var count = data.resultCount;
          renderTable(_list)
          renderPage(count,page)
        }
      function formatList(data) {
        var list = data.complaintList;
        var _list = [];
        list.forEach(function (item) {
          _list.push({
            id:item.id,
            complaintUserNickName:item.complaintUserNickName,
            complaintUserMobile:item.complaintUserMobile,
            complaintTypeString:item.complaintTypeString,
            complaintResultString:item.complaintResultString,
            orgName:item.orgName,
            receptionTimeString: item.receptionTimeString,
            staffName: item.staffName,
            staffMobile: item.staffMobile,
            remark: item.remark,
            content: item.content,
            orgId: item.orgId,
            cando: item.complaintResult
          })
        })
        return _list;
      }

      function renderTable(list) {
        table.render({
          id: "dataTable,orgId",
          elem: "#dataTable",
          limit: config.pageSize,
          height: "full-" + fixedHeight,//全屏高度减去计算出的高度
          cols: [[
            {align : "center",field : "id",title : "受理号"},
            {align : "center",field : "complaintUserNickName",title : "用户姓名"},
            {align : "center",field : "complaintUserMobile",title : "用户手机"},
            {align : "center",field : "complaintTypeString",title : "投诉类型"},
            {align : "center",field : "content",title : "投诉内容"},
            // {align : "center",field : "e",title : "金融机构"},
            // {align : "center",field : "f",title : "市县区域"},

            {align : "center",field : "orgName",title : "被投诉网点"},
            {align : "center",field : "staffName",title : "受理人"},
            {align : "center",field : "receptionTimeString",title : "受理时间"},
            {align : "center",field : "complaintResultString",title : "受理结果"},
            {align : "center",field : "staffMobile",title : "联系电话"},
            {align : "center",field : "remark",title : "拒收理由"},
            {align : "center",title : "操作",width : 120,toolbar : "#dataTableToolBarTpl",fixed: 'right' ,field:"cando"}
            ]],
          data: list,
          size: "sm"
        });

      }
      function renderPage(count,curr) {
        laypage.render({
          elem: "dataPage",
          count: count,
          limit: config.pageSize,
          curr: curr,
          prev: "<i class='layui-icon'>&#xe603;</i>",
          next: "<i class='layui-icon'>&#xe602;</i>",
          layout: ["prev", "page", "next", "skip", "count"],
          theme: "#588fd0",
          jump: function (obj, first) {
            // console.log(obj,first)
            // getData(obj.curr,dodata)
            if (!first) {
              getData(obj.curr,dodata)
            }
          }

        });
      }

      function getData(page, cb) {
          var complaintStatus = $("#complaintStatus_select").val();
          if(!complaintStatus){
            complaintStatus = 0;
          }
        var receptionTime = $("#receptionTime_input").val();
          if(!receptionTime){
            receptionTime = '';
          }

        var _data = {
          complaintStatus:complaintStatus,
          receptionTime: receptionTime,
          pageSize: 10,
          pageNo: page,
        }
        EasyAjax.post_json({
          url: config.api_host + "/business/findComplaintPage",
          data: _data
        }, function (data) {
          cb(page,data)
        });
      }
      getData(1, dodata)


        /**
         * 点击新增按钮
         */
        $("#addBtn").on("click",function () {
            modelIndex = layer.open({
                type: 1,
                title: "新增",
                shade: [0.3, '#000'],
                area: ['600px', 'auto'],
                anim: 0,
                resize: false,
                content: $('#editModel')
            });
        });
        var modelIndex = null;
        /**
         * 工具栏操作
         */
        table.on('tool(dataTable)', function(obj){
            var data = obj.data;
            console.log(data);
            var layEvent = obj.event;
            if (layEvent === 'no'){ //编辑
                modelIndex = layer.open({
                    type: 1,
                    title: "拒绝",
                    shade: [0.3, '#000'],
                    area: ['600px', 'auto'],
                    anim: 0,
                    resize: false,
                    content: $('#noModel'),
                  success:function () {
                    $("#no_id").val(data.id);
                  }
                });
            }else if(layEvent === 'ok'){
              modelIndex = layer.open({
                type: 1,
                title: "接受",
                shade: [0.3, '#000'],
                area: ['600px', 'auto'],
                anim: 0,
                resize: false,
                content: $('#okModel'),
                success:function () {
                  $("#ok_id").val(data.id);
                  EasyAjax.post_json({
                    url: config.api_host + "/organizations/findStaffsByOrgId",
                    data:{
                      orgId: data.orgId
                    }
                  }, function (data) {
                    console.log(data)
                    var _html = ``;
                    var staffList = data.staffList;
                    staffList.forEach(function (item) {
                      _html+=`
                      <option value="${item.id}">${item.staffsName}</option>
                      `;
                    })
                    $("#chosePeople").html(_html)
                    form.render();
                    // console.log(data)
                  });
                }
              });
                // layer.confirm('确认删除', {anim: 6}, function(index){
                //     layer.close(index);
                // });
            }
        });

        /**
         * 点击保存按钮
         */
        form.on('submit(ok_saveModelBtn)', function(data){
          console.log(data)
          var handlerStaffId = data.field.chosePeople;
          var id = data.field.ok_id;
          EasyAjax.post_json({
            url: config.api_host + "/business/sendComplaint",
            data: {
              handlerStaffId:handlerStaffId,
              id:id
            }
          }, function (data) {
            if(data.result == 0){
              layer.alert('处理成功', {icon: 6});
              getData(1,dodata)
            }else {
              layer.alert('处理失败', {icon: 6});
            }
          });
            layer.close(modelIndex);
        });
      form.on('submit(no_saveModelBtn)', function(data){
        console.log(data)
        var remark = data.field.remark;
        var id = data.field.no_id;
        EasyAjax.post_json({
          url: config.api_host + "/business/rejectComplaint",
          data: {
            remark:remark,
            id:id
          }
        }, function (data) {
          if(data.result == 0){
           layer.alert('处理成功', {icon: 6});
            getData(1,dodata)
          }else {
            layer.alert('处理失败', {icon: 6});
          }
        });
        layer.close(modelIndex);
      });

        /**
         * 取消模态框按钮
         */
        $("#no_cancelModelBtn").on("click",function () {
            layer.close(modelIndex);
        });
      $("#ok_cancelModelBtn").on("click",function () {
        layer.close(modelIndex);
      });

      $("#searchBtn").on('click',function () {
        getData(1,dodata)
      })
        /****************************************静态数据结束****************************************/

    });
});