/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage','tree'], function(){
  window.element = layui.element; //获取element模块
  window.form = layui.form; //获取form模块
  window.layer = layui.layer; //获取layer模块
  window.laydate = layui.laydate; //获取laydate模块
  window.table = layui.table; //获取table模块
  window.laypage = layui.laypage; //获取laypage模块
  window.tree = layui.tree; //获取tree模块
  /**
   * 表格全局默认参数设置
   */
  table.set({
    page : false, //是否开启分页
    even : true, //是否开启隔行换色
    cellMinWidth : 80 //单元格的最小宽度
  });
  /**
   * 模块化加载
   * 自定义模块
   */
  require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
    /**
     * element组件渲染
     * form组件渲染
     */
    element.init();
    form.render();
    $.ajax({
      url:config.api_host + "/user/countUser",
      type:'post',
      dataType:'json',
      success: function (data) {
        console.log(data)
        $("#todayCount").html(data.todayCount)
        $("#allCount").html(data.allCount)
      },
      error:function (err) {
        console.log(err)
        alert(err)
      }
    })
    $.ajax({
      url:config.api_host + "/reservation/getStatisticsForHomePage",
      type:'post',
      dataType:'json',
      success: function (data) {
        console.log(data)
        var json = data.json;
        $("#financing").text(json.financing);
        $("#financial").text(json.financial);
        $("#insurance").text(json.insurance);
        $("#securities").text(json.securities);
        $("#exchangeDib").text(json.exchangeDib);
        $("#agriculturalCredit").text(json.agriculturalCredit);
      },
      error:function (err) {
        console.log(err)
        alert(err)
      }
    })

  });
});


