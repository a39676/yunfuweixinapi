/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form', 'layer', 'laydate', 'table', 'laypage'], function () {
  window.element = layui.element; //获取element模块
  window.form = layui.form; //获取form模块
  window.layer = layui.layer; //获取layer模块
  window.laydate = layui.laydate; //获取laydate模块
  window.table = layui.table; //获取table模块
  window.laypage = layui.laypage; //获取laypage模块
  /**
   * 表格全局默认参数设置
   */
  table.set({
    page: false, //是否开启分页
    even: true, //是否开启隔行换色
    cellMinWidth: 80 //单元格的最小宽度
  });
  /**
   * 模块化加载
   * 自定义模块
   */
  require(["../module/config", "../module/cookie", "../module/base", "../module/verify"], function (config, cookie, base, verify) {
    /**
     * element组件渲染
     * form组件渲染
     */
    element.init();//每个页面都有
    form.render();//每个页面都有
    /**
     * 页面布局使用
     * 数据表格中计算高度部分的固定高度
     */
    var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
    /**
     * 全局变量
     */
    var modelIndex = null; //标记模态框

    /**
     * 初始化列表
     */
    function renderTable(list) {
      console.log("renderTable")
      table.render({
        id: "dataTable,topHeadOrg,orgId",
        elem: "#dataTable",
        limit: config.pageSize,
        height: "full-" + fixedHeight,//全屏高度减去计算出的高度
        cols: [[
          {align: "center", field: "orgCode", title: "机构代码"},
          {align: "center", field: "orgName", title: "机构名称"},
          {align: "center", field: "orgType", title: "机构类型", toolbar: "#dataTableOrgTypeTpl"},
          {align: "center", field: "staffNumber", title: "工号"},
          {align: "center", field: "staffName", title: "员工姓名"},
          {align: "center", field: "mobile", title: "手机号"},
          {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
        ]],
        data: list,
        size: "sm"
      });

    }

    function renderPage(count, curr) {
      console.log("renderPage")
      laypage.render({
        elem: "dataPage",
        count: count,
        limit: config.pageSize,
        curr: curr,
        prev: "<i class='layui-icon'>&#xe603;</i>",
        next: "<i class='layui-icon'>&#xe602;</i>",
        layout: ["prev", "page", "next", "skip", "count"],
        theme: "#588fd0",
        jump: function (obj, first) {
          // console.log(obj,first)
          // getData(obj.curr,dodata)
          if (!first) {
            getData(obj.curr, dodata)
          }
        }

      });
    }

    function getData(page, cb) {
      var orgCode = $('#orgCode').val();
      var orgName = $('#orgName').val();
      var orgType = $("#orgType option:selected").val();
      var staffNumber = $("#staffNumber").val();
      var staffName = $("#staffName").val();
      var mobile = $("#mobile").val();


      var _data = {
        orgCode: orgCode,
        orgName: orgName,
        orgType: orgType,
        staffNumber: staffNumber,
        staffName: staffName,
        mobile: mobile,
        geographicalAreaId: 1,
        pageSize: 10,
        pageNo: page
      }
      EasyAjax.post_json({
        url: config.api_host + "/organizations/findOrgPage",
        data: _data
      }, function (data) {
        cb(page, data)
      });
    }

    function dodata(page, data) {
      var _list = formatList(data)
      var count = data.resultCount;
      renderTable(_list)
      renderPage(count, page)
    }

    function formatList(data) {
      var list = data.orgPage;
      var _list = [];
      list.forEach(function (item) {
        _list.push({
          orgCode: item.orgCode,
          orgName: item.orgName,
          orgType: item.orgType,
          staffNumber: item.staffNumber,
          staffName: item.staffName,
          mobile: item.mobile,
          topHeadOrg:item.topHeadOrg,
          orgId:item.id
        })
      })
      return _list;
    }

    getData(1, dodata)
    // var initList = function (pageNo) {
    //     var para = {};
    //     para.orgCode = $("#searchOrgCode").val();
    //     para.orgName = $("#searchOrgName").val();
    //     para.orgType = $("#searchOrgType").val();
    //     para.pageSize = config.pageSize;
    //     para.pageNo = pageNo;
    //     EasyAjax.post_json({
    //             url: config.api_host + "organizations/findOrgPage",
    //             data: para
    //         },
    //         function (data) {
    //             table.render({
    //                 id : "dataTable",
    //                 elem : "#dataTable",
    //                 limit : config.pageSize,
    //                 height : "full-" + fixedHeight, //全屏高度减去计算出的高度
    //                 cols : [[
    //                     {align : "center",field : "orgCode",title : "机构代码"},
    //                     {align : "center",field : "orgName",title : "机构名称"},
    //                     {align : "center",field : "orgType",title : "机构类型",toolbar : "#dataTableOrgTypeTpl"},
    //                     {align : "center",title : "操作",width : 120,toolbar : "#dataTableToolBarTpl",fixed: 'right'}
    //                 ]],
    //                 data : data.orgPage,
    //                 size : "sm"
    //             });
    //             laypage.render({
    //                 elem: "dataPage",
    //                 count: data.resultCount,
    //                 limit: config.pageSize,
    //                 curr: pageNo,
    //                 prev: "<i class='layui-icon'>&#xe603;</i>",
    //                 next: "<i class='layui-icon'>&#xe602;</i>",
    //                 layout: ["prev", "page", "next", "skip", "count"],
    //                 theme: "#588fd0",
    //                 jump: function(obj,first){
    //                     if(!first){
    //                         initList(obj.curr);
    //                     }
    //                 }
    //             });
    //         });
    // };

    /**
     * 给查询按钮绑定单击事件
     */
    $("#searchBtn").on("click", function () {
      getData(1, dodata)
    });

    /**
     * 点击新增按钮
     */
    $("#addBtn").on("click", function () {

      form.render('select');
      modelIndex = layer.open({
        type: 1,
        title: "新增",
        //shadeClose: true,
        shade: [0.3, '#000'],
        area: ['600px', 'auto'],
        anim: 0,
        resize: false,
        content: $('#editModel'),
        success:function () {
          $("#model_orgCode").val('').removeAttr("disabled");
          $("#model_orgName").val('').removeAttr("disabled");
          $("#model_orgType").val('').removeAttr("disabled");
          $("#model_staffNumber").val('');
          $("#model_staffName").val('');
          $("#model_mobile").val('');
          $("#saveModelBtn").attr("data-id", "");
          form.render('select');
        }
      });
    });

    /**
     * 工具栏操作
     */
    table.on('tool(dataTable)', function (obj) {
      var data = obj.data;
      console.log(data);
      var orgId = data.orgId;
      var layEvent = obj.event;
      if (layEvent === 'edit') { //编辑

        modelIndex = layer.open({
          type: 1,
          title: "修改",
          //shadeClose: true,
          shade: [0.3, '#000'],
          area: ['600px', '400px'],
          anim: 0,
          resize: false,
          content: $('#changeModel'),
          success:function () {
            $("#changeMyNum").val(data.staffNumber);
            $("#changeMyName").val(data.staffName);
            $("#changeMyPhone").val(data.mobile);
            $("#changeMyPwd").val();
            $("#changeMyRepeatPwd").val();
            $("#changeMyOldPhone").val(data.mobile);
          }
        });
      } else if (layEvent === 'del') {
        layer.confirm('确认删除', {anim: 6}, function (index) {
          // var para = {};
          // para.id = data.id;
          EasyAjax.post_json({
              url: config.api_host + "/organizations/deleteOrg",
              data: {
                id:orgId
              }
            },
            function (result) {
            console.log(result)
              layer.msg("操作成功");
              getData(1, dodata)
              layer.close(index);
            });
        });
      }
    });

    /**
     * 点击保存按钮
     */
    form.on('submit(saveModelBtn)', function (data) {
      console.log(data)
      var orgType = data.field.model_orgType;
      var orgCode = data.field.model_orgCode;
      var orgName = data.field.model_orgName;


      var userName = data.field.model_mobile;
      var oldUserName = data.field.model_mobile_old;
      var nickName = data.field.model_staffName;
      var staffNumber = data.field.model_staffNumber;
      var pwd = data.field.model_pwd;
      var pwdRepeat = data.field.model_pwdRepeat;
      var gender = data.field.model_gender;
      var qq = data.field.model_qq;
      var mobile = data.field.model_mobile;

      var id = $("#saveModelBtn").attr("data-id");
      console.log(id)
      if (id) { //编辑

      } else { //新增
        console.log("新增")

        EasyAjax.post_json({
            url: config.api_host + "/organizations/createNewOrgByAdmin",
            data: {
              orgType: orgType,
              orgCode:orgCode,
              orgName:orgName,
              geogralphicalId: 1
            }
          },
          function (data) {
            console.log(data)
            if(data.result == 0){
              var orgId = data.newOrg.id;
              $.ajax({
                url:config.api_host + "/user/newManagerRegist",
                type:'POST',
                dataType: 'json',
                contentType: 'application/json',
                data:JSON.stringify({
                  userName:userName,
                  nickName:nickName,
                  pwd:pwd,
                  pwdRepeat:pwdRepeat,
                  gender:gender,
                  qq:qq,
                  mobile:mobile,
                  orgId:orgId,
                  staffNumber:staffNumber
                }),
                success: function (data) {
                  if(data.result == 0){
                    getData(1, dodata)
                    layer.close(modelIndex);
                  }else{
                    layer.msg(data.message || "添加机构管理员失败")
                    $.ajax({
                      url:config.api_host + "/organizations/deleteOrg",
                      type:'POST',
                      dataType: 'json',
                      contentType: 'application/json',
                      data: JSON.stringify({
                        id: orgId
                      }),
                      success: function (data) {
                      
                      },
                      error:function (err) {

                      }
                    })
                  }
                },
                error:function (err) {

                }
              })
            }else{
              layer.msg(data.message || "添加机构失败")
              return;
            }

          });
      }
    });

    /**
     * 取消模态框按钮
     */
    $("#cancelModelBtn").on("click", function () {
      layer.close(modelIndex);
    });
    form.on('select(selectChange)', function(data){
      var val = data.value;

      if(val==="changeName"){
        $("#changeName").show();
        $("#changePhone").hide();
      }else if(val==="changePhone"){
        $("#changeName").hide();
        $("#changePhone").show();
      }
      console.log(data.elem); //得到select原始DOM对象
      console.log(data.value); //得到被选中的值
      console.log(data.othis); //得到美化后的DOM对象
    });
    form.on('submit(change_saveModelBtn)', function (data) {
      console.log(data);
      var selectChange = data.field.selectChange;

      var newUserName = data.field.changeMyPhone;
      var  oldUserName = data.field.changeMyOldPhone;
      var pwd = data.field.changeMyPwd;
      var pwdRepeat = data.field.changeMyRepeatPwd;
      var nickName = data.field.changeMyName;
      var staffNumber = data.field.changeMyNum;
      var  mobile = data.field.changeMyOldPhone;
      if(selectChange ==="changeName"){
        EasyAjax.post_json({
            url: config.api_host + "/organizations/changeStaffName",
            data:{
              nickName:nickName,
              mobile:mobile,
              staffNumber: staffNumber,
            }
          },
          function (data) {
            if(data.result==0){
              getData(1, dodata);
              layer.msg("成功")

            }else{
              layer.msg("失败")
            }
            layer.close(modelIndex);
          });
      }else if(selectChange==="changePhone"){
        EasyAjax.post_json({
            url: config.api_host + "/user/changeUserName",
            data:{
              newUserName:newUserName,
              oldUserName: oldUserName,
              pwd:  pwd,
              pwdRepeat:pwdRepeat,
            }
          },
          function (data) {
            if(data.result==0){
              getData(1, dodata);
              layer.msg("成功")

            }else{
              layer.msg("失败")
            }
            layer.close(modelIndex);
          });
      }
    })
    /**
     * 页面初始化
     */
    // initList(1);
  });
});