/**
 * 模块化加载
 * Layui中的模块
 */
const ProductSubType = {
  1: "证券",
  2: "股票",
  3: "其他证券服务"
}
const ProductType = 4;
Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;

}
layui.use(['element', 'form', 'layer', 'laydate', 'table', 'laypage'], function () {
  window.element = layui.element; //获取element模块
  window.form = layui.form; //获取form模块
  window.layer = layui.layer; //获取layer模块
  window.laydate = layui.laydate; //获取laydate模块
  window.table = layui.table; //获取table模块
  window.laypage = layui.laypage; //获取laypage模块
  /**
   * 表格全局默认参数设置
   */
  table.set({
    page: false, //是否开启分页
    even: true, //是否开启隔行换色
    cellMinWidth: 80 //单元格的最小宽度
  });
  /**
   * 模块化加载
   * 自定义模块
   */
  require(["../module/config", "../module/cookie", "../module/base", "../module/verify"], function (config, cookie, base, verify) {
    /**
     * element组件渲染
     * form组件渲染
     */
    element.init();//每个页面都有
    form.render();//每个页面都有
    laydate.render({
      elem: '#presentationTime'
      , theme: '#588fd0'
      , format: 'yyyy/MM/dd HH:mm:ss'
    });
    laydate.render({
      elem: '#EndTime'
      , theme: '#588fd0'
      , format: 'yyyy/MM/dd HH:mm:ss'
    });
    /**
     * 页面布局使用
     * 数据表格中计算高度部分的固定高度
     */
    var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
    /**
     * 全局变量
     */
    var modelIndex = null; //标记模态框

    /****************************************静态数据开始****************************************/
    function dodata(page, data) {
      var _list = formatList(data)
      var count = data.resultCount;
      renderTable(_list)
      renderPage(count, page)
    }

    function formatList(data) {
      var list = data.productPage;
      var _list = [];
      list.forEach(function (item) {
        _list.push({
          id: item.id,
          a: item.productCode,
          b: item.productName,
          c: ProductSubType[item.productSubType],
          d: item.interestRate + "%",
          e: item.expirationTime ? new Date(item.expirationTime.time).Format('yyyy/MM/dd hh:mm:ss') : "",
          g: item.productIntroduction
        })
      })
      return _list;
    }

    function renderTable(list) {
      table.render({
        id: "dataTable",
        elem: "#dataTable",
        limit: config.pageSize,
        height: "full-" + fixedHeight,//全屏高度减去计算出的高度
        cols: [[
          // {type: 'checkbox'},
          {align: "center", field: "id", title: "产品ID"},
          {align: "center", field: "a", title: "产品代码"},
          {align: "center", field: "b", title: "产品名称"},
          {align: "center", field: "c", title: "产品类别"},
          {align: "center", field: "d", title: "年利率"},
          {align: "center", field: "e", title: "截止日期"},
          // {align : "center",field : "f",title : "申请周期"},
          {align: "center", field: "g", title: "产品简介"},
          {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
        ]],
        data: list,
        size: "sm"
      });

    }

    function renderPage(count, curr) {
      laypage.render({
        elem: "dataPage",
        count: count,
        limit: config.pageSize,
        curr: curr,
        prev: "<i class='layui-icon'>&#xe603;</i>",
        next: "<i class='layui-icon'>&#xe602;</i>",
        layout: ["prev", "page", "next", "skip", "count"],
        theme: "#588fd0",
        jump: function (obj, first) {
          // console.log(obj,first)
          // getData(obj.curr,dodata)
          if (!first) {
            getData(obj.curr, dodata)
          }
        }

      });
    }

    function getData(page, cb) {
      var productSubType = $('#productSubType option:selected').val();
      var expirationTime = $('#presentationTime').val();
      var productName = $("#productName").val();
      var productCode = $("#productCode").val();

      if (!productSubType) {
        productSubType = ''
      }
      if (!expirationTime) {
        expirationTime = ''
      }
      if (!productName) {
        productName = ''
      }
      if (!productCode) {
        productName = ''
      }
      var _data = {
        productType: ProductType,
        productSubType: productSubType,
        orgId: '',
        expirationTime: expirationTime,
        productName: productName,
        productCode: productCode,
        pageSize: 10,
        pageNo: page
      }
      EasyAjax.post_json({
        url: config.api_host + "/product/findProductPageForAdmin",
        data: _data
      }, function (data) {
        cb(page, data)
      });
    }

    /**
     * 打开model
     * */
    function openModel(title, cb) {
      modelIndex = layer.open({
        type: 1,
        title: title,
        shade: [0.3, '#000'],
        area: ['600px', 'auto'],
        anim: 0,
        resize: false,
        content: $('#editModel'),
        success: function (layero, index) {
          // console.log(layero,index)
          cb()
        }
      });
    }

    /**
     * 获取产品详情
     * */
    function getProductDetail(productId, cb) {
      EasyAjax.post_json({
        url: config.api_host + "/article/findArticleByProductId",
        data: {
          productId: productId
        }
      }, cb);
    }

    getData(1, dodata)
    // table.render({
    //   id: "dataTable",
    //   elem: "#dataTable",
    //   limit: config.pageSize,
    //   height: "full-" + fixedHeight,//全屏高度减去计算出的高度
    //   cols: [[
    //     {type: 'checkbox'},
    //     {align: "center", field: "a", title: "产品代码"},
    //     {align: "center", field: "b", title: "产品名称"},
    //     {align: "center", field: "c", title: "产品类别"},
    //     {align: "center", field: "d", title: "年利率"},
    //     {align: "center", field: "e", title: "截止日期"},
    //     {align: "center", field: "f", title: "申请周期"},
    //     {align: "center", field: "g", title: "产品简介"},
    //     {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
    //   ]],
    //   data: [
    //     {
    //       a: '427154',
    //       b: '产品1',
    //       c: '股票',
    //       d: '8.0%',
    //       e: '2018-08-08',
    //       f: '365天',
    //       g: ''
    //     },
    //     {
    //       a: '427155',
    //       b: '产品2',
    //       c: '股票',
    //       d: '8.0%',
    //       e: '2018-08-08',
    //       f: '365天',
    //       g: ''
    //     }
    //   ],
    //   size: "sm"
    // });
    // laypage.render({
    //   elem: "dataPage",
    //   count: 12,
    //   limit: config.pageSize,
    //   curr: 1,
    //   prev: "<i class='layui-icon'>&#xe603;</i>",
    //   next: "<i class='layui-icon'>&#xe602;</i>",
    //   layout: ["prev", "page", "next", "skip", "count"],
    //   theme: "#588fd0",
    //   jump: function (obj, first) {
    //     if (!first) {
    //
    //     }
    //   }
    // });
    /**
     * 点击查询按钮
     */
    $("#searchBtn").on("click", function () {
      getData(1, dodata)
    });
    /**
     * 点击新增按钮
     */
    $("#addBtn").on("click", function () {
      openModel("新增" ,function () {
        $("[name='model_productCode']").val('')
        $("[name='model_productName']").val('')
        $("[name='model_productSubType'] option:first").prop("selected", 'selected');
        $("[name='model_interestRate']").val('')
        $("[name='model_expirationTime']").val('')
        $("[name='model_productIntroduction']").val('')
        $("[name='model_article']").val('')
        $("[name='btnAction']").val("add");
      })
      // modelIndex = layer.open({
      //   type: 1,
      //   title: "新增",
      //   shade: [0.3, '#000'],
      //   area: ['600px', 'auto'],
      //   anim: 0,
      //   resize: false,
      //   content: $('#editModel')
      // });
    });

    /**
     * 工具栏操作
     */
    table.on('tool(dataTable)', function (obj) {
      var data = obj.data;
      var layEvent = obj.event;
      if (layEvent === 'edit') { //编辑
        openModel("编辑",function () {
          $("[name='model_productCode']").val(data.a)
          $("[name='model_productName']").val(data.b)
          $("[name='model_productSubType']").find(`option:contains(${data.c})`).attr("selected",true);
          $("[name='model_interestRate']").val(data.d)
          $("[name='model_expirationTime']").val(data.e)
          $("[name='model_productIntroduction']").val(data.g);
          $("[name='btnAction']").val("edit");
          $("[name='productId']").val(data.id)
          getProductDetail(data.id, function (res) {
            console.log(res)
            console.log(res.articleLongVO.contentLines)
            $("[name='model_article']").val(res.articleLongVO.contentLines)
          })
        })
      } else if (layEvent === 'del') {
        layer.confirm('确认删除', {anim: 6}, function (index) {

          console.log(data.id)
          EasyAjax.post_json({
            url: config.api_host + "/product/deleteProduct",
            data: {
              id:data.id
            }
          }, function (data) {
            console.log(data);
            if(data.success){
              layer.alert('删除成功', {icon: 6});
            }
            getData(1,dodata)
          });
          layer.close(index);
        });
      }
    });

    /**
     * 点击保存按钮
     */
    form.on('submit(saveModelBtn)', function (data) {
      console.log(data.field)
      var formdata = data.field;
      var _data = {

        productCode: formdata.model_productCode,
        productName: formdata.model_productName,
        productIntroduction: formdata.model_productIntroduction,
        article: formdata.model_article,
        productType: ProductType,
        productSubType: formdata.model_productSubType,
        expirationTime: formdata.model_expirationTime,
        interestRate: formdata.model_interestRate.replace(/[%]/g,''),
        interestType: 1,
      }
      console.log(formdata.btnAction)
      if(formdata.btnAction === 'add'){
        console.log("请求数据")
        //新增产品
        _data.orgId = '',
          EasyAjax.post_json({
              url: config.api_host + "/product/addNewProduct",
              data: _data
            },
            function (res) {
              console.log(res)
              if(res.productId){
                layer.alert('新增成功', {icon: 6});
                getData(1,dodata)
              }else {
                layer.alert('新增失败', {icon: 6});
              }
              layer.close(modelIndex);
            });
        // layer.close(modelIndex);
      }else if(formdata.btnAction === 'edit'){
        _data.productId = formdata.productId ;
        console.log(_data)
        EasyAjax.post_json({
            url: config.api_host + "/product/editProduct",
            data: _data
          },
          function (res) {
            console.log(res)
            if(res.productId){
              layer.alert('修改成功', {icon: 6});
              getData(1,dodata)
            }else {
              layer.alert('修改失败', {icon: 6});
            }
            layer.close(modelIndex);
          });
        //编辑产品
        // layer.close(modelIndex);
      }else{
        // layer.close(modelIndex);
        return;
      }
      layer.close(modelIndex);
    });

    /**
     * 取消模态框按钮
     */
    $("#cancelModelBtn").on("click", function () {
      layer.close(modelIndex);
    });

    /****************************************静态数据结束****************************************/

  });
});