/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form', 'layer', 'laydate', 'table', 'laypage'], function () {
  window.element = layui.element; //获取element模块
  window.form = layui.form; //获取form模块
  window.layer = layui.layer; //获取layer模块
  window.laydate = layui.laydate; //获取laydate模块
  window.table = layui.table; //获取table模块
  window.laypage = layui.laypage; //获取laypage模块
  /**
   * 表格全局默认参数设置
   */
  table.set({
    page: false, //是否开启分页
    even: true, //是否开启隔行换色
    cellMinWidth: 80 //单元格的最小宽度
  });
  /**
   * 模块化加载
   * 自定义模块
   */
  require(["../module/config", "../module/cookie", "../module/base", "../module/verify"], function (config, cookie, base, verify) {
    /**
     * element组件渲染
     * form组件渲染
     */
    element.init();//每个页面都有
    form.render();//每个页面都有
    /**
     * 页面布局使用
     * 数据表格中计算高度部分的固定高度
     */
    var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
    /**
     * 全局变量
     */
    var modelIndex = null; //标记模态框

    /**
     * 初始化列表
     */
    function renderTable(list) {
      console.log("renderTable")
      table.render({
        id: "dataTable",
        elem: "#dataTable",
        limit: 9999,
        height: "full-" + fixedHeight,//全屏高度减去计算出的高度
        cols: [[
          {align: "center", field: "id", title: "问卷ID"},
          {align: "center", field: "questionnaireName", title: "问卷名称"},
          {align: "center", field: "createTimeStr", title: "添加时间"},
          {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
        ]],
        data: list,
        size: "sm"
      });

    }

    // function renderPage(count, curr) {
    //   console.log("renderPage")
    //   laypage.render({
    //     elem: "dataPage",
    //     count: count,
    //     limit: config.pageSize,
    //     curr: curr,
    //     prev: "<i class='layui-icon'>&#xe603;</i>",
    //     next: "<i class='layui-icon'>&#xe602;</i>",
    //     layout: ["prev", "page", "next", "skip", "count"],
    //     theme: "#588fd0",
    //     jump: function (obj, first) {
    //       // console.log(obj,first)
    //       // getData(obj.curr,dodata)
    //       if (!first) {
    //         getData(obj.curr, dodata)
    //       }
    //     }
    //
    //   });
    // }

    function getData(page, cb) {
      EasyAjax.post_json({
        url: config.api_host + "/popQuiz/findQuestionnaire",
      }, function (data) {
        cb(page, data)
      });
    }

    function dodata(page, data) {
      console.log(data)
      var _list = formatList(data)
      // var count = data.resultCount;
      renderTable(_list)
      // renderPage(count, page)
    }

    function formatList(data) {
      var list = data.list;
      var _list = [];
      list.forEach(function (item) {
        _list.push({
          id: item.id,
          questionnaireName: item.questionnaireName,
          createTimeStr: item.createTimeStr
        })
      })
      return _list;
    }

    getData(1, dodata)

    /**
     * 给查询按钮绑定单击事件
     */
    $("#searchBtn").on("click", function () {
      getData(1, dodata)
    });

    /**
     * 点击新增按钮
     */
    $("#addBtn").on("click", function () {
    });

    /**
     * 工具栏操作
     */
    table.render({
      id: "rewardTable,userId,questionnaireId",
      elem: "#rewardTable",
      limit: 9999,
      height: "full-" + fixedHeight,//全屏高度减去计算出的高度
      cols: [[
        {align: "center", field: "nickName", title: "姓名"},
        {align: "center", field: "mobile", title: "联系电话"},
        {align: "center", field: "createTimeStr", title: "提交时间"},
        // {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
      ]],
      data: [],
      size: "sm"
    });
    table.on('tool(dataTable)', function (obj) {
      var data = obj.data;
      console.log(data);
      var id  = data.id;
      var layEvent = obj.event;
      if (layEvent === 'look') { //编辑

        modelIndex = layer.open({
          type: 1,
          title: "查看",
          //shadeClose: true,
          shade: [0.3, '#000'],
          area: ['600px', 'auto'],
          anim: 0,
          resize: false,
          content: $('#editModel'),
          success: function () {
            EasyAjax.post_json({
                url: config.api_host + "/popQuiz/findQuestionnaireUserByQuestionnaireId",
                data: {
                  id:id
                }
              },
              function (result) {
                console.log(result)
                var list  = result.list;
                var _list = [];
                list.forEach(function (item) {
                  _list.push({
                    userId: item.userId,
                    questionnaireId: item.questionnaireId,
                    nickName: item.nickName,
                    mobile:item.mobile,
                    createTimeStr:item.createTimeStr
                  })
                })
                table.render({
                  id: "rewardTable,userId,questionnaireId",
                  elem: "#rewardTable",
                  limit: 9999,
                  height: "full-" + fixedHeight,//全屏高度减去计算出的高度
                  cols: [[
                    {align: "center", field: "nickName", title: "姓名"},
                    {align: "center", field: "mobile", title: "联系电话"},
                    {align: "center", field: "createTimeStr", title: "提交时间"},
                    // {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
                  ]],
                  data: _list,
                  size: "sm"
                });
              });

          }
        });
      } else if (layEvent === 'del') {
        layer.confirm('确认删除', {anim: 6}, function (index) {
          // var para = {};
          // para.id = data.id;
          EasyAjax.post_json({
              url: config.api_host + "/popQuiz/deleteQuestionnaire",
              data: {
                id:id
              }
            },
            function (result) {
              console.log(result)
              layer.msg("操作成功");
              getData(1, dodata)
              layer.close(index);
            });
        });
      }
    });

    /**
     * 点击保存按钮
     */
    // form.on('submit(saveModelBtn)', function (data) {
    //   console.log(data)
    //   var orgType = data.field.model_orgType;
    //   var orgCode = data.field.model_orgCode;
    //   var orgName = data.field.model_orgName;
    //
    //
    //   var userName = data.field.model_mobile;
    //   var nickName = data.field.model_staffName;
    //   var staffNumber = data.field.model_staffNumber;
    //   var pwd = data.field.model_pwd;
    //   var pwdRepeat = data.field.model_pwdRepeat;
    //   var gender = data.field.model_gender;
    //   var qq = data.field.model_qq;
    //   var mobile = data.field.model_mobile;
    //
    //   var id = $("#saveModelBtn").attr("data-id");
    //   console.log(id)
    //   if (id) { //编辑
    //     // var para = {};
    //     // para.id = id;
    //     // para.orgCode = $("#orgCode").val();
    //     // para.orgName = $("#orgName").val();
    //     // para.orgType = $("#orgType").val();
    //     // EasyAjax.post_json({
    //     //     url: config.api_host + "organizations/updateOrgInfo",
    //     //     data: para
    //     //   },
    //     //   function (data) {
    //     //     layer.msg("操作成功");
    //     //     getData(1, dodata)
    //     //     layer.close(modelIndex);
    //     //   });
    //   } else { //新增
    //     console.log("新增")
    //
    //     EasyAjax.post_json({
    //         url: config.api_host + "/organizations/createNewOrgByAdmin",
    //         data: {
    //           orgType: orgType,
    //           orgCode:orgCode,
    //           orgName:orgName,
    //           geogralphicalId: 1
    //         }
    //       },
    //       function (data) {
    //         console.log(data)
    //         var orgId = data.newOrg.id;
    //
    //         EasyAjax.post_json({
    //             url: config.api_host + "/user/newManagerRegist",
    //             data:{
    //               userName:userName,
    //               nickName:nickName,
    //               pwd:pwd,
    //               pwdRepeat:pwdRepeat,
    //               gender:gender,
    //               qq:qq,
    //               mobile:mobile,
    //               orgId:orgId,
    //               staffNumber:staffNumber
    //             }
    //           },
    //           function (data) {
    //             getData(1, dodata)
    //             layer.close(modelIndex);
    //           });
    //       });
    //   }
    // });

    /**
     * 取消模态框按钮
     */
    $("#cancelModelBtn").on("click", function () {
      layer.close(modelIndex);
    });

    /**
     * 页面初始化
     */
    // initList(1);
  });
});