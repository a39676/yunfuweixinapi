/**
 * 模块化加载
 * Layui中的模块
 */

layui.use(['element', 'form','layer'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    /**
     * 模块化加载
     * 系统配置模块
     * cookie模块
     * base模块
     */
    require(["./module/config","./module/cookie","./module/base","./module/verify"],function (config, cookie, base, verify) {

         //获取登陆方式
        var loginType = config.loginType;
        var $layuiTab = $("#layui-tab");
        if (!loginType.account){
            $layuiTab.find("li[login-type='account']").remove();
            $layuiTab.find(".layui-tab-item[login-type='account']").remove();
        }
        if (!loginType.phone){
            $layuiTab.find("li[login-type='phone']").remove();
            $layuiTab.find(".layui-tab-item[login-type='phone']").remove();
        }
        if (!loginType.concatenation){
            $layuiTab.find("li[login-type='concatenation']").remove();
            $layuiTab.find(".layui-tab-item[login-type='concatenation']").remove();
        }
        $layuiTab.find("li").eq(0).click();
        $layuiTab.css("visibility","visible");

        //清空所有cookie
        base.clearAllCookie();

        //获取mac地址
        var isIe = base.getIeVersion();
        var macAddress = null;
        if (isIe !== -1 && isIe !== 'edge'){
            macAddress = base.getMacInfo();
        }else{
            macAddress = '00-00-00-00-00-00';
        }
        $("body").append("<p style='position: absolute;right: 10px;bottom: 10px;'>MAC地址：" + macAddress + "</p>");

        /*****************************************账户登陆*****************************************/

        /**
         * 从cookie里读取缓存登陆信息
         */
        $.ajax({
          url:config.api_host + "/_csrf",
          type:'get',
          dataType:'json',
          success: function (data) {
            console.log(data)
            localStorage.setItem("headerToken",data.header);
            localStorage.setItem(data.header,data.token)
          },
          error:function (err) {
            alert(err)
          }
        })

        // var loginMsg = cookie.get_loginMsg();
        // console.log(loginMsg)
        // if (loginMsg){
        //     $("#userCode").val(loginMsg.userCode);
        //     $("#pwd").val(loginMsg.pwd);
        //     $("#rememberLoginMsg").prop("checked",true);
        //     form.render('checkbox');
        // }
        /**
         * 事件绑定
         * 给登陆按钮绑定单击事件实现登陆
         */
        form.on('submit(loginBtn)', function(data){
            var formData = data.field; //存储获取到表单上未经过加密处理的原始数据
          console.log(formData)
          EasyAjax.post({
              url: config.api_host + "/auth/login_check",
              data: {
                user_name:formData.userCode,
                pwd:formData.pwd
              }
            },
            function (data) {
              console.log(data)
              window.location.href = "./index.html";
              // if (para.rememberLoginMsg && para.rememberLoginMsg === 'on'){
              //   cookie.set_loginMsg(formData);
              // }else{
              //   cookie.del_loginMsg();
              // }
              // cookie.set_user(data.result);
              // cookie.set_pageSize(data.result.pageSize);
              // window.location.href = "index";
            });
            return false;
        });

        /**
         * 从地址栏里读取登陆信息
         */
        var userCode = base.getrequest("userCode");
        var pwd = base.getrequest("pwd");
        if (userCode && pwd){
            $("#userCode").val(userCode);
            $("#pwd").val(pwd);
            $("#loginBtn").click();
        }

        /*****************************************短信登陆*****************************************/

        /**
         * 获取验证码
         */
        $("#getValidCodeBtn").on("click",function () {
            var $this = $(this);
            var $mobile = $("#mobile").val();
            if (!(/^1[345678]\d{9}$/.test($mobile))){
                layer.msg('手机号格式不正确');
            }else{
                var para = {};
                para.mobile = $mobile;
                EasyAjax.post_json({
                        url: config.api_host + "smsRecord/sendLoginSmsCode",
                        data: para
                    },
                    function (data) {
                        base.waitTime($this);
                    });
            }
            return false;
        });

        /**
         * 事件绑定
         * 给登陆按钮绑定单击事件实现登陆
         */
        form.on('submit(phoneLoginBtn)', function(data){
            var para = data.field;
            para.macAddress = macAddress;
            EasyAjax.post_json({
                    url: config.api_host + "sysUser/loginSms",
                    data: para
                },
                function (data) {
                    cookie.set_user(data.result);
                    cookie.set_pageSize(data.result.pageSize);
                    window.location.href = "index";
                });
            return false;
        });

        /*****************************************级联登陆*****************************************/

        /**
         * 获取验证码
         */
        $("#concatenationGetValidCodeBtn").on("click",function () {
            var $this = $(this);
            var $concatenationUserCode = $("#concatenationUserCode").val();
            if (!$concatenationUserCode){
                layer.msg('请输入登录名/用户编号');
            }else{
                var para = {};
                para.userCode = $concatenationUserCode;
                EasyAjax.post_json({
                        url: config.api_host + "sysUser/sendLoginSmsCode",
                        data: para
                    },
                    function (data) {
                        base.waitTime($this);
                    });
            }
            return false;
        });

        /**
         * 事件绑定
         * 给登陆按钮绑定单击事件实现登陆
         */
        form.on('submit(concatenationLoginBtn)', function(data){
            var formData = data.field; //存储获取到表单上未经过加密处理的原始数据
            EasyAjax.get({
                    url: config.api_host + "sysUser/customEncryption?code=" + data.field.pwd
                },
                function (data) {
                    var para = formData;
                    para.macAddress = macAddress;
                    para.pwd = data.result; //对用户输入的密码进行md5加密处理
                    EasyAjax.post_json({
                            url: config.api_host + "sysUser/loginSms",
                            data: para
                        },
                        function (data) {
                            cookie.set_user(data.result);
                            cookie.set_pageSize(data.result.pageSize);
                            window.location.href = "index";
                        });
                });
            return false;
        });

    });
});
