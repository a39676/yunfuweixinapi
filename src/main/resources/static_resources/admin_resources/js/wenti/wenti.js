/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form', 'layer', 'laydate', 'table', 'laypage'], function () {
  window.element = layui.element; //获取element模块
  window.form = layui.form; //获取form模块
  window.layer = layui.layer; //获取layer模块
  window.laydate = layui.laydate; //获取laydate模块
  window.table = layui.table; //获取table模块
  window.laypage = layui.laypage; //获取laypage模块
  /**
   * 表格全局默认参数设置
   */
  table.set({
    page: false, //是否开启分页
    even: true, //是否开启隔行换色
    cellMinWidth: 80 //单元格的最小宽度
  });
  /**
   * 模块化加载
   * 自定义模块
   */
  require(["../module/config", "../module/cookie", "../module/base", "../module/verify"], function (config, cookie, base, verify) {
    /**
     * element组件渲染
     * form组件渲染
     */
    element.init();//每个页面都有
    form.render();//每个页面都有
    /**
     * 页面布局使用
     * 数据表格中计算高度部分的固定高度
     */
    var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
    /**
     * 全局变量
     */
    var modelIndex = null; //标记模态框

    /**
     * 初始化列表
     */
    function renderTable(list) {
      console.log("renderTable")
      table.render({
        id: "dataTable",
        elem: "#dataTable",
        limit: 9999,
        height: "full-" + fixedHeight,//全屏高度减去计算出的高度
        cols: [[
          {type: 'checkbox'},
          {align: "center", field: "id", title: "Id"},
          {align: "center", field: "question", title: "题目"},
          {align: "center", field: "option1", title: "选项1(正确选项)"},
          {align: "center", field: "option2", title: "选项2"},
          {align: "center", field: "option3", title: "选项3"},
          {align: "center", field: "option4", title: "选项4"},
          {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
        ]],
        data: list,
        size: "sm"
      });

    }

    // function renderPage(count, curr) {
    //   console.log("renderPage")
    //   laypage.render({
    //     elem: "dataPage",
    //     count: count,
    //     limit: config.pageSize,
    //     curr: curr,
    //     prev: "<i class='layui-icon'>&#xe603;</i>",
    //     next: "<i class='layui-icon'>&#xe602;</i>",
    //     layout: ["prev", "page", "next", "skip", "count"],
    //     theme: "#588fd0",
    //     jump: function (obj, first) {
    //       // console.log(obj,first)
    //       // getData(obj.curr,dodata)
    //       if (!first) {
    //         getData(obj.curr, dodata)
    //       }
    //     }
    //
    //   });
    // }

    function getData(page, cb) {

      var _data = {
        questionnaireId: '',
        pageSize: 9999,
        pageNo: page
      }
      EasyAjax.post_json({
        url: config.api_host + "/popQuiz/findQuestionPage",
        data: _data
      }, function (data) {
        cb(page, data)
      });
    }

    var table_data = [];

    function dodata(page, data) {
      var _list = formatList(data)
      var count = data.resultCount;
      renderTable(_list)
      // renderPage(count, page)
    }

    function formatList(data) {
      var list = data.questions;
      var _list = [];
      list.forEach(function (item) {
        _list.push({
          id: item.id,
          question: item.question,
          option1: item.option1,
          option2: item.option2,
          option3: item.option3,
          option4: item.option4
        })
      });
      table_data = _list;
      return _list;
    }

    getData(1, dodata)

    /**
     * 给查询按钮绑定单击事件
     */
    $("#searchBtn").on("click", function () {
      getData(1, dodata)
    });

    /**
     * 点击新增按钮
     */
    $("#addBtn").on("click", function () {

      form.render('select');
      modelIndex = layer.open({
        type: 1,
        title: "新增问题",
        //shadeClose: true,
        shade: [0.3, '#000'],
        area: ['600px', 'auto'],
        anim: 0,
        resize: false,
        content: $('#addWentiModel'),
      });
    });

    /**
     * 工具栏操作
     */
    table.on('tool(dataTable)', function (obj) {
      var data = obj.data;
      console.log(data);
      var layEvent = obj.event;
      if (layEvent === 'edit') { //编辑

      } else if (layEvent === 'del') {
        layer.confirm('确认删除', {anim: 6}, function (index) {
          // var para = {};
          EasyAjax.post_json({
              url: config.api_host + "/popQuiz/deleteQuestionnaire",
              data: {
                id: data.id
              }
            },
            function (result) {
              console.log(result)
              layer.msg("删除成功");
              getData(1, dodata)
              layer.close(index);
            });
        });
      }
    });

    /**
     * 点击保存按钮
     */
    form.on('submit(saveModelBtn)', function (obj) {
      console.log(obj)
      var field = obj.field;
      var _data = {};
      _data.question = field.question;
      _data.option1 = field.option1;
      _data.option2 = field.option2;
      _data.option3 = field.option3;
      _data.option4 = field.option4;

      EasyAjax.post_json({
          url: config.api_host + "/popQuiz/addNewQuestion",
          data: _data
        },
        function (data) {
          layer.msg("新增成功");
          getData(1, dodata)
        });
    });


    var ids = new Array();

    table.on('checkbox(dataTable)', function (obj) {
      console.log(obj)
      if (obj.checked == true) {

        if (obj.type == 'one') {

          ids.push(obj.data.id);

        } else {

          for (var i = 0; i < table_data.length; i++) {

            ids.push(table_data[i].id);

          }

        }

      } else {

        if (obj.type == 'one') {

          for (var i = 0; i < ids.length; i++) {

            if (ids[i] == obj.data.id) {

              // ids.remove(i);用这个方法会报错not a function

              // ids.pop();   //建议用这个
              ids.splice(i, 1);
            }

          }

        } else {

          for (var i = 0; i < ids.length; i++) {

            for (var j = 0; j < table_data.length; j++) {

              if (ids[i] == table_data[j].id) {

                // ids.remove(i);

                ids.splice(i, 1);

              }

            }

          }

        }

      }

      console.log(ids);

    });

    $("#addWenjuan").on('click', function () {
      if (ids.length === 0) {
        layer.msg("至少选择一个题目");
        return;
      }
      modelIndex = layer.open({
        type: 1,
        title: "新增问卷",
        //shadeClose: true,
        shade: [0.3, '#000'],
        area: ['600px', 'auto'],
        anim: 0,
        resize: false,
        content: $('#addWenjuanModel'),
        success: function () {
          console.log(ids)
          // var _html = '';
          // ids.forEach(function (item) {
          //   _html +=`
          //     <tr>
          //       <td>${item.id}</td>
          //       <td>${item.question}</td>
          //       <td>${item.option1}</td>
          //       <td>${item.option2}</td>
          //       <td>${item.option3}</td>
          //       <td>${item.option4}</td>
          //     </tr>
          //   `;
          // })
          // $("#wentiBody").html(_html);
        }
      });
    })
    /**
     * 取消模态框按钮
     */
    $("#cancelModelBtn").on("click", function () {
      layer.close(modelIndex);
    });
    $("#cancelWenjuanModelBtn").on("click", function () {
      layer.close(modelIndex);
    });
    form.on('submit(saveWenjuanModelBtn)', function (obj) {
      console.log(obj)
      EasyAjax.post_json({
          url: config.api_host + "/popQuiz/buildQuestionnaire",
          data: {
            questionnaireName: obj.field.questionnaireName,
            questionIds: ids
          }
        },
        function (data) {
          layer.msg("新增问卷成功");
          layer.close(modelIndex);
        });
    })

    /**
     * 页面初始化
     */
    // initList(1);
  });
})
;