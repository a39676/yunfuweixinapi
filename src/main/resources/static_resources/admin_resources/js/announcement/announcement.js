/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();//每个页面都有
        form.render();//每个页面都有
        laydate.render({
            elem: '#releaseTime'
            ,type: 'date'
            ,theme: '#588fd0'
        });
        /**
         * 页面布局使用
         * 数据表格中计算高度部分的固定高度
         */
        var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
        /**
         * 全局变量
         */
        var modelIndex = null; //标记模态框
        var msgTpl = UM.getEditor('msgTpl'); //编辑器控件
        msgTpl.ready(function () { //初始化编辑器
            msgTpl.execCommand('cleardoc');
        });

        /****************************************静态数据开始****************************************/
        function getData(page, cb) {
          var  postDate = $("#releaseTime").val();

          EasyAjax.post_json({
              url: config.api_host + "/article/findArticlePageByChannel",
              data: {
                pageSize: 10,
                pageNo: page,
                channelId: 1,
                postDate:  postDate
              }
            },
            function (data) {
              cb(page, data)
            })
        }

      function dodata(page, data) {
        console.log(data)
        var _list = formatList(data)
        var count = data.resultCount;
        renderTable(_list)
        renderPage(count, page)
      }

      function formatList(data) {
        var list = data.articleList;
        var _list = [];
        list.forEach(function (item) {
          if (!!item && item !== 'null') {
            _list.push({
              id: item.id,
              articleTitle:item.articleTitle,
              createDateString: item.createDateString
            })
          }

        })
        return _list;
      }

      function renderTable(list) {
        table.render({
          id: "dataTable,id",
          elem: "#dataTable",
          limit: config.pageSize,
          height: "full-" + fixedHeight,//全屏高度减去计算出的高度
          cols: [[
            {align: "center", field: "createDateString", title: "发布时间"},
            {align: "center", field: "articleTitle", title: "标题"},

            {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
          ]],
          data: list,
          size: "sm"
        });

      }

      function renderPage(count, curr) {
        laypage.render({
          elem: "dataPage",
          count: count,
          limit: config.pageSize,
          curr: curr,
          prev: "<i class='layui-icon'>&#xe603;</i>",
          next: "<i class='layui-icon'>&#xe602;</i>",
          layout: ["prev", "page", "next", "skip", "count"],
          theme: "#588fd0",
          jump: function (obj, first) {
            // console.log(obj,first)
            // getData(obj.curr,dodata)
            if (!first) {
              getData(obj.curr, dodata)
            }
          }

        });
      }

      getData(1, dodata)

      $("#searchBtn").on('click',function () {
        getData(1,dodata)
      })
        /**
         * 点击新增按钮
         */
        $("#addBtn").on("click",function () {
            modelIndex = layer.open({
                type: 1,
                title: "新增",
                shadeClose: false,
                shade: [0.3, '#000'],
                area: ['100%', '100%'],
                anim: 2,
                resize: false,
                content: $('#editModel'),
              success:function () {
                $("#model_action").val('add');
                $("#model_action_id").val('');
                $("#conTitle").val("");
                msgTpl.setContent('', false)
                form.render('select')
              }
            });
        });

        /**
         * 工具栏操作
         */
        table.on('tool(dataTable)', function(obj){
            var data = obj.data;
            var layEvent = obj.event;
          $("#model_action").val(layEvent);
          $("#model_action_id").val(data.id);
            if (layEvent === 'edit'){ //编辑
                modelIndex = layer.open({
                    type: 1,
                    title: "编辑",
                    shadeClose: false,
                    shade: [0.3, '#000'],
                    area: ['100%', '100%'],
                    anim: 2,
                    resize: false,
                    content: $('#editModel'),
                  success:function () {
                    EasyAjax.post_json({
                        url: config.api_host + "/article/findArticleById",
                        data: {
                          id: data.id
                        }
                      },
                      function (data) {
                        $("#conTitle").val(data.articleLongVO.articleTitle);
                        msgTpl.setContent(data.articleLongVO.contentLines, false)
                      });

                    form.render('select')

                  }
                });
            }else if(layEvent === 'del'){
                layer.confirm('确认删除', {anim: 6}, function(index){
                  EasyAjax.post_json({
                      url: config.api_host + "/article/deleteArticle",
                      data: {
                        id: data.id
                      }
                    },
                    function (data) {
                      layer.msg("删除成功");
                      getData(1,dodata)
                    });
                  layer.close(index);
                });
            }
        });

        /**
         * 点击保存按钮
         */
        form.on('submit(saveModelBtn)', function(data){
          console.log(data)
          var editorContent = UM.getEditor('msgTpl').getContent();

          console.log(editorContent);
          if(editorContent===""){
            layer.msg('内容不能为空');
            return;
          }

          var model_action = data.field.model_action;
          var articleId = data.field.model_action_id;
          var title = data.field.conTitle;
          var content = editorContent;

          if(model_action === "add"){
            EasyAjax.post_json({
                url: config.api_host + "/article/creatingArticle",
                data: {
                  title: title,
                  content: content,
                  articleChannel: 1,
                }
              },
              function (data) {
                layer.msg("新增成功");
                getData(1,dodata)
                layer.close(modelIndex);
              });
          }else if(model_action === "edit"){
            EasyAjax.post_json({
                url: config.api_host + "/article/editArticle",
                data: {
                  articleId: articleId,
                  title: title,
                  content: content,
                }
              },
              function (data) {
                layer.msg("修改成功");
                getData(1,dodata)
                layer.close(modelIndex);
              });
          }
        });

        /**
         * 取消模态框按钮
         */
        $("#cancelModelBtn").on("click",function () {
            layer.close(modelIndex);
        });

        /****************************************静态数据结束****************************************/

    });
});