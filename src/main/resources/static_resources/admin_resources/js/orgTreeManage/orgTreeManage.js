/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage','tree'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    window.tree = layui.tree; //获取tree模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();
        form.render();

      var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;

        var modelIndex = null;

        //获取顶级机构
      EasyAjax.post_json({
        url: config.api_host + "/organizations/findTopOrgForLoginManger",
      }, function (data) {
        console.log(data)
        var org = data.org;
        var _html = `<option selected value="${org.id}">${org.orgName}</option>`;
        $("#firstOrg").html(_html)
        // $("#hide_orgId").val(org.id)
        form.render();
        findOrgByDirectBelongTo(org.id)


        // console.log(data)
      });
      //获取二级机构
      function findOrgByDirectBelongTo( orgId) {
        EasyAjax.post_json({
          url: config.api_host + "/organizations/findOrgByDirectBelongTo",
          data:{
            orgId: orgId,
          }
        }, function (data) {
          console.log(data)
          var orgList = data.orgList;
          var _html = `<option value="">请选择</option>`;
          orgList.forEach(function (org) {
            _html += `<option value="${org.id}">${org.orgName}</option>`;
          })
          console.log(_html)
          $("#secondOrg").html(_html)
          form.render();

          // console.log(data)
        });
      }
      //获取三级机构
      form.on('select(secondOrg)', function(data){
          if(!data.value){
            $("#thirdOrg").html(`<option value="">请选择</option>`)
            return;
          }
        EasyAjax.post_json({
          url: config.api_host + "/organizations/findOrgByDirectBelongTo",
          data:{
            orgId: data.value,
          }
        }, function (data) {
          console.log(data)
          var orgList = data.orgList;
          var _html = `<option value="">请选择</option>`;
          orgList.forEach(function (org) {
            _html += `<option value="${org.id}">${org.orgName}</option>`;
          })
          console.log(_html)
          $("#thirdOrg").html(_html)
          form.render('select');

          // console.log(data)
        });
      });


      //获取顶级区域
      EasyAjax.post_json({
        url: config.api_host + "/geographical/findByBelongId",
        data:{
          areaParentId: ""
        }
      }, function (data) {
        var areaList = data.areaList;
        $("#model_geogralphicalId_1").html(`
            <option selected value="${areaList[0].id}">${areaList[0].areaName}</option>
        `);
        form.render('select');
        //获取二级区域
        EasyAjax.post_json({
          url: config.api_host + "/geographical/findByBelongId",
          data:{
            areaParentId: areaList[0].id
          }
        }, function (data) {
          let areaList = data.areaList;
          var _html = `<option value="">请选择</option>`
          areaList.forEach(function (item) {
            _html+=`
                  <option value="${item.id}">${item.areaName}</option>
              `
          });
          $("#model_geogralphicalId_2").html(_html)
          form.render('select');
        })


      });
      form.on('select(model_geogralphicalId_2)', function(data){
          console.log(data.value)
          if(!data.value){
            $("#model_geogralphicalId_3").html(`
                <option value="">请选择</option>
            `)
          }else {
            EasyAjax.post_json({
              url: config.api_host + "/geographical/findByBelongId",
              data:{
                areaParentId: data.value
              }
            }, function (data) {
              let areaList = data.areaList;
              var _html = `<option value="">请选择</option>`
              areaList.forEach(function (item) {
                _html+=`
                  <option value="${item.id}">${item.areaName}</option>
              `
              });
              $("#model_geogralphicalId_3").html(_html)
              form.render('select');
            })
          }

      });
      $("#addSecendOrg").on("click",function () {
          var firstOrg = $("#firstOrg option:selected").val()
        openModel("新增二级机构", function () {
            console.log(firstOrg)
          $("#belongTo").val(firstOrg)
          $("#orgGrade").val("2")
        })
      })
      $("#addThirdOrg").on("click",function () {
          var secondOrg = $("#secondOrg option:selected").val()
        if(!secondOrg){
          layer.msg("请选择二级机构");
          return;
        }
        openModel("新增三级机构", function () {
            $("#belongTo").val(secondOrg)
          $("#orgGrade").val("3")
        })
      })
        $("#cancelModelBtn").on("click",function () {
          layer.close(modelIndex);
        })
      form.on('submit(saveModelBtn)', function (data) {
        var obj = data.field;
            var orgCode = obj.model_orgCode;
            var orgName  = obj.model_orgName;
            var geogralphicalId = obj.model_geogralphicalId_3;
            var belongTo = obj.belongTo;
            var orgGrade = obj.orgGrade;
        EasyAjax.post_json({
          url: config.api_host + "/organizations/createNewBranchOrg",
          data:{
            orgCode: orgCode,
            belongTo:belongTo,
            orgName:orgName,
            geogralphicalId:geogralphicalId
          }
        }, function (data) {
          layer.alert('新增成功', {icon: 6});
          var firstOrg = $("#firstOrg option:selected").val();
          var secondOrg = $("#secondOrg option:selected").val();
          console.log(firstOrg)
          // findOrgByDirectBelongTo();
            if(orgGrade=== "2"){
              findOrgByDirectBelongTo(firstOrg)
            }else if(orgGrade === "3"){
              EasyAjax.post_json({
                url: config.api_host + "/organizations/findOrgByDirectBelongTo",
                data:{
                  orgId: secondOrg,
                }
              }, function (data) {
                var orgList = data.orgList;
                var _html = `<option value="">请选择</option>`;
                orgList.forEach(function (org) {
                  _html += `<option value="${org.id}">${org.orgName}</option>`;
                })
                console.log(_html)
                $("#thirdOrg").html(_html)
                form.render('select');
              });
            }
          layer.close(modelIndex);
        });
      })

      function openModel(title, cb) {
        modelIndex = layer.open({
          type: 1,
          title: title,
          shade: [0.3, '#000'],
          area: ['600px', 'auto'],
          anim: 0,
          resize: false,
          content: $('#editModel'),
          success:function (layero, index) {
            // console.log(layero,index)
            cb()
          }
        });
      }

      function renderTable(list) {
        table.render({
          id: "dataTable, id",
          elem: "#dataTable",
          limit: config.pageSize,
          height: "full-" + fixedHeight,//全屏高度减去计算出的高度
          cols: [[
            // {type: 'checkbox'},
            {align: "center", field: "orgCode", title: "机构代码"},
            {align: "center", field: "orgName", title: "机构名称"},
            {align: "center", field: "belongOrgName", title: "上级机构"},
            {align: "center", field: "areaName", title: "乡镇街道"},
            {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
          ]],
          data: list,
          size: "sm"
        });

      }

      function renderPage(count, curr) {
        laypage.render({
          elem: "dataPage",
          count: count,
          limit: config.pageSize,
          curr: curr,
          prev: "<i class='layui-icon'>&#xe603;</i>",
          next: "<i class='layui-icon'>&#xe602;</i>",
          layout: ["prev", "page", "next", "skip", "count"],
          theme: "#588fd0",
          jump: function (obj, first) {
            // console.log(obj,first)
            // getData(obj.curr,dodata)
            if (!first) {
              getData(obj.curr, dodata)
            }
          }

        });
      }

      function dodata(page, data) {
          console.log(data)
        var _list = formatList(data)
        var count = data.resultCount;
        renderTable(_list)
        renderPage(count, page)
      }

      function formatList(data) {
        var list = data.orgPage;
        var _list = [];
        list.forEach(function (item) {
          _list.push({
            id: item.id,
            orgCode: item.orgCode,
            orgName: item.orgName,
            belongOrgName:item.belongOrgName,
            areaName:item.areaName,
          })
        })
        return _list;
      }

      function getData(page, cb) {
        var _data = {
          pageSize: 10,
          pageNo: page
        }
        EasyAjax.post_json({
          url: config.api_host + "/organizations/findBranchOrgPage",
          data: _data
        }, function (data) {
          cb(page, data)
        });
      }
      getData(1,dodata)

      table.on('tool(dataTable)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'edit') { //编辑
          // openModel("编辑",function () {
          //   $("[name='model_productCode']").val(data.a)
          //   $("[name='model_productName']").val(data.b)
          //   $("[name='model_productSubType']").find(`option:contains(${data.c})`).attr("selected",true);
          //   $("[name='model_interestRate']").val(data.d)
          //   $("[name='model_expirationTime']").val(data.e)
          //   $("[name='model_productIntroduction']").val(data.g);
          //   $("[name='btnAction']").val("edit");
          //   $("[name='productId']").val(data.id)
          //   getProductDetail(data.id, function (res) {
          //     console.log(res)
          //     console.log(res.articleLongVO.contentLines)
          //     $("[name='model_article']").val(res.articleLongVO.contentLines)
          //   })
          // })
          // modelIndex = layer.open({
          //   type: 1,
          //   title: "编辑",
          //   shade: [0.3, '#000'],
          //   area: ['600px', 'auto'],
          //   anim: 0,
          //   resize: false,
          //   content: $('#editModel')
          // });
        } else if (layEvent === 'del') {
          layer.confirm('确认删除', {anim: 6}, function (index) {
            console.log(data.id)
            EasyAjax.post_json({
              url: config.api_host + "/organizations/deleteOrg",
              data: {
                id:data.id
              }
            }, function (data) {
              console.log(data);
              if(data.success){
                layer.alert('删除成功', {icon: 6});
              }
              getData(1,dodata)
            });
            layer.close(index);
          });
        }
      });
        /**
         * 全局变量
         */
        var treeObj = null; //节点树对象
        var treeSelected = null; //当前选中节点信息

        /**
         * 查询机构类型
         */
        var queryOrgType = function (currPage,orgTypeId,type) {
            var para = {};
            // para.pageSize = config.maxPageSize;
            // para.currPage = currPage;
            para.orgTypeId = orgTypeId;
            EasyAjax.post_json({
                    url: config.api_host + "sysOrgTypeRelationship/selectSysOrgTypeRelationshipListNotWol",
                    data: para,
                    async: false
                },
                function (data) {
                    if (data.result.length){
                        $("#orgTypeId").html('<option value=""></option>');
                        var tpl = type === 'add' ? 'organizationTypeTpl' : 'organizationTypeAllTpl';
                        base.renderTpl({
                            tplId : tpl,
                            arr : data.result,
                            type : "append",
                            containerId : "orgTypeId"
                        });
                    }else{
                        $("#orgTypeId").html('<option value=""></option>');
                    }
                });
        };

        /**
         * 页面初始化
         * 初始化组织机构树
         */
        var initTree = function () {
            var filter = function (treeId, parentNode, responseData) {
                if (!responseData.result) return [];
                for (var i=0, l=responseData.result.length; i<l; i++) {
                    responseData.result[i].isParent = responseData.result[i].isLeaf === 'Y' ? false : true;
                    delete responseData.result[i].icon;
                }
                return responseData.result;
            };
            var zTreeOnAsyncSuccess = function (event, treeId, treeNode, msg) {
                base.treeAsyncSuccessCallback(event, treeId, treeNode, msg);
            };
            var zTreeOnAsyncError = function(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
                base.treeHandleStatus(XMLHttpRequest.status);
            };
            var setting = {
                async: {
                    enable: true,
                    contentType: "application/json",
                    url: config.api_host + "sysOrg/selectSysOrgList",
                    type: "post",
                    dataType: "json",
                    autoParam: ["orgId"],
                    otherParam: {},
                    dataFilter: filter
                },
                data: {
                    key: {
                        name: "orgName",
                        url: "",
                        isParent: "isParent"
                    }
                },
                view: {
                    showLine: false,
                    selectedMulti: false
                },
                callback: {
                    onAsyncSuccess: zTreeOnAsyncSuccess,
                    onAsyncError: zTreeOnAsyncError,
                    onClick: function (event, treeId, treeNode) {
                        treeSelected = treeNode; //将当前选中节点数据存给全局变量，给增删改查提供数据
                        $("#saveBtn").data("type","edit");
                        $("#orgCode").val(treeNode.orgCode);
                        $("#orgName").val(treeNode.orgName);
                        $("#description").val(treeNode.description);
                        $("#postalCode").val(treeNode.postalCode);
                        $("#orgAddress").val(treeNode.orgAddress);
                        $("#isEnable").val(treeNode.isEnable);

                        //为组织机构类型下拉框赋值
                        if (treeNode.isLeaf === 'Y'){
                            queryOrgType(1,treeNode.pareOrgTypeId,'edit'); //查询机构类型
                            $("#orgTypeId").val(treeNode.orgTypeId);
                            if (treeNode.orgTypeName === '法人'){
                                $("#orgTypeId").prop("disabled",true);
                            }else{
                                $("#orgTypeId").prop("disabled",false);
                                $("#orgTypeId").find("option:contains('法人')").prop("disabled",true);
                            }
                        }else{
                            base.renderTpl({
                                tplId : "organizationTypeAllTpl",
                                arr : [
                                    {
                                        orgTypeId : treeNode.orgTypeId,
                                        orgTypeName : treeNode.orgTypeName
                                    }
                                ],
                                type : "html",
                                containerId : "orgTypeId"
                            });
                            $("#orgTypeId").val(treeNode.orgTypeId).prop("disabled",true);
                        }

                        $("#seq").val(treeNode.seq);
                        $("#externalCode").val(treeNode.externalCode);
                        form.render('select');
                    }
                }
            };
            treeObj = $.fn.zTree.init($("#dataTree"), setting);
        };

        /**
         * 点击新增按钮
         */
        $("#addBtn").on("click",function () {
            if (treeSelected === null){
                layer.msg("请选择机构");
                return;
            }

            queryOrgType(1,treeSelected ? treeSelected.orgTypeId : null,'add'); //查询机构类型
            $("#orgTypeId").prop("disabled",false);

            $("#saveBtn").data("type","add");
            $("#orgCode").val("");
            $("#orgName").val("");
            $("#description").val("");
            $("#postalCode").val("");
            $("#orgAddress").val("");
            $("#isEnable").val("Y");
            $("#orgTypeId").val("");
            $("#seq").val("");
            $("#externalCode").val("");
            form.render('select');
        });

        /**
         * 点击保存按钮
         */
        $("#saveBtn").on("click",function () {
            var type = $("#saveBtn").data("type");
            if (!type){
                return;
            }
            if (!$("#orgName").val()){
                layer.msg("请输入组织机构名称");
                return;
            }
            if (!$("#orgTypeId").val()){
                layer.msg("请选择组织机构类型");
                return;
            }
            if (!$("#orgAddress").val()){
                layer.msg("请输入组织机构地址");
                return;
            }
            if (!$("#isEnable").val()){
                layer.msg("请选择启停状态");
                return;
            }
            if (!base.isNumber($("#seq").val())){
                layer.msg("请输入数字类型的排序字段");
                return;
            }
            if (type === "add"){
                EasyAjax.get_json({
                        url: config.api_host + "sequence/id"
                    },
                    function (data) {
                        var para = {};
                        para.orgId = data.result;
                        para.pareOrgId = treeSelected ? treeSelected.orgId : null;
                        para.orgCode = treeSelected ? treeSelected.orgCode : null;
                        para.orgName = $("#orgName").val();
                        para.description = $("#description").val();
                        para.postalCode = $("#postalCode").val();
                        para.orgAddress = $("#orgAddress").val();
                        para.isEnable = $("#isEnable").val();
                        para.orgTypeId = $("#orgTypeId").val();
                        para.seq = $("#seq").val();
                        para.externalCode = $("#externalCode").val();
                        para.orgPath = (treeSelected ? base.treePath(treeObj,"add") : "") + para.orgName;
                        para.legalPersonOrgId = treeSelected ? treeSelected.legalPersonOrgId : null;
                        EasyAjax.post_json({
                                url: config.api_host + "sysOrg/addSysOrg",
                                data: para
                            },
                            function (data) {
                                layer.msg("保存成功");
                                base.treeAddNode(treeObj,"orgId","pareOrgId");
                            });
                    });
            }else if (type === "edit"){
                var para = {};
                para.orgId = treeSelected.orgId;
                para.pareOrgId = treeSelected.pareOrgId;
                para.orgCode = $("#orgCode").val();
                para.orgName = $("#orgName").val();
                para.description = $("#description").val();
                para.postalCode = $("#postalCode").val();
                para.orgAddress = $("#orgAddress").val();
                para.isEnable = $("#isEnable").val();
                para.orgTypeId = $("#orgTypeId").val();
                para.seq = $("#seq").val();
                para.externalCode = $("#externalCode").val();
                para.orgPath = base.treePath(treeObj,"edit") + para.orgName;
                para.legalPersonOrgId = treeSelected.legalPersonOrgId;
                EasyAjax.post_json({
                        url: config.api_host + "sysOrg/updateSysOrg",
                        data: para
                    },
                    function (data) {
                        layer.msg("保存成功");
                        base.treeUpdateNode(treeObj,"orgId","pareOrgId");
                    });
            }
        });

        /**
         * 点击删除按钮
         */
        $("#removeBtn").on("click",function () {
            if (treeSelected === null){
                layer.msg("请选择组织机构");
            }else if (treeSelected.isLeaf === 'N'){
                layer.msg("请先删除所有下级组织机构");
            }else{
                // var pNode = treeSelected.getParentNode();
                // if (pNode !== null){
                    layer.confirm('确认删除', {anim: 6}, function(index){
                        var para = {};
                        para.orgId = treeSelected.orgId;
                        para.pareOrgId = treeSelected.pareOrgId;
                        para.orgCode = treeSelected.orgCode;
                        para.orgName = treeSelected.orgName;
                        para.description = treeSelected.description;
                        para.postalCode = treeSelected.postalCode;
                        para.orgAddress = treeSelected.orgAddress;
                        para.isEnable = treeSelected.isEnable;
                        para.orgTypeId =treeSelected.orgTypeId;
                        para.seq = treeSelected.seq;
                        para.externalCode = treeSelected.externalCode;
                        para.orgPath = treeSelected.orgPath;
                        para.legalPersonOrgId = treeSelected.legalPersonOrgId;
                        para.isLeaf = treeSelected.isLeaf;
                        EasyAjax.post_json({
                                url: config.api_host + "sysOrg/deleteSysOrg",
                                data: para
                            },
                            function (data) {
                                layer.msg("删除成功");
                                layer.close(index);
                                base.treeDeleteNode(treeObj,"orgId","pareOrgId");

                                //重置初始状态
                                treeSelected = null;
                                $("#orgCode").val("");
                                $("#orgName").val("");
                                $("#description").val("");
                                $("#postalCode").val("");
                                $("#orgAddress").val("");
                                $("#isEnable").val("Y");
                                $("#orgTypeId").val("");
                                $("#seq").val("");
                                $("#externalCode").val("");
                                form.render('select');

                            });
                    });
                // }else{
                //     layer.msg("根节点不能删除");
                // }
            }
        });

        /**
         * 页面初始化
         */
        initTree(); //页面初始化
    });
});