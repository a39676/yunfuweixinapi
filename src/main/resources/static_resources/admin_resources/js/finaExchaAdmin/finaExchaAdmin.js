/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();//每个页面都有
        form.render();//每个页面都有
        /**
         * 页面布局使用
         * 数据表格中计算高度部分的固定高度
         */
        var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
        /**
         * 全局变量
         */
        var modelIndex = null; //标记模态框

        /****************************************静态数据开始****************************************/
        EasyAjax.post_json({
          url: config.api_host + "/organizations/findTopOrgForLoginManger",
        }, function (data) {
            console.log(data)
          var org = data.org;
          var _html = `<option value="${org.id}">${org.orgName}</option>`;
          $("#orgId").append(_html)
          $("#hide_orgId").val(org.id)
          findExchangeDibOrgLimit(org.id)
          // form.render();

          // console.log(data)
        });
        function findExchangeDibOrgLimit(id){
          EasyAjax.post_json({
            url: config.api_host + "/business/findExchangeDibOrgLimit",
            data:{
              orgId:id,
            }
          }, function (res) {
            console.log(res);
            var data = res.exchangeDibOrganizationLimit;
            $("#coin01Limit").val(data.coin01Limit);
            $("#coin05Limit").val(data.coin05Limit);
            $("#coin1Limit").val(data.coin1Limit);

            $("#paper1Limit").val(data.paper1Limit);
            $("#paper5Limit").val(data.paper5Limit);
            $("#paper10Limit").val(data.paper10Limit);
            $("#paper20Limit").val(data.paper20Limit);
            $("#paper50Limit").val(data.paper50Limit);
            $("#paper100Limit").val(data.paper100Limit);
            form.render();
          });
        }
        findExchangeDibUserLimit();
        function findExchangeDibUserLimit(userId){
          EasyAjax.post_json({
            url: config.api_host + "/business/findExchangeDibUserLimit",
            data:{
              userId: userId ? userId : '',
            }
          }, function (res) {
            console.log(res);
            var data = res.userLimit;
            $("#user_coin01Limit").val(data.coin01Limit);
            $("#user_coin05Limit").val(data.coin05Limit);
            $("#user_coin1Limit").val(data.coin1Limit);
            $("#user_paper1Limit").val(data.paper1Limit);
            $("#user_paper5Limit").val(data.paper5Limit);
            $("#user_paper10Limit").val(data.paper10Limit);
            $("#user_paper20Limit").val(data.paper20Limit);
            $("#user_paper50Limit").val(data.paper50Limit);
            $("#user_paper100Limit").val(data.paper100Limit);
            form.render();
          });
        }
        /**
         * 点击保存按钮
         */
        form.on('submit(org_saveModelBtn)', function(data){
          console.log(data);
           var obj = data.field;
          EasyAjax.post_json({
            url: config.api_host + "/business/updateExchangeDibOrgLimit",
            data:{
              orgId:obj.hide_orgId,
              coin01Limit: obj.coin01Limit,
              coin1Limit:obj.coin1Limit,
              coin05Limit:obj.coin05Limit,
              paper1Limit:obj.paper1Limit,
              paper5Limit:obj.paper5Limit,
              paper10Limit:obj.paper10Limit,
              paper20Limit:obj.paper20Limit,
              paper50Limit:obj.paper50Limit,
              paper100Limit:obj.paper100Limit
            }
          }, function (res) {
            findExchangeDibOrgLimit(obj.hide_orgId)
            layer.alert('设置成功', {icon: 6});
          })


        });
        form.on('submit(user_saveModelBtn)', function(data){
          console.log(data);
          var obj = data.field;
          EasyAjax.post_json({
            url: config.api_host + "/business/initOrUpdateDefaultUserLimit",
            data:{
              userId:'',
              coin01Limit: obj.user_coin01Limit,
              coin1Limit:obj.user_coin1Limit,
              coin05Limit:obj.user_coin05Limit,
              paper1Limit:obj.user_paper1Limit,
              paper5Limit:obj.user_paper5Limit,
              paper10Limit:obj.user_paper10Limit,
              paper20Limit:obj.user_paper20Limit,
              paper50Limit:obj.user_paper50Limit,
              paper100Limit:obj.user_paper100Limit
            }
          }, function (res) {
            findExchangeDibUserLimit();
            layer.alert('设置成功', {icon: 6});
          })
        });
        /****************************************静态数据结束****************************************/

    });
});