/**
 * 模块化加载
 * Layui中的模块
 */
const ProductSubType = {
  1: "小微企业贷款",
  2: "购房贷款",
  3: "购车贷款",
  4: "个人消费贷款",
  5: "个人经营性贷款",
  6: "其他贷款"
}
const ProductType = 2;
Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;

}
layui.use(['element', 'form', 'layer', 'laydate', 'table', 'laypage'], function () {
  window.element = layui.element; //获取element模块
  window.form = layui.form; //获取form模块
  window.layer = layui.layer; //获取layer模块
  window.laydate = layui.laydate; //获取laydate模块
  window.table = layui.table; //获取table模块
  window.laypage = layui.laypage; //获取laypage模块
  /**
   * 表格全局默认参数设置
   */
  table.set({
    page: false, //是否开启分页
    even: true, //是否开启隔行换色
    cellMinWidth: 80 //单元格的最小宽度
  });
  /**
   * 模块化加载
   * 自定义模块
   */
  require(["../module/config", "../module/cookie", "../module/base", "../module/verify"], function (config, cookie, base, verify) {
    /**
     * element组件渲染
     * form组件渲染
     */
    element.init();//每个页面都有
    form.render();//每个页面都有
    laydate.render({
      elem: '#presentationTime'
      , theme: '#588fd0'
      , format: 'yyyy/MM/dd HH:mm:ss'
    });
    laydate.render({
      elem: '#EndTime'
      , theme: '#588fd0'
      , format: 'yyyy/MM/dd HH:mm:ss'
    });
    function renderForm(){
      layui.use('form', function(){
        var form = layui.form();//高版本建议把括号去掉，有的低版本，需要加()
        form.render();
      });
    }
    /**
     * 页面布局使用
     * 数据表格中计算高度部分的固定高度
     */
    var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
    /**
     * 全局变量
     */
    var modelIndex = null; //标记模态框

    /****************************************静态数据开始****************************************/
    function dodata(page,data) {
      console.log(data)
      var _list = formatList(data)
      var count = data.resultCount;
      renderTable(_list)
      renderPage(count,page)
    }
    function formatList(data) {
      var list = data.reservationList;
      var _list = [];
      list.forEach(function (item) {
        _list.push({
          a: item.id,
          b: item.nickName,
          c: item.userName,
          d: item.productCode,
          e: item.productName,
          f: item.productTypeName,
          g: item.interestRate+"%",
          // h: '365天',
          i: item.reservationStatuString,
          j: item.orgName,
          k: item.receptionTimeString ? item.receptionTimeString : '',
          l: item.staffName ? item.staffName : '',
          // m: '',
          n: item.remark ,
          yuyueTime: item.reservationTimeStartString+"至"+item.reservationTimeEndString,
          cando: item.reservationStatu,
          orgId:item.orgId
        })
      })
      return _list;
    }

    function renderTable(list) {
      table.render({
        id: "dataTable,orgId",
        elem: "#dataTable",
        limit: config.pageSize,
        height: "full-" + fixedHeight,//全屏高度减去计算出的高度
        cols: [[
          // {type: 'checkbox'},
          {align: "center", field: "a", title: "受理号"},
          {align: "center", field: "b", title: "用户姓名"},
          {align: "center", field: "c", title: "用户手机"},
          {align: "center", field: "d", title: "产品代码"},
          {align: "center", field: "e", title: "产品名称"},
          {align: "center", field: "f", title: "产品类别"},
          {align: "center", field: "g", title: "年利率"},
          // {align: "center", field: "h", title: "申请周期"},
          {align: "center", field: "yuyueTime", title: "预约时间"},
          {align: "center", field: "i", title: "受理结果"},
          {align: "center", field: "j", title: "受理网点"},
          {align: "center", field: "k", title: "受理时间"},
          {align: "center", field: "l", title: "受理人"},
          // {align: "center", field: "m", title: "联系电话"},
          {align: "center", field: "n", title: "拒收理由"},
          {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right',field:"cando"}
        ]],
        data: list,
        size: "sm"
      });

    }
    function renderPage(count,curr) {
      laypage.render({
        elem: "dataPage",
        count: count,
        limit: config.pageSize,
        curr: curr,
        prev: "<i class='layui-icon'>&#xe603;</i>",
        next: "<i class='layui-icon'>&#xe602;</i>",
        layout: ["prev", "page", "next", "skip", "count"],
        theme: "#588fd0",
        jump: function (obj, first) {
          // console.log(obj,first)
          // getData(obj.curr,dodata)
          if (!first) {
            getData(obj.curr,dodata)
          }
        }

      });
    }

    function getData(page, cb) {
      var productSubType = $('#productSubType option:selected').val();
      productSubType = productSubType ? productSubType : ''
      var orgId = $("#orgId option:selected").val();
      orgId = orgId ? orgId : '';
      var shouliId = $("#shouliId").val();
      shouliId = shouliId ? shouliId : ''
      var receptionTime = $('#presentationTime').val();
      receptionTime = receptionTime ? receptionTime : '';
      var staffName = $("#staffName").val();
      staffName = staffName ? staffName : '';
      var mobile = $("#mobile").val();
      mobile = mobile? mobile : '';

      var _data = {
        productType: ProductType,
        productSubType: productSubType,
        orgId:  orgId,//机构id
        id: shouliId,// 受理号
        receptionTime: receptionTime,//受理时间
        staffName: staffName,//受理人员姓名
        mobile: mobile,//联系电话
        pageSize: 10,
        pageNo: page,
      }
      EasyAjax.post_json({
        url: config.api_host + "/reservation/findReservationPageForAdmin",
        data: _data
      }, function (data) {
        cb(page,data)
      });
    }

    /**
     * 打开model
     * */
    function openModel(title, cb) {
      modelIndex = layer.open({
        type: 1,
        title: title,
        shade: [0.3, '#000'],
        area: ['600px', 'auto'],
        anim: 0,
        resize: false,
        content: $('#editModel'),
        success:function (layero, index) {
          // console.log(layero,index)
          cb()
        }
      });
    }
    getData(1, dodata)
    //获取受理网点
    EasyAjax.post_json({
      url: config.api_host + "/organizations/findOrgForLoginManger",
    }, function (data) {
      var _html = ``;
      var orgList = data.orgList;
      for(var i=0;i<orgList.length;i++){
        _html+=`
        <option value="${orgList[i].id}">${orgList[i].orgName}</option>
        `
      }
      $("#orgId").append(_html)
      form.render();
     // console.log(data)
    });


    /**
     * 点击新增按钮
     */
    // $("#addBtn").on("click", function () {
    //   modelIndex = layer.open({
    //     type: 1,
    //     title: "新增",
    //     shade: [0.3, '#000'],
    //     area: ['600px', 'auto'],
    //     anim: 0,
    //     resize: false,
    //     content: $('#editModel')
    //   });
    // });

    /**
     * 工具栏操作
     */
    table.on('tool(dataTable)', function (obj) {
      var data = obj.data;
      console.log(data);
      var layEvent = obj.event;
      if (layEvent === 'no') { //编辑
        modelIndex = layer.open({
          type: 1,
          title: "拒绝",
          shade: [0.3, '#000'],
          area: ['600px', 'auto'],
          anim: 0,
          resize: false,
          content: $('#noModel'),
          success:function () {
            $("[name='no_id']").val(data.a)
          }
        });
      } else if (layEvent === 'ok') {
        modelIndex = layer.open({
          type: 1,
          title: "接受",
          shade: [0.3, '#000'],
          area: ['600px', 'auto'],
          anim: 0,
          resize: false,
          content: $('#okModel'),
          success:function () {
            $("[name='ok_id']").val(data.a)
            //获取网点下的处理人
            EasyAjax.post_json({
              url: config.api_host + "/organizations/findStaffsByOrgId",
              data:{
                orgId: data.orgId
              }
            }, function (data) {
              console.log(data)
              var _html = ``;
              var staffList = data.staffList;
              staffList.forEach(function (item) {
                _html+=`
                      <option value="${item.id}">${item.staffsName}</option>
                      `;
              })
              $("#chosePeople").html(_html)
              form.render();
              // console.log(data)
            });
          }
        });
        // layer.confirm('确认删除', {anim: 6}, function(index){
        //     layer.close(index);
        // });
      }
    });

    /**
     * 点击保存按钮
     */
    form.on('submit(no_saveModelBtn)', function (data) {
      console.log(data)
      var remark = data.field.remark;
      var reservationId = data.field.no_id;
      EasyAjax.post_json({
        url: config.api_host + "/reservation/rejectReservationProduct",
        data: {
          remark:remark,
          reservationId:reservationId
        }
      }, function (data) {
          if(data.result == 0){
            layer.alert('处理成功', {icon: 6});
            getData(1,dodata)
          }else {
            layer.alert('处理失败', {icon: 6});
          }
      });
      layer.close(modelIndex);
    });
    form.on('submit(ok_saveModelBtn)', function (data) {
      console.log(data)
      var staffId = data.field.chosePeople;
      var reservationId = data.field.ok_id;
      EasyAjax.post_json({
        url: config.api_host + "/reservation/acceptReservationProduct",
        data: {
          staffId:staffId,
          reservationId:reservationId
        }
      }, function (data) {
        if(data.result == 0){
          layer.alert('处理成功', {icon: 6});
          getData(1,dodata)
        }else {
          layer.alert('处理失败', {icon: 6});
        }
      });
      layer.close(modelIndex);
    });
    /**
     * 取消模态框按钮
     */
    $("#no_cancelModelBtn").on("click", function () {
      layer.close(modelIndex);
    });
    $("#ok_cancelModelBtn").on("click", function () {
      layer.close(modelIndex);
    });
    $("#searchBtn").on("click",function () {
      getData(1, dodata)
    })

    /****************************************静态数据结束****************************************/

  });
});