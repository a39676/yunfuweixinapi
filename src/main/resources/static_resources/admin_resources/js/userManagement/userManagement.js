/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form', 'layer', 'laydate', 'table', 'laypage', 'tree'], function () {
  window.element = layui.element; //获取element模块
  window.form = layui.form; //获取form模块
  window.layer = layui.layer; //获取layer模块
  window.laydate = layui.laydate; //获取laydate模块
  window.table = layui.table; //获取table模块
  window.laypage = layui.laypage; //获取laypage模块
  window.tree = layui.tree; //获取tree模块
  /**
   * 表格全局默认参数设置
   */
  table.set({
    page: false, //是否开启分页
    even: true, //是否开启隔行换色
    cellMinWidth: 80 //单元格的最小宽度
  });
  /**
   * 模块化加载
   * 自定义模块
   */
  require(["../module/config", "../module/cookie", "../module/base", "../module/verify"], function (config, cookie, base, verify) {
    /**
     * element组件渲染
     * form组件渲染
     */
    element.init();
    form.render();
    laydate.render({
      elem: '#startTime'
      , type: 'datetime'
      , theme: '#588fd0'
      , format: 'yyyy/MM/dd HH:mm:ss'
      , min: 0
      , value: base.formatDate(new Date()).completeDate
      , btns: ['confirm']
    });
    laydate.render({
      elem: '#endTime'
      , type: 'datetime'
      , theme: '#588fd0'
      , format: 'yyyy/MM/dd HH:mm:ss'
      , min: 0
      , btns: ['confirm']
    });
    /**
     * 页面布局使用
     * 数据表格中计算高度部分的固定高度
     */
    var fixedHeight = ($(".menu-content>.layui-form>.clearAfter").height() + $(".menu-content>.layui-form legend").height() + 20 + 6 + 20 + 33);
    /**
     * 全局变量
     */
    var orgTreeObj = null; //组织机构节点树对象
    var orgTreeModelIndex = null; //标记模态框

    var treeObj = null; //节点树对象
    var treeSelected = null; //当前选中节点信息
    var modelIndex = null; //标记模态框

    var modelAsyncTree = false; //判断是否加载组织机构树


    EasyAjax.post_json({
      url: config.api_host + "/organizations/findOrgForLoginManger",
    }, function (data) {
      var _html = "";
      var orgList = data.orgList;
      orgList.forEach(function (item) {
        _html += `
          <option value="${ item.id}">${ item.orgName}</option>
          `;
      })
      $("#orgSelect").append(_html)
      form.render();
      // console.log(data)
    });
    var roles = localStorage.getItem("roles");
    if(roles.indexOf('ROLE_ADMIN') === -1){
      $("#model_auth").append(`
                <option value='2'>网点业务员</option>
                <option value='3'>网点经办员</option>`
      );
    }else{
      $("#model_auth").append("<option value='1'>机构管理员</option>");
    }


    $("#searchBtn").on('click', function () {
      getData(1, dodata)
    })

    function dodata(page, data) {
      console.log(data)
      var _list = formatList(data)
      var count = data.resultCount;
      renderTable(_list)
      renderPage(count, page)
    }

    function formatList(data) {
      var list = data.staffList;
      var _list = [];
      list.forEach(function (item) {
        if(!!item && item!== 'null'){
          _list.push({
            id: item.id,
            staffNumber: item.staffNumber,
            staffsName: item.staffsName,
            mobile: item.mobile,
            authName: item.authName,
            authId: item.authId,
            gender: item.gender,
            genderStr: item.gender == "0" ? "女" : "男",
          })
        }

      })
      return _list;
    }

    function renderTable(list) {
      table.render({
        id: "dataTable,id,authId ,gender",
        elem: "#dataTable",
        limit: config.pageSize,
        height: "full-" + fixedHeight,//全屏高度减去计算出的高度
        cols: [[
          // {type: 'checkbox'},
          {align: "center", field: "staffNumber", title: "工号"},
          {align: "center", field: "staffsName", title: "姓名"},
          {align: "center", field: "genderStr", title: "性别"},
          {align: "center", field: "authName", title: "角色"},
          {align: "center", field: "mobile", title: "手机号"},
          {align: "center", title: "操作", width: 120, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
        ]],
        data: list,
        size: "sm"
      });

    }

    function renderPage(count, curr) {
      laypage.render({
        elem: "dataPage",
        count: count,
        limit: config.pageSize,
        curr: curr,
        prev: "<i class='layui-icon'>&#xe603;</i>",
        next: "<i class='layui-icon'>&#xe602;</i>",
        layout: ["prev", "page", "next", "skip", "count"],
        theme: "#588fd0",
        jump: function (obj, first) {
          // console.log(obj,first)
          // getData(obj.curr,dodata)
          if (!first) {
            getData(obj.curr, dodata)
          }
        }

      });
    }

    function getData(page, cb) {
      var orgId = $("#orgSelect option:selected").val();
      var staffNumber = $("#staffNumber").val();
      var mobile = $("#mobile").val();
      var staffName = $("#staffName").val();

      if (!orgId) {
        layer.alert('请选择机构名称', {icon: 6});
        return;
      }
      //获取员工
      EasyAjax.post_json({
        url: config.api_host + "/organizations/findStaffsByCondition",
        data: {
          orgId: orgId,
          staffNumber: staffNumber,
          staffName: staffName,
          mobile: mobile,
          pageSize: 10,
          pageNo: page,
        }
      }, function (data) {
        cb(page, data)
      });
    }

    /**
     * 监听编辑框中是否锁定切换
     */
    form.on('select(isLock)', function (data) {
      var isLock = $("#isLock").val();
      var isEnable = $("#isEnable").val();
      if (isLock === 'N' && isEnable === 'Y') {
        $("#reasons").data("reasons", $("#reasons").val()).val("").prop("disabled", true);
      } else {
        if ($("#reasons").data("reasons")) {
          $("#reasons").val($("#reasons").data("reasons"));
        }
        $("#reasons").data("reasons", "").prop("disabled", false);
      }
      form.render();
    });

    /**
     * 监听编辑框中是否启用切换
     */
    form.on('select(isEnable)', function (data) {
      var isLock = $("#isLock").val();
      var isEnable = $("#isEnable").val();
      if (isLock === 'N' && isEnable === 'Y') {
        $("#reasons").data("reasons", $("#reasons").val()).val("").prop("disabled", true);
      } else {
        if ($("#reasons").data("reasons")) {
          $("#reasons").val($("#reasons").data("reasons"));
        }
        $("#reasons").data("reasons", "").prop("disabled", false);
      }
      form.render();
    });

    /**
     * 初始化组织机构树
     */
    var initOrgTree = function () {
      var filter = function (treeId, parentNode, responseData) {
        if (!responseData.result) return [];
        for (var i = 0, l = responseData.result.length; i < l; i++) {
          responseData.result[i].isParent = responseData.result[i].isLeaf === 'Y' ? false : true;
          delete responseData.result[i].icon;
        }
        return responseData.result;
      };
      var setting = {
        async: {
          enable: true,
          contentType: "application/json",
          url: config.api_host + "sysOrg/selectSysOrgList",
          type: "post",
          dataType: "json",
          autoParam: ["orgId"],
          otherParam: {},
          dataFilter: filter
        },
        check: {
          enable: true,
          chkStyle: "radio",
          radioType: "all"
        },
        data: {
          key: {
            name: "orgName",
            url: "",
            isParent: "isParent"
          }
        },
        view: {
          showLine: false,
          selectedMulti: false
        },
        callback: {
          onClick: function (event, treeId, treeNode) {
            orgTreeObj.checkNode(treeNode, !treeNode.checked, null, true);
            return false;
          },
          onCheck: function (event, treeId, treeNode) {
            var nodes = orgTreeObj.getCheckedNodes(true);
            $("#orgId").val(nodes[0].orgName).data("orgId", nodes[0].orgId);
            layer.close(orgTreeModelIndex);
          },
          beforeAsync: function () {
            curAsyncCount++;
          },
          onAsyncSuccess: function (event, treeId, treeNode, msg) {
            base.treeAsyncSuccessCallback(event, treeId, treeNode, msg);
            curAsyncCount--;
            if (curStatus === "expand") {
              expandNodes(treeNode.children);
            } else if (curStatus === "async") {
              asyncNodes(treeNode.children);
            }
            if (curAsyncCount <= 0) {
              if (curStatus !== "init" && curStatus !== "") {
                asyncForAll = true;
              }
              curStatus = "";
            }
          },
          onAsyncError: function (event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
            base.treeHandleStatus(XMLHttpRequest.status);
            curAsyncCount--;
            if (curAsyncCount <= 0) {
              curStatus = "";
              if (treeNode !== null) asyncForAll = true;
            }
          }
        }
      };
      var demoMsg = {
        async: "加载中,请稍等..."
      };
      var curStatus = "init", curAsyncCount = 0, asyncForAll = false, goAsync = false;

      function expandAll() {
        if (!check()) {
          return;
        }
        if (asyncForAll) {
          orgTreeObj.expandAll(true);
        } else {
          expandNodes(orgTreeObj.getNodes());
          if (!goAsync) {
            curStatus = "";
          }
        }
      }

      function expandNodes(nodes) {
        if (!nodes) return;
        curStatus = "expand";
        for (var i = 0, l = nodes.length; i < l; i++) {
          orgTreeObj.expandNode(nodes[i], true, false, false);
          if (nodes[i].isParent && nodes[i].zAsync) {
            expandNodes(nodes[i].children);
          } else {
            goAsync = true;
          }
        }
      }

      function asyncNodes(nodes) {
        if (!nodes) return;
        curStatus = "async";
        for (var i = 0, l = nodes.length; i < l; i++) {
          if (nodes[i].isParent && nodes[i].zAsync) {
            asyncNodes(nodes[i].children);
          } else {
            goAsync = true;
            orgTreeObj.reAsyncChildNodes(nodes[i], "refresh", true);
          }
        }
      }

      function check() {
        if (curAsyncCount > 0) {
          layer.msg(demoMsg.async);
          return false;
        }
        return true;
      }

      orgTreeObj = $.fn.zTree.init($("#orgTree"), setting);
      setTimeout(function () { //延迟300ms后自动展开异步树，为了确保初始化树已执行完毕
        expandAll();
      }, config.treeAutoLoadTimeOut);
    };

    /**
     * 点击所属机构输入框
     */
    // $("#orgId").on("click", function () {
    //   orgTreeObj.checkAllNodes(false);
    //   var orgId = $("#orgId").data("orgId");
    //   if (orgId) {
    //     var node = orgTreeObj.getNodeByParam("orgId", orgId);
    //     orgTreeObj.checkNode(node, true, false);
    //   }
    //   orgTreeModelIndex = layer.open({
    //     type: 1,
    //     title: "所属机构",
    //     //shadeClose: true,
    //     shade: [0.3, '#000'],
    //     area: ['300px', 'auto'],
    //     anim: 0,
    //     resize: false,
    //     content: $('#orgTreeModel')
    //   });
    // });

    /**
     * 页面初始化
     * 初始化组织机构树
     */
    // var initTree = function () {
    //   var filter = function (treeId, parentNode, responseData) {
    //     if (!responseData.result) return [];
    //     for (var i = 0, l = responseData.result.length; i < l; i++) {
    //       responseData.result[i].isParent = responseData.result[i].isLeaf === 'Y' ? false : true;
    //       delete responseData.result[i].icon;
    //     }
    //     return responseData.result;
    //   };
    //   var zTreeOnAsyncSuccess = function (event, treeId, treeNode, msg) {
    //     base.treeAsyncSuccessCallback(event, treeId, treeNode, msg);
    //   };
    //   var zTreeOnAsyncError = function (event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
    //     base.treeHandleStatus(XMLHttpRequest.status);
    //   };
    //   var setting = {
    //     async: {
    //       enable: true,
    //       contentType: "application/json",
    //       url: config.api_host + "sysOrg/selectSysOrgList",
    //       type: "post",
    //       dataType: "json",
    //       autoParam: ["orgId"],
    //       otherParam: {},
    //       dataFilter: filter
    //     },
    //     data: {
    //       key: {
    //         name: "orgName",
    //         url: "",
    //         isParent: "isParent"
    //       }
    //     },
    //     view: {
    //       showLine: false,
    //       selectedMulti: false
    //     },
    //     callback: {
    //       onAsyncSuccess: zTreeOnAsyncSuccess,
    //       onAsyncError: zTreeOnAsyncError,
    //       onClick: function (event, treeId, treeNode) {
    //         treeSelected = treeNode; //将当前选中节点数据存给全局变量，给增删改查提供数据
    //         initList(1);
    //       }
    //     }
    //   };
    //   treeObj = $.fn.zTree.init($("#dataTree"), setting);
    // };

    /**
     * 初始化用户列表
     */
    // var initList = function (currPage) {
    //   var para = {};
    //   para.model = {};
    //   para.pageSize = config.pageSize;
    //   para.currPage = currPage;
    //   para.model.orgId = treeSelected ? treeSelected.orgId : "";
    //   para.model.userName = $("#searchUserName").val();
    //   para.model.userCode = $("#searchUserCode").val();
    //   para.model.mobile = $("#searchMobile").val();
    //   para.model.isLock = $("#searchIsLock").val();
    //   para.model.isEnable = $("#searchIsEnable").val();
    //   EasyAjax.post_json({
    //       url: config.api_host + "sysUser/selectSysUserList",
    //       data: para
    //     },
    //     function (data) {
    //
    //       if (data.result.list !== null) {
    //         for (var i = 0; i < data.result.list.length; i++) {
    //           data.result.list[i].createTime = data.result.list[i].createTime && base.formatDate(new Date(data.result.list[i].createTime)).completeDate;
    //           data.result.list[i].updatePasswordTime = data.result.list[i].updatePasswordTime && base.formatDate(new Date(data.result.list[i].updatePasswordTime)).completeDate;
    //           data.result.list[i].lastLoginTime = data.result.list[i].lastLoginTime && base.formatDate(new Date(data.result.list[i].lastLoginTime)).completeDate;
    //           data.result.list[i].startTime = data.result.list[i].startTime && base.formatDate(new Date(data.result.list[i].startTime)).completeDate;
    //           data.result.list[i].endTime = data.result.list[i].endTime && base.formatDate(new Date(data.result.list[i].endTime)).completeDate;
    //           data.result.list[i].enableTime = data.result.list[i].enableTime && base.formatDate(new Date(data.result.list[i].enableTime)).completeDate;
    //           data.result.list[i].disableTime = data.result.list[i].disableTime && base.formatDate(new Date(data.result.list[i].disableTime)).completeDate;
    //           data.result.list[i].deletingTime = data.result.list[i].deletingTime && base.formatDate(new Date(data.result.list[i].deletingTime)).completeDate;
    //         }
    //       }
    //
    //       table.render({
    //         id: "dataTable",
    //         elem: "#dataTable",
    //         limit: config.pageSize,
    //         height: "full-" + fixedHeight,
    //         cols: [[
    //           {align: "center", field: "userCode", title: "用户编号"},
    //           {align: "center", field: "loginName", title: "登录名"},
    //           {align: "center", field: "userName", title: "姓名"},
    //           {align: "center", field: "enName", title: "英文名"},
    //           {align: "center", field: "orgPath", title: "所属机构"},
    //           {align: "center", field: "isEnable", title: "是否启用", templet: '#dataTableIsEnableTpl'},
    //           {align: "center", field: "isLock", title: "是否锁定", templet: '#dataTableIsLockTpl'},
    //           {
    //             align: "center",
    //             field: "openIpMacCheck",
    //             title: "是否开启登录IP和MAC校验",
    //             templet: '#dataTableOpenIpMacCheckTpl'
    //           },
    //           {align: "center", field: "mobile", title: "手机号码"},
    //           {align: "center", field: "email", title: "邮箱"},
    //           {align: "center", field: "roleNames", title: "角色名称"},
    //           {align: "center", field: "idCard", title: "身份证号码"},
    //           {align: "center", field: "contactTel", title: "联系电话"},
    //           {align: "center", field: "ipAddress", title: "IP地址"},
    //           {align: "center", field: "macAddress", title: "MAC地址"},
    //           {align: "center", field: "updatePasswordTime", title: "密码修改时间"},
    //           {align: "center", field: "lastLoginTime", title: "最后登录时间"},
    //           {align: "center", field: "createTime", title: "创建时间"},
    //           {align: "center", field: "description", title: "描述信息"},
    //           {align: "center", field: "reasons", title: "锁定/停用的原因"},
    //           {align: "center", field: "startTime", title: "开始时间"},
    //           {align: "center", field: "endTime", title: "结束时间"},
    //           {align: "center", field: "enableTime", title: "启用时间"},
    //           {align: "center", field: "disableTime", title: "禁用时间"},
    //           {align: "center", field: "deletingTime", title: "删除时间"},
    //           {align: "center", title: "操作", width: 350, toolbar: "#dataTableToolBarTpl", fixed: 'right'}
    //         ]],
    //         data: data.result.list,
    //         size: "sm"
    //       });
    //       laypage.render({
    //         elem: "dataPage",
    //         count: data.result.total,
    //         limit: config.pageSize,
    //         curr: currPage,
    //         prev: "<i class='layui-icon'>&#xe603;</i>",
    //         next: "<i class='layui-icon'>&#xe602;</i>",
    //         layout: ["prev", "page", "next", "skip", "count"],
    //         theme: "#588fd0",
    //         jump: function (obj, first) {
    //           if (!first) {
    //             initList(obj.curr);
    //           }
    //         }
    //       });
    //     });
    // };


    /**
     * 点击新增按钮
     */
    $("#addBtn").on("click", function () {
      var orgid = $("#orgSelect option:selected").val();
      var orgName = $("#orgSelect option:selected").text();
      if(!orgid){
        layer.msg("请先选择机构");
        return;
      }
      $("#model_orgSelect_text").val(orgName);
      $("#model_orgSelect").val(orgid);
      modelIndex = layer.open({
        type: 1,
        title: "新增",
        //shadeClose: true,
        shade: [0.3, '#000'],
        area: ['900px', 'auto'],
        anim: 0,
        resize: false,
        content: $('#editModel')
      });
    });

    /**
     * 工具栏操作
     */
    table.on('tool(dataTable)', function (obj) {
      var data = obj.data;
      console.log(data)
      var layEvent = obj.event;
      if (layEvent === 'edit') { //编辑

        modelIndex = layer.open({
          type: 1,
          title: "编辑",
          //shadeClose: true,
          shade: [0.3, '#000'],
          area: ['900px', 'auto'],
          anim: 0,
          resize: false,
          content: $('#editModel')
        });
      } else if (layEvent === 'del') { //删除
        layer.confirm('确认删除', {anim: 6}, function (index) {

          EasyAjax.post_json({
              url: config.api_host + "/organizations/deleteStaff",
              data: {
                staffId: data.id
              }
            },
            function (data) {
              layer.msg("删除成功");
              getData(1,dodata)
              layer.close(index);
            });
        });

      }
    });

    /**
     * 点击保存按钮
     */
    form.on('submit(saveModelBtn)', function (data) {
      console.log(data);
      var obj = data.field;
      var userName = obj.model_mobile;
      var nickName = obj.model_staffName;
      var pwd = obj.model_pwd;
      var pwdRepeat = obj.model_pwdRepeat;
      var gender = obj.model_gender;
      var qq = obj.model_qq;
      var mobile = obj.model_mobile;
      var staffNumber = obj.model_staffNumber;
      var orgId = obj.model_orgSelect;
      var authId = obj.model_auth;


      EasyAjax.post_json({
        url: config.api_host + "/user/newManagerRegist",
        data: {
          userName: userName,
          nickName: nickName,
          pwd: pwd,
          pwdRepeat: pwdRepeat,
          gender: gender,
          qq: qq,
          mobile: mobile,
          staffNumber: staffNumber,
          orgId: orgId,
          authId: authId
        }
      }, function (data) {
        console.log(data)
        layer.close(modelIndex);
        layer.msg('新增员工成功');
        getData(1,dodata)
      });
    });

    /**
     * 导出按钮
     * 访问URL方式
     */
    // $("#exportBtn").on("click",function () {
    //     window.location.href = config.api_host + "sysUser/userInformationExport?orgId=" + (treeSelected ? treeSelected.orgId : "") + "&userName=" + $("#searchUserName").val() + "&userCode=" + $("#searchUserCode").val() + "&mobile=" + $("#searchMobile").val() + "&isLock=" + $("#searchIsLock").val() + "&isEnable=" + $("#searchIsEnable").val()
    // });

    /**
     * 导出按钮
     * AJAX方式
     */
    $("#exportBtn").on("click", function () {
      var para = {};
      para.orgId = treeSelected ? treeSelected.orgId : "";
      para.userName = $("#searchUserName").val();
      para.userCode = $("#searchUserCode").val();
      para.mobile = $("#searchMobile").val();
      para.isLock = $("#searchIsLock").val();
      para.isEnable = $("#searchIsEnable").val();
      window.open(config.api_host + "sysUser/userInformationExport?jsonUser=" + encodeURIComponent(JSON.stringify(para)));
    });

    /**
     * 取消模态框按钮
     */
    $("#cancelModelBtn").on("click", function () {
      layer.close(modelIndex);
    });

    /**
     * 页面初始化
     */
    // initTree(); //页面初始化
    // initList(1); //页面初始化
  });
});