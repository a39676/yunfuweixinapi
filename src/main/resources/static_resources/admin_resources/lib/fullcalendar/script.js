$(document).ready(function(){

	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	var bgcolor=['#3398FF','#FFBB34','#82C448','#EB3941'],
	zicolor=['#165590','#986703','#439003','#3E4243'];

	if($('.calendar').length > 0){
		$('.calendar').fullCalendar({
			header: {
				left: 'prev,next,today',
				center: 'title',
			},
			buttonText:{
				today:''
			},
			editable: true,
			events: [{
			  	num: 0,
				title: '9',
				start: new Date(y, m, 1),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
			  	num: 0,
				title: '9',
				start: new Date(y, m, 2),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
			  	num: 0,
				title: '9',
				start: new Date(y, m, 3),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
			  	num: 0,
				title: '9',
				start: new Date(y, m, 4),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
				num: 1,
				title: '14',
				start: new Date(y, m, 7),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
			  	num: 0,
				title: '9',
				start: new Date(y, m, 8),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
			  	num: 0,
				title: '9',
				start: new Date(y, m, 9),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
			  	num: 0,
				title: '9',
				start: new Date(y, m, 10),
				backgroundColor:bgcolor[2],
				textColor: zicolor[2],
				allDay:true
			  },{
				num: 1,
				title: '14',
				start: new Date(y, m, 12),
				backgroundColor:bgcolor[3],
				textColor: zicolor[3],
				allDay:true
			  },{
				num: 1,
				title: '1',
				start: new Date(y, m, 11),
				backgroundColor:bgcolor[1],
				textColor: zicolor[1],
				allDay:true
			  }
			]
		});
	}
	
});