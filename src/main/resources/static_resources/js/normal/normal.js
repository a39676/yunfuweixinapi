$(function(){
  $("#goToBind").hide();
	var userType = queryString().result;
	if(userType == '0'){
    $("#goToBind").show();
	}
	var first = $(".ReservationStatusType").val();
	getData(first)
	function getData(ReservationStatusType) {
    $.ajax({
      url: host + '/reservation/findReservationByUserId',
      type : "post",
      async : true,
      cache : false,
      contentType: "text/json",
      dataType: "json",
      data:JSON.stringify({
        reservationStatus: ReservationStatusType,
      }),
      success:function(datas){
        console.log(datas);
        $(".weui_tab").html('');
        var list = datas.reservationResultList;
        var html = '';
        if(list.length===0){
          $("#nodata").show();
          return;
        }else{
          $("#nodata").hide();
          list.forEach(function (item) {

            html += `
        	<div class="margin_top">
				<hr>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">受理号：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" readonly value="${item.id}">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label"> 预约状态：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" readonly value="${ item.reservationStatus}">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">机构名：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" readonly value="${item.orgName }">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">预约时间：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" readonly value="${item.reservationTime}">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label"> 受理员工姓名：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" readonly value="${item.staffName}">
					    </div>
				  	</div>
				</div>
				<div class="weui-cells weui-cells_form">
				  	<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">备注：</label></div>
					    <div class="weui-cell__bd">
					      	<input class="weui-input" readonly value="${item.remark ? item.remark : ""}">
					    </div>
				  	</div>
				</div>
			</div>
        `;
          });
          $(".weui_tab").html(html);
        }


      },
      error:function(datas){
        console.log(datas);
      },
    });
  }

  $(".ReservationStatusType").on("change",function () {
		getData($(this).val())
  });
})