$(function(){

	function getCity(){
    $.ajax({
      url: host + '/geographical/findFromCityID',
      type: "post",
      async: true,
      cache: false,
      contentType: "text/json",
      dataType: "json",
      success: function (datas) {
        var data = datas.areaList;
        var str = "";
        data.forEach(function (item) {
          var areaName = item.areaName;
          var id = item.id;
          str += "<option value='" + id + "'>" + areaName + "</option>";
        });
        $(".district").html(str);
      }
    })
	}
	function getStreet(){
    var val=$('.district').val();
    var date=JSON.stringify({"areaParentId": val});
    $.ajax({
      url: host + '/geographical/findByBelongId',
      type: "post",
      async: true,
      cache: false,
      contentType: "text/json",
      dataType: "json",
      data: date,
      success: function (datas) {
        var data = datas.areaList;
        var str = "";
        data.forEach(function (item) {
          var areaName = item.areaName;
          var id = item.id;
          str += "<option value='" + id + "'>" + areaName + "</option>";
        });
        $(".street").html(str);
      },
    });
	}
	function getOrg(){
    var val=$('.street').val();
    var dates=JSON.stringify({"geographicalId": val});
    $.ajax({
      url: host+ '/organizations/findOrgByGeographicalId',
      type : "post",
      async : true,
      cache : false,
      contentType: "text/json",
      dataType: "json",
      data:dates,
      success:function(datas){
        var data=datas.orgList;
        var str="";
        data.forEach(function(item){
          var orgName=item.orgName;
          var id=item.id;
          str +="<option value='" + id + "'>" + orgName + "</option>";
        });
        $(".mechanism").html(str);
      },
      error:function(datas){
        console.log(datas);
      },
    });
	}
	$(".district").on("change",function () {
    getStreet()
  })
	$(".street").on("change",function () {
    getOrg();
  })
	/* 市县区域选择 */
	$.ajax({
		url: host+'/geographical/findFromCityID',
		type : "post",
	    async : true,
	    cache : false,
	    contentType: "text/json",
	    dataType: "json",
		success:function(datas){
			var data=datas.areaList;
			var str="";
			data.forEach(function(item){
				var areaName=item.areaName;
				var id=item.id;
				str +="<option value='" + id + "'>" + areaName + "</option>";
			});
			$(".district").html(str);

			var val=$('.district').val();
			var date=JSON.stringify({"areaParentId": val});
			$.ajax({
				url: host+ '/geographical/findByBelongId',
				type : "post",
			    async : true,
			    cache : false,
			    contentType: "text/json",
			    dataType: "json",
			    data:date,
				success:function(datas){
					var data=datas.areaList;
					var str="";
					data.forEach(function(item){
						var areaName=item.areaName;
						var id=item.id;
						str +="<option value='" + id + "'>" + areaName + "</option>";
					});
					$(".street").html(str);

					var val=$('.street').val();
					var dates=JSON.stringify({"geographicalId": val});
					$.ajax({
						url: host+ '/organizations/findOrgByGeographicalId',
						type : "post",
					    async : true,
					    cache : false,
					    contentType: "text/json",
					    dataType: "json",
					    data:dates,
						success:function(datas){
							var data=datas.orgList;
							var str="";
							data.forEach(function(item){
								var orgName=item.orgName;
								var id=item.id;
								str +="<option value='" + id + "'>" + orgName + "</option>";
							});
							$(".mechanism").html(str);
						},
						error:function(datas){
							console.log(datas);
						},
					});
				},
				error:function(datas){
					console.log(datas);
				},
			});
		},
		error:function(datas){
			console.log(datas);
		},
	});
	$(".Complaint").on('click',function(){
		var orgId=$(".mechanism").val();
		var complaintType=$(".complaintType").val();
		var content=$(".content").val();
		var date=JSON.stringify({
			"orgId": orgId,
			"complaintType": complaintType,
			"content": content,
		});
		console.log(date);
		$.ajax({
			url: host+'/business/addComplaint',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				console.log(datas);
				$('.Written').text("投诉提交成功");
				$(".tips").css('display','block');
			},
			error:function(datas){
				console.log(datas);
			},
		});
	});
	$(".Sure").on('click',function(){
		$('.tips').css('display','none');
    WeixinJSBridge.call('closeWindow');
	})
})