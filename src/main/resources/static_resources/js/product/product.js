$(function(){
//	var datas=JSON.stringify({ 
//		"orgId": "2",
//		"productCode": '000021',
//		"productName": '产品2-1-25',
//		"productIntroduction": '产品2-1-25的简介',
//		"article": '产品详细介绍',
//		"productType": '2',
//		"productSubType": '1',
//		"expirationTime":'2019-02-12 18:02:22',
//		"interestRate": '8.9',
//		"interestType":'2',
//	});
//	$.ajax({
//		url:'/product/addNewProduct',
//		type : "POST",  
//	    async : true,
//	    cache : false,
//	    contentType: "text/json",
//	    dataType: "json",
//	    data:datas,
//		success:function(datas){
//			console.log(datas)	
//		},
//		error:function(datas){
//			console.log(datas)
//		}
//	})
	var productId = queryString().id;
	var datas=JSON.stringify({ "productId":productId});
	$.ajax({
		url: host+'/article/findArticleByProductId',
		type : "POST",  
	    async : true,
	    cache : false,
	    contentType: "text/json",
	    dataType: "json",
	    data:datas,
		success:function(datas){
			console.log(datas)
			var articleTitle = datas.articleLongVO.articleTitle;
			var content=datas.articleLongVO.contentLines;
			$("#articleTitle").html(articleTitle);
      $('.section').html(content);
      $("#createDateString").html(datas.articleLongVO.createDateString)
			// function decodeUnicode(str) {
			//     str = str.replace(/\\/g, "%");
			//     return unescape(str);
			// }
			// var GB2312UnicodeConverter = {
			// 	ToUnicode: function (str) {
			// 		return escape(str).toLocaleLowerCase().replace(/%u/gi, '\\u');
		  //     	},ToGB2312: function (str) {
		  //   		return unescape(str.replace(/\\u/gi, '%u'));
		  //   	}
			// };

		},
		error:function(datas){
			console.log(datas)
		}
	})
})