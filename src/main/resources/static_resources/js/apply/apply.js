$(function(){
	 


	var query = queryString();
	var productId = query.productId;
	var _data = {
    productId:productId
	}
	$.ajax({
		url: host + '/product/findProduct',
		type : "post",  
	    async : true,
	    cache : false,
	    contentType: "text/json",
	    dataType: "json",
	    data:JSON.stringify(_data),
		success:function(data){
			console.log(data)
			if(data.expirationTime==null){
				// $(".expirationTime").css('display','none');
			}else{
				var time=data.expirationTime.time;
				var commonTime = new Date(time).Format('yyyy-MM-dd hh:mm:ss') ;

				$(".expiration").val(commonTime);
			}
			$(".productCode").val(data.productCode);
			$(".productName").val(data.productName);
			if(data.productType===3){
				$("#minBaoe").show();
				$("#minBaoe input").val(data.insuranceAmountMin+"元");
				$("#maxBaoe").show();
				$("#maxBaoe input").val(data.insuranceAmountMax+"元");
			}else{
				$("#lilv").show();
        $(".interestRate").val(data.interestRate + "%");
			}

			var unicode=data.productIntroduction;
			function decodeUnicode(str) {
			    str = str.replace(/\\/g, "%");
			    return unescape(str);
			}
			var GB2312UnicodeConverter = { 
				ToUnicode: function (str) {
					return escape(str).toLocaleLowerCase().replace(/%u/gi, '\\u');
		      	},ToGB2312: function (str) {
		    		return unescape(str.replace(/\\u/gi, '%u'));
		    	} 
			};
			$('.productIntroduction').html(GB2312UnicodeConverter.ToGB2312(unicode));
		},
		error:function(datas){
			console.log(datas);
		},
	});
	/* 渲染结束  */
	
	
	/*  市县区域渲染  */
	$.ajax({
		url:  host +'/geographical/findFromCityID',
		type : "post",  
	    async : true,
	    cache : false,
	    contentType: "text/json",
	    dataType: "json",
		success:function(datas){
			var data=datas.areaList;
			var str="";
			data.forEach(function(item){
				var areaName=item.areaName;
				var id=item.id;
				str +="<option value='" + id + "'>" + areaName + "</option>";
			});
			$(".district").html(str);
			var val=$('.district').val();
			var date=JSON.stringify({ "productId": productId, "geographicalId": val});
			$.ajax({
				url:  host + '/organizations/findOrgByProductIdAndGeographical',
				type : "post",  
			    async : true,
			    cache : false,
			    contentType: "text/json",
			    dataType: "json",
			    data:date,
				success:function(datas){
					console.log(datas);
					var orgList=datas.orgList;
					var str="";
					orgList.forEach(function(item){
						var orgName=item.orgName;
						var id = item.id;
						str +="<option value='" + id + "'>" + orgName + "</option>";
					});
					$(".mechanism").html(str);
				},
				error:function(datas){
					console.log(datas);
				},
			});
		},
		error:function(datas){
			console.log(datas);
		},
	});
	
	/*  市县区域渲染结束  */
	
	
	/*  机构渲染  */
	$(".region select").bind('change',function(){
		var val=$(this).val();
		var date=JSON.stringify({ "productId": productId, "geographicalId": val});
		$.ajax({
			url:  host + '/organizations/findOrgByProductIdAndGeographical',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				console.log(datas);
				var orgList=datas.orgList;
				var str="";
				orgList.forEach(function(item){
					var orgName=item.orgName;
					var id = item.id;
					str +="<option value='" + id + "'>" + orgName + "</option>";
				});
				$(".mechanism").html(str);
			},
			error:function(datas){
				console.log(datas);
			},
		});
	})
	
	/*  结构渲染结束   */
	
	
	/*  点击预约  */
	$(".appointment").on('click',function(){
		var mechanism = $('.mechanism').val();
		var datetimeOpen=$("#datetime-picker").val();
		console.log(datetimeOpen);
		var datetime=$("#datetime-picke").val()+":00";
		var date=JSON.stringify({ 
			"reservationsStartTime": datetimeOpen, 
			"reservationsEndTime": datetime, 
			"orgId":  mechanism, 
			"productId": productId,
		});
		console.log(date)
		$.ajax({
			url:  host +'/reservation/applicationReservationsProduct',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				console.log(datas);
				var data=datas.message;
				$('.Written').text(data);
				$(".tips").css('display','block');
			},
			error:function(datas){
				console.log(datas);
			},
		});
	})
	$(".Sure").on('click',function(){
		$('.tips').css('display','none');
	})
});