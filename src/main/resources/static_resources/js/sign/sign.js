$(function(){
	var wxCode = queryString().code;
	$('.immediately').on('click',function(){
		$('.tab2').addClass('weui-bar__item--on').siblings(".tab1").removeClass("weui-bar__item--on");
		var bd = $('.immediately').parents().parent(".weui-tab__bd");
	    bd.find(".weui-tab__bd-item--active").removeClass("weui-tab__bd-item--active");
	    bd.find("#tab2").addClass("weui-tab__bd-item--active");
	})
	$('.sign').on('click',function(){
		WeixinJSBridge.call('closeWindow');
	});
	
	/*  获取验证码  */
	$("#Obtain").on('click',function(){
		var phone = document.getElementById('phone').value;
		function checkPhone(){ 
//			var phone = document.getElementById('phone').value;
		    if(!(/^1(3|4|5|7|8)\d{9}$/.test(phone))){ 
		        $(".tip").css("display","block").fadeIn(500).delay(1500).fadeOut(500);
		        return false; 
		    }; 
		};
		checkPhone();
		var date=JSON.stringify({ "mobile": phone});
		$.ajax({
			url:host + '/user/sendValidSms',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				console.log(datas);
				$(".tip").html("已发送验证码");
				$(".tip").css("display","block").fadeIn(500).delay(1500).fadeOut(500);
			},
			error:function(datas){
				console.log(datas);
			},
		});
	});
	
	
	
	/*  注册  */
	$(".register").on('click',function(){
		var phone = document.getElementById('phone').value;
		var userName = $('.userName').val();
		var nickName = $('.nickName').val();
		var pwd = $('.pwd').val();
		var pwdRepeat = $('.pwdRepeat').val();
		var validCode = $(".validCode").val();

		console.log(userName);
		var date=JSON.stringify({ 
			"mobile": phone,

			"userName": userName,
			"nickName": nickName,
			"pwd": pwd,
			"pwdRepeat": pwdRepeat,
			"gender": "-1",
			"qq": "",
			"mobile": phone,
			"validCode": validCode,
			"wxCode": wxCode,
		});
		$.ajax({
			url:host + '/user/userRegist',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				console.log(datas);
				var data=datas.message;
				var dats=datas.json;
				var str="";
				if(data==""){
					$('.Written').html(`<p>注册成功</p>`);
					$(".tips").css('display','block');
				}else{
					$('.Written').text(data);
					$(".tips").css('display','block');
				}
				
				
			},
			error:function(datas){
				console.log(datas);
			},
		});
	})
	$(".Sure").on('click',function(){
		$('.tips').css('display','none');
	})
	
	
	/*  登录   */
	$(".sign_in").on('click',function(){
		var userName = $('.user_name').val();
		var paswd = $("#pwd").val();
		$.ajax({
			url:host+ '/auth/login_check',
			type : "post", 
		    dataType: "json",
		    data:{
		    	user_name: userName,
				pwd: paswd,
		    },
			success:function(datas){
				console.log(datas);
				var result=datas.result;
				if(result==0){
					var data=datas.message;
					$('.written').text('登陆成功');
					$(".tipss").css('display','block');
					$(".sure").attr('onClick','custom_close()');
				}else{
					$('.written').html(data);
					$(".tipss").css('display','block');
				}
				
			},
			error:function(datas){
				$(".tipss").css('display','block');
				$(".sure").on('click',function(){
					$('.tipss').css('display','none');
				})
				console.log(datas);
			},
		});
	});

	$("#change_ok").on("click",function () {
		var oldPassword = $("#change_pwd").val();
		var newPassword = $("#change_pwdRepeat").val();
		var newPasswordRepeat = $("#change_pwdRepeat_ok").val();
		if(!oldPassword || !newPassword || !newPasswordRepeat){
			alert("请将内容填写完整！");
			return;
		}
    $.ajax({
      url: host + '/user/isLogin',
      type : "post",
      dataType: "json",
      success:function(data){
      	if(data.result ==0){
          $.ajax({
            url: host + '/user/resetPassword',
            type: "post",
            dataType: "json",
						data: {
              oldPassword:  oldPassword,
              newPassword: newPassword,
              newPasswordRepeat:  newPasswordRepeat,
            },
            success: function (data) {
							if(data.result ==0){
								alert("密码修改成功");
							}else{
                alert("密码修改失败");
							}
            }
          });
				}else{
          alert("修改密码之前要登录！");
				}
      },
      error:function(datas){

      },
    });
  });
	$("#find_ok").on("click",function () {
		var mobile = $("#find_phone").val();
		var pwd = $("#find_pwd").val();
		var pwdRepeat = $("#find_pwdRepeat").val();
		var validCode = $("#find_validCode").val();

    $.ajax({
      url: host + '/user/resetPasswordByForgot',
      type : "post",
      dataType: "json",
			data:{
        mobile: mobile,
        validCode:validCode,
        pwd: pwd,
        pwdRepeat:pwdRepeat,
			},
      success:function(data){
      	if(data.result==0){
          alert("成功");
				}else{
          alert("失败");
				}

      },
      error:function(datas){

      },
    });
  });
  $("#find_Obtain").on('click',function(){
    var phone = $('#find_phone').val();
    function checkPhone(){
//			var phone = document.getElementById('phone').value;
      if(!(/^1(3|4|5|7|8)\d{9}$/.test(phone))){
        $(".tip").css("display","block").fadeIn(500).delay(1500).fadeOut(500);
        return false;
      };
    };
    checkPhone();
    var date=JSON.stringify({ "mobile": phone});
    $.ajax({
      url: host + '/user/sendValidSms2',
      type : "post",
      async : true,
      cache : false,
      contentType: "text/json",
      dataType: "json",
      data:date,
      success:function(datas){
        console.log(datas);
        $(".tip").html("已发送验证码");
        $(".tip").css("display","block").fadeIn(500).delay(1500).fadeOut(500);
      },
      error:function(datas){
        console.log(datas);
      },
    });
  });
})