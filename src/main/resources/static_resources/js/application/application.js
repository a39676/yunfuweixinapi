$(function(){
	var date=new Date();   
	var year=date.getFullYear(); //获取当前年份   
	var mon=date.getMonth()+1; //获取当前月份   
	var da=date.getDate(); //获取当前日   
	var h=date.getHours(); //获取小时   
	var m=date.getMinutes(); //获取分钟   
	var s=date.getSeconds(); //获取秒   
	var d=year+'-'+mon+'-'+da+' '+h+':'+m+':'+s;
	
	/* 市县区域选择 */
	$.ajax({
		url:'/geographical/findFromCityID',
		type : "post",  
	    async : true,
	    cache : false,
	    contentType: "text/json",
	    dataType: "json",
		success:function(datas){
			var data=datas.areaList;
			var str="";
			data.forEach(function(item){
				var areaName=item.areaName;
				var id=item.id;
				str +="<option value='" + id + "'>" + areaName + "</option>";
			});
			$(".district").html(str);
			var val=$('.district').val();
			var date=JSON.stringify({"areaParentId": val});
			$.ajax({
				url:'/geographical/findByBelongId',
				type : "post",  
			    async : true,
			    cache : false,
			    contentType: "text/json",
			    dataType: "json",
			    data:date,
				success:function(datas){
					var data=datas.areaList;
					var str="";
					data.forEach(function(item){
						var areaName=item.areaName;
						var id=item.id;
						str +="<option value='" + id + "'>" + areaName + "</option>";
					});
					$(".street").html(str);
					var val=$('.street').val();
					var dates=JSON.stringify({"geographicalId": val});
					$.ajax({
						url:'/organizations/findOrgByGeographicalId',
						type : "post",  
					    async : true,
					    cache : false,
					    contentType: "text/json",
					    dataType: "json",
					    data:dates,
						success:function(datas){
							var data=datas.orgList;
							var str="";
							data.forEach(function(item){
								var orgName=item.orgName;
								var id=item.id;
								str +="<option value='" + id + "'>" + orgName + "</option>";
							});
							$(".mechanism").html(str);
						},
						error:function(datas){
							console.log(datas);
						},
					});
				},
				error:function(datas){
					console.log(datas);
				},
			});
		},
		error:function(datas){
			console.log(datas);
		},
	});
	
	/*  乡镇街道选择  */
	$('.region select').bind('change',function(){
		var val=$(this).val();
		var date=JSON.stringify({"areaParentId": val});
		$.ajax({
			url:'/geographical/findByBelongId',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				var data=datas.areaList;
				var str="";
				data.forEach(function(item){
					var areaName=item.areaName;
					var id=item.id;
					str +="<option value='" + id + "'>" + areaName + "</option>";
				});
				$(".street").html(str);
			},
			error:function(datas){
				console.log(datas);
			},
		});
	});
	
	
	/*  机构选择  */
	$(".street").bind('change',function(){
		var val=$(this).val();
		var date=JSON.stringify({"geographicalId": val});
		$.ajax({
			url:'/organizations/findOrgByGeographicalId',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				var data=datas.orgList;
				var str="";
				data.forEach(function(item){
					var orgName=item.orgName;
					var id=item.id;
					str +="<option value='" + id + "'>" + orgName + "</option>";
				});
				$(".mechanism").html(str);
			},
			error:function(datas){
				console.log(datas);
			},
		});
	});
	
	
	/*  点击预约  */
	$(".appointment").on('click',function(){
		var mechanism = $('.mechanism').val();
		var datetimeOpen=$("#datetime-picker").val()+":"+s;
		var datetime=$("#datetime-picke").val()+":00";
		var orgId=$(".mechanism").val();
		var moneyType=$(".moneyType").val();
		var coin01Used=$(".coin01Used").val();
		var coin05Used=$(".coin05Used").val();
		var coin1Used=$(".coin1Used").val();
		var paper1Used=$(".paper1Used").val();
		var paper5Used=$(".paper5Used").val();
		var paper10Used=$(".paper10Used").val();
		var paper20Used=$(".paper20Used").val();
		var paper50Used=$(".paper50Used").val();
		var paper100Used=$(".paper100Used").val();
		var date=JSON.stringify({
			"reservationsStartTime": datetimeOpen,
			"reservationsEndTime": datetime,
			"orgId": orgId,
			"moneyType": moneyType,
			"coin01Used": coin01Used,
			"coin05Used": coin05Used,
			"coin1Used": coin1Used,
			"paper1Used": paper1Used,
			"paper5Used": paper5Used,
			"paper10Used": paper10Used,
			"paper20Used": paper20Used,
			"paper50Used": paper50Used,
			"paper100Used": paper100Used,
		});
		console.log(date);
		$.ajax({
			url:'/reservation/applicationExchangeDib',
			type : "post",  
		    async : true,
		    cache : false,
		    contentType: "text/json",
		    dataType: "json",
		    data:date,
			success:function(datas){
				console.log(datas);
				var data=datas.message;
				$('.Written').text(data);
				$(".tips").css('display','block');
			},
			error:function(datas){
				console.log(datas);
			},
		});
	});
	$(".Sure").on('click',function(){
		$('.tips').css('display','none');
	})
})