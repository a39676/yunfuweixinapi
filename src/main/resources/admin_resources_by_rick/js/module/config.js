/**
 * 系统配置
 * 项目中用到的公共配置信息
 */
define(['./base','./cookie'],function (base,cookie) {

    /**
     * api_host
     * 项目基础路径
     */
    var api_host = "http://localhost:8080/";
    /**
     * 获取用户信息
     */
    var userName = cookie.get_userName();
    if (!userName && window.location.href.indexOf("/login") === -1){
        $("#loading").remove();
        $("#layui-layout-admin").show();
    }else{

        $("#loading").remove();
        $("#layui-layout-admin").show();
        //针对layui框架中laypage模块bug统一处理
        $(document).on("change",".layui-laypage-skip .layui-input",function () {
            var $this = $(this),
                $count = parseInt($this.parent().prev().prev().text()),
                $curr = $this.val();
            if ($curr > $count){
                $this.val($count);
                return false;
            }
        });
        //针对layui框架中form模块统一限制input长度
        $(document).on("input",".layui-input",function () {
            var $this = $(this);
            if ($this.val().length > 120){
                $this.val($this.val().slice(0,120));
                return false;
            }
        });
        //针对layui框架中table模块点击单元td弹出详细信息，重新渲染表格时不关闭弹窗的bug
        $(document).on("click",".layui-table-view tr",function (e) {
            var event = e || window.event;
            event.stopPropagation();
        });
        $(document).on("click",function () {
            $(".layui-table-tips-c").click();
        });
        //自动给页面中所有input输入框增加autocomplete属性为了实现不缓存用户历史输入
        $("input[type='text'],input[type='number']").attr("autocomplete","off");
    }
    /**
     * pageSize
     * 系统统一数据表格每页显示条数
     */
    var pageSize = cookie.get_pageSize();
    /**
     * 系统统一查询分页接口默认传递一个最大值查询所有数据时的默认值
     */
    var maxPageSize = 999;
    /**
     * 树节点默认第一个选中延迟加载时间
     */
    var treeDefaultSelectTimeOut = 600;
    /**
     * 自动加载异步树延迟时间
     */
    var treeAutoLoadTimeOut = 800;
    /**
     * 查询默认页面延迟时间，为了确保多法人逻辑中加载的iframe页面加载完成
     */
    var queryDefaultOpenPageTimeOut = 600;

    /**
     * 登陆方式配置
     * account: 账号密码登陆方式[true:开启,false:关闭]
     * phone: 短信验证码登陆方式[true:开启,false:关闭]
     * concatenation: 账号密码短信验证码级联登陆方式[true:开启,false:关闭]
     */
    var loginType = {
        account : true,
        phone : false,
        concatenation : false
    };

    /**
     * 页面初始化
     */

    $.cookie("callbackLock",false,{path:"/"}); //页面加载时先进性给登陆超时锁解锁操作

    return{
        api_host : api_host,
        pageSize : pageSize,
        maxPageSize : maxPageSize,
        treeDefaultSelectTimeOut : treeDefaultSelectTimeOut,
        treeAutoLoadTimeOut : treeAutoLoadTimeOut,
        queryDefaultOpenPageTimeOut : queryDefaultOpenPageTimeOut,
        loginType : loginType
    }
});