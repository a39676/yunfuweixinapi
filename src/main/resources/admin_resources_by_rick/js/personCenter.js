/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    /**
     * 模块化加载
     * 系统配置模块
     * cookie模块
     * base模块
     */
    require(["./module/config","./module/cookie","./module/base","./module/verify"],function (config, cookie, base, verify) {
        /**
         * 获取分页信息
         */
        $("#pageSize").val(config.pageSize);
        /**
         * 事件绑定
         * 给保存按钮绑定单击事件实现个人设置
         */
        form.on('submit(saveBaseSet)', function(data){
            var para = data.field;
            EasyAjax.post_json({
                    url: config.api_host + "sysUser/updatePageSize",
                    data: para
                },
                function (data) {
                    layer.msg("保存成功");
                    cookie.set_pageSize(data.result.pageSize);
                });
            return false;
        });

        /**
         * code加密
         */
        var codeEncryption = function (code) {
            var codeEncryption = "";
            EasyAjax.get({
                    url: config.api_host + "sysUser/customEncryption?code=" + code,
                    async: false
                },
                function (data) {
                    codeEncryption = data.result;
                });
            return codeEncryption;
        };

        /**
         * 事件绑定
         * 给保存按钮绑定单击事件实现修改密码
         */
        form.on('submit(saveModifyPassword)', function(data){
            var passwordComplexity = $("#equalPassword").data("passwordComplexity");
            var passwordComplexityArr = passwordComplexity.split("-");
            var minPasswordLength = parseInt(passwordComplexityArr[0]);
            var maxPasswordLength = parseInt(passwordComplexityArr[1]);
            if (!(data.field.pwd.length >= minPasswordLength && data.field.pwd.length <= maxPasswordLength)){
                layer.msg("密码长度应该在" + passwordComplexity + "范围内");
                return false;
            }
            var para = data.field;
            para.oldPassword = codeEncryption(para.oldPassword); //对用户输入的密码进行md5加密处理
            para.pwd = codeEncryption(para.pwd); //对用户输入的密码进行md5加密处理
            para.confirm = codeEncryption(para.confirm); //对用户输入的密码进行md5加密处理
            EasyAjax.post_json({
                    url: config.api_host + "sysUser/modifyPassword",
                    data: para
                },
                function (data) {
                    EasyAjax.post_json({
                            url: config.api_host + "sysUser/logout"
                        },
                        function (data) {
                            var loginMsg = cookie.get_loginMsg();
                            if (loginMsg){
                                loginMsg.password = '';
                                cookie.set_loginMsg(loginMsg);
                            }
                            cookie.del_user();
                            window.parent.location.href = "login";
                        });
                });
            return false;
        });
    });
});
