/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();//每个页面都有
        form.render();//每个页面都有
        /**
         * 页面布局使用
         * 数据表格中计算高度部分的固定高度
         */
        var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
        /**
         * 全局变量
         */
        var modelIndex = null; //标记模态框

        /**
         * 初始化列表
         */
        var initList = function (pageNo) {
            var para = {};
            para.orgCode = $("#searchOrgCode").val();
            para.orgName = $("#searchOrgName").val();
            para.orgType = $("#searchOrgType").val();
            para.pageSize = config.pageSize;
            para.pageNo = pageNo;
            EasyAjax.post_json({
                    url: config.api_host + "organizations/findOrgPage",
                    data: para
                },
                function (data) {
                    table.render({
                        id : "dataTable",
                        elem : "#dataTable",
                        limit : config.pageSize,
                        height : "full-" + fixedHeight, //全屏高度减去计算出的高度
                        cols : [[
                            {align : "center",field : "orgCode",title : "机构代码"},
                            {align : "center",field : "orgName",title : "机构名称"},
                            {align : "center",field : "orgType",title : "机构类型",toolbar : "#dataTableOrgTypeTpl"},
                            {align : "center",title : "操作",width : 120,toolbar : "#dataTableToolBarTpl",fixed: 'right'}
                        ]],
                        data : data.orgPage,
                        size : "sm"
                    });
                    laypage.render({
                        elem: "dataPage",
                        count: data.resultCount,
                        limit: config.pageSize,
                        curr: pageNo,
                        prev: "<i class='layui-icon'>&#xe603;</i>",
                        next: "<i class='layui-icon'>&#xe602;</i>",
                        layout: ["prev", "page", "next", "skip", "count"],
                        theme: "#588fd0",
                        jump: function(obj,first){
                            if(!first){
                                initList(obj.curr);
                            }
                        }
                    });
                });
        };

        /**
         * 给查询按钮绑定单击事件
         */
        $("#searchBtn").on("click",function () {
            initList(1);
        });

        /**
         * 点击新增按钮
         */
        $("#addBtn").on("click",function () {
            $("#orgCode").val("");
            $("#orgName").val("");
            $("#orgType").val("");
            form.render('select');
            $("#saveModelBtn").data("id","");
            modelIndex = layer.open({
                type: 1,
                title: "新增",
                //shadeClose: true,
                shade: [0.3, '#000'],
                area: ['600px', 'auto'],
                anim: 0,
                resize: false,
                content: $('#editModel')
            });
        });

        /**
         * 工具栏操作
         */
        table.on('tool(dataTable)', function(obj){
            var data = obj.data;
            var layEvent = obj.event;
            if (layEvent === 'edit'){ //编辑
                $("#orgCode").val(data.orgCode);
                $("#orgName").val(data.orgName);
                $("#orgType").val(data.orgType);
                form.render('select');
                $("#saveModelBtn").data("id",data.id);
                modelIndex = layer.open({
                    type: 1,
                    title: "编辑",
                    //shadeClose: true,
                    shade: [0.3, '#000'],
                    area: ['600px', 'auto'],
                    anim: 0,
                    resize: false,
                    content: $('#editModel')
                });
            }else if(layEvent === 'del'){
                layer.confirm('确认删除', {anim: 6}, function(index){
                    var para = {};
                    para.id = data.id;
                    EasyAjax.post_json({
                            url: config.api_host + "organizations/deleteOrg",
                            data: para
                        },
                        function (result) {
                            layer.msg("操作成功");
                            initList(1);
                            layer.close(index);
                        });
                });
            }
        });

        /**
         * 点击保存按钮
         */
        form.on('submit(saveModelBtn)', function(data){
            var id = $("#saveModelBtn").data("id");
            if (id){ //编辑
                var para = {};
                para.id = id;
                para.orgCode = $("#orgCode").val();
                para.orgName = $("#orgName").val();
                para.orgType = $("#orgType").val();
                EasyAjax.post_json({
                        url: config.api_host + "organizations/updateOrgInfo",
                        data: para
                    },
                    function (data) {
                        layer.msg("操作成功");
                        initList(1);
                        layer.close(modelIndex);
                    });
            }else{ //新增
                var para = {};
                para.orgCode = $("#orgCode").val();
                para.orgName = $("#orgName").val();
                para.orgType = $("#orgType").val();
                EasyAjax.post_json({
                        url: config.api_host + "organizations/createNewOrgByAdmin",
                        data: para
                    },
                    function (data) {
                        layer.msg("保存成功");
                        initList(1);
                        layer.close(modelIndex);
                    });
            }
        });

        /**
         * 取消模态框按钮
         */
        $("#cancelModelBtn").on("click",function () {
            layer.close(modelIndex);
        });

        /**
         * 页面初始化
         */
        initList(1);
    });
});