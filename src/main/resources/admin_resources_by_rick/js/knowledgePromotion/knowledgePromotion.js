/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();//每个页面都有
        form.render();//每个页面都有
        laydate.render({
            elem: '#releaseTime'
            ,type: 'datetime'
            ,theme: '#588fd0'
        });
        /**
         * 页面布局使用
         * 数据表格中计算高度部分的固定高度
         */
        var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
        /**
         * 全局变量
         */
        var modelIndex = null; //标记模态框
        var msgTpl = UM.getEditor('msgTpl'); //编辑器控件
        msgTpl.ready(function () { //初始化编辑器
            msgTpl.execCommand('cleardoc');
        });

        /****************************************静态数据开始****************************************/
        table.render({
            id : "dataTable",
            elem : "#dataTable",
            limit : config.pageSize,
            height : "full-" + fixedHeight,//全屏高度减去计算出的高度
            cols : [[
                {align : "center",field : "a",title : "知识类型"},
                {align : "center",field : "b",title : "信息模板"},
                {align : "center",field : "c",title : "发布时间"},
                {align : "center",title : "操作",width : 120,toolbar : "#dataTableToolBarTpl",fixed: 'right'}
            ]],
            data : [
                {
                    a : '融资产品',
                    b : '尊敬的用户您好，你的融资申请（受理号427154）已通过，请于xx月xx号前往xx网点办理。联系人xxx,联系方式xxx.',
                    c : '2018/10/18'
                }
            ],
            size : "sm"
        });
        laypage.render({
            elem: "dataPage",
            count: 12,
            limit: config.pageSize,
            curr: 1,
            prev: "<i class='layui-icon'>&#xe603;</i>",
            next: "<i class='layui-icon'>&#xe602;</i>",
            layout: ["prev", "page", "next", "skip", "count"],
            theme: "#588fd0",
            jump: function(obj,first){
                if(!first){

                }
            }
        });

        /**
         * 点击新增按钮
         */
        $("#addBtn").on("click",function () {
            modelIndex = layer.open({
                type: 1,
                title: "新增",
                shadeClose: false,
                shade: [0.3, '#000'],
                area: ['100%', '100%'],
                anim: 2,
                resize: false,
                content: $('#editModel')
            });
        });

        /**
         * 工具栏操作
         */
        table.on('tool(dataTable)', function(obj){
            var data = obj.data;
            var layEvent = obj.event;
            if (layEvent === 'edit'){ //编辑
                modelIndex = layer.open({
                    type: 1,
                    title: "编辑",
                    shadeClose: false,
                    shade: [0.3, '#000'],
                    area: ['100%', '100%'],
                    anim: 2,
                    resize: false,
                    content: $('#editModel')
                });
            }else if(layEvent === 'del'){
                layer.confirm('确认删除', {anim: 6}, function(index){
                    layer.close(index);
                });
            }
        });

        /**
         * 点击保存按钮
         */
        form.on('submit(saveModelBtn)', function(data){
            layer.close(modelIndex);
        });

        /**
         * 取消模态框按钮
         */
        $("#cancelModelBtn").on("click",function () {
            layer.close(modelIndex);
        });

        /****************************************静态数据结束****************************************/

    });
});