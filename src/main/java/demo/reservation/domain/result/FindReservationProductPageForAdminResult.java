package demo.reservation.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.vo.ReservationProductVO;

public class FindReservationProductPageForAdminResult extends CommonResult {

	private List<ReservationProductVO> reservationList;
	private Integer resultCount;

	public List<ReservationProductVO> getReservationList() {
		return reservationList;
	}

	public void setReservationList(List<ReservationProductVO> reservationList) {
		this.reservationList = reservationList;
	}

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	@Override
	public String toString() {
		return "FindReservationPageForAdminResult [reservationList=" + reservationList + "]";
	}

}
