package demo.reservation.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.vo.ReservationsBusinessVO;

public class FindReservationBusinessPageForAdminResult extends CommonResult {

	private List<ReservationsBusinessVO> reservationList;
	private Integer resultCount;

	public List<ReservationsBusinessVO> getReservationList() {
		return reservationList;
	}

	public void setReservationList(List<ReservationsBusinessVO> reservationList) {
		this.reservationList = reservationList;
	}

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	@Override
	public String toString() {
		return "FindReservationPageForAdminResult [reservationList=" + reservationList + "]";
	}

}
