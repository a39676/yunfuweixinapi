package demo.reservation.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.bo.ReservationsBusinessBO;

public class FindReservationBusinessByUserIdResult extends CommonResult {

	List<ReservationsBusinessBO> reservationList;

	public List<ReservationsBusinessBO> getReservationList() {
		return reservationList;
	}

	public void setReservationList(List<ReservationsBusinessBO> reservationList) {
		this.reservationList = reservationList;
	}

	@Override
	public String toString() {
		return "FindReservationBusinessByUserIdResult [reservationList=" + reservationList + "]";
	}

}
