package demo.reservation.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.vo.ReservationStatisticsVO;

public class GetStatisticsForHomePageResult extends CommonResult {

	List<ReservationStatisticsVO> statisticsResultList;

	public List<ReservationStatisticsVO> getStatisticsResultList() {
		return statisticsResultList;
	}

	public void setStatisticsResultList(List<ReservationStatisticsVO> statisticsResultList) {
		this.statisticsResultList = statisticsResultList;
	}

}
