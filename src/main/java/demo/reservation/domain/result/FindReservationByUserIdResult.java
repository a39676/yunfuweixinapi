package demo.reservation.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.vo.ReservationResultVO;

public class FindReservationByUserIdResult extends CommonResult {

	private List<ReservationResultVO> reservationResultList;

	public List<ReservationResultVO> getReservationResultList() {
		return reservationResultList;
	}

	public void setReservationResultList(List<ReservationResultVO> reservationResultList) {
		this.reservationResultList = reservationResultList;
	}

}
