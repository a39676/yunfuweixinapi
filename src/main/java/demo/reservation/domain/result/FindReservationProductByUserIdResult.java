package demo.reservation.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.bo.ReservationsProductBO;

public class FindReservationProductByUserIdResult extends CommonResult {

	List<ReservationsProductBO> reservationList;

	public List<ReservationsProductBO> getReservationList() {
		return reservationList;
	}

	public void setReservationList(List<ReservationsProductBO> reservationList) {
		this.reservationList = reservationList;
	}

}
