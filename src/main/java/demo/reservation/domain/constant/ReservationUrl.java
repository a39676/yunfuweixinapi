package demo.reservation.domain.constant;

public class ReservationUrl {

	public static final String root = "/reservation";
	
	public static final String applicationReservationsProduct = "/applicationReservationsProduct";
	public static final String acceptReservationProduct = "/acceptReservationProduct";
	public static final String rejectReservationProduct = "/rejectReservationProduct";
	public static final String applicationExchangeDib = "/applicationExchangeDib";
	
	public static final String applicationReservationsExchangeDib = "/applicationReservationsExchangeDib";
	
	public static final String findReservationByUserId = "/findReservationByUserId";
	
	public static final String findReservationPageForAdmin = "/findReservationPageForAdmin";
	
	public static final String findReservationExchangeDibPageForAdmin = "/findReservationExchangeDibPageForAdmin";
	
	public static final String acceptReservationBusiness = "/acceptReservationBusiness";
	public static final String rejectReservationBusiness = "/rejectReservationBusiness";
	
	public static final String getStatisticsForHomePage = "/getStatisticsForHomePage";
}
