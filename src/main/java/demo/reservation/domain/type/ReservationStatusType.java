package demo.reservation.domain.type;

/**
 * 预约状态类型	 
 */
public enum ReservationStatusType {
	
	/** 申请中 */
	application("申请中", 1), 
	/** 已受理 */
	accepted("已受理", 2), 
	/** 已拒绝 */
	reject("已拒绝", 3), 
	/** 已履约 */
	wasDone("已履约", 4), 
	/** 已爽约 */
	wasRuin("已爽约", 5), 
	;
	
	private String typeName;
	private Integer typeCode;
	
	ReservationStatusType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static ReservationStatusType getType(String typeName) {
		for(ReservationStatusType t : ReservationStatusType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static ReservationStatusType getType(Integer typeCode) {
		for(ReservationStatusType t : ReservationStatusType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
