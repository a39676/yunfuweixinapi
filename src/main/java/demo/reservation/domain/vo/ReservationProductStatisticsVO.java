package demo.reservation.domain.vo;

public class ReservationProductStatisticsVO {

	private Integer productType;
	private Integer count;

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "ReservationProductStatisticsVO [productType=" + productType + ", count=" + count + "]";
	}

}
