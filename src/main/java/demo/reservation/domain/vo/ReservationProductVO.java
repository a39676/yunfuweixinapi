package demo.reservation.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

public class ReservationProductVO {

	private Long id;
	private String nickName;
	private String userName;
	private String interestType;
	private BigDecimal interestRate;
	private Date expirationTime;
	private String productName;
	private String productCode;
	private String productTypeName;
	private String orgName;
	private Long orgId;
	private String staffName;
	private String reservationStatuString;
	private Integer reservationStatu;
	private Date receptionTime;
	private String receptionTimeString;
	private String remark;
	private BigDecimal amount;
	private Date reservationTimeStart;
	private Date reservationTimeEnd;
	private String reservationTimeStartString;
	private String reservationTimeEndString;

	public String getReservationTimeStartString() {
		return reservationTimeStartString;
	}

	public void setReservationTimeStartString(String reservationTimeStartString) {
		this.reservationTimeStartString = reservationTimeStartString;
	}

	public String getReservationTimeEndString() {
		return reservationTimeEndString;
	}

	public void setReservationTimeEndString(String reservationTimeEndString) {
		this.reservationTimeEndString = reservationTimeEndString;
	}

	public Date getReservationTimeStart() {
		return reservationTimeStart;
	}

	public void setReservationTimeStart(Date reservationTimeStart) {
		this.reservationTimeStart = reservationTimeStart;
	}

	public Date getReservationTimeEnd() {
		return reservationTimeEnd;
	}

	public void setReservationTimeEnd(Date reservationTimeEnd) {
		this.reservationTimeEnd = reservationTimeEnd;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getInterestType() {
		return interestType;
	}

	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getReservationStatuString() {
		return reservationStatuString;
	}

	public void setReservationStatuString(String reservationStatuString) {
		this.reservationStatuString = reservationStatuString;
	}

	public Integer getReservationStatu() {
		return reservationStatu;
	}

	public void setReservationStatu(Integer reservationStatu) {
		this.reservationStatu = reservationStatu;
	}

	public Date getReceptionTime() {
		return receptionTime;
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

	public String getReceptionTimeString() {
		return receptionTimeString;
	}

	public void setReceptionTimeString(String receptionTimeString) {
		this.receptionTimeString = receptionTimeString;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "ReservationProductVO [id=" + id + ", nickName=" + nickName + ", userName=" + userName
				+ ", interestType=" + interestType + ", interestRate=" + interestRate + ", expirationTime="
				+ expirationTime + ", productName=" + productName + ", productCode=" + productCode
				+ ", productTypeName=" + productTypeName + ", orgName=" + orgName + ", orgId=" + orgId + ", staffName="
				+ staffName + ", reservationStatuString=" + reservationStatuString + ", reservationStatu="
				+ reservationStatu + ", receptionTime=" + receptionTime + ", receptionTimeString=" + receptionTimeString
				+ ", remark=" + remark + ", amount=" + amount + ", reservationTimeStart=" + reservationTimeStart
				+ ", reservationTimeEnd=" + reservationTimeEnd + ", reservationTimeStartString="
				+ reservationTimeStartString + ", reservationTimeEndString=" + reservationTimeEndString + "]";
	}

}
