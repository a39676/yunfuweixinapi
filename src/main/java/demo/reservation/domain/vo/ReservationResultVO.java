package demo.reservation.domain.vo;

public class ReservationResultVO {

	private Long id;
	private String reservationStatus;
	private String orgName;
	private String reservationTime;
	private String staffName;
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getReservationTime() {
		return reservationTime;
	}

	public void setReservationTime(String reservationTime) {
		this.reservationTime = reservationTime;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "ReservationResultVO [id=" + id + ", reservationStatus=" + reservationStatus + ", orgName=" + orgName
				+ ", reservationTime=" + reservationTime + ", staffName=" + staffName + ", remark=" + remark + "]";
	}

}
