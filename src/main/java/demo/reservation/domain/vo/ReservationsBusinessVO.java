package demo.reservation.domain.vo;

import java.util.Date;

public class ReservationsBusinessVO {

	private Long id;
	private Date createTime;
	private Date reservationTimeStart;
	private Date reservationTimeEnd;
	private String reservationTimeStartString;
	private String reservationTimeEndString;
	private Integer reservationStatus;
	private String reservationStatusString;
	private Long userId;
	private Long staffId;
	private Long orgId;
	private Long businessId;
	private String remark;
	private String staffName;
	private String orgName;
	private String nickName;
	private String userName;
	private String businessContent;
	private String moneyType;
	private Integer coin01;
	private Integer coin05;
	private Integer coin1;
	private Integer paper1;
	private Integer paper5;
	private Integer paper10;
	private Integer paper20;
	private Integer paper50;
	private Integer paper100;
	private Date receptionTime;
	private String receptionTimeString;

	public String getReceptionTimeString() {
		return receptionTimeString;
	}

	public void setReceptionTimeString(String receptionTimeString) {
		this.receptionTimeString = receptionTimeString;
	}

	public Date getReceptionTime() {
		return receptionTime;
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

	public String getReservationStatusString() {
		return reservationStatusString;
	}

	public void setReservationStatusString(String reservationStatusString) {
		this.reservationStatusString = reservationStatusString;
	}

	public String getBusinessContent() {
		return businessContent;
	}

	public void setBusinessContent(String businessContent) {
		this.businessContent = businessContent;
	}

	public String getMoneyType() {
		return moneyType;
	}

	public void setMoneyType(String moneyType) {
		this.moneyType = moneyType;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserName() {
		return userName;
	}

	public String getReservationTimeStartString() {
		return reservationTimeStartString;
	}

	public void setReservationTimeStartString(String reservationTimeStartString) {
		this.reservationTimeStartString = reservationTimeStartString;
	}

	public String getReservationTimeEndString() {
		return reservationTimeEndString;
	}

	public void setReservationTimeEndString(String reservationTimeEndString) {
		this.reservationTimeEndString = reservationTimeEndString;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getCoin01() {
		return coin01;
	}

	public void setCoin01(Integer coin01) {
		this.coin01 = coin01;
	}

	public Integer getCoin05() {
		return coin05;
	}

	public void setCoin05(Integer coin05) {
		this.coin05 = coin05;
	}

	public Integer getCoin1() {
		return coin1;
	}

	public void setCoin1(Integer coin1) {
		this.coin1 = coin1;
	}

	public Integer getPaper1() {
		return paper1;
	}

	public void setPaper1(Integer paper1) {
		this.paper1 = paper1;
	}

	public Integer getPaper5() {
		return paper5;
	}

	public void setPaper5(Integer paper5) {
		this.paper5 = paper5;
	}

	public Integer getPaper10() {
		return paper10;
	}

	public void setPaper10(Integer paper10) {
		this.paper10 = paper10;
	}

	public Integer getPaper20() {
		return paper20;
	}

	public void setPaper20(Integer paper20) {
		this.paper20 = paper20;
	}

	public Integer getPaper50() {
		return paper50;
	}

	public void setPaper50(Integer paper50) {
		this.paper50 = paper50;
	}

	public Integer getPaper100() {
		return paper100;
	}

	public void setPaper100(Integer paper100) {
		this.paper100 = paper100;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReservationTimeStart() {
		return reservationTimeStart;
	}

	public void setReservationTimeStart(Date reservationTimeStart) {
		this.reservationTimeStart = reservationTimeStart;
	}

	public Date getReservationTimeEnd() {
		return reservationTimeEnd;
	}

	public void setReservationTimeEnd(Date reservationTimeEnd) {
		this.reservationTimeEnd = reservationTimeEnd;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "ReservationsBusinessVO [id=" + id + ", createTime=" + createTime + ", reservationTimeStart="
				+ reservationTimeStart + ", reservationTimeEnd=" + reservationTimeEnd + ", reservationTimeStartString="
				+ reservationTimeStartString + ", reservationTimeEndString=" + reservationTimeEndString
				+ ", reservationStatus=" + reservationStatus + ", reservationStatusString=" + reservationStatusString
				+ ", userId=" + userId + ", staffId=" + staffId + ", orgId=" + orgId + ", businessId=" + businessId
				+ ", remark=" + remark + ", staffName=" + staffName + ", orgName=" + orgName + ", nickName=" + nickName
				+ ", userName=" + userName + ", businessContent=" + businessContent + ", moneyType=" + moneyType
				+ ", coin01=" + coin01 + ", coin05=" + coin05 + ", coin1=" + coin1 + ", paper1=" + paper1 + ", paper5="
				+ paper5 + ", paper10=" + paper10 + ", paper20=" + paper20 + ", paper50=" + paper50 + ", paper100="
				+ paper100 + ", receptionTime=" + receptionTime + ", receptionTimeString=" + receptionTimeString + "]";
	}

}