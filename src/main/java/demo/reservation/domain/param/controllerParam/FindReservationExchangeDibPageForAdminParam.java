package demo.reservation.domain.param.controllerParam;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import dateHandle.DateUtilCustom;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindReservationExchangeDibPageForAdminParam implements CommonControllerParam {

	private Long orgId;
	private Long id;
	private Date receptionTime;
	private String staffName;
	private String mobile;
	private Integer pageNo;
	private Integer pageSize;
	private Integer productType;
	private Integer productSubType;

	@Override
	public FindReservationExchangeDibPageForAdminParam fromJson(JSONObject j) {
		FindReservationExchangeDibPageForAdminParam p = new FindReservationExchangeDibPageForAdminParam();
		if (j.containsKey("orgId") && NumericUtilCustom.matchInteger(j.getString("orgId"))) {
			p.setOrgId(j.getLong("orgId"));
		}
		if (j.containsKey("id") && NumericUtilCustom.matchInteger(j.getString("id"))) {
			p.setId(j.getLong("id"));
		}
		if (j.containsKey("receptionTime") && DateUtilCustom.isDateValid(j.getString("receptionTime"))) {
			p.setReceptionTime(DateUtilCustom.stringToDateUnkonwFormat(j.getString("receptionTime")));
		}
		if (j.containsKey(staffName) && StringUtils.isNotBlank(j.getString("staffName"))) {
			p.setStaffName(j.getString("staffName"));
		}
		if (j.containsKey(mobile) && StringUtils.isNotBlank(j.getString("mobile"))) {
			p.setMobile(j.getString("mobile"));
		}
		if (j.containsKey("pageNo") && NumericUtilCustom.matchInteger(j.getString("pageNo"))) {
			p.setPageNo(j.getInt("pageNo"));
		}
		if (j.containsKey("pageSize") && NumericUtilCustom.matchInteger(j.getString("pageSize"))) {
			p.setPageSize(j.getInt("pageSize"));
		}
		if (j.containsKey("productType") && NumericUtilCustom.matchInteger(j.getString("productType"))) {
			p.setProductType(j.getInt("productType"));
		}
		if (j.containsKey("productSubType") && NumericUtilCustom.matchInteger(j.getString("productSubType"))) {
			p.setProductSubType(j.getInt("productSubType"));
		}
		return p;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStaffName() {
		return staffName;
	}

	public Date getReceptionTime() {
		return receptionTime;
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	@Override
	public String toString() {
		return "FindReservationPageForAdminParam [orgId=" + orgId + ", id=" + id + ", receptionTime=" + receptionTime
				+ ", staffName=" + staffName + ", mobile=" + mobile + ", pageNo=" + pageNo + ", pageSize=" + pageSize
				+ ", productType=" + productType + ", productSubType=" + productSubType + "]";
	}

}
