package demo.reservation.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class AcceptReservationBusinessParam implements CommonControllerParam {

	private Long staffId;
	private Long reservationId;

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	@Override
	public AcceptReservationBusinessParam fromJson(JSONObject json) {
		AcceptReservationBusinessParam param = new AcceptReservationBusinessParam();
		if (json.containsKey("staffId") && NumericUtilCustom.matchInteger(json.getString("staffId"))) {
			param.setStaffId(json.getLong("staffId"));
		}
		if (json.containsKey("reservationId") && NumericUtilCustom.matchInteger(json.getString("reservationId"))) {
			param.setReservationId(json.getLong("reservationId"));
		}
		return param;
	}

}
