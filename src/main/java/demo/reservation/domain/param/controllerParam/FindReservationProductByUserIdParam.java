package demo.reservation.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import demo.reservation.domain.type.ReservationStatusType;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindReservationProductByUserIdParam implements CommonControllerParam {

	/** {@link ReservationStatusType}} */
	private Integer reservationStatus;

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	@Override
	public String toString() {
		return "FindReservationProductByUserIdParam [reservationStatus=" + reservationStatus + "]";
	}

	@Override
	public FindReservationProductByUserIdParam fromJson(JSONObject json) {
		FindReservationProductByUserIdParam param = new FindReservationProductByUserIdParam();
		if (json.containsKey("reservationStatus")
				&& NumericUtilCustom.matchInteger(json.getString("reservationStatus"))) {
			param.setReservationStatus(json.getInt("reservationStatus"));
		}
		return param;
	}

}
