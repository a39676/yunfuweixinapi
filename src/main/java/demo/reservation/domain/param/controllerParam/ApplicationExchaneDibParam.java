package demo.reservation.domain.param.controllerParam;

import java.util.Date;

import dateHandle.DateUtilCustom;
import demo.business.domain.type.MoneyType;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

/** 申请预约零钞兑换 */
public class ApplicationExchaneDibParam implements CommonControllerParam {

	private Date reservationsStartTime;

	private Date reservationsEndTime;

	private Long orgId;

	/** {@link MoneyType} */
	private Integer moneyType;

	private Integer coin01Used = 0;
	private Integer coin05Used = 0;
	private Integer coin1Used = 0;
	private Integer paper1Used = 0;
	private Integer paper5Used = 0;
	private Integer paper10Used = 0;
	private Integer paper20Used = 0;
	private Integer paper50Used = 0;
	private Integer paper100Used = 0;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Date getReservationsStartTime() {
		return reservationsStartTime;
	}

	public void setReservationsStartTime(Date reservationsStartTime) {
		this.reservationsStartTime = reservationsStartTime;
	}

	public Date getReservationsEndTime() {
		return reservationsEndTime;
	}

	public void setReservationsEndTime(Date reservationsEndTime) {
		this.reservationsEndTime = reservationsEndTime;
	}

	public Integer getMoneyType() {
		return moneyType;
	}

	public void setMoneyType(Integer moneyType) {
		this.moneyType = moneyType;
	}

	public Integer getCoin01Used() {
		return coin01Used;
	}

	public void setCoin01Used(Integer coin01Used) {
		this.coin01Used = coin01Used;
	}

	public Integer getCoin05Used() {
		return coin05Used;
	}

	public void setCoin05Used(Integer coin05Used) {
		this.coin05Used = coin05Used;
	}

	public Integer getCoin1Used() {
		return coin1Used;
	}

	public void setCoin1Used(Integer coin1Used) {
		this.coin1Used = coin1Used;
	}

	public Integer getPaper1Used() {
		return paper1Used;
	}

	public void setPaper1Used(Integer paper1Used) {
		this.paper1Used = paper1Used;
	}

	public Integer getPaper5Used() {
		return paper5Used;
	}

	public void setPaper5Used(Integer paper5Used) {
		this.paper5Used = paper5Used;
	}

	public Integer getPaper10Used() {
		return paper10Used;
	}

	public void setPaper10Used(Integer paper10Used) {
		this.paper10Used = paper10Used;
	}

	public Integer getPaper20Used() {
		return paper20Used;
	}

	public void setPaper20Used(Integer paper20Used) {
		this.paper20Used = paper20Used;
	}

	public Integer getPaper50Used() {
		return paper50Used;
	}

	public void setPaper50Used(Integer paper50Used) {
		this.paper50Used = paper50Used;
	}

	public Integer getPaper100Used() {
		return paper100Used;
	}

	public void setPaper100Used(Integer paper100Used) {
		this.paper100Used = paper100Used;
	}

	@Override
	public ApplicationExchaneDibParam fromJson(JSONObject json) {
		ApplicationExchaneDibParam param = new ApplicationExchaneDibParam();
		if(json.containsKey("reservationsStartTime") && DateUtilCustom.isDateValid(json.getString("reservationsStartTime"))) {
			param.setReservationsStartTime(DateUtilCustom.stringToDateUnkonwFormat(json.getString("reservationsStartTime")));
		}
		if(json.containsKey("reservationsEndTime") && DateUtilCustom.isDateValid(json.getString("reservationsEndTime"))) {
			param.setReservationsEndTime(DateUtilCustom.stringToDateUnkonwFormat(json.getString("reservationsEndTime")));
		}
		if(json.containsKey("orgId") && NumericUtilCustom.matchInteger(json.getString("orgId"))) {
			param.setOrgId(json.getLong("orgId"));
		}
		if(json.containsKey("moneyType") && NumericUtilCustom.matchInteger(json.getString("moneyType"))) {
			param.setMoneyType(json.getInt("moneyType"));
		}
		if(json.containsKey("coin01Used") && NumericUtilCustom.matchInteger(json.getString("coin01Used"))) {
			param.setCoin01Used(json.getInt("coin01Used"));
			if (param.getCoin01Used() > 100) {
				param.setCoin01Used(100);
			}
		}
		if(json.containsKey("coin05Used") && NumericUtilCustom.matchInteger(json.getString("coin05Used"))) {
			param.setCoin05Used(json.getInt("coin05Used"));
			if (param.getCoin05Used() > 100) {
				param.setCoin05Used(100);
			}
		}
		if(json.containsKey("coin1Used") && NumericUtilCustom.matchInteger(json.getString("coin1Used"))) {
			param.setCoin1Used(json.getInt("coin1Used"));
			if (param.getCoin1Used() > 100) {
				param.setCoin1Used(100);
			}
		}
		if(json.containsKey("paper1Used") && NumericUtilCustom.matchInteger(json.getString("paper1Used"))) {
			param.setPaper1Used(json.getInt("paper1Used"));
			if (param.getPaper1Used() > 100) {
				param.setPaper1Used(100);
			}
		}
		if(json.containsKey("paper5Used") && NumericUtilCustom.matchInteger(json.getString("paper5Used"))) {
			param.setPaper5Used(json.getInt("paper5Used"));
			if (param.getPaper5Used() > 100) {
				param.setPaper5Used(100);
			}
		}
		if(json.containsKey("paper10Used") && NumericUtilCustom.matchInteger(json.getString("paper10Used"))) {
			param.setPaper10Used(json.getInt("paper10Used"));
			if (param.getPaper10Used() > 100) {
				param.setPaper10Used(100);
			}
		}
		if(json.containsKey("paper20Used") && NumericUtilCustom.matchInteger(json.getString("paper20Used"))) {
			param.setPaper20Used(json.getInt("paper20Used"));
			if (param.getPaper20Used() > 100) {
				param.setPaper20Used(100);
			}
		}
		if(json.containsKey("paper50Used") && NumericUtilCustom.matchInteger(json.getString("paper50Used"))) {
			param.setPaper50Used(json.getInt("paper50Used"));
			if (param.getPaper50Used() > 100) {
				param.setPaper50Used(100);
			}
		}
		if(json.containsKey("paper100Used") && NumericUtilCustom.matchInteger(json.getString("paper100Used"))) {
			param.setPaper100Used(json.getInt("paper100Used"));
			if (param.getPaper100Used() > 100) {
				param.setPaper100Used(100);
			}
		}
		
		return param;
	}

}
