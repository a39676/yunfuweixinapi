package demo.reservation.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class RejectReservationProductParam implements CommonControllerParam {

	private Long reservationId;
	private String remark;

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "RejectReservationProductParam [reservationId=" + reservationId + ", remark=" + remark + "]";
	}

	@Override
	public RejectReservationProductParam fromJson(JSONObject json) {
		RejectReservationProductParam param = new RejectReservationProductParam();
		if (json.containsKey("remark")) {
			param.setRemark(json.getString("remark"));
		}
		if (json.containsKey("reservationId") && NumericUtilCustom.matchInteger(json.getString("reservationId"))) {
			param.setReservationId(json.getLong("reservationId"));
		}
		return param;
	}

}
