package demo.reservation.domain.param.controllerParam;

import java.util.Date;

import demo.common.domain.param.CommonControllerParam;

public abstract class ApplicationReservationsCommonParam implements CommonControllerParam {
	
	private Date reservationsStartTime;

	private Date reservationsEndTime;

	private Long orgId;
	
	private Long productId;
	
	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Date getReservationsStartTime() {
		return reservationsStartTime;
	}

	public void setReservationsStartTime(Date reservationsStartTime) {
		this.reservationsStartTime = reservationsStartTime;
	}

	public Date getReservationsEndTime() {
		return reservationsEndTime;
	}

	public void setReservationsEndTime(Date reservationsEndTime) {
		this.reservationsEndTime = reservationsEndTime;
	}

}
