package demo.reservation.domain.param.controllerParam;

import dateHandle.DateUtilCustom;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class ApplicationReservationsProductParam extends ApplicationReservationsCommonParam {

	@Override
	public ApplicationReservationsProductParam fromJson(JSONObject json) {
		ApplicationReservationsProductParam param = new ApplicationReservationsProductParam();
		if (json.containsKey("reservationsStartTime") && DateUtilCustom.isDateValid(json.getString("reservationsStartTime"))) {
			param.setReservationsStartTime(DateUtilCustom.stringToDateUnkonwFormat(json.getString("reservationsStartTime")));
		}
		if (json.containsKey("reservationsEndTime") && DateUtilCustom.isDateValid(json.getString("reservationsEndTime"))) {
			param.setReservationsEndTime(DateUtilCustom.stringToDateUnkonwFormat(json.getString("reservationsEndTime")));
		}
		if (json.containsKey("orgId") && NumericUtilCustom.matchInteger(json.getString("orgId"))) {
			param.setOrgId(json.getLong("orgId"));
		}
		if (json.containsKey("productId") && NumericUtilCustom.matchInteger(json.getString("productId"))) {
			param.setProductId(json.getLong("productId"));
		}
		return param;
	}

}
