package demo.reservation.domain.param.mapperParam;

public class GetStatisticsBusinessForHomePageParam {

	private Long topHeadOrg;
	private Long orgId;
	private Integer reservationStatus;

	public Long getTopHeadOrg() {
		return topHeadOrg;
	}

	public void setTopHeadOrg(Long topHeadOrg) {
		this.topHeadOrg = topHeadOrg;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	@Override
	public String toString() {
		return "GetStatisticsBusinessForHomePageParam [topHeadOrg=" + topHeadOrg + ", orgId=" + orgId
				+ ", reservationStatus=" + reservationStatus + "]";
	}

}
