package demo.reservation.domain.param.mapperParam;

import java.util.Date;
import java.util.List;

public class FindReservationPageForAdmin {

	private List<Long> orgIdList;
	private Long orgId;
	private Long id;
	private Date receptionTimeStart;
	private Date receptionTimeEnd;
	private String staffName;
	private String mobile;
	private Integer pageStart;
	private Integer pageSize;
	private Integer productType;
	private Integer productSubType;
	
	

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProdutrSubType(Integer producrSubType) {
		this.productSubType = producrSubType;
	}

	public List<Long> getOrgIdList() {
		return orgIdList;
	}

	public void setOrgIdList(List<Long> orgIdList) {
		this.orgIdList = orgIdList;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getReceptionTimeStart() {
		return receptionTimeStart;
	}

	public void setReceptionTimeStart(Date receptionTimeStart) {
		this.receptionTimeStart = receptionTimeStart;
	}

	public Date getReceptionTimeEnd() {
		return receptionTimeEnd;
	}

	public void setReceptionTimeEnd(Date receptionTimeEnd) {
		this.receptionTimeEnd = receptionTimeEnd;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getPageStart() {
		return pageStart;
	}

	public void setPageStart(Integer pageStart) {
		this.pageStart = pageStart;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "FindReservationPageForAdmin [orgIdList=" + orgIdList + ", orgId=" + orgId + ", id=" + id
				+ ", receptionTimeStart=" + receptionTimeStart + ", receptionTimeEnd=" + receptionTimeEnd
				+ ", staffName=" + staffName + ", mobile=" + mobile + ", pageStart=" + pageStart + ", pageSize="
				+ pageSize + ", productType=" + productType + ", productSubType=" + productSubType + "]";
	}

}
