package demo.reservation.domain.param.mapperParam;

public class GetStatisticsProductForHomePageParam {

	private Long orgId;
	private Long topHeadOrg;
	private Integer reservationStatus;

	@Override
	public String toString() {
		return "GetStatisticsForHomePageParam [orgId=" + orgId + ", topHeadOrg=" + topHeadOrg + ", reservationStatus="
				+ reservationStatus + "]";
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getTopHeadOrg() {
		return topHeadOrg;
	}

	public void setTopHeadOrg(Long topHeadOrg) {
		this.topHeadOrg = topHeadOrg;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

}
