package demo.reservation.domain.param.mapperParam;

import java.util.Date;

import demo.reservation.domain.type.ReservationStatusType;

public class FindReservationProductByUserId {

	private Long userId;
	/** {@link ReservationStatusType}} */
	private Integer reservationStatus;
	private Date reservationTimeEnd;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Date getReservationTimeEnd() {
		return reservationTimeEnd;
	}

	public void setReservationTimeEnd(Date reservationTimeEnd) {
		this.reservationTimeEnd = reservationTimeEnd;
	}

	@Override
	public String toString() {
		return "FindReservationProductByUserId [userId=" + userId + ", reservationStatus=" + reservationStatus + "]";
	}

}
