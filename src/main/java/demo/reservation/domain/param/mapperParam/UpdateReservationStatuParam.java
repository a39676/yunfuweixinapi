package demo.reservation.domain.param.mapperParam;

public class UpdateReservationStatuParam {

	private Long reservationId;
	private Integer reservationStatu;
	private Long staffId;
	private String remark;

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getReservationStatu() {
		return reservationStatu;
	}

	public void setReservationStatu(Integer reservationStatu) {
		this.reservationStatu = reservationStatu;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	@Override
	public String toString() {
		return "UpdateReservationStatuParam [reservationId=" + reservationId + ", reservationStatu=" + reservationStatu
				+ ", staffId=" + staffId + ", remark=" + remark + "]";
	}

}
