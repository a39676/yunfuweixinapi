package demo.reservation.domain.po;

import java.util.Date;

public class ReservationsBusinessPO {
	private Long id;

	private Date createTime;

	private Date reservationTimeStart;

	private Date reservationTimeEnd;

	private Integer reservationStatus;

	private Long userId;

	private Long staffId;

	private Long orgId;

	private Long businessId;

	private String remark;

	private Date receptionTime;

	@Override
	public String toString() {
		return "ReservationsBusinessPO [id=" + id + ", createTime=" + createTime + ", reservationTimeStart="
				+ reservationTimeStart + ", reservationTimeEnd=" + reservationTimeEnd + ", reservationStatus="
				+ reservationStatus + ", userId=" + userId + ", staffId=" + staffId + ", orgId=" + orgId
				+ ", businessId=" + businessId + ", remark=" + remark + ", receptionTime=" + receptionTime + "]";
	}

	public Date getReceptionTime() {
		return receptionTime;
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReservationTimeStart() {
		return reservationTimeStart;
	}

	public void setReservationTimeStart(Date reservationTimeStart) {
		this.reservationTimeStart = reservationTimeStart;
	}

	public Date getReservationTimeEnd() {
		return reservationTimeEnd;
	}

	public void setReservationTimeEnd(Date reservationTimeEnd) {
		this.reservationTimeEnd = reservationTimeEnd;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
}