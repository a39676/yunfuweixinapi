package demo.reservation.domain.po;

import java.util.Date;

import demo.reservation.domain.type.ReservationStatusType;

public class ReservationProductPO {
	private Long id;

	private Date createTime;

	private Date reservationTimeStart;

	private Date reservationTimeEnd;

	/** {@link ReservationStatusType} */
	private Integer reservationStatus;

	private Long userId;

	private Long staffId;

	private Long orgId;

	private Long productId;

	private String remark;

	private Date receptionTime;

	public Date getReceptionTime() {
		return receptionTime;
	}

	@Override
	public String toString() {
		return "ReservationProductPO [id=" + id + ", createTime=" + createTime + ", reservationTimeStart="
				+ reservationTimeStart + ", reservationTimeEnd=" + reservationTimeEnd + ", reservationStatus="
				+ reservationStatus + ", userId=" + userId + ", staffId=" + staffId + ", orgId=" + orgId
				+ ", productId=" + productId + ", remark=" + remark + ", receptionTime=" + receptionTime + "]";
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReservationTimeStart() {
		return reservationTimeStart;
	}

	public void setReservationTimeStart(Date reservationTimeStart) {
		this.reservationTimeStart = reservationTimeStart;
	}

	public Date getReservationTimeEnd() {
		return reservationTimeEnd;
	}

	public void setReservationTimeEnd(Date reservationTimeEnd) {
		this.reservationTimeEnd = reservationTimeEnd;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

}