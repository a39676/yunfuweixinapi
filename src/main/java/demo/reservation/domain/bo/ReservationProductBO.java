package demo.reservation.domain.bo;

import java.math.BigDecimal;
import java.util.Date;

public class ReservationProductBO {

	private Long id;
	private String nickName;
	private String userName;
	private Integer interestType;
	private BigDecimal interestRate;
	private Date expirationTime;
	private String productName;
	private String productCode;
	private Integer productType;
	private Integer productSubType;
	private String orgName;
	private Long orgId;
	private String staffName;
	private Integer reservationStatus;
	private Date receptionTime;
	private String remark;
	private Date reservationTimeStart;
	private Date reservationTimeEnd;

	
	
	public Date getReservationTimeStart() {
		return reservationTimeStart;
	}

	public void setReservationTimeStart(Date reservationTimeStart) {
		this.reservationTimeStart = reservationTimeStart;
	}

	public Date getReservationTimeEnd() {
		return reservationTimeEnd;
	}

	public void setReservationTimeEnd(Date reservationTimeEnd) {
		this.reservationTimeEnd = reservationTimeEnd;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getInterestType() {
		return interestType;
	}

	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Date getReceptionTime() {
		return receptionTime;
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "ReservationProductBO [id=" + id + ", nickName=" + nickName + ", userName=" + userName
				+ ", interestType=" + interestType + ", interestRate=" + interestRate + ", expirationTime="
				+ expirationTime + ", productName=" + productName + ", productCode=" + productCode + ", productType="
				+ productType + ", productSubType=" + productSubType + ", orgName=" + orgName + ", orgId=" + orgId
				+ ", staffName=" + staffName + ", reservationStatus=" + reservationStatus + ", receptionTime="
				+ receptionTime + ", remark=" + remark + "]";
	}

}
