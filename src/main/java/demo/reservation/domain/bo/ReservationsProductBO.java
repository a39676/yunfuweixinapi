package demo.reservation.domain.bo;

import demo.reservation.domain.po.ReservationProductPO;

public class ReservationsProductBO extends ReservationProductPO {

	private String staffName;

	private String orgName;

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	@Override
	public String toString() {
		return "ReservationsProductBO [staffName=" + staffName + ", orgName=" + orgName + ", getId()=" + getId()
				+ ", getCreateTime()=" + getCreateTime() + ", getReservationTimeStart()=" + getReservationTimeStart()
				+ ", getReservationTimeEnd()=" + getReservationTimeEnd() + ", getReservationStatus()="
				+ getReservationStatus() + ", getUserId()=" + getUserId() + ", getStaffId()=" + getStaffId()
				+ ", getOrgId()=" + getOrgId() + ", getProductId()=" + getProductId() + ", getRemark()=" + getRemark()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}