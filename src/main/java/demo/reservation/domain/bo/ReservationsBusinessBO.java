package demo.reservation.domain.bo;

import java.util.Date;

import demo.business.domain.type.MoneyType;
import demo.reservation.domain.po.ReservationsBusinessPO;

public class ReservationsBusinessBO extends ReservationsBusinessPO {

	private String staffName;
	private String orgName;
	private String nickName;
	private String userName;
	/** {@link MoneyType} */
	private Integer moneyType;
	private Integer coin01;
	private Integer coin05;
	private Integer coin1;
	private Integer paper1;
	private Integer paper5;
	private Integer paper10;
	private Integer paper20;
	private Integer paper50;
	private Integer paper100;
	private Date reservationTimeStart;
	private Date reservationTimeEnd;

	public Date getReservationTimeStart() {
		return reservationTimeStart;
	}

	public void setReservationTimeStart(Date reservationTimeStart) {
		this.reservationTimeStart = reservationTimeStart;
	}

	public Date getReservationTimeEnd() {
		return reservationTimeEnd;
	}

	public void setReservationTimeEnd(Date reservationTimeEnd) {
		this.reservationTimeEnd = reservationTimeEnd;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getMoneyType() {
		return moneyType;
	}

	public void setMoneyType(Integer moneyType) {
		this.moneyType = moneyType;
	}

	public Integer getCoin01() {
		return coin01;
	}

	public void setCoin01(Integer coin01) {
		this.coin01 = coin01;
	}

	public Integer getCoin05() {
		return coin05;
	}

	public void setCoin05(Integer coin05) {
		this.coin05 = coin05;
	}

	public Integer getCoin1() {
		return coin1;
	}

	public void setCoin1(Integer coin1) {
		this.coin1 = coin1;
	}

	public Integer getPaper1() {
		return paper1;
	}

	public void setPaper1(Integer paper1) {
		this.paper1 = paper1;
	}

	public Integer getPaper5() {
		return paper5;
	}

	public void setPaper5(Integer paper5) {
		this.paper5 = paper5;
	}

	public Integer getPaper10() {
		return paper10;
	}

	public void setPaper10(Integer paper10) {
		this.paper10 = paper10;
	}

	public Integer getPaper20() {
		return paper20;
	}

	public void setPaper20(Integer paper20) {
		this.paper20 = paper20;
	}

	public Integer getPaper50() {
		return paper50;
	}

	public void setPaper50(Integer paper50) {
		this.paper50 = paper50;
	}

	public Integer getPaper100() {
		return paper100;
	}

	public void setPaper100(Integer paper100) {
		this.paper100 = paper100;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	@Override
	public String toString() {
		return "ReservationsBusinessBO [staffName=" + staffName + ", orgName=" + orgName + ", nickName=" + nickName
				+ ", userName=" + userName + ", moneyType=" + moneyType + ", coin01=" + coin01 + ", coin05=" + coin05
				+ ", coin1=" + coin1 + ", paper1=" + paper1 + ", paper5=" + paper5 + ", paper10=" + paper10
				+ ", paper20=" + paper20 + ", paper50=" + paper50 + ", paper100=" + paper100 + ", reservationTimeStart="
				+ reservationTimeStart + ", reservationTimeEnd=" + reservationTimeEnd + "]";
	}

}