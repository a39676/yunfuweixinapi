package demo.reservation.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.service.StaffDetailService;
import demo.reservation.domain.constant.ReservationUrl;
import demo.reservation.domain.param.controllerParam.AcceptReservationBusinessParam;
import demo.reservation.domain.param.controllerParam.AcceptReservationProductParam;
import demo.reservation.domain.param.controllerParam.ApplicationExchaneDibParam;
import demo.reservation.domain.param.controllerParam.ApplicationReservationsProductParam;
import demo.reservation.domain.param.controllerParam.FindReservationByUserIdParam;
import demo.reservation.domain.param.controllerParam.FindReservationExchangeDibPageForAdminParam;
import demo.reservation.domain.param.controllerParam.FindReservationPageForAdminParam;
import demo.reservation.domain.param.controllerParam.RejectReservationBusinessParam;
import demo.reservation.domain.param.controllerParam.RejectReservationProductParam;
import demo.reservation.domain.result.FindReservationBusinessPageForAdminResult;
import demo.reservation.domain.result.FindReservationByUserIdResult;
import demo.reservation.domain.result.FindReservationProductPageForAdminResult;
import demo.reservation.domain.result.GetStatisticsForHomePageResult;
import demo.reservation.service.ReservationsBusinessService;
import demo.reservation.service.ReservationsProductService;
import demo.reservation.service.ReservationsService;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;

@Controller
@RequestMapping( value = ReservationUrl.root)
public class ReservationController extends CommonController{

	@Autowired
	private BaseUtilCustom baseUtilCustom;
	@Autowired
	private ReservationsProductService reProService;
	@Autowired
	private ReservationsBusinessService reBusService;
	@Autowired
	private ReservationsService reService;
	@Autowired
	private StaffDetailService staffDetailService;
	
	@GetMapping(value = ReservationUrl.applicationReservationsProduct)
	public ModelAndView applicationReservationsProduct() {
		return new ModelAndView("jsp/apply/apply");
	}
	
	@GetMapping(value = ReservationUrl.applicationReservationsExchangeDib)
	public ModelAndView applicationReservationsExchangeDib() {
		if(baseUtilCustom.isLoginUser()) {
			return new ModelAndView(new RedirectView("/weixinHtml/application/application.html"));
		} else {
			return new ModelAndView(new RedirectView("/weixinHtml/sign/sign.html"));
		}
	}
	
	@PostMapping( value = ReservationUrl.applicationReservationsProduct)
	public void applicationReservationsProduct(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ApplicationReservationsProductParam param = new ApplicationReservationsProductParam().fromJson(getJson(data));
		CommonResult serviceResult = reProService.applicationReservationsProduct(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping( value = ReservationUrl.acceptReservationProduct)
	public void acceptReservationProduct(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
		AcceptReservationProductParam param = new AcceptReservationProductParam().fromJson(getJson(data));
		CommonResult serviceResult = reProService.acceptReservationProduct(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping( value = ReservationUrl.rejectReservationProduct)
	public void rejectReservationProduct(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RejectReservationProductParam param = new RejectReservationProductParam().fromJson(getJson(data));
		CommonResult serviceResult = reProService.rejectReservationProduct(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping( value = ReservationUrl.applicationExchangeDib)
	public void applicationExchangeDib(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ApplicationExchaneDibParam param = new ApplicationExchaneDibParam().fromJson(getJson(data));
		CommonResult serviceResult = reBusService.applicationExchaneDib(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@GetMapping( value = ReservationUrl.findReservationByUserId)
	public ModelAndView findReservationByUserId() {
		Long userId = baseUtilCustom.getUserId();
		if(userId == null) {
			return new ModelAndView(new RedirectView("/weixinHtml/sign/sign.html"));
		}
		
		StaffDetailPO staffDetail = staffDetailService.findByUserId(userId);
		String resultCode = "-1";
		if(staffDetail != null) {
			resultCode = "0";
		}
		
		return new ModelAndView(new RedirectView("/weixinHtml/normal/normal.html?result=" + resultCode));
	}
	
	@PostMapping( value = ReservationUrl.findReservationByUserId)
	public void findReservationByUserId(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindReservationByUserIdParam param = new FindReservationByUserIdParam().fromJson(getJson(data));
		FindReservationByUserIdResult serviceResult = reService.findReservationByUserId(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping( value = ReservationUrl.findReservationPageForAdmin)
	public void findReservationPageForAdmin(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindReservationPageForAdminParam param = new FindReservationPageForAdminParam().fromJson(getJson(data));
		FindReservationProductPageForAdminResult serviceResult = reProService.findReservationPageForAdmin(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping( value = ReservationUrl.findReservationExchangeDibPageForAdmin)
	public void findReservationBusinessPageForAdmin(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindReservationExchangeDibPageForAdminParam param = new FindReservationExchangeDibPageForAdminParam().fromJson(getJson(data));
		FindReservationBusinessPageForAdminResult serviceResult = reBusService.findReservationPageForAdmin(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping( value = ReservationUrl.acceptReservationBusiness)
	public void acceptReservationBusiness(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
		AcceptReservationBusinessParam param = new AcceptReservationBusinessParam().fromJson(getJson(data));
		CommonResult serviceResult = reBusService.acceptReservationBusiness(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping( value = ReservationUrl.rejectReservationBusiness)
	public void rejectReservationBusiness(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RejectReservationBusinessParam param = new RejectReservationBusinessParam().fromJson(getJson(data));
		CommonResult serviceResult = reBusService.rejectReservationBusiness(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping( value = ReservationUrl.getStatisticsForHomePage)
	public void getStatisticsForHomePage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		GetStatisticsForHomePageResult serviceResult = reService.getStatisticsForHomePage();
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
}