package demo.reservation.mapper;

import java.util.List;

import demo.reservation.domain.bo.ReservationProductBO;
import demo.reservation.domain.bo.ReservationsProductBO;
import demo.reservation.domain.param.mapperParam.FindReservationPageForAdmin;
import demo.reservation.domain.param.mapperParam.FindReservationProductByUserId;
import demo.reservation.domain.param.mapperParam.GetStatisticsProductForHomePageParam;
import demo.reservation.domain.param.mapperParam.UpdateReservationStatuParam;
import demo.reservation.domain.po.ReservationProductPO;
import demo.reservation.domain.vo.ReservationProductStatisticsVO;

public interface ReservationProductPOMapper {
	int insert(ReservationProductPO record);

	int insertSelective(ReservationProductPO record);

	int updateReservationStatu(UpdateReservationStatuParam param);

	ReservationProductPO findReservationProduct(Long id);

	List<ReservationsProductBO> findReservationByUserId(FindReservationProductByUserId param);

	List<ReservationProductBO> findReservationPageForAdmin(FindReservationPageForAdmin param);

	int countReservationPageForAdmin(FindReservationPageForAdmin param);

	List<ReservationProductStatisticsVO> getStatisticsForHomePage(GetStatisticsProductForHomePageParam param);
}