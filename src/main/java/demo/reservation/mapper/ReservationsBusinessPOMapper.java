package demo.reservation.mapper;

import java.util.List;

import demo.reservation.domain.bo.ReservationsBusinessBO;
import demo.reservation.domain.param.mapperParam.FindReservationPageForAdmin;
import demo.reservation.domain.param.mapperParam.FindReservationProductByUserId;
import demo.reservation.domain.param.mapperParam.GetStatisticsBusinessForHomePageParam;
import demo.reservation.domain.param.mapperParam.UpdateReservationStatuParam;
import demo.reservation.domain.po.ReservationsBusinessPO;

public interface ReservationsBusinessPOMapper {
	int insert(ReservationsBusinessPO record);

	int insertSelective(ReservationsBusinessPO record);

	int updateReservationStatu(UpdateReservationStatuParam param);

	ReservationsBusinessPO findReservationBusiness(Long id);

	List<ReservationsBusinessBO> findReservationByUserId(FindReservationProductByUserId param);

	List<ReservationsBusinessBO> findReservationPageForAdmin(FindReservationPageForAdmin mapperParam);

	int countReservationPageForAdmin(FindReservationPageForAdmin mapperParam);

	Integer getStatisticsForHomePage(GetStatisticsBusinessForHomePageParam param);
}