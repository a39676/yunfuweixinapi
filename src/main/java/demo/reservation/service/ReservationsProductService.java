package demo.reservation.service;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.param.controllerParam.AcceptReservationProductParam;
import demo.reservation.domain.param.controllerParam.ApplicationReservationsProductParam;
import demo.reservation.domain.param.controllerParam.FindReservationPageForAdminParam;
import demo.reservation.domain.param.controllerParam.FindReservationProductByUserIdParam;
import demo.reservation.domain.param.controllerParam.RejectReservationProductParam;
import demo.reservation.domain.result.FindReservationProductPageForAdminResult;
import demo.reservation.domain.result.FindReservationProductByUserIdResult;

public interface ReservationsProductService {

	/** 申请预约产品 
	 * @throws Exception */
	CommonResult applicationReservationsProduct(Long userId, ApplicationReservationsProductParam controllerParam) throws Exception;

	/** 通过预约产品申请 
	 * @throws Exception */
	CommonResult acceptReservationProduct(AcceptReservationProductParam controllerParam) throws Exception;

	/** 拒绝产品申请 
	 * @throws Exception */
	CommonResult rejectReservationProduct(RejectReservationProductParam controllerParam) throws Exception;

	/** 查找用户的预约产品情况  */
	FindReservationProductByUserIdResult findReservationProductByUserId(Long userId,
			FindReservationProductByUserIdParam controllerParam);

	/** 管理员查找产品预约分页 */
	FindReservationProductPageForAdminResult findReservationPageForAdmin(Long userId,
			FindReservationPageForAdminParam controllerParam);

}
