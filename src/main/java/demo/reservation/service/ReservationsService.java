package demo.reservation.service;

import demo.reservation.domain.param.controllerParam.FindReservationByUserIdParam;
import demo.reservation.domain.result.FindReservationByUserIdResult;
import demo.reservation.domain.result.GetStatisticsForHomePageResult;

public interface ReservationsService {

	/** 获取预约状态列表 */
	FindReservationByUserIdResult findReservationByUserId(Long userId, FindReservationByUserIdParam controllerParam);

	/** 首页统计接口 */
	GetStatisticsForHomePageResult getStatisticsForHomePage();
}
