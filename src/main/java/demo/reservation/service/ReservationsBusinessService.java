package demo.reservation.service;

import demo.common.domain.result.CommonResult;
import demo.reservation.domain.param.controllerParam.AcceptReservationBusinessParam;
import demo.reservation.domain.param.controllerParam.ApplicationExchaneDibParam;
import demo.reservation.domain.param.controllerParam.FindReservationBusinessByUserIdParam;
import demo.reservation.domain.param.controllerParam.FindReservationExchangeDibPageForAdminParam;
import demo.reservation.domain.param.controllerParam.RejectReservationBusinessParam;
import demo.reservation.domain.result.FindReservationBusinessByUserIdResult;
import demo.reservation.domain.result.FindReservationBusinessPageForAdminResult;

public interface ReservationsBusinessService {

	/** 为用户预约零钞兑换 
	 * @throws Exception */
	CommonResult applicationExchaneDib(Long userId, ApplicationExchaneDibParam controllerParam) throws Exception;

	/** 查找用户的预约服务情况  */
	FindReservationBusinessByUserIdResult findReservationBuessinessByUserId(Long userId,
			FindReservationBusinessByUserIdParam controllerParam);

	/** 管理员查找零钞兑换服务预约分页 */
	FindReservationBusinessPageForAdminResult findReservationPageForAdmin(Long userId,
			FindReservationExchangeDibPageForAdminParam controllerParam);

	CommonResult acceptReservationBusiness(AcceptReservationBusinessParam controllerParam) throws Exception;

	CommonResult rejectReservationBusiness(RejectReservationBusinessParam controllerParam) throws Exception;

}
