package demo.reservation.service.impl;

import java.util.Date;

import demo.common.domain.result.CommonResult;
import demo.common.service.CommonService;

public class ReservationsCommonService extends CommonService {

	protected int workHourStart = 0;
	protected int workHourEnd = 24;

	@SuppressWarnings("deprecation")
	protected boolean isValidReservationStartTime(Date startTime) {
		if (startTime == null) {
			return false;
		}

		if (startTime.before(new Date()) || startTime.getHours() < workHourStart) {
			return false;
		}

		return true;
	}

	@SuppressWarnings("deprecation")
	protected boolean isValidReservationEndTime(Date endTime) {
		if (endTime == null) {
			return false;
		}

		if (endTime.before(new Date()) || endTime.getHours() > workHourEnd) {
			return false;
		}

		return true;
	}

	protected boolean isValidReservationTime(Date startTime, Date endTime) {
		if(!isValidReservationStartTime(startTime) || !isValidReservationEndTime(endTime) || startTime.after(endTime)) {
			return false;
		}
		return true;
	}
	
	protected CommonResult notValidReservationTime() {
		CommonResult result = new CommonResult();
		result.failWithMessage("所选择的预约时段无效");
		return result;
	}

	protected CommonResult reservationSuccess() {
		CommonResult result = new CommonResult();
		result.successWithMessage("已成功预约");
		return result;
	}
}
