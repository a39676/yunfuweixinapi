package demo.reservation.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dateHandle.DateUtilCustom;
import demo.base.user.service.UsersService;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.organizations.controller.OrganizationsController;
import demo.organizations.controller.StaffDetailController;
import demo.organizations.domain.constant.AuthsDefault;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.domain.vo.StaffDetailVO;
import demo.products.controller.ProductController;
import demo.products.domain.po.ProductsPO;
import demo.products.domain.type.InterestType;
import demo.products.domain.type.ProductType;
import demo.reservation.domain.bo.ReservationProductBO;
import demo.reservation.domain.bo.ReservationsProductBO;
import demo.reservation.domain.param.controllerParam.AcceptReservationProductParam;
import demo.reservation.domain.param.controllerParam.ApplicationReservationsProductParam;
import demo.reservation.domain.param.controllerParam.FindReservationPageForAdminParam;
import demo.reservation.domain.param.controllerParam.FindReservationProductByUserIdParam;
import demo.reservation.domain.param.controllerParam.RejectReservationProductParam;
import demo.reservation.domain.param.mapperParam.FindReservationPageForAdmin;
import demo.reservation.domain.param.mapperParam.FindReservationProductByUserId;
import demo.reservation.domain.param.mapperParam.UpdateReservationStatuParam;
import demo.reservation.domain.po.ReservationProductPO;
import demo.reservation.domain.result.FindReservationProductByUserIdResult;
import demo.reservation.domain.result.FindReservationProductPageForAdminResult;
import demo.reservation.domain.type.ReservationStatusType;
import demo.reservation.domain.vo.ReservationProductVO;
import demo.reservation.mapper.ReservationProductPOMapper;
import demo.reservation.service.ReservationsProductService;
import demo.weixin.service.WeixinService;

/**
 * 产品预约
 */
@Service
public class ReservationsProductServiceImpl extends ReservationsCommonService implements ReservationsProductService {

	@Autowired
	private StaffDetailController staffDetailController;
	@Autowired
	private OrganizationsController orgController;
	@Autowired
	private ProductController productController;
	
	@Autowired
	private WeixinService wxService;
	@Autowired
	private UsersService userService;
	
	@Autowired
	private ReservationProductPOMapper reservationProMapper;

	@Override
	public CommonResult applicationReservationsProduct(Long userId,
			ApplicationReservationsProductParam controllerParam) throws Exception {

		if (userId == null) {
			return notLogin();
		}

		if (controllerParam.getReservationsStartTime() == null || controllerParam.getReservationsEndTime() == null) {
			CommonResult result = new CommonResult();
			result.failWithMessage("请选择预约时段");
			return result;
		}

		if (controllerParam.getOrgId() == null || controllerParam.getProductId() == null) {
			CommonResult result = new CommonResult();
			result.failWithMessage("请选择预约的产品和具体机构");
			return result;
		}

		if (!isValidReservationTime(controllerParam.getReservationsStartTime(),
				controllerParam.getReservationsEndTime())) {
			return notValidReservationTime();
		}

		if (controllerParam.getReservationsStartTime().getTime()
				- controllerParam.getReservationsEndTime().getTime() > (1000L * 60 * 60 * 24)) {
			CommonResult result = new CommonResult();
			result.failWithMessage("预约时间请设定在同一天");
			return result;
		}

		ReservationProductPO newReservation = new ReservationProductPO();
		newReservation.setOrgId(controllerParam.getOrgId());
		newReservation.setProductId(controllerParam.getProductId());
		newReservation.setReservationStatus(ReservationStatusType.application.getCode());
		newReservation.setReservationTimeStart(controllerParam.getReservationsStartTime());
		newReservation.setReservationTimeEnd(controllerParam.getReservationsEndTime());
		newReservation.setUserId(userId);
		try {
			reservationProMapper.insertSelective(newReservation);
		} catch (Exception e) {
			e.printStackTrace();
			return serviceError();
		}
		ProductsPO product = productController.findProduct(controllerParam.getProductId());
		
		String customerOpenId = userService.findOpenIdByUserId(userId);
		OrganizationsPO org = orgController.findOne(controllerParam.getOrgId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String reservationTime = sdf.format(controllerParam.getReservationsStartTime()) + " ~ " + sdf.format(controllerParam.getReservationsEndTime());
		wxService.sendReservationApplication(customerOpenId, "", product.getProductName(), "您正在申请预约", reservationTime, "", true);

		List<StaffDetailVO> staffList = orgController.findStaffsByOrgId(org.getId());
		Long targetStaffUserId = null;
		if(product.getProductType().equals(ProductType.financial.getCode())) {
			for(StaffDetailVO staffVO : staffList) {
				if(AuthsDefault.AUTH_BUSINESS_HANDLER_NAME.equals(staffVO.getAuthName())) {
					targetStaffUserId = staffVO.getUserId();
				}
			}
		} else {
//			非理财产品预约,通知机构管理员分配预约
			staffList = orgController.findStaffsByOrgId(org.getTopHeadOrg());
			for(StaffDetailVO staffVO : staffList) {
				if(AuthsDefault.AUTH_BRANCH_ADMIN_NAME.equals(staffVO.getAuthName())) {
					targetStaffUserId = staffVO.getUserId();
				}
			}
		}
		if(targetStaffUserId == null) {
			return reservationSuccess();
		}
		String staffOpenId = userService.findOpenIdByUserId(targetStaffUserId);
		if(staffOpenId == null) {
			return reservationSuccess();
		}
		wxService.sendReservationApplication(staffOpenId, "有一条新增预约", org.getOrgName(), "", reservationTime, "有新的预约申请", true);

		return reservationSuccess();
	}
	
	@Override
	public CommonResult acceptReservationProduct(AcceptReservationProductParam controllerParam) throws Exception {
		if (controllerParam.getStaffId() == null || controllerParam.getReservationId() == null) {
			return nullParam();
		}

		ReservationProductPO rp = reservationProMapper.findReservationProduct(controllerParam.getReservationId());
		if (rp == null) {
			return errorParam();
		}

		UpdateReservationStatuParam updateParam = new UpdateReservationStatuParam();
		updateParam.setReservationId(controllerParam.getReservationId());
		updateParam.setReservationStatu(ReservationStatusType.accepted.getCode());
		updateParam.setStaffId(controllerParam.getStaffId());
		reservationProMapper.updateReservationStatu(updateParam);

		String openId = userService.findOpenIdByUserId(rp.getUserId());
		OrganizationsPO org = orgController.findOne(rp.getOrgId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String reservationTime = sdf.format(rp.getReservationTimeStart()) + " ~ " + sdf.format(rp.getReservationTimeEnd());
		wxService.sendReservationSecuss(openId, "", org.getOrgName(), "", reservationTime, "");

		StaffDetailPO staffPO = staffDetailController.findOne(controllerParam.getStaffId());
		
		if(staffPO == null || staffPO.getUserId() == null) {
			return serviceError();
		}
		
		Long targetStaffUserId = staffPO.getUserId();
		if(targetStaffUserId == null) {
			return reservationSuccess();
		}
		String staffOpenId = userService.findOpenIdByUserId(targetStaffUserId);
		if(staffOpenId == null) {
			return reservationSuccess();
		}
		wxService.sendReservationApplication(staffOpenId, "有一条新增预约", org.getOrgName(), "", reservationTime, "有新的预约申请", true);
		
		
		CommonResult result = new CommonResult();
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult rejectReservationProduct(RejectReservationProductParam controllerParam) throws Exception {
		if (controllerParam.getReservationId() == null) {
			return nullParam();
		}

		if (StringUtils.isBlank(controllerParam.getRemark())) {
			CommonResult result = new CommonResult();
			result.normalFail();
			result.setMessage("请输入拒绝理由");
			return result;
		}

		ReservationProductPO rp = reservationProMapper.findReservationProduct(controllerParam.getReservationId());
		if (rp == null) {
			return errorParam();
		}

		UpdateReservationStatuParam updateParam = new UpdateReservationStatuParam();
		updateParam.setReservationId(controllerParam.getReservationId());
		updateParam.setReservationStatu(ReservationStatusType.reject.getCode());
		updateParam.setRemark(controllerParam.getRemark());

		CommonResult result = new CommonResult();
		try {
			reservationProMapper.updateReservationStatu(updateParam);
		} catch (Exception e) {
			result.normalFail();
			result.setMessage("拒绝理由过长");
			e.printStackTrace();
			return result;
		}
		
		String openId = userService.findOpenIdByUserId(rp.getUserId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String reservationTime = sdf.format(rp.getReservationTimeStart()) + " ~ " + sdf.format(rp.getReservationTimeEnd());
		wxService.sendReservationFail(openId, "", reservationTime, controllerParam.getRemark(), "");

		result.setIsSuccess();
		return result;
	}

	@Override
	public FindReservationProductByUserIdResult findReservationProductByUserId(Long userId,
			FindReservationProductByUserIdParam controllerParam) {
		FindReservationProductByUserIdResult result = new FindReservationProductByUserIdResult();
		if (userId == null) {
			result.failWithMessage("请先登陆");
			return result;
		}
		if (controllerParam == null || controllerParam.getReservationStatus() == null) {
			result.failWithMessage("参数为空");
			return result;
		}
		ReservationStatusType reservationStatus = ReservationStatusType.getType(controllerParam.getReservationStatus());
		if (reservationStatus == null) {
			result.failWithMessage("参数异常");
			return result;
		}

		FindReservationProductByUserId mapperParam = new FindReservationProductByUserId();
		mapperParam.setUserId(userId);
		mapperParam.setReservationStatus(controllerParam.getReservationStatus());
		mapperParam.setReservationTimeEnd(DateUtilCustom.dateDiffDays(-30));
		List<ReservationsProductBO> reservationList = reservationProMapper.findReservationByUserId(mapperParam);

		result.setReservationList(reservationList);
		result.setIsSuccess();

		return result;
	}

	@Override
	public FindReservationProductPageForAdminResult findReservationPageForAdmin(Long userId,
			FindReservationPageForAdminParam controllerParam) {
		FindReservationProductPageForAdminResult result = new FindReservationProductPageForAdminResult();
		if (controllerParam == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		if (controllerParam.getProductType() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		PageParam pp = setPageFromPageNo(controllerParam.getPageNo(), controllerParam.getPageSize());

		FindReservationPageForAdmin mapperParam = new FindReservationPageForAdmin();
		mapperParam.setPageStart(pp.getPageStart());
		mapperParam.setPageSize(pp.getPageSize());
		if (controllerParam.getReceptionTime() != null) {
			mapperParam.setReceptionTimeStart(DateUtilCustom.atStartOfDay(controllerParam.getReceptionTime()));
			mapperParam.setReceptionTimeEnd(DateUtilCustom.atEndOfDay(controllerParam.getReceptionTime()));
		}
		if (controllerParam.getOrgId() != null) {
			mapperParam.setOrgId(controllerParam.getOrgId());
		} else {
			List<Long> orgIdList = new ArrayList<Long>();
			StaffDetailPO staffDetail = staffDetailController.findByUserId(userId);
			List<OrganizationsPO> orgList = orgController.findBranchOrgByStaffId(staffDetail.getId());
			if (orgList == null || orgList.size() < 1) {
				orgIdList.add(0L);
			} else {
				orgList.stream().forEach(o -> orgIdList.add(o.getId()));
			}
			mapperParam.setOrgIdList(orgIdList);
		}
		mapperParam.setId(controllerParam.getId());
		mapperParam.setMobile(controllerParam.getMobile());
		mapperParam.setStaffName(controllerParam.getStaffName());
		mapperParam.setProductType(controllerParam.getProductType());
		int resultCount = reservationProMapper.countReservationPageForAdmin(mapperParam);
		result.setResultCount(resultCount);
		List<ReservationProductVO> voList = new ArrayList<ReservationProductVO>();
		if (resultCount < 1) {
			result.setReservationList(voList);
			return result;
		}
		List<ReservationProductBO> reservationBOList = reservationProMapper.findReservationPageForAdmin(mapperParam);
		if (reservationBOList != null && reservationBOList.size() >= 1) {
			reservationBOList.stream().forEach(b -> voList.add(buildReservationProductVO(b)));
		}
		result.setReservationList(voList);
		result.setIsSuccess();
		return result;
	}

	private ReservationProductVO buildReservationProductVO(ReservationProductBO bo) {
		ReservationProductVO vo = new ReservationProductVO();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		vo.setExpirationTime(bo.getExpirationTime());
		vo.setId(bo.getId());
		vo.setInterestRate(bo.getInterestRate());
		InterestType interesType = InterestType.getType(bo.getInterestType());
		if (interesType != null) {
			vo.setInterestType(interesType.getName());
		}
		vo.setNickName(bo.getNickName());
		vo.setOrgName(bo.getOrgName());
		vo.setProductCode(bo.getProductCode());
		vo.setProductName(bo.getProductName());
		vo.setProductTypeName(productController.getSubProductTypeName(bo.getProductType(), bo.getProductSubType()));
		vo.setStaffName(bo.getStaffName());
		vo.setUserName(bo.getUserName());
		vo.setOrgId(bo.getOrgId());
		vo.setReservationStatu(bo.getReservationStatus());
		vo.setReservationTimeStart(bo.getReservationTimeStart());
		vo.setReservationTimeEnd(bo.getReservationTimeEnd());
		ReservationStatusType reservationStatusType = ReservationStatusType.getType(bo.getReservationStatus());
		if (reservationStatusType != null) {
			vo.setReservationStatuString(reservationStatusType.getName());
		}
		vo.setRemark(bo.getRemark());
		if (bo.getReceptionTime() != null) {
			vo.setReceptionTime(bo.getReceptionTime());
			vo.setReceptionTimeString(sdf.format(bo.getReceptionTime()));
		}
		vo.setReservationTimeStartString(sdf.format(bo.getReservationTimeStart()));
		vo.setReservationTimeEndString(sdf.format(bo.getReservationTimeEnd()));
		return vo;
	}
}