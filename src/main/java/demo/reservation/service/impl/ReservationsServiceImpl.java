package demo.reservation.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.organizations.controller.OrganizationsController;
import demo.organizations.controller.StaffDetailController;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.po.StaffDetailPO;
import demo.products.domain.type.ProductType;
import demo.reservation.domain.bo.ReservationsBusinessBO;
import demo.reservation.domain.bo.ReservationsProductBO;
import demo.reservation.domain.param.controllerParam.FindReservationBusinessByUserIdParam;
import demo.reservation.domain.param.controllerParam.FindReservationByUserIdParam;
import demo.reservation.domain.param.controllerParam.FindReservationProductByUserIdParam;
import demo.reservation.domain.param.mapperParam.GetStatisticsBusinessForHomePageParam;
import demo.reservation.domain.param.mapperParam.GetStatisticsProductForHomePageParam;
import demo.reservation.domain.result.FindReservationBusinessByUserIdResult;
import demo.reservation.domain.result.FindReservationByUserIdResult;
import demo.reservation.domain.result.FindReservationProductByUserIdResult;
import demo.reservation.domain.result.GetStatisticsForHomePageResult;
import demo.reservation.domain.type.ReservationStatusType;
import demo.reservation.domain.vo.ReservationProductStatisticsVO;
import demo.reservation.domain.vo.ReservationResultVO;
import demo.reservation.domain.vo.ReservationStatisticsVO;
import demo.reservation.mapper.ReservationProductPOMapper;
import demo.reservation.mapper.ReservationsBusinessPOMapper;
import demo.reservation.service.ReservationsBusinessService;
import demo.reservation.service.ReservationsProductService;
import demo.reservation.service.ReservationsService;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;

/**
 * 预约
 */
@Service
public class ReservationsServiceImpl extends ReservationsCommonService implements ReservationsService {

	@Autowired
	private ReservationsBusinessService reservationBusService;
	@Autowired
	private ReservationsProductService reservationProService;
	@Autowired
	private BaseUtilCustom baseUtilCustom;
	
	@Autowired
	private ReservationProductPOMapper reservationProMapper;
	@Autowired
	private ReservationsBusinessPOMapper reservationBusMapper;
	
	@Autowired
	private OrganizationsController orgController;
	@Autowired
	private StaffDetailController staffController;
	
	@Override
	public FindReservationByUserIdResult findReservationByUserId(Long userId,
			FindReservationByUserIdParam controllerParam) {
		FindReservationByUserIdResult result = new FindReservationByUserIdResult();
		if (userId == null) {
			result.failWithMessage("请先登录");
			return result;
		}
		if (controllerParam.getReservationStatus() == null) {
			result.failWithMessage("请选择预约状态");
			return result;
		}
		ReservationStatusType reservationStatus = ReservationStatusType.getType(controllerParam.getReservationStatus());
		if (reservationStatus == null) {
			result.failWithMessage("请选择预约状态");
			return result;
		}
		
		FindReservationBusinessByUserIdParam findReserBus = new FindReservationBusinessByUserIdParam();
		findReserBus.setReservationStatus(controllerParam.getReservationStatus());
		FindReservationBusinessByUserIdResult reserBusResult = reservationBusService
				.findReservationBuessinessByUserId(userId, findReserBus);

		FindReservationProductByUserIdParam findReserPro = new FindReservationProductByUserIdParam();
		findReserPro.setReservationStatus(controllerParam.getReservationStatus());
		FindReservationProductByUserIdResult reserProResult = reservationProService
				.findReservationProductByUserId(userId, findReserPro);

		List<ReservationResultVO> voList = new ArrayList<ReservationResultVO>();
		reserBusResult.getReservationList().stream().forEach(b -> voList.add(busBoToReservationVO(b)));
		reserProResult.getReservationList().stream().forEach(b -> voList.add(proBoToReservationVO(b)));
		
		result.setReservationResultList(voList);
		result.setIsSuccess();
		
		return result;
	}

	private ReservationResultVO busBoToReservationVO(ReservationsBusinessBO bo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ReservationResultVO vo = new ReservationResultVO();
		ReservationStatusType type = ReservationStatusType.getType(bo.getReservationStatus());

		if (type == null) {
			return vo;
		}

		vo.setId(bo.getId());
		vo.setReservationStatus(type.getName());

		if (ReservationStatusType.application.equals(type)) {
			vo.setReservationTime(
					sdf.format(bo.getReservationTimeStart()) + " - " + sdf.format(bo.getReservationTimeEnd()));
			vo.setOrgName(bo.getOrgName());
		} else if (ReservationStatusType.accepted.equals(type)) {
			vo.setReservationTime(
					sdf.format(bo.getReservationTimeStart()) + " - " + sdf.format(bo.getReservationTimeEnd()));
			vo.setOrgName(bo.getOrgName());
			vo.setStaffName(bo.getStaffName());
		} else if (ReservationStatusType.reject.equals(type)) {
			vo.setRemark(bo.getRemark());
		} else if (ReservationStatusType.wasDone.equals(type)) {
			vo.setRemark("已完成");
		} else if (ReservationStatusType.wasRuin.equals(type)) {
			vo.setRemark("已过期");
		}

		return vo;
	}
	
	private ReservationResultVO proBoToReservationVO(ReservationsProductBO bo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ReservationResultVO vo = new ReservationResultVO();
		ReservationStatusType type = ReservationStatusType.getType(bo.getReservationStatus());

		if (type == null) {
			return vo;
		}

		vo.setId(bo.getId());
		vo.setReservationStatus(type.getName());

		if (ReservationStatusType.application.equals(type)) {
			vo.setReservationTime(
					sdf.format(bo.getReservationTimeStart()) + " - " + sdf.format(bo.getReservationTimeEnd()));
			vo.setOrgName(bo.getOrgName());
		} else if (ReservationStatusType.accepted.equals(type)) {
			vo.setReservationTime(
					sdf.format(bo.getReservationTimeStart()) + " - " + sdf.format(bo.getReservationTimeEnd()));
			vo.setOrgName(bo.getOrgName());
			vo.setStaffName(bo.getStaffName());
		} else if (ReservationStatusType.reject.equals(type)) {
			vo.setRemark(bo.getRemark());
		} else if (ReservationStatusType.wasDone.equals(type)) {
			vo.setRemark("已完成");
		} else if (ReservationStatusType.wasRuin.equals(type)) {
			vo.setRemark("已过期");
		}

		return vo;
	}

	@Override
	public GetStatisticsForHomePageResult getStatisticsForHomePage() {
		GetStatisticsForHomePageResult result = new GetStatisticsForHomePageResult();
		Long userId = baseUtilCustom.getUserId();
		if(userId == null) {
			return result;
		}
		
		StaffDetailPO staff = staffController.findByUserId(userId);
		if(staff == null) {
			return result;
		}
		
		OrganizationsPO org = orgController.findOrgByStaffId(staff.getId());
		if(org == null) {
			return result;
		}
		
		GetStatisticsProductForHomePageParam productStatisticsParam = new GetStatisticsProductForHomePageParam();
		GetStatisticsBusinessForHomePageParam param = new GetStatisticsBusinessForHomePageParam();
		if(org.getTopHeadOrg().equals(org.getId())) {
			productStatisticsParam.setTopHeadOrg(org.getId());
			param.setTopHeadOrg(org.getId());
		} else {
			productStatisticsParam.setOrgId(org.getId());
			param.setOrgId(org.getId());
		}
		productStatisticsParam.setReservationStatus(ReservationStatusType.application.getCode());
		List<ReservationProductStatisticsVO> productStatistics = reservationProMapper.getStatisticsForHomePage(productStatisticsParam);
		
		param.setReservationStatus(ReservationStatusType.application.getCode());
		Integer businessStatistitcs = reservationBusMapper.getStatisticsForHomePage(param);
		
		List<ReservationStatisticsVO> reservationStatisticsVOList = new ArrayList<ReservationStatisticsVO>();
		ReservationStatisticsVO vo = null;
		
//		for(ReservationProductStatisticsVO proSta : productStatistics) {
//			vo = new ReservationStatisticsVO();
//			pt = ProductType.getType(proSta.getProductType());
//			if(pt != null) {
//				vo.setName(pt.getName());
//			}
//			vo.setCount(proSta.getCount());
//			reservationStatisticsVOList.add(vo);
//		}
		JSONObject j = new JSONObject();
		for(ProductType p : ProductType.values()) {
			vo = new ReservationStatisticsVO();
			vo.setCount(0);
			for(ReservationProductStatisticsVO proSta : productStatistics) {
				if(proSta.getProductType().equals(p.getCode())) {
					j.put(p.getTypeEName(), proSta.getCount());
					vo.setName(p.getName() + "待受理");
					vo.setCount(proSta.getCount());
				}
			}
			if(vo.getCount() == 0) {
				j.put(p.getTypeEName(), 0);
				vo.setName(p.getName() + "待受理");
			}
			reservationStatisticsVOList.add(vo);
		}
		
		if(businessStatistitcs == null) {
			businessStatistitcs = 0;
		}
		vo = new ReservationStatisticsVO();
		vo.setName("零钞兑换待受理");
		vo.setCount(businessStatistitcs);
		
		reservationStatisticsVOList.add(vo);
		j.put("exchangeDib", businessStatistitcs);
		
		result.setStatisticsResultList(reservationStatisticsVOList);
		result.setJson(j);
		result.setIsSuccess();
		
		return result;
	}
}