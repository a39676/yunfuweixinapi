package demo.reservation.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dateHandle.DateUtilCustom;
import demo.base.user.service.UsersService;
import demo.business.controller.BusinessController;
import demo.business.domain.param.controllerParam.UpdateExchangeDibOrgUsedParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibUserUsedParam;
import demo.business.domain.result.UpdateExchangeDibUserUsedResult;
import demo.business.domain.type.MoneyType;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.organizations.controller.OrganizationsController;
import demo.organizations.controller.StaffDetailController;
import demo.organizations.domain.constant.AuthsDefault;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.domain.vo.StaffDetailVO;
import demo.reservation.domain.bo.ReservationsBusinessBO;
import demo.reservation.domain.param.controllerParam.AcceptReservationBusinessParam;
import demo.reservation.domain.param.controllerParam.ApplicationExchaneDibParam;
import demo.reservation.domain.param.controllerParam.FindReservationBusinessByUserIdParam;
import demo.reservation.domain.param.controllerParam.FindReservationExchangeDibPageForAdminParam;
import demo.reservation.domain.param.controllerParam.RejectReservationBusinessParam;
import demo.reservation.domain.param.mapperParam.FindReservationPageForAdmin;
import demo.reservation.domain.param.mapperParam.FindReservationProductByUserId;
import demo.reservation.domain.param.mapperParam.UpdateReservationStatuParam;
import demo.reservation.domain.po.ReservationsBusinessPO;
import demo.reservation.domain.result.FindReservationBusinessByUserIdResult;
import demo.reservation.domain.result.FindReservationBusinessPageForAdminResult;
import demo.reservation.domain.type.ReservationStatusType;
import demo.reservation.domain.vo.ReservationsBusinessVO;
import demo.reservation.mapper.ReservationsBusinessPOMapper;
import demo.reservation.service.ReservationsBusinessService;
import demo.weixin.service.WeixinService;

/**
 * 预约服务
 */
@Service
public class ReservationsBusinessServiceImpl extends ReservationsCommonService implements ReservationsBusinessService {

	@Autowired
	private StaffDetailController staffDetailController;
	@Autowired
	private OrganizationsController orgController;
	@Autowired
	private BusinessController businessController;

	@Autowired
	private UsersService userService;
	@Autowired
	private WeixinService wxService;
	
	@Autowired
	private ReservationsBusinessPOMapper reservationBusMapper;

	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	@Override
	public CommonResult applicationExchaneDib(Long userId, ApplicationExchaneDibParam controllerParam) throws Exception {
		CommonResult result = new CommonResult();

		if (userId == null) {
			return notLogin();
		}
		if (controllerParam.getOrgId() == null) {
			result.failWithMessage("请选择预约机构");
			return result;
		}
		if (!isValidReservationTime(controllerParam.getReservationsStartTime(),
				controllerParam.getReservationsEndTime())) {
			return notValidReservationTime();
		}
		if (controllerParam.getMoneyType() == null) {
			result.failWithMessage("缺少所使用的货币类型");
			return result;
		}
		MoneyType moneyType = MoneyType.getType(controllerParam.getMoneyType());
		if (moneyType == null) {
			result.failWithMessage("货币类型不存在");
			return result;
		}
		if (!orgController.existOrgId(controllerParam.getOrgId()).isSuccess()) {
			result.failWithMessage("机构不存在");
			return result;
		}

		CommonResult subResult = null;

		UpdateExchangeDibUserUsedParam updateUserLimitParam = new UpdateExchangeDibUserUsedParam();
		updateUserLimitParam.setUserId(userId);
		updateUserLimitParam.setOrgId(controllerParam.getOrgId());
		updateUserLimitParam.setCoin01Used(controllerParam.getCoin01Used());
		updateUserLimitParam.setCoin05Used(controllerParam.getCoin05Used());
		updateUserLimitParam.setCoin1Used(controllerParam.getCoin1Used());
		updateUserLimitParam.setPaper1Used(controllerParam.getPaper1Used());
		updateUserLimitParam.setPaper5Used(controllerParam.getPaper5Used());
		updateUserLimitParam.setPaper10Used(controllerParam.getPaper10Used());
		updateUserLimitParam.setPaper20Used(controllerParam.getPaper20Used());
		updateUserLimitParam.setPaper50Used(controllerParam.getPaper50Used());
		updateUserLimitParam.setPaper100Used(controllerParam.getPaper100Used());
		updateUserLimitParam.setMoneyType(controllerParam.getMoneyType());
		UpdateExchangeDibUserUsedResult updateExchangeDibUserUsedResult = businessController
				.updateExchangeDibUserUsed(updateUserLimitParam);
		if (!updateExchangeDibUserUsedResult.isSuccess()) {
			result.failWithMessage(updateExchangeDibUserUsedResult.getMessage());
			return result;
		}

		UpdateExchangeDibOrgUsedParam updateOrgLimitParam = new UpdateExchangeDibOrgUsedParam();
		updateOrgLimitParam.setOrgId(controllerParam.getOrgId());
		updateOrgLimitParam.setCoin01Used(controllerParam.getCoin01Used());
		updateOrgLimitParam.setCoin05Used(controllerParam.getCoin05Used());
		updateOrgLimitParam.setCoin1Used(controllerParam.getCoin1Used());
		updateOrgLimitParam.setPaper1Used(controllerParam.getPaper1Used());
		updateOrgLimitParam.setPaper5Used(controllerParam.getPaper5Used());
		updateOrgLimitParam.setPaper10Used(controllerParam.getPaper10Used());
		updateOrgLimitParam.setPaper20Used(controllerParam.getPaper20Used());
		updateOrgLimitParam.setPaper50Used(controllerParam.getPaper50Used());
		updateOrgLimitParam.setPaper100Used(controllerParam.getPaper100Used());
		subResult = businessController.updateExchangeDibOrgUsed(updateOrgLimitParam);
		if (!subResult.isSuccess()) {
			return subResult;
		}

		ReservationsBusinessPO newReservation = new ReservationsBusinessPO();
		newReservation.setUserId(userId);
		newReservation.setBusinessId(updateExchangeDibUserUsedResult.getBusinessId());
		newReservation.setReservationStatus(ReservationStatusType.application.getCode());
		newReservation.setReservationTimeStart(controllerParam.getReservationsStartTime());
		newReservation.setReservationTimeEnd(controllerParam.getReservationsEndTime());
		newReservation.setOrgId(controllerParam.getOrgId());
		reservationBusMapper.insertSelective(newReservation);
		
		String customerOpenId = userService.findOpenIdByUserId(userId);
		OrganizationsPO org = orgController.findOne(controllerParam.getOrgId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String reservationTime = sdf.format(controllerParam.getReservationsStartTime()) + " ~ " + sdf.format(controllerParam.getReservationsEndTime());
		wxService.sendReservationApplication(customerOpenId, "申请预约零钞兑换", "处理中", "", reservationTime, "", true);
		
		List<StaffDetailVO> staffList = orgController.findStaffsByOrgId(org.getId());
		if(staffList == null) {
			return reservationSuccess();
		}
		Long targetStaffUserId = null;
		for(StaffDetailVO staffVO : staffList) {
			if(AuthsDefault.AUTH_BRANCH_HANDLER_NAME.equals(staffVO.getAuthName())) {
				targetStaffUserId = staffVO.getUserId();
			}
		}
		if(targetStaffUserId == null) {
			return reservationSuccess();
		}
		String staffOpenId = userService.findOpenIdByUserId(targetStaffUserId);
		if(staffOpenId == null) {
			return reservationSuccess();
		}
		wxService.sendReservationApplication(staffOpenId, "", "申请预约零钞兑换", "处理中", reservationTime, "有新的预约申请", false);

		return reservationSuccess();
	}

	@Override
	public FindReservationBusinessByUserIdResult findReservationBuessinessByUserId(Long userId,
			FindReservationBusinessByUserIdParam controllerParam) {
		FindReservationBusinessByUserIdResult result = new FindReservationBusinessByUserIdResult();
		if (userId == null) {
			result.failWithMessage("请先登陆");
			return result;
		}
		if (controllerParam == null || controllerParam.getReservationStatus() == null) {
			result.failWithMessage("参数为空");
			return result;
		}
		ReservationStatusType reservationStatus = ReservationStatusType.getType(controllerParam.getReservationStatus());
		if (reservationStatus == null) {
			result.failWithMessage("参数异常");
			return result;
		}

		FindReservationProductByUserId mapperParam = new FindReservationProductByUserId();
		mapperParam.setUserId(userId);
		mapperParam.setReservationStatus(controllerParam.getReservationStatus());
		mapperParam.setReservationTimeEnd(DateUtilCustom.dateDiffDays(-30));
		List<ReservationsBusinessBO> reservationList = reservationBusMapper.findReservationByUserId(mapperParam);

		result.setReservationList(reservationList);
		result.setIsSuccess();

		return result;
	}

	private ReservationsBusinessVO buildReservationBusinessVO(ReservationsBusinessBO bo) {
		ReservationsBusinessVO vo = new ReservationsBusinessVO();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		vo.setId(bo.getId());
		vo.setNickName(bo.getNickName());
		vo.setOrgName(bo.getOrgName());
		vo.setStaffName(bo.getStaffName());
		vo.setUserName(bo.getUserName());
		vo.setOrgId(bo.getOrgId());
		vo.setReservationStatus(bo.getReservationStatus());
		ReservationStatusType rsType = ReservationStatusType.getType(bo.getReservationStatus());
		if (rsType != null) {
			vo.setReservationStatusString(rsType.getName());
		}
		vo.setReservationTimeStart(bo.getReservationTimeStart());
		vo.setReservationTimeEnd(bo.getReservationTimeEnd());
		vo.setReservationTimeStartString(sdf.format(bo.getReservationTimeStart()));
		vo.setReservationTimeEndString(sdf.format(bo.getReservationTimeEnd()));
		vo.setRemark(bo.getRemark());
		if (bo.getReceptionTime() != null) {
			vo.setReceptionTime(bo.getReceptionTime());
			vo.setReceptionTimeString(sdf.format(bo.getReceptionTime()));
		}
		MoneyType mt = MoneyType.getType(bo.getMoneyType());
		String moneyTypeStr = String.valueOf(mt.getName());
		StringBuffer sb = new StringBuffer(moneyTypeStr + "兑换: ");
		if (bo.getCoin01() != null && bo.getCoin01() > 0) {
			sb.append("1角硬币" + bo.getCoin01() + "个,");
		}
		if (bo.getCoin05() != null && bo.getCoin05() > 0) {
			sb.append("5角硬币" + bo.getCoin05() + "个,");
		}
		if (bo.getCoin1() != null && bo.getCoin1() > 0) {
			sb.append("1元硬币" + bo.getCoin1() + "个,");
		}
		if (bo.getPaper1() != null && bo.getPaper1() > 0) {
			sb.append("1元纸币" + bo.getPaper1() + "张,");
		}
		if (bo.getPaper5() != null && bo.getPaper5() > 0) {
			sb.append("5元纸币" + bo.getPaper5() + "张,");
		}
		if (bo.getPaper10() != null && bo.getPaper10() > 0) {
			sb.append("10元纸币" + bo.getPaper10() + "张,");
		}
		if (bo.getPaper20() != null && bo.getPaper20() > 0) {
			sb.append("20元纸币" + bo.getPaper20() + "张,");
		}
		if (bo.getPaper50() != null && bo.getPaper50() > 0) {
			sb.append("50元纸币" + bo.getPaper50() + "张,");
		}
		if (bo.getPaper100() != null && bo.getPaper100() > 0) {
			sb.append("100元纸币" + bo.getPaper100() + "张,");
		}
		vo.setBusinessContent(sb.toString());

		return vo;
	}

	@Override
	public FindReservationBusinessPageForAdminResult findReservationPageForAdmin(Long userId,
			FindReservationExchangeDibPageForAdminParam controllerParam) {
		FindReservationBusinessPageForAdminResult result = new FindReservationBusinessPageForAdminResult();
		if (controllerParam == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		PageParam pp = setPageFromPageNo(controllerParam.getPageNo(), controllerParam.getPageSize());

		FindReservationPageForAdmin mapperParam = new FindReservationPageForAdmin();
		mapperParam.setPageStart(pp.getPageStart());
		mapperParam.setPageSize(pp.getPageSize());
		if (controllerParam.getReceptionTime() != null) {
			mapperParam.setReceptionTimeStart(DateUtilCustom.atStartOfDay(controllerParam.getReceptionTime()));
			mapperParam.setReceptionTimeEnd(DateUtilCustom.atEndOfDay(controllerParam.getReceptionTime()));
		}
		if (controllerParam.getOrgId() != null) {
			mapperParam.setOrgId(controllerParam.getOrgId());
		} else {
			List<Long> orgIdList = new ArrayList<Long>();
			StaffDetailPO staffDetail = staffDetailController.findByUserId(userId);
			List<OrganizationsPO> orgList = orgController.findBranchOrgByStaffId(staffDetail.getId());
			if (orgList == null || orgList.size() < 1) {
				orgIdList.add(0L);
			} else {
				orgList.stream().forEach(o -> orgIdList.add(o.getId()));
			}
			mapperParam.setOrgIdList(orgIdList);
		}
		mapperParam.setId(controllerParam.getId());
		mapperParam.setMobile(controllerParam.getMobile());
		mapperParam.setStaffName(controllerParam.getStaffName());
		mapperParam.setProductType(controllerParam.getProductType());
		int resultCount = reservationBusMapper.countReservationPageForAdmin(mapperParam);
		result.setResultCount(resultCount);
		List<ReservationsBusinessVO> voList = new ArrayList<ReservationsBusinessVO>();
		if (resultCount < 1) {
			result.setReservationList(voList);
			return result;
		}
		List<ReservationsBusinessBO> reservationBOList = reservationBusMapper.findReservationPageForAdmin(mapperParam);
		if (reservationBOList != null && reservationBOList.size() >= 1) {
			reservationBOList.stream().forEach(b -> voList.add(buildReservationBusinessVO(b)));
		}
		result.setReservationList(voList);
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult acceptReservationBusiness(AcceptReservationBusinessParam controllerParam) throws Exception {
		if (controllerParam.getStaffId() == null || controllerParam.getReservationId() == null) {
			return nullParam();
		}

		ReservationsBusinessPO rp = reservationBusMapper.findReservationBusiness(controllerParam.getReservationId());
		if (rp == null) {
			return errorParam();
		}

		UpdateReservationStatuParam updateParam = new UpdateReservationStatuParam();
		updateParam.setReservationId(controllerParam.getReservationId());
		updateParam.setReservationStatu(ReservationStatusType.accepted.getCode());
		updateParam.setStaffId(controllerParam.getStaffId());
		reservationBusMapper.updateReservationStatu(updateParam);
		
		String openId = userService.findOpenIdByUserId(rp.getUserId());
		OrganizationsPO org = orgController.findOne(rp.getOrgId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String reservationTime = sdf.format(rp.getReservationTimeStart()) + " ~ " + sdf.format(rp.getReservationTimeEnd());
		wxService.sendReservationSecuss(openId, "", org.getOrgName(), "", reservationTime, "");

		CommonResult result = new CommonResult();
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult rejectReservationBusiness(RejectReservationBusinessParam controllerParam) throws Exception {
		if (controllerParam.getReservationId() == null) {
			return nullParam();
		}

		if (StringUtils.isBlank(controllerParam.getRemark())) {
			CommonResult result = new CommonResult();
			result.normalFail();
			result.setMessage("请输入拒绝理由");
			return result;
		}

		ReservationsBusinessPO rp = reservationBusMapper.findReservationBusiness(controllerParam.getReservationId());
		if (rp == null) {
			return errorParam();
		}

		UpdateReservationStatuParam updateParam = new UpdateReservationStatuParam();
		updateParam.setReservationId(controllerParam.getReservationId());
		updateParam.setReservationStatu(ReservationStatusType.reject.getCode());
		updateParam.setRemark(controllerParam.getRemark());

		CommonResult result = new CommonResult();
		try {
			reservationBusMapper.updateReservationStatu(updateParam);
		} catch (Exception e) {
			result.normalFail();
			result.setMessage("拒绝理由过长");
			e.printStackTrace();
			return result;
		}

		String openId = userService.findOpenIdByUserId(rp.getUserId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String reservationTime = sdf.format(rp.getReservationTimeStart()) + " ~ " + sdf.format(rp.getReservationTimeEnd());
		wxService.sendReservationFail(openId, "", reservationTime, controllerParam.getRemark(), "");
		
		result.setIsSuccess();
		return result;
	}
}