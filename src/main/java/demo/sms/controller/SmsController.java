package demo.sms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;

import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.sms.service.SmsService;

@Controller
public class SmsController extends CommonController {
	
	@Autowired
	private SmsService smsService;

	public CommonResult sendNewValidCode(String mobileStr) throws ServerException, ClientException {
		return smsService.sendNewValidCode(mobileStr);
	}
	
	public CommonResult sendNewValidCode2(String mobileStr) throws ServerException, ClientException {
		return smsService.sendNewValidCode2(mobileStr);
	}
	
	public Integer findNewValidCode(String mobileStr) {
		return smsService.findNewValidCode(mobileStr);
	}
}
