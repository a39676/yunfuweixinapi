package demo.sms.service;

import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;

import demo.common.domain.result.CommonResult;

public interface SmsService {

	CommonResult sendNewValidCode(String mobileStr) throws ServerException, ClientException;

	Integer findNewValidCode(String mobileStr);

	CommonResult sendNewValidCode2(String mobileStr) throws ServerException, ClientException;

}
