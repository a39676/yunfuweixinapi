package demo.sms.service.impl;

import java.time.LocalDateTime;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import dateHandle.DateUtilCustom;
import demo.common.domain.result.CommonResult;
import demo.common.service.CommonService;
import demo.sms.domain.constant.SmsConstantName;
import demo.sms.domain.param.mapperParam.FindExistsJustSendParam;
import demo.sms.domain.po.SmsVerify;
import demo.sms.mapper.SmsConstantMapper;
import demo.sms.mapper.SmsVerifyMapper;
import demo.sms.service.SmsService;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

@Service
public class SmsServiceImpl extends CommonService implements SmsService {

	// 阿里雲短信固定參數,不能修改
	private static final String product = "Dysmsapi";
	private static final String domain = "dysmsapi.aliyuncs.com";

	// 短信驗證碼有效分鐘數
	private static final int validMinute = 5;

	@Autowired
	private SmsVerifyMapper smsVerifyMapper;
	@Autowired
	private SmsConstantMapper smsConstantMapper;

	private boolean validMobile(String mobile) {
		if (mobile == null) {
			return false;
		}
		return mobile.matches("^1[345789]\\d{9}");
	}

	private boolean justSend(Long mobile) {
		FindExistsJustSendParam param = new FindExistsJustSendParam();
		param.setMobile(mobile);
		param.setTargetTime(DateUtilCustom.localDateTimeToDate(LocalDateTime.now().minusMinutes(validMinute)));
		String result = String.valueOf(smsVerifyMapper.findExistsJustSend(param));
		if("0".equals(result)) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public CommonResult sendNewValidCode(String mobileStr) throws ServerException, ClientException {
		CommonResult result = new CommonResult();
		
		if(StringUtils.isBlank(mobileStr) || !validMobile(mobileStr)) {
			return nullParam();
		}
		
		Long mobile = Long.parseLong(mobileStr);
		
		if(justSend(mobile)) {
			result.failWithMessage("正在发送验证码,请稍候。");
			return result;
		}
		
		SendSmsResponse smsResponse = sendVerifySms(mobile);
		if (smsResponse.getCode() != null && smsResponse.getCode().equals("OK")) {
			result.setIsSuccess();
			return result;
		} else {
			return serviceError();
		}
	}
	
	@Override
	public CommonResult sendNewValidCode2(String mobileStr) throws ServerException, ClientException {
		CommonResult result = new CommonResult();
		
		if(StringUtils.isBlank(mobileStr) || !validMobile(mobileStr)) {
			return nullParam();
		}
		
		Long mobile = Long.parseLong(mobileStr);
		
		if(justSend(mobile)) {
			result.failWithMessage("正在发送验证码,请稍候。");
			return result;
		}
		
		SendSmsResponse smsResponse = sendVerifySms2(mobile);
		if (smsResponse.getCode() != null && smsResponse.getCode().equals("OK")) {
			result.setIsSuccess();
			return result;
		} else {
			return serviceError();
		}
	}

	private SendSmsResponse sendVerifySms(Long mobile) throws ServerException, ClientException {
		SendSmsResponse sendSmsResponse = null;

		if (mobile == null || !validMobile(mobile.toString())) {
			return new SendSmsResponse();
		}

		String accessKeyId = smsConstantMapper.getConstant(SmsConstantName.aliAccessKeyId);
		String accessKeySecret = smsConstantMapper.getConstant(SmsConstantName.accessKeySecret);

		if (StringUtils.isAnyBlank(accessKeyId, accessKeySecret)) {
			return new SendSmsResponse();
		}

		Integer randomCode = new Random().nextInt((999999 - 100000) + 1) + 100000;

		System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
		System.setProperty("sun.net.client.defaultReadTimeout", "10000");

		IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
		DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
		IAcsClient acsClient = new DefaultAcsClient(profile);

		SmsVerify newVerify = new SmsVerify();
		newVerify.setMobile(mobile);
		LocalDateTime now = LocalDateTime.now();
		newVerify.setValidTime(DateUtilCustom.localDateTimeToDate(now.plusMinutes(validMinute)));
		newVerify.setVerifyCode(randomCode.toString());
		smsVerifyMapper.insertSelective(newVerify);
		if (newVerify.getId() == null) {
			return new SendSmsResponse();
		}

		SendSmsRequest request = new SendSmsRequest();
		request.setPhoneNumbers(mobile.toString());
		request.setSignName("云浮市金融消费权益保护协");
		request.setTemplateCode("SMS_147419621");
		JSONObject json = new JSONObject();
		json.put("code", randomCode.toString());
		request.setTemplateParam(json.toString());
		request.setOutId(newVerify.getId().toString());

		sendSmsResponse = acsClient.getAcsResponse(request);

		// 测试环境修改
//		sendSmsResponse = new SendSmsResponse();
//		sendSmsResponse.setCode("OK");
//		randomCode = new Random().nextInt((999999 - 100000) + 1) + 100000;
//		sendSmsResponse.setBizId(randomCode.toString());
		// 测试环境修改结束
		if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
			smsVerifyMapper.updateWasSendAndBizId(sendSmsResponse.getBizId(), newVerify.getId(), true);
		} else {
			smsVerifyMapper.updateWasSendAndBizId(sendSmsResponse.getBizId(), newVerify.getId(), false);
		}
		
		return sendSmsResponse;
	}
	
	private SendSmsResponse sendVerifySms2(Long mobile) throws ServerException, ClientException {
		SendSmsResponse sendSmsResponse = null;

		if (mobile == null || !validMobile(mobile.toString())) {
			return new SendSmsResponse();
		}

		String accessKeyId = smsConstantMapper.getConstant(SmsConstantName.aliAccessKeyId);
		String accessKeySecret = smsConstantMapper.getConstant(SmsConstantName.accessKeySecret);

		if (StringUtils.isAnyBlank(accessKeyId, accessKeySecret)) {
			return new SendSmsResponse();
		}

		Integer randomCode = new Random().nextInt((999999 - 100000) + 1) + 100000;

		System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
		System.setProperty("sun.net.client.defaultReadTimeout", "10000");

		IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
		DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
		IAcsClient acsClient = new DefaultAcsClient(profile);

		SmsVerify newVerify = new SmsVerify();
		newVerify.setMobile(mobile);
		LocalDateTime now = LocalDateTime.now();
		newVerify.setValidTime(DateUtilCustom.localDateTimeToDate(now.plusMinutes(validMinute)));
		newVerify.setVerifyCode(randomCode.toString());
		smsVerifyMapper.insertSelective(newVerify);
		if (newVerify.getId() == null) {
			return new SendSmsResponse();
		}

		SendSmsRequest request = new SendSmsRequest();
		request.setPhoneNumbers(mobile.toString());
		request.setSignName("云浮市金融消费权益保护协");
		request.setTemplateCode("SMS_151085062");
		JSONObject json = new JSONObject();
		json.put("code", randomCode.toString());
		request.setTemplateParam(json.toString());
		request.setOutId(newVerify.getId().toString());

		sendSmsResponse = acsClient.getAcsResponse(request);

		// 测试环境修改
//		sendSmsResponse = new SendSmsResponse();
//		sendSmsResponse.setCode("OK");
//		randomCode = new Random().nextInt((999999 - 100000) + 1) + 100000;
//		sendSmsResponse.setBizId(randomCode.toString());
		// 测试环境修改结束
		if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
			smsVerifyMapper.updateWasSendAndBizId(sendSmsResponse.getBizId(), newVerify.getId(), true);
		} else {
			smsVerifyMapper.updateWasSendAndBizId(sendSmsResponse.getBizId(), newVerify.getId(), false);
		}
		
		return sendSmsResponse;
	}

	@Override
	public Integer findNewValidCode(String mobileStr) {
		if(StringUtils.isBlank(mobileStr) || !validMobile(mobileStr)) {
			return -1;
		}
		
		SmsVerify smsVerify = smsVerifyMapper.findNewValidCode(Long.parseLong(mobileStr));
		if(smsVerify == null || !NumericUtilCustom.matchInteger(smsVerify.getVerifyCode())) {
			return -1;
		}
		
		Integer verifyCode = Integer.parseInt(smsVerify.getVerifyCode());
		return verifyCode;
	}

}
