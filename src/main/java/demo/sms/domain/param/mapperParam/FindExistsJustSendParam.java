package demo.sms.domain.param.mapperParam;

import java.util.Date;

public class FindExistsJustSendParam {

	private Long mobile;
	private Date targetTime;

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public Date getTargetTime() {
		return targetTime;
	}

	public void setTargetTime(Date targetTime) {
		this.targetTime = targetTime;
	}

	@Override
	public String toString() {
		return "FindExistsJustSendParam [mobile=" + mobile + ", targetTime=" + targetTime + "]";
	}

}
