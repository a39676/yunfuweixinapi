package demo.sms.domain.po;

import java.util.Date;

public class SmsVerify {
    private Long id;

	private Long mobile;

	private String bizId;

	private String verifyCode;

	private Date createTime;

	private Date validTime;

	private Boolean wasUsed;

	private Boolean wasSend;

	private Boolean wasRecived;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId == null ? null : bizId.trim();
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode == null ? null : verifyCode.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getValidTime() {
		return validTime;
	}

	public void setValidTime(Date validTime) {
		this.validTime = validTime;
	}

	public Boolean getWasUsed() {
		return wasUsed;
	}

	public void setWasUsed(Boolean wasUsed) {
		this.wasUsed = wasUsed;
	}

	public Boolean getWasSend() {
		return wasSend;
	}

	public void setWasSend(Boolean wasSend) {
		this.wasSend = wasSend;
	}

	public Boolean getWasRecived() {
		return wasRecived;
	}

	public void setWasRecived(Boolean wasRecived) {
		this.wasRecived = wasRecived;
	}

}