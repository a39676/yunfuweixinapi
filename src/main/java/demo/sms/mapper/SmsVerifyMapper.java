package demo.sms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import demo.sms.domain.param.mapperParam.FindExistsJustSendParam;
import demo.sms.domain.po.SmsVerify;

public interface SmsVerifyMapper {
	int insert(SmsVerify record);

	int insertSelective(SmsVerify record);

    SmsVerify findNewValidCode(Long mobile);
    
    int batchUpdateWasRecived(List<Long> idList);
    
    Integer findExistsJustSend(FindExistsJustSendParam mapperParam);
    
    int updateWasSendAndBizId(@Param("bizId")String bizId, @Param("id")Long id, @Param("wasSend")Boolean wasSend);
}