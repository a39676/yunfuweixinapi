package demo.sms.mapper;

import demo.sms.domain.po.SmsConstant;

public interface SmsConstantMapper {
    int insert(SmsConstant record);

    int insertSelective(SmsConstant record);
    
    String getConstant(String constantName);
}