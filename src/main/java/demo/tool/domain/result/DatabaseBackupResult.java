package demo.tool.domain.result;

import demo.common.domain.result.CommonResult;

public class DatabaseBackupResult extends CommonResult {

	private String filePath;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
