package demo.tool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import demo.base.user.service.UsersService;
import demo.business.service.BusinessService;
import demo.tool.service.TaskToolService;
import demo.weixin.domain.po.WXStateOid;
import demo.weixin.service.WeixinService;

@Component
public class TaskToolServiceImpl implements TaskToolService {

	@Autowired
	private BusinessService businessService;
	@Autowired
	private WeixinService wxService;
	@Autowired
	private UsersService userService;

	/** 删除过期的用户零钞兑换记录 */
	@Scheduled(cron = "03 02 01 * * *") // 每天01:02:03执行
	public void deleteExpiredExchangeDibUserLimit() {
		businessService.deleteExpiredExchangeDibRecord();
	}

	@Scheduled(cron="0 */2 * * * ?") // 每2分钟执行一次
	public void bindWXOpenIdAndMobile() {
		wxService.getOpenIdByCode();
		List<WXStateOid> wxStateOidList = wxService.findOpenIdAndMobile();
		userService.updateOpenIdWithMobile(wxStateOidList);
	}
	
	@Scheduled(cron="0 */20 * * * ?") // 每20分钟执行一次
	public void deleteOldWxStateOid() {
		wxService.deleteOldRecord();
	}
	
	/** 发送滞留的微信消息 */
	@Scheduled(cron="0 */15 * * * ?") // 每15分钟执行一次
	public void sendMessageLine() {
		if(wxService.isPushTime()) {
			wxService.sendMessageLine();
		}
	}
	
	/** 删除已发送的微信消息 */
	@Scheduled(cron = "08 05 01 * * *") // 每天01:05:08执行
	public void deleteByMark() {
		wxService.deleteByMark();
	}
}
