package demo.organizations.domain.param.mapperParam;

public class UpdateOrgInfo {

	private Long id;
	private String orgCode;
	private String orgName;
	private String orgType;
	private Long topHeadOrgId;
	private Long belongTo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public Long getTopHeadOrgId() {
		return topHeadOrgId;
	}

	public void setTopHeadOrgId(Long topHeadOrgId) {
		this.topHeadOrgId = topHeadOrgId;
	}

	public Long getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(Long belongTo) {
		this.belongTo = belongTo;
	}

	@Override
	public String toString() {
		return "UpdateOrgInfoParam [id=" + id + ", orgCode=" + orgCode + ", orgName=" + orgName + ", orgType=" + orgType
				+ ", topHeadOrgId=" + topHeadOrgId + "]";
	}

}
