package demo.organizations.domain.param.mapperParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindOrgByGeographicalIdParam implements CommonControllerParam{

	private Long geographicalId;

	public Long getGeographicalId() {
		return geographicalId;
	}

	public void setGeographicalId(Long geographicalId) {
		this.geographicalId = geographicalId;
	}

	@Override
	public FindOrgByGeographicalIdParam fromJson(JSONObject json) {
		FindOrgByGeographicalIdParam param = new FindOrgByGeographicalIdParam();
		if(json.containsKey("geographicalId") && NumericUtilCustom.matchInteger(json.getString("geographicalId"))) {
			param.setGeographicalId(json.getLong("geographicalId"));
		}
		return param;
	}

}
