package demo.organizations.domain.param.mapperParam;

import java.util.List;

public class FindByGeographicalAreaAndTopHeadOrgMapperParam {

	private Long topHeadOrgId;
	private List<Long> geoIdList;

	public Long getTopHeadOrgId() {
		return topHeadOrgId;
	}

	public void setTopHeadOrgId(Long topHeadOrgId) {
		this.topHeadOrgId = topHeadOrgId;
	}

	public List<Long> getGeoIdList() {
		return geoIdList;
	}

	public void setGeoIdList(List<Long> geoIdList) {
		this.geoIdList = geoIdList;
	}

	@Override
	public String toString() {
		return "FindByGeographicalAreaAndTopHeadOrgMapper [topHeadOrgId=" + topHeadOrgId + "]";
	}

}
