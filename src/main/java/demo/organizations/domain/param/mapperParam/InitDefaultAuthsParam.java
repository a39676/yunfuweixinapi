package demo.organizations.domain.param.mapperParam;

import java.util.List;

import demo.organizations.domain.po.AuthsPO;

public class InitDefaultAuthsParam {

	private List<AuthsPO> authList;

	public List<AuthsPO> getAuthList() {
		return authList;
	}

	public void setAuthList(List<AuthsPO> authList) {
		this.authList = authList;
	}

	@Override
	public String toString() {
		return "InitDefaultAuthsParam [authList=" + authList + "]";
	}

}
