package demo.organizations.domain.param.mapperParam;

import demo.common.domain.param.PageParam;

public class FindOrgByConditionParam extends PageParam {

	private Long id;
	private String orgCode;
	private String orgName;
	private Integer orgType;
	private String staffNumber;
	private String staffName;
	private Long mobile;
	private Long geographicalAreaId;
	private Boolean isDelete = false;
	private Long topHeadOrg;

	public Long getTopHeadOrg() {
		return topHeadOrg;
	}

	public void setTopHeadOrg(Long topHeadOrg) {
		this.topHeadOrg = topHeadOrg;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getOrgType() {
		return orgType;
	}

	public void setOrgType(Integer orgType) {
		this.orgType = orgType;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public Long getGeographicalAreaId() {
		return geographicalAreaId;
	}

	public void setGeographicalAreaId(Long geographicalAreaId) {
		this.geographicalAreaId = geographicalAreaId;
	}

	@Override
	public String toString() {
		return "FindOrgByConditionParam [orgCode=" + orgCode + ", orgName=" + orgName + ", orgType=" + orgType
				+ ", staffNumber=" + staffNumber + ", staffName=" + staffName + ", mobile=" + mobile
				+ ", geographicalAreaId=" + geographicalAreaId + ", isDelete=" + isDelete + "]";
	}

}
