package demo.organizations.domain.param.mapperParam;

public class DeleteOrg {

	private Long id;
	private Boolean isDelete = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "DeleteOrgParam [id=" + id + ", isDelete=" + isDelete + "]";
	}

}
