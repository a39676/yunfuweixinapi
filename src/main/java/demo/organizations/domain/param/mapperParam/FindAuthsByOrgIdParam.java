package demo.organizations.domain.param.mapperParam;

public class FindAuthsByOrgIdParam {

	private Long orgId;
	private Boolean isDelete = false;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "FindAuthsByGroupIdParam [orgId=" + orgId + ", isDelete=" + isDelete + "]";
	}

}
