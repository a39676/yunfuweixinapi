package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class CreateNewOrgParam implements CommonControllerParam {

	private Integer orgType;
	private String orgCode;
	private Long belongTo;
	private Long topHeadOrg;
	private String orgName;
	private Long geogralphicalId;

	@Override
	public String toString() {
		return "CreateNewOrgParam [orgType=" + orgType + ", orgCode=" + orgCode + ", belongTo=" + belongTo
				+ ", topHeadOrg=" + topHeadOrg + ", orgName=" + orgName + "]";
	}

	public Long getGeogralphicalId() {
		return geogralphicalId;
	}

	public void setGeogralphicalId(Long geogralphicalId) {
		this.geogralphicalId = geogralphicalId;
	}

	public Integer getOrgType() {
		return orgType;
	}

	public void setOrgType(Integer orgType) {
		this.orgType = orgType;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public Long getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(Long belongTo) {
		this.belongTo = belongTo;
	}

	public Long getTopHeadOrg() {
		return topHeadOrg;
	}

	public void setTopHeadOrg(Long topHeadOrg) {
		this.topHeadOrg = topHeadOrg;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	@Override
	public CreateNewOrgParam fromJson(JSONObject json) {
		CreateNewOrgParam param = new CreateNewOrgParam();
		if(json.containsKey("belongTo") && NumericUtilCustom.matchInteger(json.getString("belongTo"))) {
			param.setBelongTo(json.getLong("belongTo"));
		}
		if(json.containsKey("orgCode")) {
			param.setOrgCode(json.getString("orgCode"));
		}
		if(json.containsKey("orgType") && NumericUtilCustom.matchInteger(json.getString("orgType"))) {
			param.setOrgType(json.getInt("orgType"));
		}
		if(json.containsKey("topHeadOrg") && NumericUtilCustom.matchInteger(json.getString("topHeadOrg"))) {
			param.setTopHeadOrg(json.getLong("topHeadOrg"));
		}
		if(json.containsKey("orgName")) {
			param.setOrgName(json.getString("orgName"));
		}
		if(json.containsKey("geogralphicalId") && NumericUtilCustom.matchInteger(json.getString("geogralphicalId"))) {
			param.setGeogralphicalId(json.getLong("geogralphicalId"));
		}
		
		
		return param;
	}

}
