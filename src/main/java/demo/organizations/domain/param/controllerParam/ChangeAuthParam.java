package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class ChangeAuthParam implements CommonControllerParam {

	private Long userId;
	private Long newAuthId;

	@Override
	public ChangeAuthParam fromJson(JSONObject j) {
		ChangeAuthParam p = new ChangeAuthParam();
		if (j.containsKey("userId") && NumericUtilCustom.matchInteger(j.getString("userId"))) {
			p.setUserId(j.getLong("userId"));
		}
		if (j.containsKey("newAuthId") && NumericUtilCustom.matchInteger(j.getString("newAuthId"))) {
			p.setNewAuthId(j.getLong("newAuthId"));
		}
		return p;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getNewAuthId() {
		return newAuthId;
	}

	public void setNewAuthId(Long newAuthId) {
		this.newAuthId = newAuthId;
	}

	@Override
	public String toString() {
		return "ChangeAuthParam [userId=" + userId + ", newAuthId=" + newAuthId + "]";
	}

}
