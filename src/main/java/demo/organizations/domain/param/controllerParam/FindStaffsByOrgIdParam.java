package demo.organizations.domain.param.controllerParam;

import org.apache.commons.lang3.StringUtils;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindStaffsByOrgIdParam implements CommonControllerParam {

	private Long orgId;
	private String staffNumber;
	private String staffName;
	private String mobile;
	private Integer pageSize;
	private Integer pageNo;

	@Override
	public FindStaffsByOrgIdParam fromJson(JSONObject j) {
		FindStaffsByOrgIdParam p = new FindStaffsByOrgIdParam();
		if (j.containsKey("orgId") && NumericUtilCustom.matchInteger(j.getString("orgId"))) {
			p.setOrgId(j.getLong("orgId"));
		}
		if (j.containsKey("staffNumber") && StringUtils.isNotBlank(j.getString("staffNumber"))) {
			p.setStaffNumber(j.getString("staffNumber"));
		}
		if (j.containsKey("staffName") && StringUtils.isNotBlank(j.getString("staffName"))) {
			p.setStaffName(j.getString("staffName"));
		}
		if (j.containsKey("mobile") && StringUtils.isNotBlank(j.getString("mobile"))) {
			p.setMobile(j.getString("mobile"));
		}
		if (j.containsKey("pageSize") && NumericUtilCustom.matchInteger(j.getString("pageSize"))) {
			p.setPageSize(j.getInt("pageSize"));
		}
		if (j.containsKey("pageNo") && NumericUtilCustom.matchInteger(j.getString("pageNo"))) {
			p.setPageNo(j.getInt("pageNo"));
		}
		return p;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

}
