package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindByDirectBelongToParam implements CommonControllerParam {

	private Long orgId;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	@Override
	public FindByDirectBelongToParam fromJson(JSONObject j) {
		FindByDirectBelongToParam p = new FindByDirectBelongToParam();
		if (j.containsKey("orgId") && NumericUtilCustom.matchInteger(j.getString("orgId"))) {
			p.setOrgId(j.getLong("orgId"));
		}
		return p;
	}
}
