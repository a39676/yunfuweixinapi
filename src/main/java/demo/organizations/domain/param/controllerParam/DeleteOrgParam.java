package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class DeleteOrgParam implements CommonControllerParam {

	private Long orgId;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	@Override
	public DeleteOrgParam fromJson(JSONObject json) {
		DeleteOrgParam param = new DeleteOrgParam();
		if (json.containsKey("id") && NumericUtilCustom.matchInteger(json.getString("id"))) {
			param.setOrgId(json.getLong("id"));
		}
		return param;
	}

}
