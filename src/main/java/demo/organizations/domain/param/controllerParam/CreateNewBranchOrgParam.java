package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class CreateNewBranchOrgParam implements CommonControllerParam {

	private String orgCode;
	private Long belongTo;
	private String orgName;
	private Long geogralphicalId;

	@Override
	public CreateNewBranchOrgParam fromJson(JSONObject json) {
		CreateNewBranchOrgParam param = new CreateNewBranchOrgParam();
		if (json.containsKey("belongTo") && NumericUtilCustom.matchInteger(json.getString("belongTo"))) {
			param.setBelongTo(json.getLong("belongTo"));
		}
		if (json.containsKey("orgCode")) {
			param.setOrgCode(json.getString("orgCode"));
		}
		if (json.containsKey("orgName")) {
			param.setOrgName(json.getString("orgName"));
		}
		if (json.containsKey("geogralphicalId") && NumericUtilCustom.matchInteger(json.getString("geogralphicalId"))) {
			param.setGeogralphicalId(json.getLong("geogralphicalId"));
		}

		return param;
	}

	@Override
	public String toString() {
		return "CreateNewBranchOrgParam [orgCode=" + orgCode + ", belongTo=" + belongTo + ", orgName=" + orgName
				+ ", geogralphicalId=" + geogralphicalId + "]";
	}

	public Long getGeogralphicalId() {
		return geogralphicalId;
	}

	public void setGeogralphicalId(Long geogralphicalId) {
		this.geogralphicalId = geogralphicalId;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public Long getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(Long belongTo) {
		this.belongTo = belongTo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
