package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import demo.organizations.domain.param.mapperParam.UpdateOrgInfo;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class UpdateOrgInfoParam extends UpdateOrgInfo implements CommonControllerParam {

	@Override
	public UpdateOrgInfoParam fromJson(JSONObject json) {
		UpdateOrgInfoParam param = new UpdateOrgInfoParam();
		if(json.containsKey("orgCode")) {
			param.setOrgCode(json.getString("orgCode"));
		}
		if(json.containsKey("orgName")) {
			param.setOrgName(json.getString("orgName"));
		}
		if(json.containsKey("orgType") && NumericUtilCustom.matchInteger(json.getString("orgType"))) {
			param.setOrgType(json.getString("orgType"));
		}
		if(json.containsKey("id") && NumericUtilCustom.matchInteger(json.getString("id"))) {
			param.setId(json.getLong("id"));
		}
		if(json.containsKey("topHeadOrgId") && NumericUtilCustom.matchInteger(json.getString("topHeadOrgId"))) {
			param.setTopHeadOrgId(json.getLong("topHeadOrgId"));
		}
		if(json.containsKey("belongTo") && NumericUtilCustom.matchInteger(json.getString("belongTo"))) {
			param.setBelongTo(json.getLong("belongTo"));
		}
		
		return param;
	}

}
