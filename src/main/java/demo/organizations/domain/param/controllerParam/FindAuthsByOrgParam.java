package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindAuthsByOrgParam implements CommonControllerParam {

	private Long orgId;
	private Boolean isDelete = false;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public FindAuthsByOrgParam fromJson(JSONObject json) {
		FindAuthsByOrgParam p = new FindAuthsByOrgParam();
		if(json.containsKey("orgId") && NumericUtilCustom.matchInteger(json.getString("orgId"))) {
			p.setOrgId(json.getLong("orgId"));
		}
		if(json.containsKey("isDelete") && "1".equals(json.getString("isDelete"))) {
			p.setIsDelete(true);
		}
		return p;
	}

}
