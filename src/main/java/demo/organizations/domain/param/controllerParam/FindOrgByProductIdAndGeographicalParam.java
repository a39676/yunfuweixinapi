package demo.organizations.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindOrgByProductIdAndGeographicalParam implements CommonControllerParam {

	private Long productId;
	private Long geographicalId;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getGeographicalId() {
		return geographicalId;
	}

	public void setGeographicalId(Long geographicalId) {
		this.geographicalId = geographicalId;
	}

	
	@Override
	public String toString() {
		return "FindOrgByProductIdAndGeographicalParam [productId=" + productId + ", geographicalId=" + geographicalId
				+ "]";
	}

	@Override
	public FindOrgByProductIdAndGeographicalParam fromJson(JSONObject json) {
		FindOrgByProductIdAndGeographicalParam param = new FindOrgByProductIdAndGeographicalParam();
		if(json.containsKey("productId") && NumericUtilCustom.matchInteger(json.getString("productId"))) {
			param.setProductId(json.getLong("productId"));
		}
		if(json.containsKey("geographicalId") && NumericUtilCustom.matchInteger(json.getString("geographicalId"))) {
			param.setGeographicalId(json.getLong("geographicalId"));
		}
		return param;
	}

}
