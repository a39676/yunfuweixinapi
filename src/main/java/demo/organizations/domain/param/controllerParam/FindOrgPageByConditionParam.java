package demo.organizations.domain.param.controllerParam;

import org.apache.commons.lang3.StringUtils;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindOrgPageByConditionParam implements CommonControllerParam {

	private String orgCode;
	private String orgName;
	private Integer orgType;
	private String staffNumber;
	private String staffName;
	private Long mobile;
	private Long geographicalAreaId;
	private Integer pageSize;
	private Integer pageNo;
	private Long topHeadOrg;

	@Override
	public FindOrgPageByConditionParam fromJson(JSONObject json) {
		FindOrgPageByConditionParam param = new FindOrgPageByConditionParam();
		if (json.containsKey("topHeadOrg") && NumericUtilCustom.matchInteger(json.getString("topHeadOrg"))) {
			param.setTopHeadOrg(json.getLong("pageSize"));
		}
		if (json.containsKey("pageSize") && NumericUtilCustom.matchInteger(json.getString("pageSize"))) {
			param.setPageSize(json.getInt("pageSize"));
		}
		if (json.containsKey("pageNo") && NumericUtilCustom.matchInteger(json.getString("pageNo"))) {
			param.setPageNo(json.getInt("pageNo"));
		}
		if (json.containsKey("orgType") && NumericUtilCustom.matchInteger(json.getString("orgType"))) {
			param.setOrgType(json.getInt("orgType"));
		}
		if (json.containsKey("mobile") && NumericUtilCustom.matchInteger(json.getString("mobile"))) {
			param.setMobile(json.getLong("mobile"));
		}
		if (json.containsKey("geographicalAreaId")
				&& NumericUtilCustom.matchInteger(json.getString("geographicalAreaId"))) {
			param.setGeographicalAreaId(json.getLong("geographicalAreaId"));
		}

		if (json.containsKey("orgCode") && StringUtils.isNotBlank(json.getString("orgCode"))) {
			param.setOrgCode(json.getString("orgCode"));
		}
		if (json.containsKey("orgName") && StringUtils.isNotBlank(json.getString("orgName"))) {
			param.setOrgName(json.getString("orgName"));
		}
		if (json.containsKey("staffNumber")) {
			param.setStaffNumber(json.getString("staffNumber"));
		}
		if (json.containsKey("staffName")) {
			param.setStaffName(json.getString("staffName"));
		}
		return param;
	}

	@Override
	public String toString() {
		return "FindOrgPageByConditionParam [orgCode=" + orgCode + ", orgName=" + orgName + ", orgType=" + orgType
				+ ", staffNumber=" + staffNumber + ", staffName=" + staffName + ", mobile=" + mobile
				+ ", geographicalAreaId=" + geographicalAreaId + "]";
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getOrgType() {
		return orgType;
	}

	public void setOrgType(Integer orgType) {
		this.orgType = orgType;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public Long getGeographicalAreaId() {
		return geographicalAreaId;
	}

	public void setGeographicalAreaId(Long geographicalAreaId) {
		this.geographicalAreaId = geographicalAreaId;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Long getTopHeadOrg() {
		return topHeadOrg;
	}

	public void setTopHeadOrg(Long topHeadOrg) {
		this.topHeadOrg = topHeadOrg;
	};

}
