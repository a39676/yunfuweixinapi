package demo.organizations.domain.type;

/**
 * 机构类型 
 */
public enum OrganizationsType {
	
	union("协会", 1), // 协会
	bank("银行", 2), // 银行
	securities("证券", 3), // 证券
	insurance("保险", 4), // 保险行
	other("其他", 5), // 其他
	;
	
	private String typeName;
	private Integer typeCode;
	
	OrganizationsType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static OrganizationsType getType(String typeName) {
		for(OrganizationsType t : OrganizationsType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static OrganizationsType getType(Integer typeCode) {
		for(OrganizationsType t : OrganizationsType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
