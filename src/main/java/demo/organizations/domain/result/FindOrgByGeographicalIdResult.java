package demo.organizations.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.po.OrganizationsPO;

public class FindOrgByGeographicalIdResult extends CommonResult {

	private List<OrganizationsPO> orgList;

	public List<OrganizationsPO> getOrgList() {
		return orgList;
	}

	public void setOrgList(List<OrganizationsPO> orgList) {
		this.orgList = orgList;
	}

}
