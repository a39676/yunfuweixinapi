package demo.organizations.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.vo.OrganizationsVO;

public class FindOrgPageResult extends CommonResult {

	private List<OrganizationsVO> orgPage;

	private int resultCount = 0;

	public List<OrganizationsVO> getOrgPage() {
		return orgPage;
	}

	public void setOrgPage(List<OrganizationsVO> orgPage) {
		this.orgPage = orgPage;
	}

	public int getResultCount() {
		return resultCount;
	}

	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}

	@Override
	public String toString() {
		return "FindOrgPageResult [orgPage=" + orgPage + ", resultCount=" + resultCount + "]";
	}

}
