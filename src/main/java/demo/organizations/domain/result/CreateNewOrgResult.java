package demo.organizations.domain.result;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.po.OrganizationsPO;

public class CreateNewOrgResult extends CommonResult {

	private OrganizationsPO newOrg;

	public OrganizationsPO getNewOrg() {
		return newOrg;
	}

	public void setNewOrg(OrganizationsPO newOrg) {
		this.newOrg = newOrg;
	}

	@Override
	public String toString() {
		return "CreateNewOrgResult [newOrg=" + newOrg + "]";
	}

}
