package demo.organizations.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.vo.StaffDetailVO;

public class FindStaffsByConditionResult extends CommonResult {

	private List<StaffDetailVO> staffList;

	private Integer resultCount;

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	public List<StaffDetailVO> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<StaffDetailVO> staffList) {
		this.staffList = staffList;
	}

	@Override
	public String toString() {
		return "FindStaffsByConditionResult [staffList=" + staffList + ", resultCount=" + resultCount + "]";
	}

}
