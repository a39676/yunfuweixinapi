package demo.organizations.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.po.AuthsPO;

public class FindAuthsByOrgIdResult extends CommonResult {

	private List<AuthsPO> authList;

	public List<AuthsPO> getAuthList() {
		return authList;
	}

	public void setAuthList(List<AuthsPO> authList) {
		this.authList = authList;
	}

	@Override
	public String toString() {
		return "FindAuthsByOrgIdResult [authList=" + authList + "]";
	}

}
