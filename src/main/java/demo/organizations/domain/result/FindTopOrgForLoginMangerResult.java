package demo.organizations.domain.result;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.po.OrganizationsPO;

public class FindTopOrgForLoginMangerResult extends CommonResult {

	private OrganizationsPO org;

	public OrganizationsPO getOrg() {
		return org;
	}

	public void setOrg(OrganizationsPO org) {
		this.org = org;
	}

}
