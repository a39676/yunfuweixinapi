package demo.organizations.domain.result;

import demo.common.domain.result.CommonResult;

public class CopyAuthResult extends CommonResult {

	private Long newAuthId;

	public Long getNewAuthId() {
		return newAuthId;
	}

	public void setNewAuthId(Long newAuthId) {
		this.newAuthId = newAuthId;
	}

	@Override
	public String toString() {
		return "CopyAuthResult [newAuthId=" + newAuthId + "]";
	}

}
