package demo.organizations.domain.po;

import java.util.Date;

/**
 * 机构对应地域关联表
 *
 */
public class OrganizationGeographicalArea {
    private Long id;

    private Date createTime;

    private Long orgId;

    private Long geographicalAreaId;

    private Boolean isDelete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getGeographicalAreaId() {
        return geographicalAreaId;
    }

    public void setGeographicalAreaId(Long geographicalAreaId) {
        this.geographicalAreaId = geographicalAreaId;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }
}