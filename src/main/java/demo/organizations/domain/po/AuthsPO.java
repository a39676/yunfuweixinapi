package demo.organizations.domain.po;

import java.util.Date;

import demo.base.base.domain.po.RoleGroupsPO;

/**
 * 机构角色
 */
public class AuthsPO {
    private Long id;

    /** 角色名 */
    private String authName;

    /** 
     * {@link RoleGroupsPO }
     * 对应权限组ID 
     * */
    private Long roleGroupId;

    private Date createTime;

    private Boolean isDelete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName == null ? null : authName.trim();
    }

    public Long getRoleGroupId() {
        return roleGroupId;
    }

    public void setRoleGroupId(Long roleGroupId) {
        this.roleGroupId = roleGroupId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }
}