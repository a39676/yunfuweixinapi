package demo.organizations.domain.po;

import java.util.Date;

/** 机构,工作人员关联表  */
public class OrganizationStaffPO {
	/** 机构ID */
    private Long orgId;
    
    /** 工作人员ID */
    private Long staffId;

    private Date createTime;

    private Boolean isDelete;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }
}