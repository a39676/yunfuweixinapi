package demo.organizations.domain.po;

import java.util.Date;

/** 工作人員表 */
public class StaffAuthPO {
    /** 工作人员ID */
	private Long staffId;
	/** 对应角色ID  */
    private Long authId;

    private Date createTime;

    private Boolean isDelete;

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getAuthId() {
        return authId;
    }

    public void setAuthId(Long authId) {
        this.authId = authId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }
}