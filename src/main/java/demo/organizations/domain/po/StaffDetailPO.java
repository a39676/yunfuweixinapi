package demo.organizations.domain.po;

import java.util.Date;

/** 工作人員細節ID */
public class StaffDetailPO {
	private Long id;

	private Date createTime;

	private Long userId;

	private Integer positionType;

	private String staffsName;

	private Long wxAccount;
	/** 工号 */
	private String staffNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getPositionType() {
		return positionType;
	}

	public void setPositionType(Integer positionType) {
		this.positionType = positionType;
	}

	public String getStaffsName() {
		return staffsName;
	}

	public void setStaffsName(String staffsName) {
		this.staffsName = staffsName == null ? null : staffsName.trim();
	}

	public Long getWxAccount() {
		return wxAccount;
	}

	public void setWxAccount(Long wxAccount) {
		this.wxAccount = wxAccount;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber == null ? null : staffNumber.trim();
	}
}