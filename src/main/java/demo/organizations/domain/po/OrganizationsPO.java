package demo.organizations.domain.po;

import java.util.Date;

import demo.organizations.domain.type.OrganizationsType;

/**
 * 机构表
 *
 */
public class OrganizationsPO {

	private Long id;
	/** 机构名 */
	private String orgName;
	/**
	 * 机构类型 {@link OrganizationsType}
	 */
	private Integer orgType;
	/** 机构代码 */
	private String orgCode;
	/** 隶属机构ID */
	private Long belongTo;
	/** 隶属机构顶端的机构ID */
	private Long topHeadOrg;
	private Date createTime;
	private Date editTime;
	private Integer editCount;
	private Boolean isDelete;
	/** 已认证 */
	private Boolean isAuthenticate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName == null ? null : orgName.trim();
	}

	public Integer getOrgType() {
		return orgType;
	}

	public void setOrgType(Integer orgType) {
		this.orgType = orgType;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode == null ? null : orgCode.trim();
	}

	public Long getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(Long belongTo) {
		this.belongTo = belongTo;
	}

	public Long getTopHeadOrg() {
		return topHeadOrg;
	}

	public void setTopHeadOrg(Long topHeadOrg) {
		this.topHeadOrg = topHeadOrg;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public Integer getEditCount() {
		return editCount;
	}

	public void setEditCount(Integer editCount) {
		this.editCount = editCount;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Boolean getIsAuthenticate() {
		return isAuthenticate;
	}

	public void setIsAuthenticate(Boolean isAuthenticate) {
		this.isAuthenticate = isAuthenticate;
	}

}