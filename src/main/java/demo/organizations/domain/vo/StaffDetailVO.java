package demo.organizations.domain.vo;

import demo.organizations.domain.po.StaffDetailPO;

public class StaffDetailVO extends StaffDetailPO {

	private String mobile;
	private Integer gender;
	private String authName;
	private Long authId;
	private String openId;

	@Override
	public String toString() {
		return "StaffDetailVO [mobile=" + mobile + ", gender=" + gender + ", authName=" + authName + ", authId="
				+ authId + ", openId=" + openId + "]";
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}

	public Long getAuthId() {
		return authId;
	}

	public void setAuthId(Long authId) {
		this.authId = authId;
	}

}
