package demo.organizations.domain.vo;

import demo.organizations.domain.po.OrganizationsPO;

public class OrganizationsVO extends OrganizationsPO {

	private String staffName;

	private String staffNumber;

	private String mobile;

	private String belongOrgName;

	private String areaName;

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getBelongOrgName() {
		return belongOrgName;
	}

	public void setBelongOrgName(String belongOrgName) {
		this.belongOrgName = belongOrgName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "OrganizationsVO [staffName=" + staffName + ", staffNumber=" + staffNumber + ", mobile=" + mobile
				+ ", getId()=" + getId() + ", getOrgName()=" + getOrgName() + ", getOrgType()=" + getOrgType()
				+ ", getOrgCode()=" + getOrgCode() + ", getBelongTo()=" + getBelongTo() + ", getTopHeadOrg()="
				+ getTopHeadOrg() + ", getCreateTime()=" + getCreateTime() + ", getEditTime()=" + getEditTime()
				+ ", getEditCount()=" + getEditCount() + ", getIsDelete()=" + getIsDelete() + ", getIsAuthenticate()="
				+ getIsAuthenticate() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
