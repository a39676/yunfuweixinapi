package demo.organizations.domain.constant;

import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.type.OrganizationsType;

public class OrgDefault {
	
	public static final OrganizationsPO defaultOrgUnion;
	public static final Long orgUnionId = 1L;
	public static final String orgUnionName = "协会";
	
	static {
		defaultOrgUnion = new OrganizationsPO();
		defaultOrgUnion.setId(orgUnionId);
		defaultOrgUnion.setIsAuthenticate(true);
		defaultOrgUnion.setOrgName(orgUnionName);
		defaultOrgUnion.setOrgType(OrganizationsType.union.getCode());
		defaultOrgUnion.setTopHeadOrg(orgUnionId);
	}

}
