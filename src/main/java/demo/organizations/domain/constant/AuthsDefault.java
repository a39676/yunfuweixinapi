package demo.organizations.domain.constant;

import java.util.ArrayList;
import java.util.List;

import demo.base.base.domain.constant.RoleGroupDefault;
import demo.organizations.domain.po.AuthsPO;

public class AuthsDefault {

	public static final String AUTH_BRANCH_ADMIN_NAME = "机构管理员";
	public static final String AUTH_BUSINESS_HANDLER_NAME = "网点业务员";
	public static final String AUTH_BRANCH_HANDLER_NAME = "网点经办员";
	public static final String AUTH_UNION_MANAGE_NAME = "平台管理员";
	
	public static final Long AUTH_BRANCH_ADMIN_ID = 1L;
	public static final Long AUTH_BUSINESS_HANDLER_ID = 2L;
	public static final Long AUTH_BRANCH_HANDLER_ID = 3L;
	public static final Long AUTH_UNION_MANAGE_ID = 4L;
	
	public static final List<AuthsPO> authsList;
	
	static {
		authsList = new ArrayList<AuthsPO>();
		AuthsPO po = new AuthsPO();
		po.setAuthName(AUTH_BRANCH_ADMIN_NAME);
		po.setId(AUTH_BRANCH_ADMIN_ID);
		po.setIsDelete(false);
		po.setRoleGroupId(RoleGroupDefault.ROLE_GROUP_BRANCH_ADMIN_ID);
		authsList.add(po);
		
		po = new AuthsPO();
		po.setAuthName(AUTH_BUSINESS_HANDLER_NAME);
		po.setId(AUTH_BUSINESS_HANDLER_ID);
		po.setIsDelete(false);
		po.setRoleGroupId(RoleGroupDefault.ROLE_GROUP_BUSINESS_HANDLER_ID);
		authsList.add(po);
		
		po = new AuthsPO();
		po.setAuthName(AUTH_BRANCH_HANDLER_NAME);
		po.setId(AUTH_BRANCH_HANDLER_ID);
		po.setIsDelete(false);
		po.setRoleGroupId(RoleGroupDefault.ROLE_GROUP_BRANCH_HANDLER_ID);
		authsList.add(po);
		
		po = new AuthsPO();
		po.setAuthName(AUTH_UNION_MANAGE_NAME);
		po.setId(AUTH_UNION_MANAGE_ID);
		po.setIsDelete(false);
		po.setRoleGroupId(RoleGroupDefault.ROLE_GROUP_UNION_ID);
		authsList.add(po);
	}
	
}
