package demo.organizations.domain.constant;

public class AuthUrl {

	public static final String root = "/auth";

	public static final String changeAuth = "/changeAuth";
}
