package demo.organizations.domain.constant;

public class OrganizationsUrl {

	public static final String root = "/organizations";
	public static final String findOrgByGeographicalId = "/findOrgByGeographicalId";
	public static final String findOrgByProductIdAndGeographical = "/findOrgByProductIdAndGeographical";
	public static final String findOrgPage = "/findOrgPage";
	public static final String createNewOrgByAdmin = "/createNewOrgByAdmin";
	public static final String createNewBranchOrg = "/createNewBranchOrg";
	public static final String findBranchOrgPage = "/findBranchOrgPage";
	public static final String updateOrgInfo = "/updateOrgInfo";
	public static final String deleteOrg = "/deleteOrg";
	
	public static final String findStaffsByCondition = "/findStaffsByCondition";
	public static final String findStaffsByOrgId = "/findStaffsByOrgId";
	
	public static final String findOrgForLoginManger = "/findOrgForLoginManger";
	public static final String findTopOrgForLoginManger = "/findTopOrgForLoginManger";
	public static final String findOrgByDirectBelongTo = "/findOrgByDirectBelongTo";
	
	public static final String deleteStaff = "/deleteStaff";
	public static final String changeStaffName = "/changeStaffName";
}
