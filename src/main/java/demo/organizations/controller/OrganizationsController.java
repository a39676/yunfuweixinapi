package demo.organizations.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.base.user.domain.param.controllerParam.UpdateNickNameStaffNumberParam;
import demo.base.user.domain.po.Users;
import demo.base.user.mapper.UsersMapper;
import demo.base.user.service.UsersService;
import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.organizations.domain.constant.OrganizationsUrl;
import demo.organizations.domain.param.controllerParam.CreateNewBranchOrgParam;
import demo.organizations.domain.param.controllerParam.CreateNewOrgParam;
import demo.organizations.domain.param.controllerParam.DeleteOrgParam;
import demo.organizations.domain.param.controllerParam.FindByDirectBelongToParam;
import demo.organizations.domain.param.controllerParam.FindOrgByProductIdAndGeographicalParam;
import demo.organizations.domain.param.controllerParam.FindOrgPageByConditionParam;
import demo.organizations.domain.param.controllerParam.FindStaffsByOrgIdParam;
import demo.organizations.domain.param.controllerParam.UpdateOrgInfoParam;
import demo.organizations.domain.param.mapperParam.FindOrgByGeographicalIdParam;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.result.CreateNewOrgResult;
import demo.organizations.domain.result.FindOrgByDirectBelongToResult;
import demo.organizations.domain.result.FindOrgByGeographicalIdResult;
import demo.organizations.domain.result.FindOrgByProductIdAndGeographicalResult;
import demo.organizations.domain.result.FindOrgForLoginMangerResult;
import demo.organizations.domain.result.FindOrgPageResult;
import demo.organizations.domain.result.FindStaffsByConditionResult;
import demo.organizations.domain.result.FindTopOrgForLoginMangerResult;
import demo.organizations.domain.vo.StaffDetailVO;
import demo.organizations.service.OrganizationsService;
import demo.organizations.service.StaffDetailService;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

@Controller
@RequestMapping(value = OrganizationsUrl.root)
public class OrganizationsController extends CommonController {

	@Autowired
	private BaseUtilCustom baseUtilCustom;
	
	@Autowired
	private OrganizationsService orgService;

	@Autowired
	private StaffDetailService staffService;
	@Autowired
	private UsersService userSercive;
	@Autowired
	private UsersMapper userMapper;
	
	public CommonResult existOrgId(Long id) {
		return orgService.existOrgId(id);
	}

	@PostMapping(value = OrganizationsUrl.findOrgByGeographicalId)
	public void findOrgByGeographicalId(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		FindOrgByGeographicalIdParam param = new FindOrgByGeographicalIdParam().fromJson(getJson(data));
		FindOrgByGeographicalIdResult serviceResult = orgService.findOrgByGeographicalId(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = OrganizationsUrl.findOrgByProductIdAndGeographical)
	public void findOrgByProductIdAndGeographical(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		FindOrgByProductIdAndGeographicalParam param = new FindOrgByProductIdAndGeographicalParam()
				.fromJson(getJson(data));
		FindOrgByProductIdAndGeographicalResult serviceResult = orgService.findOrgByProductIdAndGeographical(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = OrganizationsUrl.findOrgPage)
	public void findOrgPage(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindOrgPageByConditionParam param = new FindOrgPageByConditionParam().fromJson(getJson(data));
		FindOrgPageResult serviceResult = orgService.findOrgPage(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = OrganizationsUrl.findBranchOrgPage)
	public void findBranchOrgPage(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindOrgPageByConditionParam param = new FindOrgPageByConditionParam().fromJson(getJson(data));
		FindOrgPageResult serviceResult = orgService.findOrgPage(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = OrganizationsUrl.createNewOrgByAdmin)
	public void createNewOrg(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		CreateNewOrgParam param = new CreateNewOrgParam().fromJson(getJson(data));
		CreateNewOrgResult serviceResult = orgService.createNewTopHeadOrg(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = OrganizationsUrl.updateOrgInfo)
	public void updateOrgInfo(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		UpdateOrgInfoParam param = new UpdateOrgInfoParam().fromJson(getJson(data));
		CommonResult serviceResult = orgService.updateOrgInfo(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = OrganizationsUrl.deleteOrg)
	public void deleteOrg(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		DeleteOrgParam param = new DeleteOrgParam().fromJson(getJson(data));
		CommonResult serviceResult = orgService.deleteOrg(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	public List<OrganizationsPO> findBranchOrgByStaffId(Long staffId) {
		return orgService.findBranchOrgByStaffId(staffId);
	}

	public OrganizationsPO findOrgByStaffId(Long staffId) {
		return orgService.findOrgByStaffId(staffId);
	}

	@PostMapping(value = OrganizationsUrl.findStaffsByOrgId)
	public void findStaffsByOrgId(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindStaffsByOrgIdParam param = new FindStaffsByOrgIdParam().fromJson(getJson(data));
		FindStaffsByConditionResult serviceResult = staffService.findStaffsByCondition(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = OrganizationsUrl.findStaffsByCondition)
	public void findStaffsByCondition(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindStaffsByOrgIdParam param = new FindStaffsByOrgIdParam().fromJson(getJson(data));
		FindStaffsByConditionResult serviceResult = staffService.findStaffsByCondition(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = OrganizationsUrl.createNewBranchOrg)
	public void createNewBranchOrg(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		CreateNewBranchOrgParam param = new CreateNewBranchOrgParam().fromJson(getJson(data));
		CreateNewOrgResult serviceResult = orgService.createNewBranchOrg(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	public List<StaffDetailVO> findStaffsByOrgId(Long orgId) {
		FindStaffsByOrgIdParam param = new FindStaffsByOrgIdParam();
		param.setOrgId(orgId);
		FindStaffsByConditionResult serviceResult = staffService.findStaffsByCondition(param);
		return serviceResult.getStaffList();
	}
	
	@PostMapping(value = OrganizationsUrl.findOrgForLoginManger)
	public void findOrgForLoginManger(HttpServletRequest request, HttpServletResponse response) {
		FindOrgForLoginMangerResult serviceResult = orgService.findOrgForLoginManger(baseUtilCustom.getUserId());
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = OrganizationsUrl.findTopOrgForLoginManger)
	public void findTopOrgForLoginManger(HttpServletRequest request, HttpServletResponse response) {
		FindTopOrgForLoginMangerResult serviceResult = orgService.findTopOrgForLoginManger(baseUtilCustom.getUserId());
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = OrganizationsUrl.findOrgByDirectBelongTo)
	public void findOrgByDirectBelongTo(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindByDirectBelongToParam param = new FindByDirectBelongToParam().fromJson(getJson(data));
		FindOrgByDirectBelongToResult serviceResult = orgService.findOrgByDirectBelongTo(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = OrganizationsUrl.deleteStaff)
	public void findTopOrgForLoginManger(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		JSONObject j = getJson(data);
		Long staffId = null;
		if(j.containsKey("staffId") && NumericUtilCustom.matchInteger(j.getString("staffId"))) {
			staffId = j.getLong("staffId");
		}
		CommonResult serviceResult = orgService.deleteStaff(staffId);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	public OrganizationsPO findOne(Long orgId) {
		return orgService.findOne(orgId);
	}
	
	@PostMapping(value = OrganizationsUrl.changeStaffName)
	public void changeStaffName(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		UpdateNickNameStaffNumberParam param = new UpdateNickNameStaffNumberParam().fromJson(getJson(data));
		Users user = userMapper.findUserByUserName(param.getMobile());
		if(user != null && user.getId() != null) {
			param.setUserId(user.getId());
		}
		CommonResult serviceResult = staffService.changeStaffName(param);
		userSercive.updateNickName(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
}
