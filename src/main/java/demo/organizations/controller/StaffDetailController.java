package demo.organizations.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.service.StaffDetailService;
import demo.util.BaseUtilCustom;

@Controller
public class StaffDetailController {

	@Autowired
	private StaffDetailService staffDetailService;
	@Autowired
	private BaseUtilCustom baseUtilCustom;

	public StaffDetailPO findByUserId(Long userId) {
		return staffDetailService.findByUserId(userId);
	}
	
	public Long findOrgIdByUserId(Long userId) {
		return staffDetailService.findOrgIdByUserId(userId);
	}
	
	public void deleteStaff(Long staffId) {
		staffDetailService.deleteStaff(baseUtilCustom.getUserId(), staffId);
	}
	
	public StaffDetailPO findOne(Long staffId) {
		return staffDetailService.findOne(staffId);
	}
}
