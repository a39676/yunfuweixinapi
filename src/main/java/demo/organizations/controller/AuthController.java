package demo.organizations.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.organizations.domain.constant.AuthUrl;
import demo.organizations.domain.param.controllerParam.ChangeAuthParam;
import demo.organizations.service.AuthsService;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = AuthUrl.root)
public class AuthController extends CommonController {

	@Autowired
	private AuthsService authService;

	@PostMapping(value = AuthUrl.changeAuth)
	public void changeAuth(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		ChangeAuthParam param = new ChangeAuthParam().fromJson(getJson(data));
		CommonResult serviceResult = authService.changeAuth(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

}
