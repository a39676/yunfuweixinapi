package demo.organizations.service;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.param.controllerParam.CreateNewBranchOrgParam;
import demo.organizations.domain.param.controllerParam.CreateNewOrgParam;
import demo.organizations.domain.param.controllerParam.DeleteOrgParam;
import demo.organizations.domain.param.controllerParam.FindByDirectBelongToParam;
import demo.organizations.domain.param.controllerParam.FindOrgByProductIdAndGeographicalParam;
import demo.organizations.domain.param.controllerParam.FindOrgPageByConditionParam;
import demo.organizations.domain.param.controllerParam.UpdateOrgInfoParam;
import demo.organizations.domain.param.mapperParam.FindAuthsByOrgIdParam;
import demo.organizations.domain.param.mapperParam.FindOrgByGeographicalIdParam;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.result.CreateNewOrgResult;
import demo.organizations.domain.result.FindAuthsByOrgIdResult;
import demo.organizations.domain.result.FindOrgByDirectBelongToResult;
import demo.organizations.domain.result.FindOrgByGeographicalIdResult;
import demo.organizations.domain.result.FindOrgByProductIdAndGeographicalResult;
import demo.organizations.domain.result.FindOrgForLoginMangerResult;
import demo.organizations.domain.result.FindOrgPageResult;
import demo.organizations.domain.result.FindTopOrgForLoginMangerResult;

public interface OrganizationsService {

	/** 查询机构ID是否有效 */
	CommonResult existOrgId(Long id);

	/** 凭机构ID查询角色*/
	FindAuthsByOrgIdResult findAuthsByOrgId(FindAuthsByOrgIdParam controllerParam);

	/** 创建机构的基础角色 */
	void createDefaultAuthByOrgId(Long orgId);

	CreateNewOrgResult createNewOrg(CreateNewOrgParam param, Long orgId);

	/** 根据区域ID 查找对应机构 */
	FindOrgByGeographicalIdResult findOrgByGeographicalId(FindOrgByGeographicalIdParam controllerParam);

	/** 根据产品ID,区域ID查找对应机构 */
	FindOrgByProductIdAndGeographicalResult findOrgByProductIdAndGeographical(
			FindOrgByProductIdAndGeographicalParam controllerParam);

	/** 按条件分页搜索机构 */
	FindOrgPageResult findOrgPage(FindOrgPageByConditionParam controllerParam);

	/** 创建顶级机构 */ 
	CreateNewOrgResult createNewTopHeadOrg(CreateNewOrgParam param);

	/** 编辑机构信息 */
	CommonResult updateOrgInfo(UpdateOrgInfoParam param);

	/** 删除机构 */
	CommonResult deleteOrg(DeleteOrgParam controllerParam);

	/** 以员工ID查找其机构及其所有下属分支机构 */
	List<OrganizationsPO> findBranchOrgByStaffId(Long staffId);

	/** 以员工ID查找其机构 */
	OrganizationsPO findOrgByStaffId(Long staffId);

	/** 为已经登录管理员查找其下所有机构 */
	FindOrgForLoginMangerResult findOrgForLoginManger(Long userId);

	/** 为已登录管理员查找其对应机构 */
	FindTopOrgForLoginMangerResult findTopOrgForLoginManger(Long userId);

	/** 机构管理员新增分支机构 */
	CreateNewOrgResult createNewBranchOrg(CreateNewBranchOrgParam param);

	/** 删除工作人员 */
	CommonResult deleteStaff(Long staffId);

	OrganizationsPO findOne(Long orgId);

	/** 根据上一级机构ID 获取其下直属机构 */
	FindOrgByDirectBelongToResult findOrgByDirectBelongTo(FindByDirectBelongToParam param);

	/** 为机构管理员分页查询分支机构 */
	FindOrgPageResult findOrgPage(Long userId, FindOrgPageByConditionParam controllerParam);

}
