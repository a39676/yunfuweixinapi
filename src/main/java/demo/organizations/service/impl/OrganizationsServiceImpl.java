package demo.organizations.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.business.domain.po.ExchangeDibOrganizationLimitPO;
import demo.business.mapper.ExchangeDibOrganizationLimitPOMapper;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;
import demo.geographical.controller.GeographicalController;
import demo.geographical.domain.result.FindByBelongIdResult;
import demo.organizations.controller.StaffDetailController;
import demo.organizations.domain.constant.AuthsDefault;
import demo.organizations.domain.param.controllerParam.CreateNewBranchOrgParam;
import demo.organizations.domain.param.controllerParam.CreateNewOrgParam;
import demo.organizations.domain.param.controllerParam.DeleteOrgParam;
import demo.organizations.domain.param.controllerParam.FindByDirectBelongToParam;
import demo.organizations.domain.param.controllerParam.FindOrgByProductIdAndGeographicalParam;
import demo.organizations.domain.param.controllerParam.FindOrgPageByConditionParam;
import demo.organizations.domain.param.controllerParam.UpdateOrgInfoParam;
import demo.organizations.domain.param.mapperParam.DeleteOrg;
import demo.organizations.domain.param.mapperParam.FindAuthsByOrgIdParam;
import demo.organizations.domain.param.mapperParam.FindByGeographicalAreaAndTopHeadOrgMapperParam;
import demo.organizations.domain.param.mapperParam.FindOrgByConditionParam;
import demo.organizations.domain.param.mapperParam.FindOrgByGeographicalIdParam;
import demo.organizations.domain.param.mapperParam.UpdateOrgInfo;
import demo.organizations.domain.po.AuthsPO;
import demo.organizations.domain.po.OrganizationAuthPO;
import demo.organizations.domain.po.OrganizationGeographicalArea;
import demo.organizations.domain.po.OrganizationStaffPO;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.result.CreateNewOrgResult;
import demo.organizations.domain.result.FindAuthsByOrgIdResult;
import demo.organizations.domain.result.FindOrgByDirectBelongToResult;
import demo.organizations.domain.result.FindOrgByGeographicalIdResult;
import demo.organizations.domain.result.FindOrgByProductIdAndGeographicalResult;
import demo.organizations.domain.result.FindOrgForLoginMangerResult;
import demo.organizations.domain.result.FindOrgPageResult;
import demo.organizations.domain.result.FindTopOrgForLoginMangerResult;
import demo.organizations.domain.type.OrganizationsType;
import demo.organizations.domain.vo.OrganizationsVO;
import demo.organizations.mapper.AuthsPOMapper;
import demo.organizations.mapper.OrganizationAuthPOMapper;
import demo.organizations.mapper.OrganizationGeographicalAreaMapper;
import demo.organizations.mapper.OrganizationStaffPOMapper;
import demo.organizations.mapper.OrganizationsPOMapper;
import demo.organizations.service.AuthsService;
import demo.organizations.service.OrganizationsService;
import demo.products.controller.ProductController;
import demo.products.domain.po.ProductOrganizationPO;
import demo.util.BaseUtilCustom;

@Service
public class OrganizationsServiceImpl extends CommonService implements OrganizationsService {

	
	@Autowired
	private BaseUtilCustom baseUtilCustom;
	@Autowired
	private GeographicalController geoController;
	@Autowired
	private ProductController productController;
	@Autowired
	private StaffDetailController staffController;
	
	@Autowired
	private AuthsService authsService;
	
	@Autowired
	private OrganizationsPOMapper orgMapper;
	@Autowired
	private AuthsPOMapper authMapper;
	@Autowired
	private OrganizationAuthPOMapper orgAuthMapper;
	@Autowired
	private OrganizationGeographicalAreaMapper orgGeoMapper;
	@Autowired
	private ExchangeDibOrganizationLimitPOMapper exchangeDibOrgLimitMapper;
	@Autowired
	private OrganizationStaffPOMapper orgStaffMapper;
	
	@Override
	public CommonResult existOrgId(Long id) {
		CommonResult result = new CommonResult();
		result.setIsFail();
		
		if(id == null) {
			return result;
		}
		
		Integer r = orgMapper.existOrgId(id);
		if(r.equals(1)) {
			result.setIsSuccess();
			return result;
		}
		return result;
	}
	
	@Override
	public FindAuthsByOrgIdResult findAuthsByOrgId(FindAuthsByOrgIdParam controllerParam) {
		FindAuthsByOrgIdResult result = new FindAuthsByOrgIdResult();
		if(controllerParam.getOrgId() == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		FindAuthsByOrgIdParam mapperParam = new FindAuthsByOrgIdParam();
		mapperParam.setOrgId(controllerParam.getOrgId());
		mapperParam.setIsDelete(controllerParam.getIsDelete());
		List<AuthsPO> authList = authMapper.findAuthsByOrgId(mapperParam);
		// 如果查询该机构名下角色数为0, 创建3个基础角色
		if(authList == null || authList.size() < 1) {
			createDefaultAuthByOrgId(controllerParam.getOrgId());
			authList = authMapper.findAuthsByOrgId(mapperParam);
		}
		
		result.setAuthList(authList);
		return result;
	}
	
	@Override
	public void createDefaultAuthByOrgId(Long orgId) {
		if(orgId == null) {
			return;
		}
		OrganizationsPO org = orgMapper.findOrg(orgId);
		if(org == null) {
			return;
		}
		if(org.getOrgType() == null || org.getTopHeadOrg() == null || OrganizationsType.union.getCode().equals(org.getOrgType())) {
			return;
		}
		
		List<AuthsPO> authsList = AuthsDefault.authsList;
		OrganizationAuthPO newOrgAuth = null;
		AuthsPO tmpAuth = null;
		for(AuthsPO auth : authsList) {
			tmpAuth = new AuthsPO();
			tmpAuth.setAuthName(auth.getAuthName());
			tmpAuth.setRoleGroupId(auth.getRoleGroupId());
			authMapper.insertSelective(tmpAuth);
			
			newOrgAuth = new OrganizationAuthPO();
			newOrgAuth.setAuthId(tmpAuth.getId());
			newOrgAuth.setOrgId(org.getTopHeadOrg());
			orgAuthMapper.insertSelective(newOrgAuth);
		}
	}
	
	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	@Override
	public CreateNewOrgResult createNewTopHeadOrg(CreateNewOrgParam param) {
		CreateNewOrgResult result = new CreateNewOrgResult();
		if(param.getOrgType() == null || StringUtils.isBlank(param.getOrgCode()) || StringUtils.isBlank(param.getOrgName())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		OrganizationsType orgType = OrganizationsType.getType(param.getOrgType());
		if(orgType == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		OrganizationsPO oldOrg = orgMapper.findOrgByName(param.getOrgName());
		if(oldOrg != null) {
			result.failWithMessage("机构已存在");
			return result;
		}
		
		
		OrganizationsPO newOrg = new OrganizationsPO();
		newOrg.setBelongTo(param.getBelongTo());
		newOrg.setOrgCode(param.getOrgCode());
		newOrg.setOrgName(param.getOrgName());
		newOrg.setOrgType(orgType.getCode());
		newOrg.setTopHeadOrg(param.getTopHeadOrg());
		orgMapper.insertSelective(newOrg);

		UpdateOrgInfo updateOrgInfoParam = new UpdateOrgInfo();
		updateOrgInfoParam.setId(newOrg.getId());
		updateOrgInfoParam.setTopHeadOrgId(newOrg.getId());
		updateOrgInfoParam.setBelongTo(newOrg.getId());
		orgMapper.updateOrgInfo(updateOrgInfoParam);
		Long orgId = newOrg.getId();
		
		OrganizationGeographicalArea newOrgGeo = new OrganizationGeographicalArea();
		newOrgGeo.setOrgId(orgId);
		newOrgGeo.setGeographicalAreaId(param.getGeogralphicalId());
		orgGeoMapper.insertSelective(newOrgGeo);

		authsService.copyAuth(orgId, AuthsDefault.AUTH_BRANCH_ADMIN_ID);
		
		ExchangeDibOrganizationLimitPO exchangeLimitPo = new ExchangeDibOrganizationLimitPO();
		exchangeLimitPo.setOrgId(orgId);
		exchangeLimitPo.setCoin01Limit(9999);
		exchangeLimitPo.setCoin01Used(0);
		exchangeLimitPo.setCoin05Limit(9999);
		exchangeLimitPo.setCoin05Used(0);
		exchangeLimitPo.setCoin1Limit(9999);
		exchangeLimitPo.setCoin1Used(0);
		exchangeLimitPo.setPaper1Limit(9999);
		exchangeLimitPo.setPaper1Used(0);
		exchangeLimitPo.setPaper5Limit(9999);
		exchangeLimitPo.setPaper5Used(0);
		exchangeLimitPo.setPaper10Limit(9999);
		exchangeLimitPo.setPaper10Used(0);
		exchangeLimitPo.setPaper20Limit(9999);
		exchangeLimitPo.setPaper20Used(0);
		exchangeLimitPo.setPaper50Limit(9999);
		exchangeLimitPo.setPaper50Used(0);
		exchangeLimitPo.setPaper100Limit(9999);
		exchangeLimitPo.setPaper100Used(0);
		exchangeDibOrgLimitMapper.insertSelective(exchangeLimitPo);
		
		result.setNewOrg(newOrg);
		result.setIsSuccess();
		return result;
	}
	
	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	@Override
	public CreateNewOrgResult createNewBranchOrg(CreateNewBranchOrgParam param) {
		CreateNewOrgResult result = new CreateNewOrgResult();
		if(StringUtils.isBlank(param.getOrgCode()) || StringUtils.isBlank(param.getOrgName()) || param.getBelongTo() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		OrganizationsPO belongOrg = orgMapper.findOrg(param.getBelongTo());
		if(belongOrg == null || belongOrg.getId() == null || belongOrg.getOrgType() == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		OrganizationsType orgType = OrganizationsType.getType(belongOrg.getOrgType());
		if(orgType == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		OrganizationsPO oldOrg = orgMapper.findOrgByName(param.getOrgName());
		if(oldOrg != null) {
			result.failWithMessage("机构已存在");
			return result;
		}
		
		OrganizationsPO newOrg = new OrganizationsPO();
		newOrg.setBelongTo(param.getBelongTo());
		newOrg.setOrgCode(param.getOrgCode());
		newOrg.setOrgName(param.getOrgName());
		newOrg.setOrgType(orgType.getCode());
		newOrg.setTopHeadOrg(belongOrg.getTopHeadOrg());
		orgMapper.insertSelective(newOrg);

		Long orgId = newOrg.getId();
		
		OrganizationGeographicalArea newOrgGeo = new OrganizationGeographicalArea();
		newOrgGeo.setOrgId(orgId);
		newOrgGeo.setGeographicalAreaId(param.getGeogralphicalId());
		orgGeoMapper.insertSelective(newOrgGeo);

		authsService.copyAuth(orgId, AuthsDefault.AUTH_BUSINESS_HANDLER_ID);
		authsService.copyAuth(orgId, AuthsDefault.AUTH_BRANCH_HANDLER_ID);
		
		result.setNewOrg(newOrg);
		result.setIsSuccess();
		return result;
	}
	
	@Override
	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	public CreateNewOrgResult createNewOrg(CreateNewOrgParam param, Long orgId) {
		CreateNewOrgResult result = new CreateNewOrgResult();
		if(param.getBelongTo() == null || param.getTopHeadOrg() == null || param.getOrgType() == null || StringUtils.isBlank(param.getOrgCode()) || StringUtils.isBlank(param.getOrgName())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		if(orgId != null && param.getGeogralphicalId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		OrganizationsType orgType = OrganizationsType.getType(param.getOrgType());
		if(orgType == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		OrganizationsPO newOrg = new OrganizationsPO();
		newOrg.setBelongTo(param.getBelongTo());
		newOrg.setOrgCode(param.getOrgCode());
		newOrg.setOrgName(param.getOrgName());
		newOrg.setOrgType(orgType.getCode());
		newOrg.setTopHeadOrg(param.getTopHeadOrg());
		if(orgId != null) {
			newOrg.setId(orgId);
		}
		OrganizationGeographicalArea newOrgGeo = new OrganizationGeographicalArea();
		newOrgGeo.setOrgId(orgId);
		newOrgGeo.setGeographicalAreaId(param.getGeogralphicalId());
		orgGeoMapper.insertSelective(newOrgGeo);
		orgMapper.insertSelective(newOrg);

		result.setNewOrg(newOrg);
		result.setIsSuccess();
		return result;
	}

	@Override
	public FindOrgByGeographicalIdResult findOrgByGeographicalId(FindOrgByGeographicalIdParam controllerParam) {
		FindOrgByGeographicalIdResult result = new FindOrgByGeographicalIdResult();
		List<OrganizationsPO> orgList = null;
		if(controllerParam == null || controllerParam.getGeographicalId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		List<OrganizationGeographicalArea> orgGeoList = orgGeoMapper.findByGeoId(controllerParam.getGeographicalId());
		if(orgGeoList == null || orgGeoList.size() < 1) {
			result.setOrgList(new ArrayList<OrganizationsPO>());
			return result;
		}
		
		List<Long> orgIdList = new ArrayList<Long>();
		orgGeoList.stream().forEach(p -> orgIdList.add(p.getOrgId()));
		
		orgList = orgMapper.findByIdList(orgIdList);
		result.setOrgList(orgList);
		
		return result;
	}

	@Override
	public FindOrgByProductIdAndGeographicalResult findOrgByProductIdAndGeographical(FindOrgByProductIdAndGeographicalParam controllerParam) {
		FindOrgByProductIdAndGeographicalResult result = new FindOrgByProductIdAndGeographicalResult();;
		if(controllerParam == null || controllerParam.getGeographicalId() == null || controllerParam.getProductId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		ProductOrganizationPO productOrgRelation = productController.findProductOrgRelation(controllerParam.getProductId());
		if(productOrgRelation == null || productOrgRelation.getProductId() == null || productOrgRelation.getOrgId() == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		FindByBelongIdResult geoListResult = geoController.findByBelongId(controllerParam.getGeographicalId());
		if(geoListResult == null || geoListResult.getAreaList() == null || geoListResult.getAreaList().size() < 1) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		List<Long> getIdList = new ArrayList<Long>();
		geoListResult.getAreaList().stream().forEach(gp -> getIdList.add(gp.getId()));
		
		FindByGeographicalAreaAndTopHeadOrgMapperParam mapperParam = new FindByGeographicalAreaAndTopHeadOrgMapperParam();
		mapperParam.setGeoIdList(getIdList);
		mapperParam.setTopHeadOrgId(productOrgRelation.getOrgId());
		List<OrganizationsPO> orgList = orgMapper.findByGeographicalAreaAndTopHeadOrg(mapperParam);
		
		result.setOrgList(orgList);
		result.setIsSuccess();
		return result;
	}

	@Override
	public FindOrgPageResult findOrgPage(FindOrgPageByConditionParam controllerParam) {
		PageParam pageParam = setPageFromPageNo(controllerParam.getPageNo(), controllerParam.getPageSize());
		
		FindOrgByConditionParam mapperParam = new FindOrgByConditionParam();
		mapperParam.setGeographicalAreaId(controllerParam.getGeographicalAreaId());
		mapperParam.setMobile(controllerParam.getMobile());
		mapperParam.setOrgCode(controllerParam.getOrgCode());
		mapperParam.setOrgName(controllerParam.getOrgName());
		mapperParam.setOrgType(controllerParam.getOrgType());
		mapperParam.setPageEnd(pageParam.getPageEnd());
		mapperParam.setPageSize(pageParam.getPageSize());
		mapperParam.setPageStart(pageParam.getPageStart());
		
		if(controllerParam.getTopHeadOrg() != null) {
			mapperParam.setTopHeadOrg(controllerParam.getTopHeadOrg());
		}
		
		List<OrganizationsVO> orgPage = orgMapper.findOrgPageByCondition(mapperParam);
		int resultCount = orgMapper.countOrgPageByCondition(mapperParam);
		
		
		
		FindOrgPageResult result = new FindOrgPageResult();
		if(orgPage != null) {
			result.setIsSuccess();
		} else {
			result.setIsFail();
		}
		result.setResultCount(resultCount);
		result.setOrgPage(orgPage);
		return result;
	}
	
	@Override
	public FindOrgPageResult findOrgPage(Long userId, FindOrgPageByConditionParam controllerParam) {
		FindOrgPageResult result = new FindOrgPageResult();
		if(userId == null) {
			result.failWithMessage("请先登录");
		}
		FindTopOrgForLoginMangerResult orgResult = findTopOrgForLoginManger(userId);

		if(!orgResult.isSuccess()) {
			result.setMessage(orgResult.getMessage());
			return result;
		}
		controllerParam.setTopHeadOrg(orgResult.getOrg().getId());
		
		return findOrgPage(controllerParam);
		
	}

	@Override
	public CommonResult updateOrgInfo(UpdateOrgInfoParam param) { 
		UpdateOrgInfo updateOrgInfoParam = param;
		int updateCount = orgMapper.updateOrgInfo(updateOrgInfoParam);
		
		if(updateCount == 1) {
			return normalSuccess();
		} else {
			return serviceError();
		}
	}
	
	@Override
	public CommonResult deleteOrg(DeleteOrgParam controllerParam) {
		if(controllerParam == null || controllerParam.getOrgId() == null) {
			return nullParam();
		}
		DeleteOrg mapperParam = new DeleteOrg();
		mapperParam.setId(controllerParam.getOrgId());
		mapperParam.setIsDelete(true);
		orgMapper.deleteOrg(mapperParam);
		
		List<OrganizationStaffPO> orgStaffList = orgStaffMapper.findStaffIdListByOrgId(controllerParam.getOrgId());
		
		orgStaffList.stream().forEach(p -> staffController.deleteStaff(p.getStaffId()));
		
		return normalSuccess();
	}
	
	@Override
	public List<OrganizationsPO> findBranchOrgByStaffId(Long staffId) {
		List<OrganizationsPO> orgList;
		if(staffId == null) {
			return new ArrayList<OrganizationsPO>();
		}
		
		OrganizationsPO org = findOrgByStaffId(staffId);
		orgList = orgMapper.findByBelongTo(org.getId());
		return orgList;
	}
	
	@Override
	public OrganizationsPO findOrgByStaffId(Long staffId) {
		if(staffId == null) {
			return new OrganizationsPO();
		}
		
		return orgMapper.findOrgByStaffId(staffId);
	}
	
	@Override
	public FindOrgForLoginMangerResult findOrgForLoginManger(Long userId) {
		FindOrgForLoginMangerResult result = new FindOrgForLoginMangerResult();
		if(userId == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		List<OrganizationsPO> orgList = null;
		if(baseUtilCustom.hasAdminRole()) {
			orgList = findTopOrgForAdmin();
		} else {
			Long orgId = staffController.findOrgIdByUserId(userId);
			if(orgId == null) {
				result.fillWithResult(ResultTypeBase.nullParam);
				return result;
			}
			
			orgList = orgMapper.findByBelongTo(orgId);
		}
		
		result.setOrgList(orgList);
		result.setIsSuccess();
		return result;
	}
	
	private List<OrganizationsPO> findTopOrgForAdmin() {
		return orgMapper.findTopOrg();
	}
	
	@Override
	public FindTopOrgForLoginMangerResult findTopOrgForLoginManger(Long userId) {
		FindTopOrgForLoginMangerResult result = new FindTopOrgForLoginMangerResult();
		if(userId == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		Long orgId = staffController.findOrgIdByUserId(userId);
		if(orgId == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		OrganizationsPO org = orgMapper.findOrg(orgId);
		if(org == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		result.setOrg(org);
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult deleteStaff(Long staffId) {
		if(staffId == null) {
			return nullParam();
		}
		staffController.deleteStaff(staffId);
		return normalSuccess();
	}

	@Override
	public OrganizationsPO findOne(Long orgId) {
		if(orgId == null) {
			return null;
		}
		return orgMapper.findOrg(orgId);
	}
	
	@Override
	public FindOrgByDirectBelongToResult findOrgByDirectBelongTo(FindByDirectBelongToParam param) {
		FindOrgByDirectBelongToResult result = new FindOrgByDirectBelongToResult();
		if(param.getOrgId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		List<OrganizationsPO> orgList = orgMapper.findOrgByDirectBelongTo(param.getOrgId());
		
		result.setOrgList(orgList);
		result.setIsSuccess();
		return result;
	}
}