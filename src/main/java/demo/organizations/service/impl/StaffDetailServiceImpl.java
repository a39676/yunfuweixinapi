package demo.organizations.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.base.user.domain.param.controllerParam.UpdateNickNameStaffNumberParam;
import demo.base.user.service.UsersService;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.service.CommonService;
import demo.organizations.domain.constant.AuthsDefault;
import demo.organizations.domain.param.controllerParam.FindStaffsByOrgIdParam;
import demo.organizations.domain.param.mapperParam.FindStaffsByConditionParam;
import demo.organizations.domain.po.AuthsPO;
import demo.organizations.domain.po.StaffAuthPO;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.domain.result.FindStaffsByConditionResult;
import demo.organizations.domain.vo.StaffDetailVO;
import demo.organizations.mapper.AuthsPOMapper;
import demo.organizations.mapper.OrganizationStaffPOMapper;
import demo.organizations.mapper.StaffAuthPOMapper;
import demo.organizations.mapper.StaffDetailPOMapper;
import demo.organizations.service.StaffDetailService;

@Service
public class StaffDetailServiceImpl extends CommonService implements StaffDetailService {

	@Autowired
	private UsersService userService;
	@Autowired
	private StaffDetailPOMapper staffDetailMapper;
	@Autowired
	private OrganizationStaffPOMapper orgStaffMapper;
	@Autowired
	private StaffAuthPOMapper staffAuthMapper;
	@Autowired
	private AuthsPOMapper authMapper;

	@Override
	public StaffDetailPO findByUserId(Long userId) {
		if(userId == null) {
			return new StaffDetailPO();
		}
		return staffDetailMapper.findByUserId(userId);
	}
	
	@Override
	public StaffDetailPO findOne(Long staffId) {
		if(staffId == null) {
			return new StaffDetailPO();
		}
		
		return staffDetailMapper.findOne(staffId);
	}
	
	@Override
	public FindStaffsByConditionResult findStaffsByCondition(FindStaffsByOrgIdParam param) {
		FindStaffsByConditionResult result = new FindStaffsByConditionResult();
		if(param.getOrgId() == null) {
			return result;
		}
		PageParam pp = setPageFromPageNo(param.getPageNo(), param.getPageSize());
		
		FindStaffsByConditionParam mapperParam = new FindStaffsByConditionParam();
		mapperParam.setPageStart(pp.getPageStart());
		mapperParam.setPageSize(pp.getPageSize());
		mapperParam.setOrgId(param.getOrgId());
		if(StringUtils.isNotBlank(param.getMobile())) {
			mapperParam.setMobile(param.getMobile());
		}
		if(StringUtils.isNotBlank(param.getStaffName())) {
			mapperParam.setStaffName(param.getStaffName());
		}
		if(StringUtils.isNotBlank(param.getStaffNumber())) {
			mapperParam.setStaffNumber(param.getStaffNumber());
		}
		
		List<StaffDetailVO> staffList = staffDetailMapper.findStaffsPageByCondition(mapperParam);
		Integer resultCount = staffDetailMapper.countStaffsPageByCondition(mapperParam);
		
		if(staffList != null && staffList.size() == 1 && staffList.get(0) == null) {
			staffList.remove(0);
			resultCount = 0;
		}
		
		result.setResultCount(resultCount);
		result.setStaffList(staffList);
		result.setIsSuccess();
		return result;
	}

	@Override
	public Long findOrgIdByUserId(Long userId) {
		if(userId == null) {
			return null;
		}
		Long orgId = staffDetailMapper.findOrgIdByUserId(userId);
		return orgId;
	}
	
	@Override
	public CommonResult deleteStaff(Long userId, Long staffId) {
		CommonResult result = new CommonResult();
		if(userId == null) {
			return result;
		}
		
		StaffDetailPO commonder = staffDetailMapper.findByUserId(userId);
		if(commonder == null) {
			return result;
		}
		StaffAuthPO commonderSataffAuth = staffAuthMapper.findByStaffId(commonder.getId());
		if(commonderSataffAuth == null || commonderSataffAuth.getAuthId() == null) {
			return result;
		}
		AuthsPO commonderAuth = authMapper.findAuth(commonderSataffAuth.getAuthId());
		if(commonderAuth == null || (!commonderAuth.getAuthName().equals(AuthsDefault.AUTH_BRANCH_ADMIN_NAME) && !commonderAuth.getAuthName().equals(AuthsDefault.AUTH_UNION_MANAGE_NAME))) {
			return result;
		}
		
		StaffDetailPO targetSd = staffDetailMapper.findOne(staffId);
		if(targetSd == null) {
			return result;
		}
		StaffAuthPO targetSataffAuth = staffAuthMapper.findByStaffId(targetSd.getId());
		if(targetSataffAuth == null || targetSataffAuth.getAuthId() == null) {
			return result;
		}
		AuthsPO targetAuth = authMapper.findAuth(targetSataffAuth.getAuthId());
		if(targetAuth == null || (commonderAuth.getAuthName().equals(AuthsDefault.AUTH_BRANCH_ADMIN_NAME) && targetAuth.getAuthName().equals(AuthsDefault.AUTH_BRANCH_ADMIN_NAME))) {
			result.failWithMessage("机构管理员不能删除机构管理员");
			return result;
		}
		
		
		userService.deleteUserAndDetail(targetSd.getUserId());
		staffDetailMapper.deleteOne(staffId);
		staffAuthMapper.deleteByStaffId(staffId);
		orgStaffMapper.deleteStaff(staffId);
		
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult changeStaffName(UpdateNickNameStaffNumberParam param) {
		if(param.getUserId() == null || (StringUtils.isBlank(param.getNickName()) && StringUtils.isBlank(param.getStaffNumber()))) {
			return nullParam();
		}
		
		staffDetailMapper.changeStaffName(param);
		return normalSuccess();
	}
}