package demo.organizations.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.base.user.controller.UsersController;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;
import demo.organizations.domain.param.controllerParam.ChangeAuthParam;
import demo.organizations.domain.po.AuthsPO;
import demo.organizations.domain.po.OrganizationAuthPO;
import demo.organizations.domain.result.CopyAuthResult;
import demo.organizations.mapper.AuthsPOMapper;
import demo.organizations.mapper.OrganizationAuthPOMapper;
import demo.organizations.service.AuthsService;

@Service
public class AuthsServiceImpl extends CommonService implements AuthsService {

	@Autowired
	private UsersController userController;
	
	@Autowired
	private AuthsPOMapper authMapper;
	@Autowired
	private OrganizationAuthPOMapper orgAuthMapper;
	
	
	@Override
	public CopyAuthResult copyAuth(Long orgId, Long authId) {
		CopyAuthResult result = new CopyAuthResult();
		if(orgId == null || authId == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		AuthsPO sourceAuth = authMapper.findAuth(authId);
		if(sourceAuth == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		
		List<Long> targetAuthIds = orgAuthMapper.findAuthIdByOrgIdRoleGroupId(orgId, sourceAuth.getRoleGroupId());
		if(targetAuthIds != null && targetAuthIds.size() > 0) {
			result.setNewAuthId(targetAuthIds.get(0));
			result.setIsSuccess();
			return result;
		}
		
		AuthsPO targetAuth = new AuthsPO();
		targetAuth.setAuthName(sourceAuth.getAuthName());
		targetAuth.setRoleGroupId(sourceAuth.getRoleGroupId());
		
		int updateCount = authMapper.insertSelective(targetAuth);
		if(updateCount < 1) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		
		OrganizationAuthPO newOrgAuth = new OrganizationAuthPO();
		newOrgAuth.setOrgId(orgId);
		newOrgAuth.setAuthId(targetAuth.getId());
		updateCount = orgAuthMapper.insertSelective(newOrgAuth);
		if(updateCount < 1) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		
		result.setNewAuthId(newOrgAuth.getAuthId());
		result.setIsSuccess();
		return result;
	}
	
	@Override
	public CommonResult changeAuth(ChangeAuthParam param) {
		if(param.getUserId() == null || param.getNewAuthId() == null) {
			return nullParam();
		}
		
		AuthsPO newAuth = authMapper.findAuth(param.getNewAuthId());
		if(newAuth == null || newAuth.getRoleGroupId() == null) {
			return errorParam();
		}
		
		return userController.changeUserRoleByRoleGroupId(param.getUserId(), newAuth.getRoleGroupId());
		
	}
}
