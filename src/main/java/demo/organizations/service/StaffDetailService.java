package demo.organizations.service;

import demo.base.user.domain.param.controllerParam.UpdateNickNameStaffNumberParam;
import demo.common.domain.result.CommonResult;
import demo.organizations.domain.param.controllerParam.FindStaffsByOrgIdParam;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.domain.result.FindStaffsByConditionResult;

public interface StaffDetailService {

	StaffDetailPO findByUserId(Long userId);

	FindStaffsByConditionResult findStaffsByCondition(FindStaffsByOrgIdParam param);

	Long findOrgIdByUserId(Long userId);

	CommonResult deleteStaff(Long userId, Long staffId);

	CommonResult changeStaffName(UpdateNickNameStaffNumberParam param);

	StaffDetailPO findOne(Long staffId);

}
