package demo.organizations.service;

import demo.common.domain.result.CommonResult;
import demo.organizations.domain.param.controllerParam.ChangeAuthParam;
import demo.organizations.domain.result.CopyAuthResult;

public interface AuthsService {

	/** 为指定机构复制出角色 */
	CopyAuthResult copyAuth(Long orgId, Long authId);

	/** 将指定用户转换角色 */
	CommonResult changeAuth(ChangeAuthParam param);

}
