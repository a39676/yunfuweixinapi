package demo.organizations.mapper;

import java.util.List;

import demo.organizations.domain.po.OrganizationGeographicalArea;

public interface OrganizationGeographicalAreaMapper {
    int insert(OrganizationGeographicalArea record);

    int insertSelective(OrganizationGeographicalArea record);
    
    List<OrganizationGeographicalArea> findByGeoId(Long geoId);
    
    List<OrganizationGeographicalArea> findByOrgId(Long orgId);
}