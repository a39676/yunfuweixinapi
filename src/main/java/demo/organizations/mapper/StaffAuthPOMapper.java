package demo.organizations.mapper;

import demo.organizations.domain.po.StaffAuthPO;

public interface StaffAuthPOMapper {
    int insert(StaffAuthPO record);

    int insertSelective(StaffAuthPO record);
    
    StaffAuthPO findByStaffId(Long staffId);
    
    int deleteByStaffId(Long staffId);
}