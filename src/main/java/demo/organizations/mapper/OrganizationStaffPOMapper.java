package demo.organizations.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import demo.organizations.domain.po.OrganizationStaffPO;

public interface OrganizationStaffPOMapper {
    int insert(OrganizationStaffPO record);

    int insertSelective(OrganizationStaffPO record);
    
    int checkExists(@Param("orgId")Long orgId, @Param("staffId")Long staffId);
    
    int deleteStaff(Long staffId);
    
    List<OrganizationStaffPO> findStaffIdListByOrgId(Long orgId);
}