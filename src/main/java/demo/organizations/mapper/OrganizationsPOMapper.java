package demo.organizations.mapper;

import java.util.List;

import demo.organizations.domain.param.mapperParam.DeleteOrg;
import demo.organizations.domain.param.mapperParam.FindByGeographicalAreaAndTopHeadOrgMapperParam;
import demo.organizations.domain.param.mapperParam.FindOrgByConditionParam;
import demo.organizations.domain.param.mapperParam.UpdateOrgInfo;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.vo.OrganizationsVO;

public interface OrganizationsPOMapper {
	int insert(OrganizationsPO record);

	int insertSelective(OrganizationsPO record);

	OrganizationsPO findOrg(Long id);
	List<OrganizationsPO> findByIdList(List<Long> idList);

	List<OrganizationsVO> findOrgPageByCondition(FindOrgByConditionParam param);
	
	int countOrgPageByCondition(FindOrgByConditionParam param);

	int deleteOrg(DeleteOrg param);

	int updateOrgInfo(UpdateOrgInfo param);

	int existOrgId(Long id);
	
	List<OrganizationsPO> findByGeographicalAreaAndTopHeadOrg(FindByGeographicalAreaAndTopHeadOrgMapperParam param);
	
	int hasBranchOrg(Long id);
	
	List<OrganizationsPO> findByBelongTo(Long id);
	
	List<OrganizationsPO> findOrgByDirectBelongTo(Long id);
	
	List<OrganizationsPO> findTopOrg();
	
	OrganizationsPO findOrgByStaffId(Long staffId);
	
	OrganizationsPO findOrgByName(String orgName);
}