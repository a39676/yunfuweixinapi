package demo.organizations.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import demo.base.user.domain.param.controllerParam.UpdateNickNameStaffNumberParam;
import demo.organizations.domain.param.mapperParam.FindStaffsByConditionParam;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.domain.vo.StaffDetailVO;

public interface StaffDetailPOMapper {
	int insert(StaffDetailPO record);

	int insertSelective(StaffDetailPO record);

	StaffDetailPO findByUserId(Long userId);

	Long findOrgIdByUserId(@Param("userId") Long userId);

	List<StaffDetailVO> findStaffsPageByCondition(FindStaffsByConditionParam param);
	
	Integer countStaffsPageByCondition(FindStaffsByConditionParam param);
	
	StaffDetailPO findOne(Long id);
	
	int deleteOne(Long id);
	
	Integer staffMobileExists(String mobile);
	
	int changeStaffName(UpdateNickNameStaffNumberParam param);
}