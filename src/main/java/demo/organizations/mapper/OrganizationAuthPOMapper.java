package demo.organizations.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import demo.organizations.domain.po.OrganizationAuthPO;

public interface OrganizationAuthPOMapper {
    int insert(OrganizationAuthPO record);

    int insertSelective(OrganizationAuthPO record);
    
    int findExists(@Param("orgId")Long orgId, @Param("authId")Long authId);
    
    List<Long> findAuthIdByOrgIdRoleGroupId(@Param("orgId")Long orgId, @Param("roleGroupId")Long roleGroupId);
}