package demo.organizations.mapper;

import java.util.List;

import demo.organizations.domain.param.mapperParam.FindAuthsByOrgIdParam;
import demo.organizations.domain.param.mapperParam.InitDefaultAuthsParam;
import demo.organizations.domain.po.AuthsPO;

public interface AuthsPOMapper {
    int insert(AuthsPO record);

    int insertSelective(AuthsPO record);
    
    List<AuthsPO> findAuthsByOrgId(FindAuthsByOrgIdParam param);
    
    int initDefaultAuths(InitDefaultAuthsParam param);
    
    AuthsPO findAuth(Long id);
}