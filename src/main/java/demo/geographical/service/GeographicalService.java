package demo.geographical.service;

import demo.common.domain.result.CommonResult;
import demo.geographical.domain.param.controllerParam.AddNewGeographicalAreaParam;
import demo.geographical.domain.param.controllerParam.DeleteGeographicalAreaParam;
import demo.geographical.domain.param.controllerParam.FindByBelongIdParam;
import demo.geographical.domain.param.controllerParam.FindGeographicalAreaByIdParam;
import demo.geographical.domain.po.GeographicalAreaPO;
import demo.geographical.domain.result.AddNewGeographicalAreaResult;
import demo.geographical.domain.result.FindByBelongIdResult;

public interface GeographicalService {

	/**
	 * 根据区域，获取区域列表
	 * 
	 * @param param
	 * @return
	 */
	FindByBelongIdResult FindByBelongId(FindByBelongIdParam param);

	/**
	 * 新增地理位置区域
	 * 
	 * @param param
	 * @return
	 */
	AddNewGeographicalAreaResult addNewGeographicalArea(AddNewGeographicalAreaParam param);

	/**
	 * 删除地理位置区域
	 * 
	 * @param controllerParam
	 * @return
	 */
	CommonResult deleteGeographicalArea(DeleteGeographicalAreaParam controllerParam);

	GeographicalAreaPO findById(FindGeographicalAreaByIdParam param);

}
