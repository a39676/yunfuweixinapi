package demo.geographical.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;
import demo.geographical.domain.param.controllerParam.AddNewGeographicalAreaParam;
import demo.geographical.domain.param.controllerParam.DeleteGeographicalAreaParam;
import demo.geographical.domain.param.controllerParam.FindByBelongIdParam;
import demo.geographical.domain.param.controllerParam.FindGeographicalAreaByIdParam;
import demo.geographical.domain.param.mapperParam.FindByBelongIdListParam;
import demo.geographical.domain.param.mapperParam.FindGroByAreaTypeAreaNameParam;
import demo.geographical.domain.po.GeographicalAreaPO;
import demo.geographical.domain.result.AddNewGeographicalAreaResult;
import demo.geographical.domain.result.FindByBelongIdResult;
import demo.geographical.domain.type.GeographicalAreaResultType;
import demo.geographical.domain.type.GeographicalAreaType;
import demo.geographical.mapper.GeographicalAreaMapper;
import demo.geographical.service.GeographicalService;

@Service
public class GeographicalServiceImpl extends CommonService implements GeographicalService {

	@Autowired
	private GeographicalAreaMapper geographicalAreaMapper;
	
	@Override
	public FindByBelongIdResult FindByBelongId(FindByBelongIdParam param) {
		FindByBelongIdResult result = new FindByBelongIdResult();
		
		FindByBelongIdListParam mapperParam = new FindByBelongIdListParam();
		if(param.getAreaParentId() != null) {
			mapperParam.setBelongIdList(Arrays.asList(new Long[] {param.getAreaParentId()}));
		}
		List<GeographicalAreaPO> areaList = geographicalAreaMapper.findByBelongIdList(mapperParam);
		if(areaList == null) {
			areaList = new ArrayList<GeographicalAreaPO>();
		}
		result.setAreaList(areaList);
		result.setIsSuccess();
		
		return result;
	}
	
	@Override
	public AddNewGeographicalAreaResult addNewGeographicalArea(AddNewGeographicalAreaParam param) {
		AddNewGeographicalAreaResult result = new AddNewGeographicalAreaResult();
		if(StringUtils.isBlank(param.getAreaName())) {
			result.fillWithResult(GeographicalAreaResultType.nullGeographicalAreaName);
			return result;
		}
		if(param.getAreaType() == null) {
			result.fillWithResult(GeographicalAreaResultType.nullGeographicalAreaType);
			return result;
		}
		GeographicalAreaType areaType = GeographicalAreaType.getType(param.getAreaType());
		if(areaType == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		Long belongTo = null;
		if(param.getBelongTo() != null) {
			GeographicalAreaPO parentArea = geographicalAreaMapper.findArea(param.getBelongTo());
			if(parentArea == null) {
				result.fillWithResult(ResultTypeBase.errorParam);
				return result;
			}
			belongTo = param.getBelongTo();
		}
		
		FindGroByAreaTypeAreaNameParam findGroParam = new FindGroByAreaTypeAreaNameParam();
		findGroParam.setAreaName(param.getAreaName());
		findGroParam.setAreaType(param.getAreaType());
		GeographicalAreaPO tmpGeo = geographicalAreaMapper.findGroByAreaTypeAreaName(findGroParam);
		if(tmpGeo != null) {
			result.fillWithResult(GeographicalAreaResultType.geographicalAreaExists);
			return result;
		}
		
		GeographicalAreaPO newArea = new GeographicalAreaPO();
		newArea.setAreaName(param.getAreaName());
		newArea.setAreaType(param.getAreaType());
		newArea.setBelongTo(belongTo);
		geographicalAreaMapper.insertSelective(newArea);
		
		if(newArea.getId() == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		
		result.setNewAreaId(newArea.getId());
		result.setIsSuccess();
		
		return result;
	}

	@Override
	public CommonResult deleteGeographicalArea(DeleteGeographicalAreaParam controllerParam) {
		CommonResult result = new CommonResult();
		if(controllerParam.getId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		geographicalAreaMapper.deleteById(controllerParam.getId());
		result.setIsSuccess();
		return result;
	}

	@Override
	public GeographicalAreaPO findById(FindGeographicalAreaByIdParam param) {
		if(param == null || param.getGeographicalId() == null) {
			return new GeographicalAreaPO();
		}
		return geographicalAreaMapper.findArea(param.getGeographicalId());
	}
}