package demo.geographical.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.geographical.domain.constant.GeographicalUrl;
import demo.geographical.domain.param.controllerParam.AddNewGeographicalAreaParam;
import demo.geographical.domain.param.controllerParam.DeleteGeographicalAreaParam;
import demo.geographical.domain.param.controllerParam.FindByBelongIdParam;
import demo.geographical.domain.param.controllerParam.FindGeographicalAreaByIdParam;
import demo.geographical.domain.po.GeographicalAreaPO;
import demo.geographical.domain.result.AddNewGeographicalAreaResult;
import demo.geographical.domain.result.FindByBelongIdResult;
import demo.geographical.domain.type.GeographicalArea;
import demo.geographical.service.GeographicalService;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = GeographicalUrl.root)
public class GeographicalController extends CommonController {

	@Autowired
	private GeographicalService geographicalService;
	
	@PostMapping(value = GeographicalUrl.findByBelongId)
	public void findByBelongId(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		JSONObject jsonInput = getJson(data);
		FindByBelongIdParam param = new FindByBelongIdParam().fromJson(jsonInput);
		FindByBelongIdResult serviceResult = geographicalService.FindByBelongId(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = GeographicalUrl.findFromCityID)
	public void findFromCityID(HttpServletRequest request, HttpServletResponse response) {
		FindByBelongIdParam param = new FindByBelongIdParam();
		param.setAreaParentId(GeographicalArea.g1.getAreaId());
		FindByBelongIdResult serviceResult = geographicalService.FindByBelongId(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	public FindByBelongIdResult findByBelongId(Long belongId) {
		FindByBelongIdParam param = new FindByBelongIdParam();
		param.setAreaParentId(belongId);
		FindByBelongIdResult serviceResult = geographicalService.FindByBelongId(param);
		return serviceResult;
	}
	
	@PostMapping(value = GeographicalUrl.deleteGeographicalArea)
	public void deleteGeographicalArea(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		JSONObject jsonInput = getJson(data);
		DeleteGeographicalAreaParam controllerParam = new DeleteGeographicalAreaParam().fromJson(jsonInput);
		CommonResult result = geographicalService.deleteGeographicalArea(controllerParam);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = GeographicalUrl.addNewGeographicalArea)
	public void addNewGeographicalArea(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		JSONObject jsonInput = getJson(data);
		AddNewGeographicalAreaParam controllerParam = new AddNewGeographicalAreaParam().fromJson(jsonInput);
		AddNewGeographicalAreaResult result = geographicalService.addNewGeographicalArea(controllerParam);
		outputJson(response, JSONObject.fromObject(result));
	}

	@PostMapping(value = GeographicalUrl.findById)
	public void findById(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		JSONObject jsonInput = getJson(data);
		FindGeographicalAreaByIdParam param = new FindGeographicalAreaByIdParam().fromJson(jsonInput);
		GeographicalAreaPO po = geographicalService.findById(param);
		outputJson(response, JSONObject.fromObject(po));
	}
}
