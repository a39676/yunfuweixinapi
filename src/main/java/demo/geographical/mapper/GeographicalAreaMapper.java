package demo.geographical.mapper;

import java.util.List;

import demo.geographical.domain.param.mapperParam.FindByBelongIdListParam;
import demo.geographical.domain.param.mapperParam.FindGroByAreaTypeAreaNameParam;
import demo.geographical.domain.po.GeographicalAreaPO;

public interface GeographicalAreaMapper {
    int insert(GeographicalAreaPO record);

    int insertSelective(GeographicalAreaPO record);
    
    int insertOrUpdate(GeographicalAreaPO newGeo);
    
    GeographicalAreaPO findArea(Long id);
    
    List<GeographicalAreaPO> findByIdList(List<Long> geographicalIdList);
    
    List<GeographicalAreaPO> findByBelongIdList(FindByBelongIdListParam param);
    
    List<Long> findAreaIdByNoParent();
    
    int deleteById(Long id);
    
    GeographicalAreaPO findGroByAreaTypeAreaName(FindGroByAreaTypeAreaNameParam param);
}