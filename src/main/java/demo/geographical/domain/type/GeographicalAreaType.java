package demo.geographical.domain.type;

/**
 * 地理位置类型 
 */
public enum GeographicalAreaType {
	
	street("street", 1), // 乡，镇，街道
	county("county", 2), // 区，县
	city("city", 3), // 市
	;
	
	private String typeName;
	private Integer typeCode;
	
	GeographicalAreaType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static GeographicalAreaType getType(String typeName) {
		for(GeographicalAreaType t : GeographicalAreaType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static GeographicalAreaType getType(Integer typeCode) {
		for(GeographicalAreaType t : GeographicalAreaType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
