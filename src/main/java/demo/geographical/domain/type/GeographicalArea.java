package demo.geographical.domain.type;

/**
 * 固定地理位置区域
 * 前端工作紧,要求省下地理区域管理页面,需要将区域固定
 */
public enum GeographicalArea {
	
	g1("云浮市", 1L, null, 3),
	
	g1_1("云城区", 2L, 1L, 2),
	g1_2("罗定市", 3L, 1L, 2),
	g1_3("云安区", 4L, 1L, 2),
	g1_4("新兴县", 5L, 1L, 2),
	g1_5("郁南县", 6L, 1L, 2),
	
	g1_2_1("云城街道", 7L, 2L, 1), 
	g1_2_2("高峰街道", 8L, 2L, 1), 
	g1_2_3("河口街道", 9L, 2L, 1), 
	g1_2_4("安塘街道", 10L, 2L, 1), 	
	g1_2_5("腰古镇", 11L, 2L, 1), 
	g1_2_6("思劳镇", 12L, 2L, 1), 
	g1_2_7("前锋镇", 13L, 2L, 1), 
	g1_2_8("南盛镇", 14L, 2L, 1),
	
	g1_3_1("罗城街", 15L, 3L, 1), 
	g1_3_2("附城街", 16L, 3L, 1), 
	g1_3_3("双东街", 17L, 3L, 1), 
	g1_3_4("素龙街道", 18L, 3L, 1), 
	g1_3_5("华石", 19L, 3L, 1), 
	g1_3_7("金鸡", 21L, 3L, 1), 
	g1_3_8("围底", 22L, 3L, 1), 
	g1_3_9("苹塘", 23L, 3L, 1), 
	g1_3_10("船步", 24L, 3L, 1), 
	g1_3_11("罗平", 25L, 3L, 1), 
	g1_3_12("太平", 26L, 3L, 1), 
	g1_3_13("罗镜", 27L, 3L, 1), 
	g1_3_14("生江", 28L, 3L, 1), 
	g1_3_15("连州", 29L, 3L, 1), 
	g1_3_16("分界", 30L, 3L, 1), 
	g1_3_17("黎少", 31L, 3L, 1), 
	g1_3_18("泗纶", 32L, 3L, 1), 
	g1_3_19("加益", 33L, 3L, 1), 
	g1_3_20("榃滨", 34L, 3L, 1), 
	g1_3_21("龙湾镇", 35L, 3L, 1), 
	
	g1_4_1("六都镇", 36L, 4L, 1), 
	g1_4_2("高村镇", 37L, 4L, 1), 
	g1_4_3("白石镇", 38L, 4L, 1), 
	g1_4_4("镇安镇", 39L, 4L, 1), 
	g1_4_5("富林镇", 40L, 4L, 1), 
	g1_4_6("石城镇", 41L, 4L, 1), 
	g1_4_7("都杨镇", 42L, 4L, 1), 
	
	g1_5_1("新城镇", 43L, 5L, 1),
	g1_5_2("水台镇", 44L, 5L, 1),
	g1_5_3("车岗镇", 45L, 5L, 1),
	g1_5_4("东成镇", 46L, 5L, 1),
	g1_5_5("稔村镇", 47L, 5L, 1),
	g1_5_6("太平镇", 48L, 5L, 1),
	g1_5_7("六祖镇", 49L, 5L, 1),
	g1_5_8("大江镇", 50L, 5L, 1),
	g1_5_9("河头镇", 51L, 5L, 1),
	g1_5_10("天堂镇", 52L, 5L, 1),
	g1_5_11("簕竹镇", 53L, 5L, 1),
	g1_5_12("里洞镇", 54L, 5L, 1),
	
	g1_6_1("都城镇", 55L, 6L, 1), 
	g1_6_2("东坝镇", 56L, 6L, 1), 
	g1_6_3("宋桂镇", 57L, 6L, 1), 
	g1_6_4("连滩镇", 58L, 6L, 1), 
	g1_6_5("河口镇", 59L, 6L, 1), 
	g1_6_6("大湾镇", 60L, 6L, 1), 
	g1_6_7("建城镇", 61L, 6L, 1), 
	g1_6_8("千官镇", 62L, 6L, 1), 
	g1_6_9("通门镇", 63L, 6L, 1), 
	g1_6_10("桂圩镇", 64L, 6L, 1), 
	g1_6_11("平台镇", 65L, 6L, 1), 
	g1_6_12("宝珠镇", 66L, 6L, 1), 
	g1_6_13("历洞镇", 67L, 6L, 1), 
	g1_6_14("大方镇", 68L, 6L, 1), 
	g1_6_15("南江口镇", 69L, 6L, 1), 
	;
	
	private String areaName;
	private Long areaId;
	private Long belongTo;
	private Integer areaType;
	
	GeographicalArea(String typeName, Long areaId, Long belongTo, Integer areaType) {
		this.areaName = typeName;
		this.areaId = areaId;
		this.belongTo = belongTo;
		this.areaType = areaType;
	}
	

	public String getAreaName() {
		return areaName;
	}

	public Long getAreaId() {
		return areaId;
	}
	
	public Long getBelongTo() {
		return belongTo;
	}
	
	public Integer getAreaType() {
		return areaType;
	}

	public static GeographicalArea getType(String typeName) {
		for(GeographicalArea t : GeographicalArea.values()) {
			if(t.getAreaName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static GeographicalArea getType(Integer typeCode) {
		if(typeCode == null) {
			return null;
		}
		for(GeographicalArea t : GeographicalArea.values()) {
			if(t.getAreaId().equals(typeCode.longValue())) {
				return t;
			}
		}
		return null;
	}

}
