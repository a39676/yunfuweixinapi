package demo.geographical.domain.type;

public enum GeographicalAreaResultType {
	
	nullGeographicalAreaName("区域名为空", "-3-4-1"),
	nullGeographicalAreaType("区域类型为空", "-3-4-2"),
	geographicalAreaExists("区域已存在", "-3-4-3"),
	
	communicationError("网络通讯异常", "-8-1"),
	;
	
	
	private String resultName;
	private String resultCode;
	
	GeographicalAreaResultType(String name, String code) {
		this.resultName = name;
		this.resultCode = code;
	}

	public String getName() {
		return this.resultName;
	}
	
	public String getCode() {
		return this.resultCode;
	}
}
