package demo.geographical.domain.constant;

public class GeographicalUrl {
	
	public static final String root = "/geographical";
	public static final String findById = "/findById";
	public static final String findByBelongId = "/findByBelongId";
	public static final String findFromCityID = "/findFromCityID";
	public static final String deleteGeographicalArea = "/deleteGeographicalArea";
	public static final String addNewGeographicalArea = "/addNewGeographicalArea";
}
