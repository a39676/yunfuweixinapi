package demo.geographical.domain.result;

import demo.common.domain.result.CommonResult;
import demo.geographical.domain.type.GeographicalAreaResultType;

public class AddNewGeographicalAreaResult extends CommonResult {

	private Long newAreaId;

	public Long getNewAreaId() {
		return newAreaId;
	}

	public void setNewAreaId(Long newAreaId) {
		this.newAreaId = newAreaId;
	}

	public void fillWithResult(GeographicalAreaResultType resultType) {
		this.setResult(resultType.getCode());
		this.setMessage(resultType.getName());
	}

}
