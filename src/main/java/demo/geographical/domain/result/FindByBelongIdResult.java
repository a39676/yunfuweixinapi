package demo.geographical.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.geographical.domain.po.GeographicalAreaPO;

public class FindByBelongIdResult extends CommonResult {

	private List<GeographicalAreaPO> areaList;

	public List<GeographicalAreaPO> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<GeographicalAreaPO> areaList) {
		this.areaList = areaList;
	}

	@Override
	public String toString() {
		return "GetAreaByTypeResult [areaList=" + areaList + "]";
	}

}
