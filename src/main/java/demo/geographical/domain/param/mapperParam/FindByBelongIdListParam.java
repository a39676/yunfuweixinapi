package demo.geographical.domain.param.mapperParam;

import java.util.List;

public class FindByBelongIdListParam {

	private List<Long> belongIdList;

	public List<Long> getBelongIdList() {
		return belongIdList;
	}

	public void setBelongIdList(List<Long> belongIdList) {
		this.belongIdList = belongIdList;
	}

	@Override
	public String toString() {
		return "FindByBelongIdListParam []";
	}

}
