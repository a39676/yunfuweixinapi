package demo.geographical.domain.param.mapperParam;

public class FindGroByAreaTypeAreaNameParam {

	private String areaName;
	private Integer areaType;

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getAreaType() {
		return areaType;
	}

	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}

	@Override
	public String toString() {
		return "FindGroByAreaTypeAreaNameParam [areaName=" + areaName + ", areaType=" + areaType + "]";
	}

}
