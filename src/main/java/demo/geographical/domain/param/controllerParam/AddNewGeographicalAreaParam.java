package demo.geographical.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import demo.geographical.domain.type.GeographicalAreaType;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class AddNewGeographicalAreaParam implements CommonControllerParam {

	/**
	 * {@link GeographicalAreaType}
	 */
	private Integer areaType;

	private String areaName;

	private Long belongTo;

	public Integer getAreaType() {
		return areaType;
	}

	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(Long belongTo) {
		this.belongTo = belongTo;
	}

	@Override
	public String toString() {
		return "AddNewGeographicalAreaParam [areaType=" + areaType + ", areaName=" + areaName + ", belongTo=" + belongTo
				+ "]";
	}

	@Override
	public AddNewGeographicalAreaParam fromJson(JSONObject json) {
		AddNewGeographicalAreaParam param = new AddNewGeographicalAreaParam();

		if (json.containsKey("areaType") && NumericUtilCustom.matchInteger(json.getString("areaType"))) {
			param.setAreaType(json.getInt("areaType"));
		}
		if (json.containsKey("belongTo") && NumericUtilCustom.matchInteger(json.getString("belongTo"))) {
			param.setBelongTo(json.getLong("belongTo"));
		}
		if (json.containsKey("areaName")) {
			param.setAreaName(json.getString("areaName"));
		}

		return param;
	}

}
