package demo.geographical.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindByBelongIdParam implements CommonControllerParam {

	private Long areaParentId;

	public Long getAreaParentId() {
		return areaParentId;
	}

	public void setAreaParentId(Long areaParentId) {
		this.areaParentId = areaParentId;
	}

	@Override
	public String toString() {
		return "GetAreaByTypeParam [areaParentId=" + areaParentId + "]";
	}

	@Override
	public FindByBelongIdParam fromJson(JSONObject json) {
		FindByBelongIdParam param = new FindByBelongIdParam();

		if (json.containsKey("areaParentId") && NumericUtilCustom.matchInteger(json.getString("areaParentId"))) {
			param.setAreaParentId(json.getLong("areaParentId"));
		}

		return param;
	}

}
