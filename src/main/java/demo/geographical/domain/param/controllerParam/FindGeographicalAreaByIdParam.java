package demo.geographical.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindGeographicalAreaByIdParam implements CommonControllerParam {

	private Long geographicalId;

	public Long getGeographicalId() {
		return geographicalId;
	}

	public void setGeographicalId(Long geographicalId) {
		this.geographicalId = geographicalId;
	}

	@Override
	public FindGeographicalAreaByIdParam fromJson(JSONObject json) {
		FindGeographicalAreaByIdParam param = new FindGeographicalAreaByIdParam();
		if (json.containsKey("geographicalId") && NumericUtilCustom.matchInteger(json.getString("geographicalId"))) {
			param.setGeographicalId(json.getLong("geographicalId"));
		}

		return param;
	}

}
