package demo.geographical.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class DeleteGeographicalAreaParam implements CommonControllerParam {

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeleteGeographicalAreaParam [id=" + id + "]";
	}

	@Override
	public DeleteGeographicalAreaParam fromJson(JSONObject json) {
		DeleteGeographicalAreaParam param = new DeleteGeographicalAreaParam();

		if (json.containsKey("id") && NumericUtilCustom.matchInteger(json.getString("id"))) {
			param.setId(json.getLong("id"));
		}

		return param;
	}

}
