package demo.products.domain.param.mapperParam;

import java.util.Date;

public class ProductIdExistsParam {

	private Boolean isDelete = false;
	private Long productId;
	private Date expirationTime;
	private String productName;
	private String productCode;
	private Integer productType;
	private Integer productSubType;

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	@Override
	public String toString() {
		return "ProductIdExistsParam [isDelete=" + isDelete + ", productId=" + productId + ", expirationTime="
				+ expirationTime + ", productName=" + productName + ", productCode=" + productCode + ", productType="
				+ productType + ", productSubType=" + productSubType + "]";
	}

}
