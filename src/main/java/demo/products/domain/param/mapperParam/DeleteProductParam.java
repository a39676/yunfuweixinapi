package demo.products.domain.param.mapperParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class DeleteProductParam implements CommonControllerParam {

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public DeleteProductParam fromJson(JSONObject j) {
		DeleteProductParam p = new DeleteProductParam();
		if (j.containsKey("id") && NumericUtilCustom.matchInteger(j.getString("id"))) {
			p.setId(j.getLong("id"));
		}
		return p;
	}

}
