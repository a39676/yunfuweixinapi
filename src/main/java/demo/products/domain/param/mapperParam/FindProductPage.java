package demo.products.domain.param.mapperParam;

import java.util.Date;

public class FindProductPage {

	private Integer productType;
	private Integer productSubType;
	private Long orgId;
	private Date expirationTime;
	private String productName;
	private String productCode;
	private Integer pageSize;
	private Integer pageStart;
	private Boolean isDelete = false;

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageStart() {
		return pageStart;
	}

	public void setPageStart(Integer pageStart) {
		this.pageStart = pageStart;
	}

	@Override
	public String toString() {
		return "FindProductPageForAdmin [productType=" + productType + ", productSubType=" + productSubType + ", orgId="
				+ orgId + ", expirationTime=" + expirationTime + ", productName=" + productName + ", productCode="
				+ productCode + ", pageSize=" + pageSize + ", pageStart=" + pageStart + ", isDelete=" + isDelete + "]";
	}

}
