package demo.products.domain.param.mapperParam;

import java.util.Date;

public class FindProductPageForAppMapperParam {

	private Date startTime;
	private Date endTime;
	private Integer productType;
	private Integer productSubType;
	private Integer limit = 10;

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "FindProductPageForAppMapperParam [endTime=" + endTime + ", productType=" + productType
				+ ", productSubType=" + productSubType + ", limit=" + limit + "]";
	}

}
