package demo.products.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import demo.products.domain.type.ProductType;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindProductInfoPageForAppParam implements CommonControllerParam {

	/** {@link ProductType} */
	private Integer productType;
	private Integer productSubType;
	private Integer pageNo = 1;

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	@Override
	public String toString() {
		return "FindProductInfoPageForAppParam [productType=" + productType + ", productSubType=" + productSubType
				+ ", pageNo=" + pageNo + "]";
	}

	@Override
	public FindProductInfoPageForAppParam fromJson(JSONObject json) {
		FindProductInfoPageForAppParam param = new FindProductInfoPageForAppParam();
		if (json.containsKey("productType") && NumericUtilCustom.matchInteger(json.getString("productType"))) {
			param.setProductType(json.getInt("productType"));
		}
		if (json.containsKey("productSubType") && NumericUtilCustom.matchInteger(json.getString("productSubType"))) {
			param.setProductSubType(json.getInt("productSubType"));
		}
		if (json.containsKey("pageNo") && NumericUtilCustom.matchInteger(json.getString("pageNo"))) {
			param.setPageNo(json.getInt("pageNo"));
		}
		return param;
	}

}
