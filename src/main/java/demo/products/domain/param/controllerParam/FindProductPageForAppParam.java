package demo.products.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindProductPageForAppParam implements CommonControllerParam {

	private Integer productType;
	private Integer productSubType;

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	@Override
	public String toString() {
		return "FindProductPageForAppParam [productType=" + productType + ", productSubType=" + productSubType + "]";
	}

	@Override
	public FindProductPageForAppParam fromJson(JSONObject json) {
		FindProductPageForAppParam param = new FindProductPageForAppParam();
		if (json.containsKey("productType") && NumericUtilCustom.matchInteger(json.getString("productType"))) {
			param.setProductType(json.getInt("productType"));
		}
		if (json.containsKey("productSubType") && NumericUtilCustom.matchInteger(json.getString("productSubType"))) {
			param.setProductSubType(json.getInt("productSubType"));
		}
		return param;
	}

}
