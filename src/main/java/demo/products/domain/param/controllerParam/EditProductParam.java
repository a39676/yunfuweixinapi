package demo.products.domain.param.controllerParam;

import java.math.BigDecimal;

import dateHandle.DateUtilCustom;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class EditProductParam extends AddNewProductParam  implements CommonControllerParam {

	private Long productId;

	

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "EditProductParam [productId=" + productId + ", getProductCode()=" + getProductCode()
				+ ", getProductName()=" + getProductName() + ", getProductIntroduction()=" + getProductIntroduction()
				+ ", getArticle()=" + getArticle() + ", getExpirationTime()=" + getExpirationTime()
				+ ", getInterestRate()=" + getInterestRate() + ", getInterestType()=" + getInterestType()
				+ ", getProductType()=" + getProductType() + ", getProductSubType()=" + getProductSubType()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}

	@Override
	public EditProductParam fromJson(JSONObject j) {
		EditProductParam p = new EditProductParam();

		if (j.containsKey("productId") && NumericUtilCustom.matchInteger(j.getString("productId"))) {
			p.setProductId(j.getLong("productId"));
		}
		if (j.containsKey("productCode")) {
			p.setProductCode(j.getString("productCode"));
		}
		if (j.containsKey("productName")) {
			p.setProductName(j.getString("productName"));
		}
		if (j.containsKey("productIntroduction")) {
			p.setProductIntroduction(j.getString("productIntroduction"));
		}
		if (j.containsKey("article")) {
			p.setArticle(j.getString("article"));
		}
		if (j.containsKey("productType") && NumericUtilCustom.matchInteger(j.getString("productType"))) {
			p.setProductType(j.getInt("productType"));
		}
		if (j.containsKey("productSubType") && NumericUtilCustom.matchInteger(j.getString("productSubType"))) {
			p.setProductSubType(j.getInt("productSubType"));
		}
		if (j.containsKey("expirationTime") && DateUtilCustom.isNormalDate(j.getString("expirationTime"))) {
			p.setExpirationTime(j.getString("expirationTime"));
		}
		if (j.containsKey("interestRate") && (NumericUtilCustom.matchDecimal(j.getString("interestRate"))
				|| NumericUtilCustom.matchInteger(j.getString("interestRate")))) {
			p.setInterestRate(new BigDecimal(j.getString("interestRate")));
		}
		if (j.containsKey("interestType") && NumericUtilCustom.matchInteger(j.getString("interestType"))) {
			p.setInterestType(j.getInt("interestType"));
		}
		if (j.containsKey("insuranceAmountMin") && (NumericUtilCustom.matchDecimal(j.getString("insuranceAmountMin")) || NumericUtilCustom.matchInteger(j.getString("insuranceAmountMin")))) {
			p.setInsuranceAmountMin(new BigDecimal(j.getString("insuranceAmountMin")));
		}
		if (j.containsKey("insuranceAmountMax") && (NumericUtilCustom.matchDecimal(j.getString("insuranceAmountMax")) || NumericUtilCustom.matchInteger(j.getString("insuranceAmountMax")))) {
			p.setInsuranceAmountMax(new BigDecimal(j.getString("insuranceAmountMax")));
		}

		return p;
	}

}
