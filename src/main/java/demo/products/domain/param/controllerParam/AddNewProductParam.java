package demo.products.domain.param.controllerParam;

import java.math.BigDecimal;

import dateHandle.DateUtilCustom;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class AddNewProductParam implements CommonControllerParam {

	private String productCode;
	private String productName;
	private String productIntroduction;
	private String article;
	private Integer productType;
	private Integer productSubType;
	private String expirationTime;
	private BigDecimal interestRate;
	private Integer interestType;
	private BigDecimal insuranceAmountMin;
	private BigDecimal insuranceAmountMax;
	private BigDecimal amount;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductIntroduction() {
		return productIntroduction;
	}

	public void setProductIntroduction(String productIntroduction) {
		this.productIntroduction = productIntroduction;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getInterestType() {
		return interestType;
	}

	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	public BigDecimal getInsuranceAmountMin() {
		return insuranceAmountMin;
	}

	public void setInsuranceAmountMin(BigDecimal insuranceAmountMin) {
		this.insuranceAmountMin = insuranceAmountMin;
	}

	public BigDecimal getInsuranceAmountMax() {
		return insuranceAmountMax;
	}

	public void setInsuranceAmountMax(BigDecimal insuranceAmountMax) {
		this.insuranceAmountMax = insuranceAmountMax;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "AddNewProductParam [productCode=" + productCode + ", productName=" + productName
				+ ", productIntroduction=" + productIntroduction + ", article=" + article + ", productType="
				+ productType + ", productSubType=" + productSubType + ", expirationTime=" + expirationTime
				+ ", interestRate=" + interestRate + ", interestType=" + interestType + ", insuranceAmountMin="
				+ insuranceAmountMin + ", insuranceAmountMax=" + insuranceAmountMax + "]";
	}

	@Override
	public AddNewProductParam fromJson(JSONObject json) {
		AddNewProductParam param = new AddNewProductParam();

		if (json.containsKey("productCode")) {
			param.setProductCode(json.getString("productCode"));
		}
		if (json.containsKey("productName")) {
			param.setProductName(json.getString("productName"));
		}
		if (json.containsKey("productIntroduction")) {
			param.setProductIntroduction(json.getString("productIntroduction"));
		}
		if (json.containsKey("article")) {
			param.setArticle(json.getString("article"));
		}
		if (json.containsKey("productType") && NumericUtilCustom.matchInteger(json.getString("productType"))) {
			param.setProductType(json.getInt("productType"));
		}
		if (json.containsKey("productSubType") && NumericUtilCustom.matchInteger(json.getString("productSubType"))) {
			param.setProductSubType(json.getInt("productSubType"));
		}
		if (json.containsKey("expirationTime") && DateUtilCustom.isNormalDate(json.getString("expirationTime"))) {
			param.setExpirationTime(json.getString("expirationTime"));
		}
		if (json.containsKey("interestRate") && (NumericUtilCustom.matchDecimal(json.getString("interestRate"))
				|| NumericUtilCustom.matchInteger(json.getString("interestRate")))) {
			param.setInterestRate(new BigDecimal(json.getString("interestRate")));
		}
		if (json.containsKey("interestType") && NumericUtilCustom.matchInteger(json.getString("interestType"))) {
			param.setInterestType(json.getInt("interestType"));
		}
		if (json.containsKey("insuranceAmountMin")
				&& (NumericUtilCustom.matchDecimal(json.getString("insuranceAmountMin"))
						|| NumericUtilCustom.matchInteger(json.getString("insuranceAmountMin")))) {
			param.setInsuranceAmountMin(new BigDecimal(json.getString("insuranceAmountMin")));
		}
		if (json.containsKey("insuranceAmountMax")
				&& (NumericUtilCustom.matchDecimal(json.getString("insuranceAmountMax"))
						|| NumericUtilCustom.matchInteger(json.getString("insuranceAmountMax")))) {
			param.setInsuranceAmountMax(new BigDecimal(json.getString("insuranceAmountMax")));
		}
		if (json.containsKey("amount") && (NumericUtilCustom.matchDecimal(json.getString("amount"))
				|| NumericUtilCustom.matchInteger(json.getString("amount")))) {
			param.setAmount(new BigDecimal(json.getString("amount")));
		}

		return param;
	}

}
