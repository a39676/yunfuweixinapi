package demo.products.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindProductParam implements CommonControllerParam {

	private Long productId;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "FindProductParam [productId=" + productId + "]";
	}


	@Override
	public FindProductParam fromJson(JSONObject json) {
		FindProductParam param = new FindProductParam();

		if (json.containsKey("productId") && NumericUtilCustom.matchInteger(json.getString("productId"))) {
			param.setProductId(json.getLong("productId"));
		}

		return param;
	}

}
