package demo.products.domain.param.controllerParam;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import dateHandle.DateUtilCustom;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindProductPageParam implements CommonControllerParam {

	private Integer productType;
	private Integer productSubType;
	private Long orgId;
	private Date expirationTime;
	private String productName;
	private String productCode;
	private Integer pageSize = 10;
	private Integer pageNo = 1;

	@Override
	public FindProductPageParam fromJson(JSONObject j) {
		FindProductPageParam p = new FindProductPageParam();
		if (j.containsKey("productType") && NumericUtilCustom.matchInteger(j.getString("productType"))) {
			p.setProductType(j.getInt("productType"));
		}
		if (j.containsKey("productSubType") && NumericUtilCustom.matchInteger(j.getString("productSubType"))) {
			p.setProductSubType(j.getInt("productSubType"));
		}
		if (j.containsKey("orgId") && NumericUtilCustom.matchInteger(j.getString("orgId"))) {
			p.setOrgId(j.getLong("orgId"));
		}
		if (j.containsKey("pageSize") && NumericUtilCustom.matchInteger(j.getString("pageSize"))) {
			p.setPageSize(j.getInt("pageSize"));
		}
		if (j.containsKey("pageNo") && NumericUtilCustom.matchInteger(j.getString("pageNo"))) {
			p.setPageNo(j.getInt("pageNo"));
		}
		if (j.containsKey("expirationTime") && DateUtilCustom.isDateValid(j.getString("expirationTime"))) {
			p.setExpirationTime(DateUtilCustom.stringToDateUnkonwFormat(j.getString("expirationTime")));
		}
		if (j.containsKey("productName") && StringUtils.isNotBlank(j.getString("productName"))) {
			p.setProductName(j.getString("productName"));
		}
		if (j.containsKey("productCode") && StringUtils.isNotBlank(j.getString("productCode"))) {
			p.setProductCode(j.getString("productCode"));
		}
		return p;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	@Override
	public String toString() {
		return "FindProductPageForAdminParam [productType=" + productType + ", productSubType=" + productSubType
				+ ", orgId=" + orgId + ", expirationTime=" + expirationTime + ", productName=" + productName
				+ ", productCode=" + productCode + ", pageSize=" + pageSize + ", pageNo=" + pageNo + "]";
	}

}
