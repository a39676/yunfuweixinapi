package demo.products.domain.type;

/**
 * 证券产品类型 
 */
public enum ProductSecuritiesType {
	
//	证券， 股票， 其他
	securities("证券", 1), 
	stock("股票", 2), 
	other("其他证券服务", 3), 
	;
	
	private String typeName;
	private Integer typeCode;
	
	ProductSecuritiesType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static ProductSecuritiesType getType(String typeName) {
		for(ProductSecuritiesType t : ProductSecuritiesType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static ProductSecuritiesType getType(Integer typeCode) {
		for(ProductSecuritiesType t : ProductSecuritiesType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
