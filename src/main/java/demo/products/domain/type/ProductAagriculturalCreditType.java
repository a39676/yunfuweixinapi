package demo.products.domain.type;

/**
 * 信用三农产品类型 
 */
public enum ProductAagriculturalCreditType {
	
//	筹资产品， 融资申请
	fundraisingProducts("筹资产品", 1), 
//	financialApplication("融资申请", 2), 
	;
	
	private String typeName;
	private Integer typeCode;
	
	ProductAagriculturalCreditType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static ProductAagriculturalCreditType getType(String typeName) {
		for(ProductAagriculturalCreditType t : ProductAagriculturalCreditType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static ProductAagriculturalCreditType getType(Integer typeCode) {
		for(ProductAagriculturalCreditType t : ProductAagriculturalCreditType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
