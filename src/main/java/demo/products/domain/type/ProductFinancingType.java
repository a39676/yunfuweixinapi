package demo.products.domain.type;

/**
 * 融资产品类型 
 */
public enum ProductFinancingType {
	
//	小微企业贷款，购房贷款，购车贷款，个人消费贷款，个人经营性贷款，其他贷款
	smallBusiness("小微企业贷款", 1), 
	buyHouse("购房贷款", 2), 
	buyCar("购车贷款", 3), 
	personalConsumption("个人消费贷款", 4), 
	personalBusiness("个人经营性贷款", 5),
	other("其他贷款", 6),
	;
	
	private String typeName;
	private Integer typeCode;
	
	ProductFinancingType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static ProductFinancingType getType(String typeName) {
		for(ProductFinancingType t : ProductFinancingType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static ProductFinancingType getType(Integer typeCode) {
		for(ProductFinancingType t : ProductFinancingType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
