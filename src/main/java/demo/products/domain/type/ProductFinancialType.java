package demo.products.domain.type;

/**
 * 理财产品类型 
 */
public enum ProductFinancialType {
	
//	存款、国债、货币基金、理财产品、其他理财
	deposit("存款", 1), 
	nationalDebt("国债", 2), 
	monetaryFund("货币基金", 3), 
	financial("理财", 4), 
	other("其他理财", 5), 
	;
	
	private String typeName;
	private Integer typeCode;
	
	ProductFinancialType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static ProductFinancialType getType(String typeName) {
		for(ProductFinancialType t : ProductFinancialType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static ProductFinancialType getType(Integer typeCode) {
		for(ProductFinancialType t : ProductFinancialType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
