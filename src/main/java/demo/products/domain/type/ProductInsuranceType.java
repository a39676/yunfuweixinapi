package demo.products.domain.type;

/**
 * 保险产品类型 
 */
public enum ProductInsuranceType {
	
//	人寿保险，财产保险，医疗保险，其他保险服务
	lifeInsurance("人寿保险", 1), 
	propertyInsurance("财产保险", 2), 
	medicalInsurance("医疗保险", 3), 
	other("其他保险服务", 4), 
	;
	
	private String typeName;
	private Integer typeCode;
	
	ProductInsuranceType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static ProductInsuranceType getType(String typeName) {
		for(ProductInsuranceType t : ProductInsuranceType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static ProductInsuranceType getType(Integer typeCode) {
		for(ProductInsuranceType t : ProductInsuranceType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
