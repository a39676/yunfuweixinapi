package demo.products.domain.type;

public enum InterestType {
	
	/** 年化利率 */
	AnnualizedInterestRate("年化利率", 1),
	/** 月利率 */
	MonthlyInterestRate("月利率", 2),
	;
	
	
	private String typeName;
	private Integer typeCode;
	
	InterestType(String name, Integer code) {
		this.typeName = name;
		this.typeCode = code;
	}

	public String getName() {
		return this.typeName;
	}
	
	public Integer getCode() {
		return this.typeCode;
	}
	
	public static InterestType getType(Integer typeCode) {
		for(InterestType t : InterestType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}
}
