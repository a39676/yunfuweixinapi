package demo.products.domain.type;

public enum ProductType {

	/** 理财产品 */
	financial("理财产品", "financial", 1),
	/** 融资产品 */
	financing("融资产品", "financing", 2),
	/** 保险产品 */
	insurance("保险产品", "insurance", 3),
	/** 证券产品 */
	securities("证券产品", "securities", 4),
	/** 互联网+信用三农 */
	agriculturalCredit("互联网信用三农", "agriculturalCredit", 5),;

	private String typeName;
	private String typeEName;
	private Integer typeCode;

	ProductType(String name, String typeEName, Integer code) {
		this.typeName = name;
		this.typeEName = typeEName;
		this.typeCode = code;
	}

	public String getName() {
		return this.typeName;
	}

	public Integer getCode() {
		return this.typeCode;
	}

	public String getTypeEName() {
		return typeEName;
	}

	public static ProductType getType(Integer typeCode) {
		for (ProductType t : ProductType.values()) {
			if (t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}
}
