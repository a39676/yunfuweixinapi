package demo.products.domain.vo;

import java.math.BigDecimal;

import demo.products.domain.po.ProductsPO;

public class ProductVO extends ProductsPO {

	private String interestTypeName;

	private String expirationTimeString;

	private BigDecimal insuranceAmountMin;

	private BigDecimal insuranceAmountMax;

	public String getInterestTypeName() {
		return interestTypeName;
	}

	public void setInterestTypeName(String interestTypeName) {
		this.interestTypeName = interestTypeName;
	}

	public BigDecimal getInsuranceAmountMin() {
		return insuranceAmountMin;
	}

	public void setInsuranceAmountMin(BigDecimal insuranceAmountMin) {
		this.insuranceAmountMin = insuranceAmountMin;
	}

	public BigDecimal getInsuranceAmountMax() {
		return insuranceAmountMax;
	}

	public void setInsuranceAmountMax(BigDecimal insuranceAmountMax) {
		this.insuranceAmountMax = insuranceAmountMax;
	}

	public String getExpirationTimeString() {
		return expirationTimeString;
	}

	public void setExpirationTimeString(String expirationTimeString) {
		this.expirationTimeString = expirationTimeString;
	}

	@Override
	public String toString() {
		return "ProductVO [interestTypeName=" + interestTypeName + ", expirationTimeString=" + expirationTimeString
				+ ", insuranceAmountMin=" + insuranceAmountMin + ", insuranceAmountMax=" + insuranceAmountMax
				+ ", getWeights()=" + getWeights() + ", getId()=" + getId() + ", getCreateTime()=" + getCreateTime()
				+ ", getExpirationTime()=" + getExpirationTime() + ", getProductName()=" + getProductName()
				+ ", getProductCode()=" + getProductCode() + ", getInterestType()=" + getInterestType()
				+ ", getInterestRate()=" + getInterestRate() + ", getProductIntroduction()=" + getProductIntroduction()
				+ ", getProductSubType()=" + getProductSubType() + ", getProductType()=" + getProductType()
				+ ", getIsDelete()=" + getIsDelete() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
