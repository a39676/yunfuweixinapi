package demo.products.domain.result;

public enum ResultTypeProduct {

	nullProductCode("产品代码不能为空", "-2-1"),
	nullProductName("产品名称不能为空", "-2-2"),
	nullProductIntroduction("产品简介不能为空", "-2-3"),
//	nullExpirationTime("产品截止日期不能为空", "-2-4"),  // 为空视为长期， 确定前由前端提示
	nullInterestType("产品利率类型不能为空", "-2-6"),
	nullProductType("产品类型不能为空", "-2-7"),
	nullArticle("产品详细说明不能为空", "-2-8"),
	
	errorProductCode("产品代码参数异常", "-3-1"),
	errorProductName("产品名称参数异常", "-3-2"),
	errorProductIntroduction("产品简介参数异常", "-3-3"),
	errorExpirationTime("产品截止日期参数异常", "-3-4"),  // 为空视为长期， 确定前由前端提示
	errorInterestRate("产品利率参数异常", "-3-5"),
	errorInterestType("产品利率类型参数异常", "-3-6"),
	productIntroductionTooLong("产品简介过长", "-3-7"),
	productCodeTooLong("产品代码过长", "-3-8"),
	productNameTooLong("产品名称过长", "-3-9"),
	errorOrgId("无效的机构ID", "-3-10"),
	errorProductType("产品类型参数异常", "-3-11"),
	;
		
		
	private String typeName;
	private String typeCode;
	
	ResultTypeProduct(String name, String code) {
		this.typeName = name;
		this.typeCode = code;
	}
	
	public String getName() {
		return this.typeName;
	}
	
	public String getCode() {
		return this.typeCode;
	}
}
