package demo.products.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.products.domain.vo.ProductVO;

public class FindProductPageForAdminResult extends CommonResult {

	private List<ProductVO> productPage;
	private int resultCount;

	public List<ProductVO> getProductPage() {
		return productPage;
	}

	public void setProductPage(List<ProductVO> productPage) {
		this.productPage = productPage;
	}

	public int getResultCount() {
		return resultCount;
	}

	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}

	@Override
	public String toString() {
		return "FindProductPageForAdminResult [productPage=" + productPage + ", resultCount=" + resultCount + "]";
	}

}
