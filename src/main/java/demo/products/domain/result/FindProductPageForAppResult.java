package demo.products.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.products.domain.vo.ProductVO;

public class FindProductPageForAppResult extends CommonResult {

	private List<ProductVO> productList;
	private Integer resultCount;

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	public List<ProductVO> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductVO> productList) {
		this.productList = productList;
	}

	public void fillWithResult(ResultTypeProduct resultType) {
		this.setResult(resultType.getCode());
		this.setMessage(resultType.getName());
	}
}
