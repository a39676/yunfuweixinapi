package demo.products.domain.result;

import demo.common.domain.result.CommonResult;

public class AddNewProductResult extends CommonResult {

	private Long productId;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public void fillWithResult(ResultTypeProduct resultType) {
		this.setResult(resultType.getCode());
		this.setMessage(resultType.getName());
	}
}
