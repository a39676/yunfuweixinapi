package demo.products.domain.po;

import java.math.BigDecimal;
import java.util.Date;

import demo.products.domain.type.InterestType;

/**
 * -理财产品
 *
 */
public class ProductsPO {
	private Long id;

	private Date createTime;

	private Boolean isDelete;

	private Date expirationTime;

	private String productName;

	private String productCode;

	private Integer productType;

	private Integer productSubType;

	/** {@link InterestType} */
	private Integer interestType;

	private BigDecimal interestRate;

	private String productIntroduction;

	private Integer weights;

	private BigDecimal amount;

	public Integer getWeights() {
		return weights;
	}

	public void setWeights(Integer weights) {
		this.weights = weights;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName == null ? null : productName.trim();
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode == null ? null : productCode.trim();
	}

	public Integer getInterestType() {
		return interestType;
	}

	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public String getProductIntroduction() {
		return productIntroduction;
	}

	public void setProductIntroduction(String productIntroduction) {
		this.productIntroduction = productIntroduction == null ? null : productIntroduction.trim();
	}

	public Integer getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(Integer productSubType) {
		this.productSubType = productSubType;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "ProductsPO [id=" + id + ", createTime=" + createTime + ", isDelete=" + isDelete + ", expirationTime="
				+ expirationTime + ", productName=" + productName + ", productCode=" + productCode + ", productType="
				+ productType + ", productSubType=" + productSubType + ", interestType=" + interestType
				+ ", interestRate=" + interestRate + ", productIntroduction=" + productIntroduction + ", weights="
				+ weights + ", amount=" + amount + "]";
	}
}