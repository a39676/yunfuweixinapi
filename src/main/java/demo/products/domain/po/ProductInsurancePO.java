package demo.products.domain.po;

import java.math.BigDecimal;
import java.util.Date;

public class ProductInsurancePO {
    private Long id;

    private Date createTime;

    private Long productId;

    private Boolean isDelete;

    private BigDecimal insuranceAmountMin;

    private BigDecimal insuranceAmountMax;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public BigDecimal getInsuranceAmountMin() {
        return insuranceAmountMin;
    }

    public void setInsuranceAmountMin(BigDecimal insuranceAmountMin) {
        this.insuranceAmountMin = insuranceAmountMin;
    }

    public BigDecimal getInsuranceAmountMax() {
        return insuranceAmountMax;
    }

    public void setInsuranceAmountMax(BigDecimal insuranceAmountMax) {
        this.insuranceAmountMax = insuranceAmountMax;
    }
}