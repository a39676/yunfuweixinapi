package demo.products.domain.constant;

public class ProductUrl {

	public static final String root = "/product";
	public static final String financing = "/financing";
	public static final String financial = "/financial";
	public static final String insurance = "/insurance";
	public static final String securities = "/securities";
	public static final String agriculturalCredit = "/agriculturalCredit";
	public static final String exchangeDib = "/exchangeDib";
	public static final String productPageForApp = "/productPageForApp";
	public static final String findProductInfoPageForApp = "/findProductInfoPageForApp";
	public static final String addNewProduct = "/addNewProduct";
	public static final String findProduct = "/findProduct";
	
	public static final String findProductDetail = "/findProductDetail";
	
	public static final String findProductPageForAdmin = "/findProductPageForAdmin";
	public static final String deleteProduct = "/deleteProduct";
	public static final String editProduct = "/editProduct";
	
}