package demo.products.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dateHandle.DateUtilCustom;
import demo.article.controller.ArticleController;
import demo.article.domain.param.controllerParam.CreatingArticleProductParam;
import demo.article.domain.param.controllerParam.EditArticleProductParam;
import demo.base.base.domain.type.RolesType;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;
import demo.organizations.controller.OrganizationsController;
import demo.organizations.controller.StaffDetailController;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.mapper.StaffDetailPOMapper;
import demo.products.domain.param.controllerParam.AddNewProductParam;
import demo.products.domain.param.controllerParam.EditProductParam;
import demo.products.domain.param.controllerParam.FindProductPageParam;
import demo.products.domain.param.controllerParam.FindProductParam;
import demo.products.domain.param.mapperParam.DeleteProductParam;
import demo.products.domain.param.mapperParam.FindProductPage;
import demo.products.domain.param.mapperParam.ProductIdExistsParam;
import demo.products.domain.po.ProductInsurancePO;
import demo.products.domain.po.ProductOrganizationPO;
import demo.products.domain.po.ProductsPO;
import demo.products.domain.result.AddNewProductResult;
import demo.products.domain.result.FindProductPageForAdminResult;
import demo.products.domain.result.FindProductPageForAppResult;
import demo.products.domain.result.ResultTypeProduct;
import demo.products.domain.type.InterestType;
import demo.products.domain.type.ProductAagriculturalCreditType;
import demo.products.domain.type.ProductFinancialType;
import demo.products.domain.type.ProductFinancingType;
import demo.products.domain.type.ProductInsuranceType;
import demo.products.domain.type.ProductSecuritiesType;
import demo.products.domain.type.ProductType;
import demo.products.domain.vo.ProductVO;
import demo.products.mapper.ProductInsurancePOMapper;
import demo.products.mapper.ProductOrganizationPOMapper;
import demo.products.mapper.ProductsPOMapper;
import demo.products.service.ProductService;
import demo.util.BaseUtilCustom;

@Service
public class ProductServiceImpl extends CommonService implements ProductService {

	@Autowired
	private BaseUtilCustom baseUtilCustom;

	@Autowired
	private OrganizationsController orgController;
	@Autowired
	private ArticleController articleController;
	@Autowired
	private StaffDetailController staffController;

	@Autowired
	private ProductsPOMapper productMapper;
	@Autowired
	private ProductInsurancePOMapper productInsuranceMapper;
	@Autowired
	private ProductOrganizationPOMapper productOrgMapper;
	@Autowired
	private ProductInsurancePOMapper insurancePOMapoer;
	@Autowired
	private StaffDetailPOMapper staffDetailMapper;

	@Override
	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	public AddNewProductResult addNewProduct(Long userId, AddNewProductParam param) throws Exception {
		AddNewProductResult result = new AddNewProductResult();
		if (userId == null) {
			result.failWithMessage("请先登录");
			return result;
		}

		StaffDetailPO staffDetail = staffController.findByUserId(userId);
		OrganizationsPO org = orgController.findOrgByStaffId(staffDetail.getId());

		AddNewProductResult validResult = validProductParam(param);
		if(!validResult.isSuccess()) {
			return validResult;
		}

		Date newExDate = null;
		if (param.getExpirationTime() != null) {
			newExDate = DateUtilCustom.stringToDateUnkonwFormat(param.getExpirationTime());
		}
		
		// 如果参数无异常， 保存产品
		ProductsPO newProduct = new ProductsPO();
		newProduct.setExpirationTime(newExDate);
		newProduct.setProductType(param.getProductType());
		newProduct.setProductSubType(param.getProductSubType());
		newProduct.setInterestRate(param.getInterestRate());
		newProduct.setInterestType(param.getInterestType());
		newProduct.setProductCode(param.getProductCode());
		newProduct.setProductIntroduction(param.getProductIntroduction());
		newProduct.setProductName(param.getProductName());

		CommonResult insertProductResult = insertNewProduct(newProduct, org.getId());
		
		ProductType productType = ProductType.getType(param.getProductType());
		
		if(ProductType.insurance.equals(productType)) {
			insertOrUpdateInsuranceInfo(Long.parseLong(insertProductResult.getMessage()), param.getInsuranceAmountMin(), param.getInsuranceAmountMax());
		}
		
		// 增加保存产品详细说明逻辑
		CreatingArticleProductParam creatingArticleProductParam = new CreatingArticleProductParam();
		creatingArticleProductParam.setContent(param.getArticle());
		creatingArticleProductParam.setProductId(newProduct.getId());
		creatingArticleProductParam.setProductType(ProductType.financial.getCode());
		creatingArticleProductParam.setTitle(param.getProductName());
		creatingArticleProductParam.setUserId(userId);
		CommonResult saveArticleResult = articleController.addNewArticleProduct(creatingArticleProductParam);
		if (!saveArticleResult.isSuccess()) {
			throw new Exception();
		}

		result.setProductId(newProduct.getId());
		result.setIsSuccess();
		return result;
	}
	
	private CommonResult insertOrUpdateInsuranceInfo(Long productId, BigDecimal insuranceAmountMin, BigDecimal insuranceAmountMax) {
		ProductInsurancePO p = new ProductInsurancePO();
		p.setProductId(productId);
		p.setInsuranceAmountMax(insuranceAmountMax);
		p.setInsuranceAmountMin(insuranceAmountMin);
		int insertCount = productInsuranceMapper.insertOrUpdte(p);
		
		if(insertCount > 0) {
			return normalSuccess();
		} else {
			return errorParam();
		}
	}
	
	@Override
	public AddNewProductResult updateProduct(EditProductParam param) throws Exception {
		AddNewProductResult result = new AddNewProductResult();

		AddNewProductResult validResult = validProductParam(param);
		if(!validResult.isSuccess()) {
			return validResult;
		}

		Date newExDate = null;
		if (param.getExpirationTime() != null) {
			newExDate = DateUtilCustom.stringToDateUnkonwFormat(param.getExpirationTime());
		}
		
		// 如果参数无异常， 更新产品
		ProductsPO product = productMapper.findProduct(param.getProductId());
		if(product == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		product.setExpirationTime(newExDate);
		product.setProductSubType(param.getProductSubType());
		product.setInterestRate(param.getInterestRate());
		product.setInterestType(param.getInterestType());
		product.setProductCode(param.getProductCode());
		product.setProductIntroduction(param.getProductIntroduction());
		product.setProductName(param.getProductName());

		productMapper.updateProductInfo(product);
		
		ProductType productType = ProductType.getType(param.getProductType());
		
		if(ProductType.insurance.equals(productType)) {
			insertOrUpdateInsuranceInfo(product.getId(), param.getInsuranceAmountMin(), param.getInsuranceAmountMax());
		}

		// 编辑产品详细说明逻辑
		EditArticleProductParam subParam = new EditArticleProductParam();
		subParam.setContent(param.getArticle());
		subParam.setProductId(product.getId());
		subParam.setTitle(param.getProductName());
		CommonResult saveArticleResult = articleController.editArticleProduct(subParam);
		if (!saveArticleResult.isSuccess()) {
			throw new Exception();
		}

		result.setProductId(product.getId());
		result.setIsSuccess();
		return result;
	}
	
	private AddNewProductResult validProductParam(AddNewProductParam param) {
		AddNewProductResult result = new AddNewProductResult();
		ProductType productType = ProductType.getType(param.getProductType());
		if (productType == null || !validSubProductType(productType, param.getProductSubType())) {
			result.fillWithResult(ResultTypeProduct.errorProductType);
			return result;
		}
		
		if(ProductType.insurance.equals(productType)) {
			param.setInterestRate(BigDecimal.ZERO);
		}
		
		if (param.getInterestRate() == null) {
			result.fillWithResult(ResultTypeProduct.errorInterestRate);
			return result;
		}
		if (StringUtils.isBlank(param.getArticle())) {
			result.fillWithResult(ResultTypeProduct.nullArticle);
			return result;
		}
		if (param.getInterestType() == null) {
			result.fillWithResult(ResultTypeProduct.nullInterestType);
			return result;
		}
		InterestType interestType = InterestType.getType(param.getInterestType());
		if (interestType == null) {
			result.fillWithResult(ResultTypeProduct.errorInterestType);
			return result;
		}
		if (param.getProductType() == null || param.getProductSubType() == null) {
			result.fillWithResult(ResultTypeProduct.nullProductType);
			return result;
		}
		

		if (StringUtils.isBlank(param.getProductCode())) {
			result.fillWithResult(ResultTypeProduct.nullProductCode);
			return result;
		} else if (param.getProductCode().length() > 64) {
			result.fillWithResult(ResultTypeProduct.productCodeTooLong);
			return result;
		}
		if (StringUtils.isBlank(param.getProductName())) {
			result.fillWithResult(ResultTypeProduct.nullProductName);
			return result;
		} else if (param.getProductName().length() > 64) {
			result.fillWithResult(ResultTypeProduct.productNameTooLong);
			return result;
		}
		Date newExDate = null;
		if (param.getExpirationTime() != null) {
			newExDate = DateUtilCustom.stringToDateUnkonwFormat(param.getExpirationTime());
			if (newExDate.before(new Date())) {
				result.fillWithResult(ResultTypeProduct.errorExpirationTime);
				return result;
			}
		}
		if (StringUtils.isBlank(param.getProductIntroduction())) {
			result.fillWithResult(ResultTypeProduct.nullProductIntroduction);
			return result;
		} else if (param.getProductIntroduction().length() > 30) {
			result.fillWithResult(ResultTypeProduct.productIntroductionTooLong);
			return result;
		}

		result.setIsSuccess();
		return result;
	}

	@Override
	public ProductVO findProductVO(FindProductParam param) {
		ProductVO vo = null;
		if (param == null || param.getProductId() == null) {
			return new ProductVO();
		}

		ProductsPO product = productMapper.findProduct(param.getProductId());
		if (product == null) {
			return new ProductVO();
		}

		if (ProductType.insurance.getCode().equals(product.getProductType())) {
			ProductInsurancePO insurancePO = insurancePOMapoer.findByProductId(product.getId());
			vo = buildProductVO(product, insurancePO);
		} else {
			vo = buildProductVO(product);
		}

		return vo;
	}

	private CommonResult insertNewProduct(ProductsPO newProduct, Long orgId) throws java.lang.Exception {
		int insertCount = productMapper.insertSelective(newProduct);

		if (insertCount < 1 || newProduct.getId() == null) {
			throw new Exception();
		}

		ProductOrganizationPO newProOrg = new ProductOrganizationPO();
		newProOrg.setOrgId(orgId);
		newProOrg.setProductId(newProduct.getId());

		insertCount = productOrgMapper.insertSelective(newProOrg);
		if (insertCount < 1) {
			throw new Exception();
		}
		
		CommonResult result = new CommonResult();
		result.successWithMessage(newProduct.getId().toString());
		return result;
	}

	
	@Override
	public FindProductPageForAppResult findProductInfoPageForApp(FindProductPageParam controllerParam) {
		FindProductPageForAppResult result = new FindProductPageForAppResult();
		if (controllerParam == null || controllerParam.getProductType() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		ProductType productType = ProductType.getType(controllerParam.getProductType());
		if (productType == null) {
			result.failWithMessage("产品类型异常");
			return result;
		}
		if(controllerParam.getProductSubType() != null) {
			if (!validSubProductType(productType, controllerParam.getProductSubType())) {
				result.failWithMessage("产品类型参数异常");
				return result;
			}
		}

		PageParam pageParam = setPageFromPageNo(controllerParam.getPageNo(), controllerParam.getPageSize());
		FindProductPage mapperParam = new FindProductPage();
		mapperParam.setPageStart(pageParam.getPageStart());
		mapperParam.setPageSize(pageParam.getPageSize());
		mapperParam.setExpirationTime(controllerParam.getExpirationTime());
		mapperParam.setProductCode(controllerParam.getProductCode());
		mapperParam.setProductName(controllerParam.getProductName());
		mapperParam.setProductSubType(controllerParam.getProductSubType());
		mapperParam.setProductType(controllerParam.getProductType());

		List<ProductVO> productVOPage = productMapper.findProductPageForApp(mapperParam);
		Integer resultCount = productMapper.countProductPageForApp(mapperParam);
		
		if (productVOPage == null || productVOPage.size() < 1) {
			productVOPage = new ArrayList<ProductVO>();
		}

		result.setProductList(productVOPage);
		result.setResultCount(resultCount);
		result.setIsSuccess();

		return result;
	}

//	private List<ProductVO> buildProductVOPage(List<ProductsPO> productPOPage) {
//		if (productPOPage == null || productPOPage.size() < 1) {
//			return new ArrayList<ProductVO>();
//		}
//
//		List<ProductVO> voPage = new ArrayList<ProductVO>();
//		ProductVO tmpVO = null;
//		for (ProductsPO po : productPOPage) {
//			tmpVO = buildProductVO(po);
//			voPage.add(tmpVO);
//		}
//
//		return voPage;
//	}
//
//	private List<ProductVO> buildProductVOPage(List<ProductsPO> productPOPage,
//			List<ProductInsurancePO> insurancePOList) {
//		if (productPOPage == null || productPOPage.size() < 1 || insurancePOList == null
//				|| insurancePOList.size() < 1) {
//			return new ArrayList<ProductVO>();
//		}
//
//		HashMap<Long, ProductInsurancePO> insurancePOMap = new HashMap<Long, ProductInsurancePO>();
//		for (ProductInsurancePO po : insurancePOList) {
//			insurancePOMap.put(po.getId(), po);
//		}
//
//		List<ProductVO> voPage = new ArrayList<ProductVO>();
//		ProductVO tmpVO = null;
//		for (ProductsPO po : productPOPage) {
//			tmpVO = buildProductVO(po, insurancePOMap.get(po.getId()));
//			voPage.add(tmpVO);
//		}
//
//		return voPage;
//	}

	private ProductVO buildProductVO(ProductsPO po) {
		return buildProductVO(po, null);
	}

	private ProductVO buildProductVO(ProductsPO po, ProductInsurancePO insurancePo) {
		if (po == null) {
			return new ProductVO();
		}

		ProductVO tmpVO = new ProductVO();
		if (po.getExpirationTime() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			tmpVO.setExpirationTimeString(sdf.format(po.getExpirationTime()));
		}
		tmpVO.setCreateTime(po.getCreateTime());
		tmpVO.setExpirationTime(po.getExpirationTime());
		tmpVO.setId(po.getId());
		tmpVO.setInterestRate(po.getInterestRate());
		InterestType interestType = InterestType.getType(po.getInterestType());
		if (interestType != null) {
			tmpVO.setInterestTypeName(interestType.getName());
		}
		tmpVO.setProductCode(po.getProductCode());
		tmpVO.setProductIntroduction(po.getProductIntroduction());
		tmpVO.setProductName(po.getProductName());
		tmpVO.setProductSubType(po.getProductSubType());
		tmpVO.setProductType(po.getProductType());
		if (insurancePo != null) {
			tmpVO.setInsuranceAmountMax(insurancePo.getInsuranceAmountMax());
			tmpVO.setInsuranceAmountMin(insurancePo.getInsuranceAmountMin());
			tmpVO.setInterestTypeName("保额");
		}

		return tmpVO;
	}

	private boolean validSubProductType(ProductType productType, Integer subProductType) {
		if (ProductType.financial.equals(productType)) {
			ProductFinancialType productSubType = ProductFinancialType.getType(subProductType);
			if (productSubType == null) {
				return false;
			} else {
				return true;
			}
		} else if (ProductType.financing.equals(productType)) {
			ProductFinancingType productSubType = ProductFinancingType.getType(subProductType);
			if (productSubType == null) {
				return false;
			} else {
				return true;
			}
		} else if (ProductType.insurance.equals(productType)) {
			ProductInsuranceType productSubType = ProductInsuranceType.getType(subProductType);
			if (productSubType == null) {
				return false;
			} else {
				return true;
			}
		} else if (ProductType.securities.equals(productType)) {
			ProductSecuritiesType productSubType = ProductSecuritiesType.getType(subProductType);
			if (productSubType == null) {
				return false;
			} else {
				return true;
			}
		} else if (ProductType.agriculturalCredit.equals(productType)) {
			ProductAagriculturalCreditType productSubType = ProductAagriculturalCreditType.getType(subProductType);
			if (productSubType == null) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getSubProductTypeName(Integer productType, Integer subProductType) {
		ProductType pt = ProductType.getType(productType);
		if (pt == null) {
			return null;
		} else if (ProductType.financial.equals(pt)) {
			ProductFinancialType productSubType = ProductFinancialType.getType(subProductType);
			if (productSubType == null) {
				return null;
			} else {
				return productSubType.getName();
			}
		} else if (ProductType.financing.equals(pt)) {
			ProductFinancingType productSubType = ProductFinancingType.getType(subProductType);
			if (productSubType == null) {
				return null;
			} else {
				return productSubType.getName();
			}
		} else if (ProductType.insurance.equals(pt)) {
			ProductInsuranceType productSubType = ProductInsuranceType.getType(subProductType);
			if (productSubType == null) {
				return null;
			} else {
				return productSubType.getName();
			}
		} else if (ProductType.securities.equals(pt)) {
			ProductSecuritiesType productSubType = ProductSecuritiesType.getType(subProductType);
			if (productSubType == null) {
				return null;
			} else {
				return productSubType.getName();
			}
		} else if (ProductType.agriculturalCredit.equals(pt)) {
			ProductAagriculturalCreditType productSubType = ProductAagriculturalCreditType.getType(subProductType);
			if (productSubType == null) {
				return null;
			} else {
				return productSubType.getName();
			}
		}
		return null;
	}

	@Override
	public boolean productIdExists(ProductIdExistsParam param) {
		if ("1".equals(String.valueOf(productMapper.productIdExists(param)))) {
			return true;
		}
		return false;
	}

	@Override
	public boolean productIdExists(Long productId) {
		ProductIdExistsParam param = new ProductIdExistsParam();
		param.setProductId(productId);
		return productIdExists(param);
	}

	@Override
	public ProductOrganizationPO findProductOrgRelation(Long productId) {
		if (productId == null) {
			return new ProductOrganizationPO();
		}
		return productOrgMapper.findByProductId(productId);
	}

	@Override
	public FindProductPageForAdminResult findProductPageForAdmin(Long userId,
			FindProductPageParam controllerParam) {
		FindProductPageForAdminResult result = new FindProductPageForAdminResult();
		if (userId == null) {
			result.failWithMessage("请先登录");
			return result;
		}
		if (controllerParam == null || controllerParam.getProductType() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		ProductType productType = ProductType.getType(controllerParam.getProductType());
		if (productType == null) {
			result.failWithMessage("产品类型异常");
			return result;
		}
		if(controllerParam.getProductSubType() != null) {
			if (!validSubProductType(productType, controllerParam.getProductSubType())) {
				result.failWithMessage("产品类型参数异常");
				return result;
			}
		}

		PageParam pageParam = setPageFromPageNo(controllerParam.getPageNo(), controllerParam.getPageSize());
		FindProductPage mapperParam = new FindProductPage();
		mapperParam.setPageStart(pageParam.getPageStart());
		mapperParam.setPageSize(pageParam.getPageSize());
		mapperParam.setExpirationTime(controllerParam.getExpirationTime());
		mapperParam.setProductCode(controllerParam.getProductCode());
		mapperParam.setProductName(controllerParam.getProductName());
		mapperParam.setProductSubType(controllerParam.getProductSubType());
		mapperParam.setProductType(controllerParam.getProductType());
		List<String> roleList = baseUtilCustom.getRoles();
		if (roleList.contains(RolesType.ROLE_ADMIN.getRoleName())) {
			if (controllerParam.getOrgId() != null) {
				mapperParam.setOrgId(controllerParam.getOrgId());
			}
		} else {
			Long orgId = staffDetailMapper.findOrgIdByUserId(userId);
			mapperParam.setOrgId(orgId);
		}

		List<ProductVO> productPage = productMapper.findProductPageForAdmin(mapperParam);
		int resultCount = productMapper.countProductPageForAdmin(mapperParam);

		result.setProductPage(productPage);
		result.setResultCount(resultCount);
		result.setIsSuccess();

		return result;
	}

	@Override
	public CommonResult deleteProduct(DeleteProductParam p) {
		if (p.getId() == null) {
			return nullParam();
		}

		productMapper.deleteProduct(p.getId());

		return normalSuccess();
	}

	@Override
	public ProductsPO findProduct(Long productId) {
		if(productId == null) {
			return null;
		}
		return productMapper.findProduct(productId);
	}
}