package demo.products.service;

import java.io.IOException;

import demo.common.domain.result.CommonResult;
import demo.products.domain.param.controllerParam.AddNewProductParam;
import demo.products.domain.param.controllerParam.EditProductParam;
import demo.products.domain.param.controllerParam.FindProductPageParam;
import demo.products.domain.param.controllerParam.FindProductParam;
import demo.products.domain.param.mapperParam.DeleteProductParam;
import demo.products.domain.param.mapperParam.ProductIdExistsParam;
import demo.products.domain.po.ProductOrganizationPO;
import demo.products.domain.po.ProductsPO;
import demo.products.domain.result.AddNewProductResult;
import demo.products.domain.result.FindProductPageForAdminResult;
import demo.products.domain.result.FindProductPageForAppResult;
import demo.products.domain.vo.ProductVO;

public interface ProductService {

	/** 移动端加载理财产品列表 */
	FindProductPageForAppResult findProductInfoPageForApp(FindProductPageParam controllerParam);

	/**
	 * 增加产品
	 * 
	 * @throws Exception
	 */
	AddNewProductResult addNewProduct(Long userId, AddNewProductParam param) throws IOException, Exception;

	ProductVO findProductVO(FindProductParam param);

	boolean productIdExists(ProductIdExistsParam param);

	boolean productIdExists(Long productId);

	ProductOrganizationPO findProductOrgRelation(Long productId);

	/** 机构管理员查询产品分页 */
	FindProductPageForAdminResult findProductPageForAdmin(Long userId, FindProductPageParam controllerParam);

	/** 机构管理员删除产品 */
	CommonResult deleteProduct(DeleteProductParam p);

	/** 根据产品类型,子类型获取产品名称 */
	String getSubProductTypeName(Integer productType, Integer subProductType);

	/** 编辑产品信息 */
	AddNewProductResult updateProduct(EditProductParam param) throws Exception;

	ProductsPO findProduct(Long productId);


}
