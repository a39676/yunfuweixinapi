package demo.products.mapper;

import demo.products.domain.po.ProductOrganizationPO;

public interface ProductOrganizationPOMapper {
	int insert(ProductOrganizationPO record);

	int insertSelective(ProductOrganizationPO record);

	ProductOrganizationPO findByProductId(Long productId);
}