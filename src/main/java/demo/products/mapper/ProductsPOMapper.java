package demo.products.mapper;

import java.util.List;

import demo.products.domain.param.mapperParam.FindProductPage;
import demo.products.domain.param.mapperParam.ProductIdExistsParam;
import demo.products.domain.po.ProductsPO;
import demo.products.domain.vo.ProductVO;

public interface ProductsPOMapper {
	int insert(ProductsPO record);

	int insertSelective(ProductsPO record);

	ProductsPO findProduct(Long id);

	List<ProductVO> findProductPageForApp(FindProductPage param);
	
	Integer countProductPageForApp(FindProductPage param);

	int insertNew(ProductsPO po);

	int updateProductInfo(ProductsPO po);

	int productIdExists(ProductIdExistsParam param);

	List<ProductVO> findProductPageForAdmin(FindProductPage param);

	int countProductPageForAdmin(FindProductPage param);
	
	int deleteProduct(Long id);
}