package demo.products.mapper;

import java.util.List;

import demo.products.domain.po.ProductInsurancePO;

public interface ProductInsurancePOMapper {
    int insert(ProductInsurancePO record);

    int insertSelective(ProductInsurancePO record);
    
    List<ProductInsurancePO> findByProductIdList(List<Long> idList);
    
    ProductInsurancePO findByProductId(Long id);
    
    int insertOrUpdte(ProductInsurancePO po);
}