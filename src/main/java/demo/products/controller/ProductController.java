package demo.products.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.products.domain.constant.ProductUrl;
import demo.products.domain.param.controllerParam.AddNewProductParam;
import demo.products.domain.param.controllerParam.EditProductParam;
import demo.products.domain.param.controllerParam.FindProductPageParam;
import demo.products.domain.param.controllerParam.FindProductParam;
import demo.products.domain.param.mapperParam.DeleteProductParam;
import demo.products.domain.po.ProductOrganizationPO;
import demo.products.domain.po.ProductsPO;
import demo.products.domain.result.AddNewProductResult;
import demo.products.domain.result.FindProductPageForAdminResult;
import demo.products.domain.result.FindProductPageForAppResult;
import demo.products.domain.vo.ProductVO;
import demo.products.service.ProductService;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = ProductUrl.root)
public class ProductController extends CommonController {

	@Autowired
	private BaseUtilCustom baseUtilCustom;
	@Autowired
	private ProductService productService;

	public boolean productIdExists(Long productId) {
		return productService.productIdExists(productId);
	}

	@GetMapping(value = ProductUrl.financing)
	public ModelAndView financing() {
		return new ModelAndView("jsp/financing/financing");
	}

	@GetMapping(value = ProductUrl.financial)
	public ModelAndView financial() {
		return new ModelAndView("jsp/docking/docking");
	}

	@GetMapping(value = ProductUrl.insurance)
	public ModelAndView insurance() {
		return new ModelAndView("jsp/insurance/insurance");
	}

	@GetMapping(value = ProductUrl.securities)
	public ModelAndView securities() {
		return new ModelAndView("jsp/securities/securities");
	}

	@GetMapping(value = ProductUrl.agriculturalCredit)
	public ModelAndView agriculturalCredit() {
		return new ModelAndView("jsp/areas/areas");
	}

	@GetMapping(value = ProductUrl.productPageForApp)
	public ModelAndView productPageForApp() {
		return new ModelAndView("jsp/loan/loan");
	}

	@PostMapping(value = ProductUrl.findProductInfoPageForApp)
	public void findProductInfoPageForApp(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		FindProductPageParam param = new FindProductPageParam().fromJson(getJson(data));
		FindProductPageForAppResult serviceResult = productService.findProductInfoPageForApp(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = ProductUrl.addNewProduct)
	public void addNewProduct(@RequestBody String data, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AddNewProductParam param = new AddNewProductParam().fromJson(getJson(data));
		AddNewProductResult serviceResult = productService.addNewProduct(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = ProductUrl.findProduct)
	public void findProduct(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindProductParam param = new FindProductParam().fromJson(getJson(data));
		ProductVO vo = productService.findProductVO(param);
		outputJson(response, JSONObject.fromObject(vo));
	}

	public ProductOrganizationPO findProductOrgRelation(Long productId) {
		ProductOrganizationPO productOrgRelation = productService.findProductOrgRelation(productId);
		return productOrgRelation;
	}

	@GetMapping(value = ProductUrl.findProductDetail)
	public ModelAndView findProductDetail() {
		return new ModelAndView("jsp/product/product");
	}
	
	@PostMapping(value = ProductUrl.findProductPageForAdmin)
	public void FindProductPageForAdminParam(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		FindProductPageParam param = new FindProductPageParam().fromJson(getJson(data));
		FindProductPageForAdminResult result = productService.findProductPageForAdmin(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(result));
	}

	@PostMapping(value = ProductUrl.deleteProduct)
	public void deleteProduct(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		DeleteProductParam param = new DeleteProductParam().fromJson(getJson(data));
		CommonResult result = productService.deleteProduct(param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = ProductUrl.editProduct)
	public void editProduct(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		EditProductParam param = new EditProductParam().fromJson(getJson(data));
		AddNewProductResult result = productService.updateProduct(param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	public String getSubProductTypeName(Integer productType, Integer subProductType) {
		return productService.getSubProductTypeName(productType, subProductType);
	}
	
	public ProductsPO findProduct(Long productId) {
		return productService.findProduct(productId);
	}
}
