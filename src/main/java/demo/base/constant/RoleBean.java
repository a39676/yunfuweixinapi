package demo.base.constant;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import demo.base.base.domain.po.Roles;
import demo.base.base.mapper.RolesMapper;

public class RoleBean {
	
	@Autowired
	RolesMapper rolesMapper;
	
	public static List<Roles> roleList;
	
	public static int createCount = 0;
	
	public List<Roles> getRoleList() {
		
		if(roleList == null || roleList.isEmpty()) {
			roleList = rolesMapper.getRoleList();
		}
		
		return roleList;
		
	}

}
