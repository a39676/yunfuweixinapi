package demo.base.constant;

public class UrlConstant {
	
	public static final String baseRoot = "/";
//	public static final String welcome = "/welcome**";
	
	public static final String login = "/login";
	public static final String logout = "/logout";
	public static final String wxLogin = "/wxLogin";
	public static final String managerBind = "/managerBind";
	
	public static final String error403 = "/403";
	
	
}
