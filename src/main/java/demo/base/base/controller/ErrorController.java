package demo.base.base.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import demo.base.constant.UrlConstant;
import demo.common.domain.result.CommonResult;
import net.sf.json.JSONObject;

/**
 */
@Controller
public class ErrorController extends ExceptionController {
	
	@RequestMapping(value = UrlConstant.error403, method = RequestMethod.GET)
	public void accesssDenied(HttpServletRequest request, HttpServletResponse response) {
		
		CommonResult result = new CommonResult();
		result.failWithMessage("您未被授权访问此页面");
		
		outputJson(response, JSONObject.fromObject(result));
	}
	
}
