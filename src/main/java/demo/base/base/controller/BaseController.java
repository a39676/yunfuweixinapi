package demo.base.base.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import demo.base.constant.UrlConstant;
import demo.common.controller.CommonController;

@Controller
public class BaseController extends CommonController {
	
//	@Autowired
//	private BaseUtilCustom baseUtilCustom;
//	@Autowired
//	private SystemConstantService systemConstantService;

	@GetMapping(value = { UrlConstant.baseRoot })
	public ModelAndView welcomePage(HttpServletRequest request) {
		return new ModelAndView(new RedirectView("/weixinHtml/login.html"));
	}
	
	@RequestMapping(value = { "/robots.txt" })
	public void robots(HttpServletRequest request, HttpServletResponse response) {
		insertVisitIp(request);
		
		Resource resource = new ClassPathResource("/static_resources/robots.txt");
		
		String mimeType= "application/text/plain";
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", String.format("inline; filename=\"" + resource.getFilename() +"\""));
		try {
			response.setContentLength((int)resource.contentLength());
			InputStream is = resource.getInputStream();
			FileCopyUtils.copy(is, response.getOutputStream());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = { "/favicon.ico" })
	public void favicon(HttpServletResponse response) {
		Resource resource = new ClassPathResource("/static_resources/favicon.ico");
		
		String mimeType= "application/image/x-icon";
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", String.format("inline; filename=\"" + resource.getFilename() +"\""));
		try {
			response.setContentLength((int)resource.contentLength());
			InputStream is = resource.getInputStream();
			FileCopyUtils.copy(is, response.getOutputStream());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}