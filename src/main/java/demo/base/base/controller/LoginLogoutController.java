package demo.base.base.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import demo.base.base.service.LoginLogoutService;
import demo.base.constant.UrlConstant;
import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import net.sf.json.JSONObject;


@Controller
@RequestMapping(value = UrlConstant.login)
public class LoginLogoutController extends CommonController {
	
	@Autowired
	private LoginLogoutService LoginLogoutService;
	
	@GetMapping(value = UrlConstant.login)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout,
		@RequestParam(value = "code", required = false) String code,
		@RequestParam(value = "state", required = false) String state,
		HttpServletRequest request,
		HttpServletResponse response) {

		ModelAndView view = new ModelAndView();
		CommonResult result = new CommonResult();
		if (error != null) {
			view.setViewName("jsp/sign/sign");
			view.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
			
			result.failWithMessage(error);
			outputJson(response, JSONObject.fromObject(result));
		}

		view = LoginLogoutService.wxLoginPageHandler(error, logout, code, state, request, response);
		return view;

	}
	
	@GetMapping(value = UrlConstant.wxLogin)
	public ModelAndView wxLogin() {
		return new ModelAndView(new RedirectView("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf13c1b9265f72eff&redirect_uri=http%3A%2F%2Fwww.yunfucpa.com%2FweixinHtml%2Fsign%2Fsign.html&response_type=code&scope=snsapi_base&state=#wechat_redirect")); 
	}
	
	@GetMapping(value = UrlConstant.managerBind)
	public ModelAndView managerBind() {
		return new ModelAndView(new RedirectView("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf13c1b9265f72eff&redirect_uri=http%3A%2F%2Fwww.yunfucpa.com%2FweixinHtml%2FmanagerBind%2FmanagerBind.html&response_type=code&scope=snsapi_base&state=#wechat_redirect"));
	}
	
	@GetMapping(value = UrlConstant.logout)
	public void logout (HttpServletRequest request, HttpServletResponse response) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    CommonResult result = new CommonResult();
	    result.setIsSuccess();
	    outputJson(response, JSONObject.fromObject(result));
	}

	//customize the error message
	private String getErrorMessage(HttpServletRequest request, String key){

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "用户名或密码有误";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "用户名或密码有误";
		}

		return error;
	}
	
}
