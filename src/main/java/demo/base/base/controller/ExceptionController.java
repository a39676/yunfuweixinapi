package demo.base.base.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import demo.base.base.service.SystemConstantService;
import demo.common.controller.CommonController;

@ControllerAdvice
public class ExceptionController extends CommonController implements HandlerExceptionResolver {

	
	@Autowired
	protected SystemConstantService systemConstantService;

	protected static final String[] description = {""};

	protected int getRan() {
		return 0;
	}
	private static boolean debugStatus = true;

	@ExceptionHandler({ Exception.class })
	public ModelAndView handleException(HttpServletRequest request, Exception e, String message) {
		ModelAndView view = new ModelAndView("jsp/testJSP/customError");
		findDebugStatus();
		if(debugStatus) {
			view.addObject("message", e.toString());
		} else {
			view.addObject("message", "很抱歉,出现了" + description[getRan()] + "的异常");
		}
		view.addObject("urlRedirect", foundHostNameFromRequst(request));

		e.printStackTrace();
		return view;
	}

	@ExceptionHandler({ IOException.class })
	public ModelAndView handleIOException(HttpServletRequest request, Exception e) {
		ModelAndView view = new ModelAndView("baseJSP/errorCustom");
		findDebugStatus();
		if(debugStatus) {
			view.addObject("message", e.toString());
		} else {
			view.addObject("message", "很抱歉,出现了" + description[getRan()] + "的异常");
		}
		view.addObject("urlRedirect", foundHostNameFromRequst(request));
//		mailService.sendErrorMail(e.toString());
		e.printStackTrace();
		return view;
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public ModelAndView handleNoHandlerFoundException(HttpServletRequest request, Exception e) {
		ModelAndView view = new ModelAndView("jsp/testJSP/customError");
		findDebugStatus();
		if(debugStatus) {
			view.addObject("message", e.toString());
		} else {
			view.addObject("message", "很抱歉,出现了" + description[getRan()] + "的异常");
		}
		view.addObject("urlRedirect", foundHostNameFromRequst(request));

		e.printStackTrace();
		return view;
	}

	@ExceptionHandler({ SQLException.class, DataAccessException.class })
	public ModelAndView handleSQLException(HttpServletRequest request, Exception e) {
		ModelAndView view = new ModelAndView("jsp/testJSP/customError");
		findDebugStatus();
		if(debugStatus) {
			view.addObject("message", e.toString());
		} else {
			view.addObject("message", "很抱歉,出现了" + description[getRan()] + "的异常");
		}
		view.addObject("urlRedirect", foundHostNameFromRequst(request));
//		mailService.sendErrorMail(e.toString());
		e.printStackTrace();
		return view;
	}

	@ExceptionHandler({ RuntimeException.class })
	public ModelAndView hanedleRuntimeException(HttpServletRequest request, Exception e) {
		ModelAndView view = new ModelAndView("jsp/testJSP/customError");
		findDebugStatus();
		if(debugStatus) {
			view.addObject("message", e.toString());
		} else {
			view.addObject("message", "很抱歉,出现了" + description[getRan()] + "的异常");
		}
		view.addObject("urlRedirect", foundHostNameFromRequst(request));
//		mailService.sendErrorMail(e.toString());
		e.printStackTrace();
		return view;
	}

	@ExceptionHandler({ IllegalArgumentException.class, IllegalStateException.class })
	public ModelAndView hanedleIllegalArgumentException(HttpServletRequest request, Exception e) {
		ModelAndView view = new ModelAndView("jsp/testJSP/customError");
		findDebugStatus();
		if(debugStatus) {
			view.addObject("message", e.toString());
		} else {
			view.addObject("message", "很抱歉,出现了" + description[getRan()] + "的异常");
		}
		view.addObject("urlRedirect", foundHostNameFromRequst(request));
//		mailService.sendErrorMail(e.toString());
		e.printStackTrace();
		return view;
	}

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		try {

			if (response.getStatus() == 500) {
				return handleException(request, ex, "500");
			}
			else if (ex instanceof MyBatisSystemException) {
				return handleException(request, ex, "TooManyResultsException");
			}

			return handleException(request, ex, null);
		} catch (Exception handlerException) {

		}
		return null;
	}

	private void findDebugStatus() {
		String debugStatusStr = "0";
		if("1".equals(debugStatusStr)) {
			debugStatus = true;
		} else {
			debugStatus = false;
		}
	}
}
