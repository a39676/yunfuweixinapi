package demo.base.base.service;

import java.util.List;

import demo.common.domain.result.CommonResult;

public interface RoleGroupService {

	CommonResult insertNewRoleGroup(Long groupId, List<Long> roleIds);

	CommonResult deleteRoleGroupByGroupId(Long groupId);

	/** 按权限组ID为用户增加权限 */
	CommonResult insertRoleByGroupId(Long groupId, Long userId);

	/** 按照权限组ID为用户删除权限 */ 
	CommonResult deleteRoleByGroupId(Long groupId, Long userId);

}
