package demo.base.base.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.base.base.domain.param.mapperParam.RoleGroupsBatchInsertParam;
import demo.base.base.domain.po.RoleGroupsPO;
import demo.base.base.mapper.RoleGroupsPOMapper;
import demo.base.base.service.RoleGroupService;
import demo.base.user.domain.param.mapperParam.DeleteUserRolesParam;
import demo.base.user.domain.param.mapperParam.InsertIgnoreUserRoleParam;
import demo.base.user.mapper.UserRolesMapper;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;

@Service
public class RoleGroupServiceImpl implements RoleGroupService {
	
	@Autowired
	private RoleGroupsPOMapper roleGroupMapper;
	@Autowired
	private UserRolesMapper usrRolesMapper;
	
	@Override
 	public CommonResult insertNewRoleGroup(Long groupId, List<Long> roleIds) {
		CommonResult result = new CommonResult();
		if(groupId == null || roleIds == null || roleIds.size() < 1) {
			result.setIsFail();
			return result;
		}
		
		RoleGroupsBatchInsertParam p = new RoleGroupsBatchInsertParam();
		RoleGroupsPO newRG = null;
		List<RoleGroupsPO> newRGList = new ArrayList<RoleGroupsPO>();
		for(Long roleId : roleIds) {
			newRG = new RoleGroupsPO();
			newRG.setGroupId(groupId);
			newRG.setRoleId(roleId);
			newRGList.add(newRG);
		}
		
		p.setRoleGroupList(newRGList);
		int insertCount = roleGroupMapper.batchInsert(p);
		if(insertCount < 1) {
			result.setIsFail();
			return result;
		}
		
		result.isSuccess();
		return result;
	}
	
	@Override
	public CommonResult deleteRoleGroupByGroupId(Long groupId) {
		CommonResult result = new CommonResult();
		
		if(groupId == null) {
			result.setIsFail();
			return result;
		}
		
		int deleteCount = roleGroupMapper.deleteByGroupId(groupId);
		if(deleteCount < 1) {
			result.setIsFail();
			return result;
		}
		
		result.isSuccess();
		return result;
	}

	@Override
	public CommonResult insertRoleByGroupId(Long groupId, Long userId) {
		CommonResult result = new CommonResult();
		if(groupId == null || userId == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		List<RoleGroupsPO> roleGroup = roleGroupMapper.findByGroupId(groupId);
		if(roleGroup == null || roleGroup.size() < 1) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		
		List<Long> roleIdList = new ArrayList<Long>();
		for(RoleGroupsPO p : roleGroup) {
			roleIdList.add(p.getRoleId());
		}
		
		InsertIgnoreUserRoleParam insertParam = new InsertIgnoreUserRoleParam();
		insertParam.setUserId(userId);
		insertParam.setRoleIdList(roleIdList);
		usrRolesMapper.insertIgnore(insertParam);

		result.setIsSuccess();
		
		return result;
	}
	
	@Override
	public CommonResult deleteRoleByGroupId(Long groupId, Long userId) {
		CommonResult result = new CommonResult();
		if(groupId == null || userId == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		List<RoleGroupsPO> roleGroup = roleGroupMapper.findByGroupId(groupId);
		if(roleGroup == null || roleGroup.size() < 1) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		
		List<Long> roleIdList = new ArrayList<Long>();
		for(RoleGroupsPO p : roleGroup) {
			roleIdList.add(p.getRoleId());
		}
		
		DeleteUserRolesParam deleteUserRolesParam = new DeleteUserRolesParam();
		deleteUserRolesParam.setRoleIdList(roleIdList);
		deleteUserRolesParam.setUserId(userId);
		usrRolesMapper.deleteUserRoles(deleteUserRolesParam);
		
		result.setIsSuccess();
		
		return result;
	}
}