package demo.base.base.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import demo.base.base.service.LoginLogoutService;
import demo.base.constant.UrlConstant;
import demo.common.service.CommonService;

@Service
public class LoginLogoutServiceImpl extends CommonService implements LoginLogoutService {

	@Override
	public ModelAndView wxLoginPageHandler(String error, String logout, String wxCode, String state,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView view = new ModelAndView();

		if (logout != null) {
			view.setViewName("redirect:" + UrlConstant.baseRoot);
			return view;
		}

		if (StringUtils.isNotBlank(wxCode)) {
			view.addObject("wxCode", wxCode);
		}

		view.setViewName("jsp/sign/sign");

		return view;
	}

}
