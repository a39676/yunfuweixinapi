package demo.base.base.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

public interface LoginLogoutService {

	ModelAndView wxLoginPageHandler(String error, String logout, String code, String state, HttpServletRequest request,
			HttpServletResponse response);

}
