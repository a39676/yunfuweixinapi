package demo.base.base.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.base.base.domain.SystemConstant;
import demo.base.base.domain.SystemConstantStore;
import demo.base.base.mapper.SystemConstantMapper;

@Service
public class SystemConstantService {

	@Autowired
	private SystemConstantMapper systemConstantMapper;
	
	public String getValByName(String constantName) {
		if(StringUtils.isBlank(constantName)) {
			return "";
		}
		
		if(SystemConstantStore.store.containsKey(constantName)) {
			return String.valueOf(SystemConstantStore.store.get(constantName));
		} else {
			SystemConstant tmpConstant = systemConstantMapper.getValByName(constantName);
			if(tmpConstant == null || StringUtils.isBlank(tmpConstant.getConstantValue())) {
				return "";
			}
			SystemConstantStore.store.put(tmpConstant.getConstantName(), tmpConstant.getConstantValue());
			return tmpConstant.getConstantValue();
		}
	}
	
	public String getValByName(String constantName, boolean refreshFlag) {
		if(refreshFlag) {
			SystemConstantStore.store.remove(constantName);
		}
		return getValByName(constantName);
	}
	
	public HashMap<String, String> getValsByName(List<String> constantNames, boolean refreshFlag) {
		if(refreshFlag) {
			for(String constantName : constantNames) {
				SystemConstantStore.store.remove(constantName);
			}
		}
		return getValsByName(constantNames);
	}

	public HashMap<String, String> getValsByName(List<String> constantNames) {
		if(constantNames == null || constantNames.isEmpty()) {
			return new HashMap<String, String>();
		}
		
		List<String> queryNames = new ArrayList<String>();
		HashMap<String, String> result = new HashMap<String, String>();
		
		String tmpConstantName;
		for(int i = 0; i < constantNames.size(); i++) {
			tmpConstantName = constantNames.get(i);
			if(StringUtils.isNotBlank(tmpConstantName)) {
				if(SystemConstantStore.store.containsKey(tmpConstantName)) {
					result.put(tmpConstantName, SystemConstantStore.store.get(tmpConstantName));
				} else {
					queryNames.add(tmpConstantName);
				}
			}
		}
		
		List<SystemConstant> queryResult = null;
		if(queryNames.size() > 0) {
			queryResult = systemConstantMapper.getValsByName(queryNames);
		}
		
		if(queryResult != null && queryResult.size() > 0) {
			queryResult.stream().forEach(tmpConstant -> result.put(tmpConstant.getConstantName(), tmpConstant.getConstantValue()));
		}
		
		SystemConstantStore.store.putAll(result);
		return result;
		
	}
	


}
