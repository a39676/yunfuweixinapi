package demo.base.base.mapper;

import java.util.List;

import demo.base.base.domain.param.mapperParam.RoleGroupsBatchInsertParam;
import demo.base.base.domain.po.RoleGroupsPO;
import demo.base.base.domain.po.Roles;

public interface RoleGroupsPOMapper {
    int insert(RoleGroupsPO record);

    int insertSelective(RoleGroupsPO record);
    
    int batchInsert(RoleGroupsBatchInsertParam param);
    
    int deleteByGroupId(Long groupId);
    
    List<RoleGroupsPO> findByGroupId(Long groupId);
    
    List<Roles> findRoleListByRoleGroupId(Long groupId);
}