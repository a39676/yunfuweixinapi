package demo.base.base.mapper;

import java.util.List;

import demo.base.base.domain.SystemConstant;

public interface SystemConstantMapper {
    int insert(SystemConstant record);

    int insertSelective(SystemConstant record);
    
    SystemConstant getValByName(String constantName);
    
    List<SystemConstant> getValsByName(List<String> constantNames);
}