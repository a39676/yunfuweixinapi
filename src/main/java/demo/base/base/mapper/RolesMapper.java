package demo.base.base.mapper;

import java.util.List;

import demo.base.base.domain.po.Roles;

public interface RolesMapper {
    int insert(Roles record);

    int insertSelective(Roles record);

	List<Roles> getRoleList();
	
	int insertOrUpdate(Roles role);
	
}