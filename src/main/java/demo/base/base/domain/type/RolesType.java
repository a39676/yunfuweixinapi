package demo.base.base.domain.type;

public enum RolesType {
	
	ROLE_ADMIN("ROLE_ADMIN", 1L),
	ROLE_USER("ROLE_USER", 2L),
	
//	/** 机构角色管理 */
//	ROLE_AUTH_MANAGE("ROLE_AUTH_MANAGE", 3L),
	/** 机构组织管理(机构管理员,管理其下分支机构) */
	ROLE_ORG_GROUP_MANAGE("ROLE_ORG_GROUP_MANAGE", 4L),
//	/** 机构权限管理 */
//	ROLE_ORG_ROLE_MANAGE("ROLE_ORG_ROLE_MANAGE", 5L),
	/** 机构人员管理(机构管理员/协会管理员,管理旗下分支机构的工作人员) */
	ROLE_ORG_STAFF_MANAGE("ROLE_ORG_STAFF_MANAGE", 6L),
	
	/** 理财产品管理 */
	ROLE_PRODUCT_FINANCING_MANAGE("ROLE_PRODUCT_FINANCING_MANAGE", 7L),
	/** 理财产品受理 */
	ROLE_PRODUCT_FINANCING_HANDLE("ROLE_PRODUCT_FINANCING_HANDLE", 8L),
	/** 融资产品管理 */
	ROLE_PRODUCT_FINANCIAL_MANAGE("ROLE_PRODUCT_FINANCIAL_MANAGE", 9L),
	/** 融资产品受理 */
	ROLE_PRODUCT_FINANCIAL_HANDLE("ROLE_PRODUCT_FINANCIAL_HANDLE", 10L),
	/** 保险产品管理 */
	ROLE_PRODUCT_INSURANCE_MANAGE("ROLE_PRODUCT_INSURANCE_MANAGE", 11L),
	/** 保险产品受理 */
	ROLE_PRODUCT_INSURANCE_HANDLE("ROLE_PRODUCT_INSURANCE_HANDLE", 12L),
	/** 证券产品管理 */
	ROLE_PRODUCT_SECURITIES_MANAGE("ROLE_PRODUCT_SECURITIES_MANAGE", 13L),
	/** 证券产品受理 */
	ROLE_PRODUCT_SECURITIES_HANDLE("ROLE_PRODUCT_SECURITIES_HANDLE", 14L),
	/** 互联网+信用三农管理 */
	ROLE_PRODUCT_AGRICULTURAL_CREDIT_MANAGE("ROLE_PRODUCT_AGRICULTURAL_CREDIT_MANAGE", 15L),
	/** 互联网+信用三农受理 */
	ROLE_PRODUCT_AGRICULTURAL_CREDIT_HANDLE("ROLE_PRODUCT_AGRICULTURAL_CREDIT_HANDLE", 16L),
	/** 零钞兑换受理 */
	ROLE_SEV_EXCHANGE_DIB_HANDLE("ROLE_SEV_EXCHANGE_DIB_HANDLE", 17L),
//	/** 信息模板管理(机构) */
//	ROLE_MESSAGE_MODEL_ORG_MANAGE("ROLE_MESSAGE_MODEL_ORG_MANAGE", 18L),
	/** 零钞兑换限额管理(机构) */
	ROLE_EXCHANGE_DIB_ORG_LIMIT_MANAGE("ROLE_EXCHANGE_DIB_ORG_LIMIT_MANAGE", 19L),

	/** 投诉举报管理(协会) */
	ROLE_COMPLAINT_MANAGE("ROLE_COMPLAINT_MANAGE", 20L),
	/** 知识宣传管理(协会) */
	ROLE_PROMO_MANAGE("ROLE_PROMO_MANAGE", 21L),
	/** 公告管理(协会) */
	ROLE_ANNOUNCEMENT_MANAGE("ROLE_ANNOUNCEMENT_MANAGE", 22L),
	/** 信息推送管理(协会, 管理微信信息推送时间) */
	ROLE_MESSAGE_PUSH_MANAGE("ROLE_MESSAGE_PUSH_MANAGE", 23L),
//	/** 信息模板管理(协会, 微信模板暂时无法订制) */
//	ROLE_MESSAGE_MODEL_GLOBAL_MANAGE("ROLE_MESSAGE_MODEL_GLOBAL_MANAGE", 24L),
	/** 零钞兑换限额(个人限额)管理(协会) */
	ROLE_EXCHANGE_DIB_GLOBAL_LIMIT_MANAGE("ROLE_EXCHANGE_DIB_GLOBAL_LIMIT_MANAGE", 25L),
	/** 区域管理(协会, 暂时数据库填充默认) */
	ROLE_GEOGRAPHICAL_AREA_MANAGE("ROLE_GEOGRAPHICAL_AREA_MANAGE", 26L),
	/** 机构管理(协会, 协会管理顶级机构) */
	ROLE_ORG_MANAGE("ROLE_ORG_MANAGE", 27L),
	/** 问答题库(协会) */
	ROLE_POP_QUIZ_QUESTION_MANAGE("ROLE_POP_QUIZ_QUESTION_MANAGE", 29L),
	/** 问答卷库(协会) */
	ROLE_POP_QUIZ_QUESTIONNAIRE_MANAGE("ROLE_POP_QUIZ_QUESTIONNAIRE_MANAGE", 30L),
	
	/** 微信用户 */
	ROLE_WXUSER("ROLE_WXUSER", 28L),
	;
	
	private String roleName;
	private Long roleId;
	
	RolesType(String roleName, Long roleId) {
		this.roleName = roleName;
		this.roleId = roleId;
	}
	
	public String getRoleName() {
		return roleName;
	}
	public Long getId() {
		return roleId;
	}
	
	public static RolesType getRole(Long id) {
		for(RolesType role : RolesType.values()) {
			if(role.getId() == id) {
				return role;
			}
		}
		return null;
	}
	
	public static RolesType getRole(String roleName) {
		for(RolesType role : RolesType.values()) {
			if(role.getRoleName().equals(roleName)) {
				return role;
			}
		}
		return null;
	}
}