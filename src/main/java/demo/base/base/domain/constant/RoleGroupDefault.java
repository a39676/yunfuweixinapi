package demo.base.base.domain.constant;

import java.util.ArrayList;
import java.util.List;

import demo.base.base.domain.po.RoleGroupsPO;
import demo.base.base.domain.type.RolesType;

public class RoleGroupDefault {
	
	public static final List<RoleGroupsPO> roleGroupList;
	
	/** 机构管理 */
	public static final Long[] ROLE_GROUP_BRANCH_ADMIN = {
			RolesType.ROLE_ORG_GROUP_MANAGE.getId(),
			RolesType.ROLE_ORG_STAFF_MANAGE.getId(),
			RolesType.ROLE_PRODUCT_FINANCING_MANAGE.getId(),
			RolesType.ROLE_PRODUCT_FINANCING_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_FINANCIAL_MANAGE.getId(),
			RolesType.ROLE_PRODUCT_FINANCIAL_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_INSURANCE_MANAGE.getId(),
			RolesType.ROLE_PRODUCT_INSURANCE_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_SECURITIES_MANAGE.getId(),
			RolesType.ROLE_PRODUCT_SECURITIES_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_AGRICULTURAL_CREDIT_MANAGE.getId(),
			RolesType.ROLE_PRODUCT_AGRICULTURAL_CREDIT_HANDLE.getId(),
			RolesType.ROLE_EXCHANGE_DIB_ORG_LIMIT_MANAGE.getId(),
			RolesType.ROLE_SEV_EXCHANGE_DIB_HANDLE.getId(),
	} ;
	
	/** 业务经办 */
	public static final Long[] ROLE_GROUP_BUSINESS_HANDLER = {
			RolesType.ROLE_PRODUCT_FINANCING_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_FINANCIAL_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_INSURANCE_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_SECURITIES_HANDLE.getId(),
			RolesType.ROLE_PRODUCT_AGRICULTURAL_CREDIT_HANDLE.getId(),
	} ;
	
	/** 网点经办 */
	public static final Long[] ROLE_GROUP_BRANCH_HANDLER = {
			RolesType.ROLE_SEV_EXCHANGE_DIB_HANDLE.getId()
	} ;
	
	/** 协会(平台管理员) */
	public static final Long[] ROLE_GROUP_UNION = {
			RolesType.ROLE_ADMIN.getId(),
			RolesType.ROLE_GEOGRAPHICAL_AREA_MANAGE.getId(),
			RolesType.ROLE_ORG_STAFF_MANAGE.getId(),
			RolesType.ROLE_ORG_MANAGE.getId(),
			RolesType.ROLE_COMPLAINT_MANAGE.getId(),
			RolesType.ROLE_PROMO_MANAGE.getId(),
			RolesType.ROLE_ANNOUNCEMENT_MANAGE.getId(),
			RolesType.ROLE_MESSAGE_PUSH_MANAGE.getId(),
//			RolesType.ROLE_MESSAGE_MODEL_GLOBAL_MANAGE.getId(),
			RolesType.ROLE_EXCHANGE_DIB_GLOBAL_LIMIT_MANAGE.getId(),
			RolesType.ROLE_POP_QUIZ_QUESTION_MANAGE.getId(),
			RolesType.ROLE_POP_QUIZ_QUESTIONNAIRE_MANAGE.getId(),
	};
	
	/** 微信用户 */
	public static final Long[] ROLE_GROUP_WXUSER = {
			RolesType.ROLE_WXUSER.getId(),
	};
	
	public static final Long ROLE_GROUP_BRANCH_ADMIN_ID = 1L;
	public static final Long ROLE_GROUP_BUSINESS_HANDLER_ID = 2L;
	public static final Long ROLE_GROUP_BRANCH_HANDLER_ID = 3L;
	public static final Long ROLE_GROUP_UNION_ID = 4L;
	public static final Long ROLE_GROUP_WXUSER_ID = 5L;
	
	static {
		roleGroupList = new ArrayList<RoleGroupsPO>();
		RoleGroupsPO tmpRG = null;
		for(Long l : ROLE_GROUP_BRANCH_ADMIN) {
			tmpRG = new RoleGroupsPO();
			tmpRG.setGroupId(ROLE_GROUP_BRANCH_ADMIN_ID);
			tmpRG.setRoleId(l);
			roleGroupList.add(tmpRG);
		}
		for(Long l : ROLE_GROUP_BUSINESS_HANDLER) {
			tmpRG = new RoleGroupsPO();
			tmpRG.setGroupId(ROLE_GROUP_BUSINESS_HANDLER_ID);
			tmpRG.setRoleId(l);
			roleGroupList.add(tmpRG);
		}
		for(Long l : ROLE_GROUP_BRANCH_HANDLER) {
			tmpRG = new RoleGroupsPO();
			tmpRG.setGroupId(ROLE_GROUP_BRANCH_HANDLER_ID);
			tmpRG.setRoleId(l);
			roleGroupList.add(tmpRG);
		}
		for(Long l : ROLE_GROUP_UNION) {
			tmpRG = new RoleGroupsPO();
			tmpRG.setGroupId(ROLE_GROUP_UNION_ID);
			tmpRG.setRoleId(l);
			roleGroupList.add(tmpRG);
		}
		for(Long l : ROLE_GROUP_WXUSER) {
			tmpRG = new RoleGroupsPO();
			tmpRG.setGroupId(ROLE_GROUP_WXUSER_ID);
			tmpRG.setRoleId(l);
			roleGroupList.add(tmpRG);
		}
	}
}
