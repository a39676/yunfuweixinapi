package demo.base.base.domain;

import java.util.HashMap;

public class SystemConstantStore {

	public static final String maxAttempts = "maxAttempts";
	public static final String maxArticleLength = "maxArticleLength";
	public static final String aesKey = "aesKey";
	public static final String aesInitVector = "aesInitVector";
	public static final String webSiteTitle = "webSiteTitle";

	
	public static HashMap<String, String> store = new HashMap<String, String>();
	
	static {{
		
	}}
	
}
