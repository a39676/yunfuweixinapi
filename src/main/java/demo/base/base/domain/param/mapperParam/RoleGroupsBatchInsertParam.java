package demo.base.base.domain.param.mapperParam;

import java.util.List;

import demo.base.base.domain.po.RoleGroupsPO;

public class RoleGroupsBatchInsertParam {

	private List<RoleGroupsPO> roleGroupList;

	public List<RoleGroupsPO> getRoleGroupList() {
		return roleGroupList;
	}

	public void setRoleGroupList(List<RoleGroupsPO> roleGroupList) {
		this.roleGroupList = roleGroupList;
	}
	
}
