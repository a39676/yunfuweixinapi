package demo.base.admin;

public class AdminViewConstants {
	
	// adminViews
	public static final String adminView = "jsp/adminJSP/admin";
	public static final String userManager = "jsp/adminJSP/userManager";
	public static final String updateAccountMarker = "jsp/adminJSP/updateAccountMarker";
	public static final String tmpAdminView = "jsp/adminJSP/tmpAdmin";
}
