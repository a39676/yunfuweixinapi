package demo.base.admin.domain.constant;

public class AdminUrlConstant {
	
	public static final String adminRoot = "/admin";
	public static final String userManager = "/userManager";
	public static final String userEraser = "/userEraser";
	public static final String dba = "/dba**";
	public static final String refreshSystemConstant = "/refreshSystemConstant";
	
}
