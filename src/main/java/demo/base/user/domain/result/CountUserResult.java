package demo.base.user.domain.result;

import demo.common.domain.result.CommonResult;

public class CountUserResult extends CommonResult {

	private Long todayCount = 0L;
	private Long allCount = 0L;

	public Long getTodayCount() {
		return todayCount;
	}

	public void setTodayCount(Long todayCount) {
		this.todayCount = todayCount;
	}

	public Long getAllCount() {
		return allCount;
	}

	public void setAllCount(Long allCount) {
		this.allCount = allCount;
	}

}
