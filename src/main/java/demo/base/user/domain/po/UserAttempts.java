package demo.base.user.domain.po;

import java.util.Date;

public class UserAttempts {
	private Integer userId;

	private String userName;

	private Date attemptTime;

	private Integer isDelete;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public Date getAttemptTime() {
		return attemptTime;
	}

	public void setAttemptTime(Date attemptTime) {
		this.attemptTime = attemptTime;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "UserAttempts [userId=" + userId + ", userName=" + userName + ", attemptTime=" + attemptTime
				+ ", isDelete=" + isDelete + "]";
	}

}