package demo.base.user.domain.po;

import java.util.Date;

public class UsersDetail {

	private Long id;
	private Long userId;
	private String openId;
	private String unionId;
	private Date createTime;
	private Long mobile;
	private String nickName;
	private Integer gender;
	private String idCardNumber;
	private Integer breakDateCount;
	private Date lastBreakDateTime;
	private Long qq;
	private Date lastLoginTime;
	private Date editTime;
	private Integer editCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId == null ? null : openId.trim();
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId == null ? null : unionId.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName == null ? null : nickName.trim();
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber == null ? null : idCardNumber.trim();
	}

	public Integer getBreakDateCount() {
		return breakDateCount;
	}

	public void setBreakDateCount(Integer breakDateCount) {
		this.breakDateCount = breakDateCount;
	}

	public Date getLastBreakDateTime() {
		return lastBreakDateTime;
	}

	public void setLastBreakDateTime(Date lastBreakDateTime) {
		this.lastBreakDateTime = lastBreakDateTime;
	}

	public Long getQq() {
		return qq;
	}

	public void setQq(Long qq) {
		this.qq = qq;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public Integer getEditCount() {
		return editCount;
	}

	public void setEditCount(Integer editCount) {
		this.editCount = editCount;
	}

	@Override
	public String toString() {
		return "UsersDetail [id=" + id + ", userId=" + userId + ", openId=" + openId + ", unionId=" + unionId
				+ ", createTime=" + createTime + ", mobile=" + mobile + ", nickName=" + nickName + ", gender=" + gender
				+ ", idCardNumber=" + idCardNumber + ", breakDateCount=" + breakDateCount + ", lastBreakDateTime="
				+ lastBreakDateTime + ", qq=" + qq + ", lastLoginTime=" + lastLoginTime + ", editTime=" + editTime
				+ ", editCount=" + editCount + "]";
	}

}