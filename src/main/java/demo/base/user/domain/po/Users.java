package demo.base.user.domain.po;

import demo.base.user.domain.param.controllerParam.UserRegistParam;

public class Users {

	private Long id;

	private String userName;

	private String pwd;

	private Boolean enabled;

	private Boolean accountNonLocked;

	private Boolean accountNonExpired;

	private Boolean credentialsNonExpired;

	@Override
	public String toString() {
		return "Users [id=" + id + ", userName=" + userName + ", pwd=" + pwd + ", enabled=" + enabled
				+ ", accountNonLocked=" + accountNonLocked + ", accountNonExpired=" + accountNonExpired
				+ ", credentialsNonExpired=" + credentialsNonExpired + "]";
	}

	public Boolean getCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd == null ? null : pwd.trim();
	}

	public Boolean getEnable() {
		return enabled;
	}

	public void setEnable(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(Boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public Boolean getAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(Boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Users createUserFromUserRegistParam(UserRegistParam param) {
		Users user = new Users();
		user.setUserName(param.getUserName());
		user.setPwd(param.getPwd());
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		user.setEnable(true);
		return user;
	}

}