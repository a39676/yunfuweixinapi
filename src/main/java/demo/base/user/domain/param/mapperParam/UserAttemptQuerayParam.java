package demo.base.user.domain.param.mapperParam;

public class UserAttemptQuerayParam {

	private String userName;
	private Long userId;
	private Integer attemptType;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getAttemptType() {
		return attemptType;
	}

	public void setAttemptType(Integer attemptType) {
		this.attemptType = attemptType;
	}

	@Override
	public String toString() {
		return "UserAttemptQuerayParam [userName=" + userName + ", userId=" + userId + ", attemptType=" + attemptType
				+ "]";
	}

}
