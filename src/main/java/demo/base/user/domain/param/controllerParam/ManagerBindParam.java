package demo.base.user.domain.param.controllerParam;

import org.apache.commons.lang3.StringUtils;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class ManagerBindParam implements CommonControllerParam {

	private String wxCode;
	private String mobile;
	private Integer validCode;

	public String getWxCode() {
		return wxCode;
	}

	public void setWxCode(String wxCode) {
		this.wxCode = wxCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getValidCode() {
		return validCode;
	}

	public void setValidCode(Integer validCode) {
		this.validCode = validCode;
	}

	@Override
	public ManagerBindParam fromJson(JSONObject json) {
		ManagerBindParam param = new ManagerBindParam();
		if (json.containsKey("mobile")) {
			param.setMobile(json.getString("mobile").replaceAll("\\s", ""));
		}
		if (json.containsKey("validCode") && NumericUtilCustom.matchInteger(json.getString("validCode"))) {
			param.setValidCode(json.getInt("validCode"));
		}
		if (json.containsKey("wxCode") && StringUtils.isNotBlank(json.getString("wxCode"))) {
			param.setWxCode(json.getString("wxCode"));
		}

		return param;
	}
}
