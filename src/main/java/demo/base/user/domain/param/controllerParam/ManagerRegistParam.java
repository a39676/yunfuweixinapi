package demo.base.user.domain.param.controllerParam;

import org.apache.commons.lang.StringUtils;

import demo.base.user.domain.po.UserConstant;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class ManagerRegistParam extends UserRegistParam {

	private Long orgId;
	private Long rouleGroupId;
	private String staffNumber;
	private Integer authId;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getRouleGroupId() {
		return rouleGroupId;
	}

	public void setRouleGroupId(Long rouleGroupId) {
		this.rouleGroupId = rouleGroupId;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public Integer getAuthId() {
		return authId;
	}

	public void setAuthId(Integer authId) {
		this.authId = authId;
	}

	@Override
	public ManagerRegistParam fromJson(JSONObject j) {
		ManagerRegistParam p = new ManagerRegistParam();
		if (j.containsKey("userName")) {
			p.setUserName(j.getString("userName").replaceAll("\\s", ""));
		}
		if (j.containsKey("nickName")) {
			p.setNickName(j.getString("nickName").replaceAll("\\s", ""));
		}
		if (j.containsKey("gender")) {
			if (UserConstant.male.toString().equals(j.getString("gender"))) {
				p.setGender(UserConstant.male);
			} else if (UserConstant.female.toString().equals(j.getString("gender"))) {
				p.setGender(UserConstant.female);
			} else {
				p.setGender(UserConstant.secret);
			}
		}
		if (j.containsKey("pwd")) {
			p.setPwd(j.getString("pwd"));
		}
		if (j.containsKey("pwdRepeat")) {
			p.setPwdRepeat(j.getString("pwdRepeat"));
		}
		if (j.containsKey("qq")) {
			p.setQq(j.getString("qq").replaceAll("\\s", ""));
		}
		if (j.containsKey("mobile")) {
			p.setMobile(j.getString("mobile").replaceAll("\\s", ""));
		}
		if (j.containsKey("orgId") && NumericUtilCustom.matchInteger(j.getString("orgId"))) {
			p.setOrgId(j.getLong("orgId"));
		}
		if (j.containsKey("roleGroupId") && NumericUtilCustom.matchInteger(j.getString("roleGroupId"))) {
			p.setRouleGroupId(j.getLong("roleGroupId"));
		}
		if (j.containsKey("staffNumber") && StringUtils.isNotBlank(j.getString("staffNumber"))) {
			p.setStaffNumber(j.getString("staffNumber"));
		}
		if (j.containsKey("authId") && NumericUtilCustom.matchInteger(j.getString("authId"))) {
			p.setAuthId(j.getInt("authId"));
		}

		return p;
	}

}
