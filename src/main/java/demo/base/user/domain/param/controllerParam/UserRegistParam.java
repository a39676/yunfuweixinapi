package demo.base.user.domain.param.controllerParam;

import org.apache.commons.lang3.StringUtils;

import demo.base.user.domain.po.UserConstant;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class UserRegistParam implements CommonControllerParam {

	private String userName;
	private String nickName;
	private String pwd;
	private String pwdRepeat;
	private Integer gender;
	private String qq;
	private String mobile;
	private Integer validCode;
	private String wxCode;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwdRepeat() {
		return pwdRepeat;
	}

	public void setPwdRepeat(String pwdRepeat) {
		this.pwdRepeat = pwdRepeat;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getValidCode() {
		return validCode;
	}

	public void setValidCode(Integer validCode) {
		this.validCode = validCode;
	}

	public String getWxCode() {
		return wxCode;
	}

	public void setWxCode(String wxCode) {
		this.wxCode = wxCode;
	}

	@Override
	public String toString() {
		return "UserRegistParam [userName=" + userName + ", nickName=" + nickName + ", pwd=" + pwd + ", pwdRepeat="
				+ pwdRepeat + ", gender=" + gender + ", qq=" + qq + ", mobile=" + mobile + ", validCode=" + validCode
				+ ", wxCode=" + wxCode + "]";
	}

	@Override
	public UserRegistParam fromJson(JSONObject json) {
		UserRegistParam param = new UserRegistParam();
		if (json.containsKey("userName")) {
			param.setUserName(json.getString("userName").replaceAll("\\s", ""));
		}
		if (json.containsKey("nickName")) {
			param.setNickName(json.getString("nickName").replaceAll("\\s", ""));
		}
		if (json.containsKey("gender")) {
			if (UserConstant.male.toString().equals(json.getString("gender"))) {
				param.setGender(UserConstant.male);
			} else if (UserConstant.female.toString().equals(json.getString("gender"))) {
				param.setGender(UserConstant.female);
			} else {
				param.setGender(UserConstant.secret);
			}
		}
		if (json.containsKey("pwd")) {
			param.setPwd(json.getString("pwd"));
		}
		if (json.containsKey("pwdRepeat")) {
			param.setPwdRepeat(json.getString("pwdRepeat"));
		}
		if (json.containsKey("qq")) {
			param.setQq(json.getString("qq").replaceAll("\\s", ""));
		}
		if (json.containsKey("mobile")) {
			param.setMobile(json.getString("mobile").replaceAll("\\s", ""));
		}
		if (json.containsKey("validCode") && NumericUtilCustom.matchInteger(json.getString("validCode"))) {
			param.setValidCode(json.getInt("validCode"));
		}
		if (json.containsKey("wxCode") && StringUtils.isNotBlank(json.getString("wxCode"))) {
			param.setWxCode(json.getString("wxCode"));
		}

		return param;
	}

}
