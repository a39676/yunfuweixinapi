package demo.base.user.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class UpdateNickNameStaffNumberParam implements CommonControllerParam {

	private String nickName;

	private Long userId;

	private String mobile;

	private String staffNumber;

	@Override
	public UpdateNickNameStaffNumberParam fromJson(JSONObject j) {
		UpdateNickNameStaffNumberParam p = new UpdateNickNameStaffNumberParam();
		if (j.containsKey("nickName")) {
			p.setNickName(j.getString("nickName").trim());
		}
		if (j.containsKey("staffNumber")) {
			p.setStaffNumber(j.getString("staffNumber").trim());
		}
		if (j.containsKey("userId") && NumericUtilCustom.matchInteger(j.getString("userId"))) {
			p.setUserId(j.getLong("userId"));
		}
		if(j.containsKey("mobile") && NumericUtilCustom.matchInteger(j.getString("mobile"))) {
			p.setMobile(j.getString("mobile"));
		}
		return p;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "UpdateNickNameStaffNumberParam [nickName=" + nickName + ", userId=" + userId + ", mobile=" + mobile
				+ ", staffNumber=" + staffNumber + "]";
	}

}
