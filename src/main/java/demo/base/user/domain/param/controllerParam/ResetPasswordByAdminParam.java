package demo.base.user.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class ResetPasswordByAdminParam implements CommonControllerParam {

	private String mobile;
	private String pwd;
	private String pwdRepeat;

	@Override
	public ResetPasswordByAdminParam fromJson(JSONObject j) {
		ResetPasswordByAdminParam p = new ResetPasswordByAdminParam();
		if (j.containsKey("mobile") && NumericUtilCustom.matchInteger(j.getString("mobile"))) {
			p.setMobile("mobile");
		}
		if (j.containsKey("pwd")) {
			p.setPwd(j.getString("pwd"));
		}
		if (j.containsKey("pwdRepeat")) {
			p.setPwdRepeat(j.getString("pwdRepeat"));
		}
		return p;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwdRepeat() {
		return pwdRepeat;
	}

	public void setPwdRepeat(String pwdRepeat) {
		this.pwdRepeat = pwdRepeat;
	}

}
