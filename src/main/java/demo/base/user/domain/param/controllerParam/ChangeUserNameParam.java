package demo.base.user.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class ChangeUserNameParam implements CommonControllerParam {

	private String newUserName;
	private String oldUserName;
	private String pwd;
	private String pwdRepeat;

	@Override
	public ChangeUserNameParam fromJson(JSONObject j) {
		ChangeUserNameParam p = new ChangeUserNameParam();
		if (j.containsKey("newUserName") && NumericUtilCustom.matchInteger(j.getString("newUserName"))) {
			p.setNewUserName(j.getString("newUserName"));
		}
		if (j.containsKey("oldUserName") && NumericUtilCustom.matchInteger(j.getString("oldUserName"))) {
			p.setOldUserName(j.getString("oldUserName"));
		}
		if (j.containsKey("pwd")) {
			p.setPwd(j.getString("pwd"));
		}
		if (j.containsKey("pwdRepeat")) {
			p.setPwdRepeat(j.getString("pwdRepeat"));
		}
		return p;
	}

	public String getNewUserName() {
		return newUserName;
	}

	public void setNewUserName(String newUserName) {
		this.newUserName = newUserName;
	}

	public String getOldUserName() {
		return oldUserName;
	}

	public void setOldUserName(String oldUserName) {
		this.oldUserName = oldUserName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwdRepeat() {
		return pwdRepeat;
	}

	public void setPwdRepeat(String pwdRepeat) {
		this.pwdRepeat = pwdRepeat;
	}

}
