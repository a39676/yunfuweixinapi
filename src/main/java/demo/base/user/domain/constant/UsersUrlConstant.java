package demo.base.user.domain.constant;

public class UsersUrlConstant {
	
	public static final String root = "/user";
	public static final String userNameExistCheck = "/ususerNameExistChecker";
	public static final String userRegist = "/userRegist";
	public static final String newManagerRegist = "/newManagerRegist";
	public static final String userInfo = "/userInfo";
	/** 登录用户修改密码 */
	public static final String resetPassword = "/resetPassword";
	/** 通过短信验证码重置密码 */
	public static final String resetPasswordByForgot = "/resetPasswordByForgot";
	/** 注册验证码 */
	public static final String sendValidSms = "/sendValidSms";
	/** 找回验证码 */
	public static final String sendValidSms2 = "/sendValidSms2";
	public static final String getUserRoles = "/getUserRoles";

	public static final String countUser = "/countUser";
	public static final String managerBind = "/managerBind";
	
	public static final String isLogin = "/isLogin";
	
	public static final String changeUserName = "/changeUserName";
	
	public static final String resetPasswordByAdmin = "/resetPasswordByAdmin";
}
