package demo.base.user.service;

import java.util.ArrayList;
import java.util.List;

import demo.base.user.domain.param.controllerParam.ManagerRegistParam;
import demo.base.user.domain.param.controllerParam.ResetPasswordByAdminParam;
import demo.base.user.domain.param.controllerParam.ResetPasswordByForgotParam;
import demo.base.user.domain.param.controllerParam.UpdateNickNameStaffNumberParam;
import demo.base.user.domain.param.controllerParam.ChangeUserNameParam;
import demo.base.user.domain.param.controllerParam.ManagerBindParam;
import demo.base.user.domain.param.controllerParam.UserRegistParam;
import demo.base.user.domain.param.mapperParam.UserAttemptQuerayParam;
import demo.base.user.domain.po.UserAttempts;
import demo.base.user.domain.po.Users;
import demo.base.user.domain.result.CountUserResult;
import demo.base.user.domain.vo.UsersDetailVO;
import demo.common.domain.result.CommonResult;
import demo.weixin.domain.po.WXStateOid;


public interface UsersService {

	int insertFailAttempts(String userName);

	int setLockeds(Users user);
	
	int resetFailAttempts(String userName);

	Long getUserIdByUserName(String userName);
	
	/**
	 * 可输入 userName || userId(better)
	 * 2017年4月13日
	 * @param userName
	 * @param userId
	 * @return
	 * ArrayList<UserAttempts>
	 */
	ArrayList<UserAttempts> getUserAttempts(UserAttemptQuerayParam param);
	
	CommonResult newUserRegist(UserRegistParam param, String ip);
	CommonResult newManagerRegist(ManagerRegistParam param, String ip);

	boolean isUserExists(String userName);
	
	int eraserUsersAndRoles(Users user);

	Users getUserbyUserName(String userName);

	UsersDetailVO findUserDetail(Long userId);

	int countAttempts(String userName);

	CommonResult resetPasswordByLoginUser(Long userId, String oldPassword, String newPassword,
			String newPasswordRepeat);

	CommonResult changeUserRoleByRoleGroupId(Long userId, Long newGroupId);

	CommonResult getUserRoles();

	CountUserResult countUser();

	void deleteUserAndDetail(Long userId);

	/** 更新用户资料中的openId */
	void updateOpenIdWithMobile(List<WXStateOid> wxStateOidList);

	String findOpenIdByUserId(Long userId);

	/** 后台工作人员微信绑定 */
	CommonResult managerBind(ManagerBindParam controllerParam);

	/** 忘记密码,通过短信验证码重置 */
	CommonResult resetPasswordByForgot(ResetPasswordByForgotParam rpParam);

	/** 更改用户名 */
	CommonResult changeUserName(ChangeUserNameParam param) throws Exception;

	CommonResult updateNickName(UpdateNickNameStaffNumberParam param);

	CommonResult resetPasswordByAdmin(ResetPasswordByAdminParam param);

}
