package demo.base.user.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.base.base.domain.SystemConstantStore;
import demo.base.base.domain.constant.RoleGroupDefault;
import demo.base.base.domain.po.RoleGroupsPO;
import demo.base.base.mapper.RoleGroupsPOMapper;
import demo.base.base.service.RoleGroupService;
import demo.base.base.service.SystemConstantService;
import demo.base.user.domain.bo.UserCountBO;
import demo.base.user.domain.param.controllerParam.ChangeUserNameParam;
import demo.base.user.domain.param.controllerParam.ManagerBindParam;
import demo.base.user.domain.param.controllerParam.ManagerRegistParam;
import demo.base.user.domain.param.controllerParam.ResetPasswordByAdminParam;
import demo.base.user.domain.param.controllerParam.ResetPasswordByForgotParam;
import demo.base.user.domain.param.controllerParam.UpdateNickNameStaffNumberParam;
import demo.base.user.domain.param.controllerParam.UserRegistParam;
import demo.base.user.domain.param.mapperParam.InsertIgnoreUserRoleParam;
import demo.base.user.domain.param.mapperParam.ResetFailAttemptParam;
import demo.base.user.domain.param.mapperParam.UserAttemptQuerayParam;
import demo.base.user.domain.po.UserAttempts;
import demo.base.user.domain.po.UserConstant;
import demo.base.user.domain.po.Users;
import demo.base.user.domain.po.UsersDetail;
import demo.base.user.domain.result.CountUserResult;
import demo.base.user.domain.vo.UsersDetailVO;
import demo.base.user.mapper.UserRolesMapper;
import demo.base.user.mapper.UsersDetailMapper;
import demo.base.user.mapper.UsersMapper;
import demo.base.user.service.UsersService;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.config.costom_component.CustomPasswordEncoder;
import demo.organizations.domain.constant.AuthsDefault;
import demo.organizations.domain.constant.OrgDefault;
import demo.organizations.domain.po.OrganizationStaffPO;
import demo.organizations.domain.po.StaffAuthPO;
import demo.organizations.domain.po.StaffDetailPO;
import demo.organizations.mapper.OrganizationStaffPOMapper;
import demo.organizations.mapper.StaffAuthPOMapper;
import demo.organizations.mapper.StaffDetailPOMapper;
import demo.sms.controller.SmsController;
import demo.util.BaseUtilCustom;
import demo.weixin.domain.po.WXStateOid;
import demo.weixin.service.WeixinService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

@Service
public class UsersServiceImpl extends JdbcDaoSupport implements UsersService {

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Autowired
	private BaseUtilCustom baseUtilCustom;

	@Autowired
	private SmsController smsController;

	@Autowired
	private SystemConstantService systemConstantService;
	@Autowired
	private RoleGroupService roleGroupService;
	@Autowired
	private WeixinService wxService;

	@Autowired
	private UsersMapper usersMapper;
	@Autowired
	private UserRolesMapper userRoleMapper;
	@Autowired
	private OrganizationStaffPOMapper orgStaffMapper;
	@Autowired
	private StaffDetailPOMapper staffDetailMapper;
	@Autowired
	private StaffAuthPOMapper staffAuthMapper;

	@Autowired
	private UsersDetailMapper usersDetailMapper;
	@Autowired
	private RoleGroupsPOMapper roleGroupMapper;

	@Autowired
	private CustomPasswordEncoder passwordEncoder;

	@Override
	public int insertFailAttempts(String userName) {
		int insertCount = 0;

		if (!isUserExists(userName)) {
			return insertCount;
		}
		Long userId = usersMapper.getUserIdByUserName(userName);

		insertCount = usersMapper.insertFailAttempts(userId, userName);
		UserAttemptQuerayParam param = new UserAttemptQuerayParam();
		param.setUserName(userName);
		int maxAttempts = Integer.parseInt(systemConstantService.getValByName(SystemConstantStore.maxAttempts));
		if (getUserAttempts(param).size() >= maxAttempts) {
			usersMapper.lockUserWithAttempts(userName);
		}

		return insertCount;

	}

	@Override
	public int countAttempts(String userName) {
		if (StringUtils.isBlank(userName)) {
			return 0;
		}
		return usersMapper.countAttempts(userName);
	}

	@Override
	public int setLockeds(Users user) {
		return usersMapper.setLockeds(user);
	}

	@Override
	public int resetFailAttempts(String userName) {
		if (StringUtils.isBlank(userName)) {
			return 0;
		}
		ResetFailAttemptParam param = new ResetFailAttemptParam();
		param.setUserName(userName);
		return usersMapper.resetFailAttempts(param);
	}

	@Override
	public Long getUserIdByUserName(String userName) {
		if (StringUtils.isBlank(userName)) {
			return null;
		}
		return usersMapper.getUserIdByUserName(userName);
	}

	@Override
	public ArrayList<UserAttempts> getUserAttempts(UserAttemptQuerayParam param) {
		ArrayList<UserAttempts> userAttemptsList = usersMapper.getUserAttempts(param);
		return userAttemptsList;
	}

	@Override
	public boolean isUserExists(String userName) {

		boolean result = false;

		int count = usersMapper.isUserExists(userName);

		if (count > 0) {
			result = true;
		}

		return result;
	}

	@Override
	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	public CommonResult newUserRegist(UserRegistParam param, String ip) {
		// 此接口默认作为微信用户
		CommonResult result = new CommonResult();
		UsersDetail userDetail = new UsersDetail();
		JSONObject outputJson = new JSONObject();
		boolean exceptionFlag = false;

		param.setUserName(param.getMobile());
		if (!validNormalUserName(param.getUserName())) {
			outputJson.put("userName", "\"" + param.getUserName() + "\" 请填入正确手机号");
			exceptionFlag = true;
		}

		if (usersMapper.isUserExists(param.getUserName()) > 0) {
			outputJson.put("userName", "手机号已注册");
			exceptionFlag = true;
		}

		String nickName = param.getNickName();
		if (StringUtils.isBlank(param.getNickName())) {
			outputJson.put("nickName", "");
			exceptionFlag = true;
		} else if (nickName.length() > 64) {
			outputJson.put("nickName", "昵称太长");
			exceptionFlag = true;
		} else {
			userDetail.setNickName(nickName);
		}

		if (!validPassword(param.getPwd())) {
			outputJson.put("pwd", "密码长度不正确(6到16位)");
			exceptionFlag = true;
		}

		if (!param.getPwd().equals(param.getPwdRepeat())) {
			outputJson.put("pwdRepeat", "两次输入的密码不一致");
			exceptionFlag = true;
		}

		if (validMobile(param.getMobile())) {
			userDetail.setMobile(Long.parseLong(param.getMobile()));
		} else {
			outputJson.put("mobile", "请填入正确的手机号");
			exceptionFlag = true;
		}

		Integer rightValidCode = smsController.findNewValidCode(param.getMobile());
		if (param.getValidCode() == null || rightValidCode == null || !rightValidCode.equals(param.getValidCode())) {
			outputJson.put("validCode", "请填入正确的验证码");
			exceptionFlag = true;
		}

		if (exceptionFlag) {
			result.normalFail();
			result.setMessage(outputJson.toString());
			return result;
		}

		if (param.getGender() == null || param.getGender().equals(UserConstant.secret)) {
			userDetail.setGender(UserConstant.secret);
		} else if (param.getGender().equals(UserConstant.male)) {
			userDetail.setGender(UserConstant.male);
		} else {
			userDetail.setGender(UserConstant.female);
		}

		param.setPwd(passwordEncoder.encode(param.getPwd()));
		Users user = new Users().createUserFromUserRegistParam(param);
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		user.setEnable(true);
		usersMapper.insertNewUser(user);

		userDetail.setUserId(user.getId());
		usersDetailMapper.insertSelective(userDetail);

		if (StringUtils.isNotBlank(param.getWxCode())) {
			wxService.insertCodeAndMobile(param.getWxCode(), param.getUserName());
		}

		try {
			roleGroupService.insertRoleByGroupId(RoleGroupDefault.ROLE_GROUP_WXUSER_ID, user.getId());
		} catch (Exception e) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		result.normalSuccess();
		result.setJson(outputJson);

		return result;
	}

	@Override
	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	public CommonResult newManagerRegist(ManagerRegistParam param, String ip) {
		CommonResult result = new CommonResult();
		UsersDetail userDetail = new UsersDetail();
		JSONObject outputJson = new JSONObject();
		boolean exceptionFlag = false;

		if (param.getOrgId() == null) {
			outputJson.put("orgId", "请填入机构ID");
			exceptionFlag = true;
		}

		if (!validMobile(param.getUserName())) {
			outputJson.put("userName", "\"" + param.getUserName() + "\" 请输入手机号");
			exceptionFlag = true;
		}

		if (usersMapper.isUserExists(param.getUserName()) > 0) {
			outputJson.put("userName", "用户名已存在");
			exceptionFlag = true;
		}

		String nickNameAfterEscapeHtml = param.getNickName();
		if (StringUtils.isBlank(param.getNickName())) {
			outputJson.put("nickName", "请输入姓名");
			exceptionFlag = true;
		} else if (nickNameAfterEscapeHtml.length() > 64) {
			outputJson.put("nickName", "昵称太长");
			exceptionFlag = true;
		} else {
			userDetail.setNickName(nickNameAfterEscapeHtml);
		}

		if (!validPassword(param.getPwd())) {
			outputJson.put("pwd", "密码长度不正确(6到16位)");
			exceptionFlag = true;
		}

		if (!param.getPwd().equals(param.getPwdRepeat())) {
			outputJson.put("pwdRepeat", "两次输入的密码不一致");
			exceptionFlag = true;
		}

		if (validMobile(param.getMobile())) {
			userDetail.setMobile(Long.parseLong(param.getMobile()));
		} else {
			outputJson.put("mobile", "请填入正确的手机号");
			exceptionFlag = true;
		}

		if (param.getAuthId() == null && !baseUtilCustom.hasAdminRole()) {
			outputJson.put("authId", "请选择角色");
			exceptionFlag = true;
		}

		if (exceptionFlag) {
			result.normalFail();
			result.setMessage(outputJson.toString());
			return result;
		}

		if (NumericUtilCustom.matchInteger(param.getQq())) {
			userDetail.setQq(Long.parseLong(param.getQq()));
		}

		if (param.getGender() == null || param.getGender().equals(UserConstant.secret)) {
			userDetail.setGender(UserConstant.secret);
		} else if (param.getGender().equals(UserConstant.male)) {
			userDetail.setGender(UserConstant.male);
		} else {
			userDetail.setGender(UserConstant.female);
		}

		param.setPwd(passwordEncoder.encode(param.getPwd()));
		Users user = new Users().createUserFromUserRegistParam(param);
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		user.setEnable(true);
		usersMapper.insertNewUser(user);

		userDetail.setUserId(user.getId());
		usersDetailMapper.insertSelective(userDetail);

		StaffDetailPO staffDetail = staffDetailMapper.findByUserId(user.getId());
		if (staffDetail == null) {
			staffDetail = new StaffDetailPO();
			staffDetail.setUserId(user.getId());
			staffDetail.setStaffNumber(param.getStaffNumber());
			staffDetail.setStaffsName(param.getNickName());
			staffDetailMapper.insertSelective(staffDetail);
			staffDetail = staffDetailMapper.findByUserId(user.getId());
		}

		int osgStaffExiests = orgStaffMapper.checkExists(param.getOrgId(), staffDetail.getId());
		if (osgStaffExiests != 1) {
			OrganizationStaffPO newOrgStaff = new OrganizationStaffPO();
			newOrgStaff.setOrgId(param.getOrgId());
			newOrgStaff.setStaffId(staffDetail.getId());
			orgStaffMapper.insertSelective(newOrgStaff);
		}

		StaffAuthPO sa = staffAuthMapper.findByStaffId(staffDetail.getId());
		if (sa == null) {
			sa = new StaffAuthPO();
			Long authId = null;
			if (param.getAuthId() == null) {
				authId = AuthsDefault.AUTH_BRANCH_ADMIN_ID;
			} else {
				authId = param.getAuthId().longValue();
			}
			sa.setAuthId(authId);
			sa.setStaffId(staffDetail.getId());
			staffAuthMapper.insertSelective(sa);
		}

		if (param.getOrgId().equals(OrgDefault.orgUnionId)) {
			roleGroupService.insertRoleByGroupId(RoleGroupDefault.ROLE_GROUP_UNION_ID, user.getId());
		} else {
			if(param.getAuthId() == null) {
				roleGroupService.insertRoleByGroupId(RoleGroupDefault.ROLE_GROUP_BRANCH_ADMIN_ID, user.getId());
			} else {
				roleGroupService.insertRoleByGroupId(param.getAuthId().longValue(), user.getId());
			}
		}

		result.normalSuccess();
		result.setJson(outputJson);
		return result;
	}

	@Override
	public int eraserUsersAndRoles(Users user) {
		return usersMapper.eraserUsersAndRoles(user);
	}

	@Override
	public Users getUserbyUserName(String userName) {
		return usersMapper.findUserByUserName(userName);
	}

	private boolean validNormalUserName(String userNameInput) {
		// if (userNameInput == null) {
		// return false;
		// }
		// return userNameInput.matches("[a-z][a-zA-Z0-9_]{5,15}");
		return validMobile(userNameInput);
	}

	private boolean validPassword(String passwordInput) {
		if (passwordInput == null) {
			return false;
		}
		return passwordInput.matches(".{6,16}");
	}

	private boolean validMobile(String mobile) {
		if (mobile == null) {
			return false;
		}
		return mobile.matches("^1[345789]\\d{9}");
	}

	@Override
	public UsersDetailVO findUserDetail(Long userId) {
		if (userId == null) {
			return new UsersDetailVO();
		}

		return buildUserDetailVOByPo(usersDetailMapper.findUserDetail(userId));
	}

	private UsersDetailVO buildUserDetailVOByPo(UsersDetail ud) {
		UsersDetailVO vo = new UsersDetailVO();
		if (ud == null) {
			return vo;
		}

		vo.setNickName(ud.getNickName());

		if (ud.getGender() == 1) {
			vo.setGender("男");
		} else if (ud.getGender() == 0) {
			vo.setGender("女");
		} else {
			vo.setGender("保密");
		}
		vo.setLastLoginTime(ud.getLastLoginTime());
		vo.setMobile(ud.getMobile());
		vo.setQq(ud.getQq());
		vo.setOpenId(ud.getOpenId());

		return vo;
	}

	@Override
	public CommonResult resetPasswordByLoginUser(Long userId, String oldPassword, String newPassword,
			String newPasswordRepeat) {
		CommonResult result = new CommonResult();
		String encodePassword = passwordEncoder.encode(oldPassword);
		if (usersMapper.matchUserPassword(userId, encodePassword) < 1) {
			result.fillWithResult(ResultTypeBase.wrongOldPassword);
			return result;
		}

		return resetPassword(userId, newPassword, newPasswordRepeat);
	}

	private CommonResult resetPassword(Long userId, String newPassword, String newPasswordRepeat) {
		CommonResult result = new CommonResult();
		if (!validPassword(newPassword)) {
			result.fillWithResult(ResultTypeBase.invalidPassword);
			return result;
		}

		if (!newPassword.equals(newPasswordRepeat)) {
			result.fillWithResult(ResultTypeBase.differentPassword);
			return result;
		}

		Users user = usersMapper.findUser(userId);

		int resetCount = usersMapper.resetPassword(passwordEncoder.encode(newPassword), newPassword, user.getId());
		if (resetCount > 0) {
			result.fillWithResult(ResultTypeBase.resetPassword);
			return result;
		} else {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
	}

	@Override
	public CommonResult resetPasswordByForgot(ResetPasswordByForgotParam rpParam) {
		CommonResult result = new CommonResult();
		if (StringUtils.isAnyBlank(rpParam.getMobile(), rpParam.getPwd(), rpParam.getPwdRepeat(),
				rpParam.getValidCode())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		if (!validMobile(rpParam.getMobile())) {
			result.failWithMessage("手机号不正确");
			return result;
		}

		if (!validPassword(rpParam.getPwd())) {
			result.failWithMessage("密码长度不正确(6到16位)");
			return result;
		}

		if (!rpParam.getPwd().equals(rpParam.getPwdRepeat())) {
			result.failWithMessage("两次输入的密码不一致");
			return result;
		}

		Integer rightValidCode = smsController.findNewValidCode(rpParam.getMobile());
		if (rpParam.getValidCode() == null || rightValidCode == null
				|| !rightValidCode.toString().equals(rpParam.getValidCode())) {
			result.failWithMessage("请填入正确的验证码");
			return result;
		}

		Users targetUser = usersMapper.findUserByUserName(rpParam.getMobile());
		if (targetUser == null || targetUser.getId() == null) {
			result.failWithMessage("手机号不正确");
			return result;
		}

		result = resetPassword(targetUser.getId(), rpParam.getPwd(), rpParam.getPwdRepeat());

		return result;
	}

	@Override
	public CommonResult changeUserRoleByRoleGroupId(Long userId, Long newGroupId) {
		CommonResult result = new CommonResult();
		if (userId == null || newGroupId == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		List<RoleGroupsPO> roleGroup = roleGroupMapper.findByGroupId(newGroupId);
		if (roleGroup == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}

		userRoleMapper.deleteRolesByUserId(userId);
		InsertIgnoreUserRoleParam param = new InsertIgnoreUserRoleParam();
		param.setUserId(userId);
		List<Long> roleIdList = new ArrayList<Long>();
		roleGroup.stream().forEach(l -> roleIdList.add(l.getRoleId()));
		param.setRoleIdList(roleIdList);
		int updateCount = userRoleMapper.insertIgnore(param);
		if (updateCount < 1) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult getUserRoles() {
		CommonResult result = new CommonResult();
		List<String> roleList = baseUtilCustom.getRoles();
		JSONArray ja = JSONArray.fromObject(roleList);
		JSONObject json = new JSONObject();
		json.put("roles", ja);
		result.setJson(json);
		result.setIsSuccess();
		return result;
	}

	@Override
	public CountUserResult countUser() {
		CountUserResult result = new CountUserResult();
		UserCountBO userCount = usersMapper.userCount();
		if (userCount == null) {
			return result;
		}
		result.setAllCount(userCount.getAllCount());
		result.setTodayCount(userCount.getTodayCount());
		result.setIsSuccess();
		return result;
	}

	@Override
	public void deleteUserAndDetail(Long userId) {
		usersMapper.deleteUser(userId);
		usersDetailMapper.deleteUserDetail(userId);
	}

	@Override
	public void updateOpenIdWithMobile(List<WXStateOid> wxStateOidList) {
		if (wxStateOidList == null || wxStateOidList.size() < 1) {
			return;
		}

		wxStateOidList.stream()
				.forEach(p -> usersDetailMapper.updateOpenIdWithMobile(p.getOpenId(), p.getMobile().toString()));
	}

	@Override
	public String findOpenIdByUserId(Long userId) {
		if (userId == null) {
			return "";
		}
		UsersDetail ud = usersDetailMapper.findUserDetail(userId);
		if (ud == null) {
			return "";
		}
		return ud.getOpenId();
	}

	@Override
	public CommonResult managerBind(ManagerBindParam controllerParam) {
		CommonResult result = new CommonResult();
		if (controllerParam.getValidCode() == null || controllerParam.getMobile() == null
				|| StringUtils.isBlank(controllerParam.getWxCode())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		Integer rightValidCode = smsController.findNewValidCode(controllerParam.getMobile());
		if (rightValidCode == null || !rightValidCode.equals(controllerParam.getValidCode())) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}

		if (!validMobile(controllerParam.getMobile())) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}

		Integer staffExists = staffDetailMapper.staffMobileExists(controllerParam.getMobile());
		if (staffExists == null || !staffExists.equals(1)) {
			result.failWithMessage("员工不存在");
			return result;
		}

		return wxService.insertCodeAndMobile(controllerParam.getWxCode(), controllerParam.getMobile());
	}

	@Override
	@Transactional(value = "transactionManager", rollbackFor = Exception.class)
	public CommonResult changeUserName(ChangeUserNameParam param) throws Exception {
		CommonResult result = new CommonResult();
		if (StringUtils.isAnyBlank(param.getNewUserName(), param.getOldUserName(), param.getPwd(),
				param.getPwdRepeat())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		if (!validMobile(param.getNewUserName()) || !validMobile(param.getOldUserName())) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}

		if ("1".equals(String.valueOf(usersMapper.isUserExists(param.getNewUserName())))) {
			deleteUserAndDetail(Long.parseLong(param.getNewUserName()));
		}

		int updateCount = 0;
		updateCount = usersMapper.changeUserName(param);
		if (updateCount < 1) {
			throw new Exception();
		}

		updateCount = usersDetailMapper.updateMobile(param);
		if (updateCount < 1) {
			throw new Exception();
		}

		Users user = usersMapper.findUserByUserName(param.getNewUserName());

		resetPassword(user.getId(), param.getPwd(), param.getPwdRepeat());

		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult updateNickName(UpdateNickNameStaffNumberParam param) {
		CommonResult result = new CommonResult();
		if (param.getUserId() == null || StringUtils.isBlank(param.getNickName())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		usersDetailMapper.updateNickName(param);
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult resetPasswordByAdmin(ResetPasswordByAdminParam param) {
		CommonResult result = new CommonResult();
		if(StringUtils.isAnyBlank(param.getMobile(), param.getPwd(), param.getPwdRepeat())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		if(!validMobile(param.getMobile())) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		Users user = usersMapper.findUserByUserName(param.getMobile());
		if(user == null || user.getId() == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}

		result = resetPassword(user.getId(), param.getPwd(), param.getPwdRepeat());
		return result;
	}
}