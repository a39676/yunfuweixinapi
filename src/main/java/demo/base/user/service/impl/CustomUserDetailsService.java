package demo.base.user.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class CustomUserDetailsService extends JdbcDaoImpl {
	
	@Autowired
	private DataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	@Value("select "
			+ "  uss.id, "
			+ "  uss.user_name, "
			+ "  uss.pwd, "
			+ "  uss.enabled, "
			+ "  uss.account_non_locked as accountNonLocked, "
			+ "  uss.account_non_expired as accountNonExpired, "
			+ "  uss.credentials_non_expired as credentialsNonExpired "
			+ "from "
			+ "  users as uss "
			+ "where "
			+ "  uss.user_name = ? "
			+ "")
	public void setUsersByUsernameQuery(String usersByUsernameQueryString) {
		super.setUsersByUsernameQuery(usersByUsernameQueryString);
	}
	
	@Override
	@Value("select "
			+ "    u.user_name, "
			+ "	   r.role as role "
			+ "from "
			+ "    users as u"
			+ "    left join user_roles as ur "
			+ "        on u.id = ur.user_id "
			+ "	   left join roles as r "
			+ "	       on ur.role_id = r.role_id "
			+ "where "
			+ "    u.user_name = ?")
	public void setAuthoritiesByUsernameQuery(String queryString) {
		super.setAuthoritiesByUsernameQuery(queryString);
	}
	
	
	//override to get accountNonLocked
	@Override
	public List<UserDetails> loadUsersByUsername(String userName) {
		return getJdbcTemplate().query(
			super.getUsersByUsernameQuery(), new String[] { userName },
			new RowMapper<UserDetails>() {
				public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
					String username = rs.getString("user_name");
					String password = rs.getString("pwd");
					boolean enabled = rs.getBoolean("enabled");
					boolean accountNonExpired = rs.getBoolean("accountNonExpired");
					boolean accountNonLocked = rs.getBoolean("accountNonLocked");
					boolean credentialsNonExpired = rs.getBoolean("credentialsNonExpired");

					return new User(username, password, enabled, accountNonExpired, credentialsNonExpired,
							accountNonLocked, AuthorityUtils.NO_AUTHORITIES);
				}

		  	}
		);
	}
	
	//override to pass accountNonLocked
	@Override
	public UserDetails createUserDetails(String username, UserDetails userFromUserQuery,
			List<GrantedAuthority> combinedAuthorities) {
		String returnUsername = userFromUserQuery.getUsername();

		if (super.isUsernameBasedPrimaryKey()) {
		    returnUsername = username;
		}

		return new User(returnUsername, 
				userFromUserQuery.getPassword(),
                userFromUserQuery.isEnabled(),
		        userFromUserQuery.isAccountNonExpired(),
                userFromUserQuery.isCredentialsNonExpired(),
                userFromUserQuery.isAccountNonLocked(), 
                combinedAuthorities);
	}
}
