package demo.base.user.service.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import demo.base.base.domain.type.RolesType;
import demo.base.user.domain.po.UsersDetail;
import demo.base.user.mapper.UsersMapper;
import demo.common.domain.result.CommonResult;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private BaseUtilCustom baseUtilCustom;

	@Autowired
	private UsersMapper usersMapper;
	
	protected Log logger = LogFactory.getLog(this.getClass());
	 
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		HashMap<String, Object> detailMap = new HashMap<String, Object>();
		UsersDetail userDetail = usersMapper.getUserDetailByUserName(auth.getName());
		detailMap.put("userId", userDetail.getUserId());
		detailMap.put("nickName", userDetail.getNickName());
		baseUtilCustom.setAuthDetail(detailMap);
		
//		Collection<? extends GrantedAuthority> authDetail = auth.getAuthorities();
		
		
//		handle(request, response, auth);
		
		CommonResult result = new CommonResult();
		result.setIsSuccess();
		JSONObject json = JSONObject.fromObject(result);
		response.getWriter().println(json.toString());
		clearAuthenticationAttributes(request);
	}

	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {

		String targetUrl = determineTargetUrl(authentication);

		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);
//		CommonResult result = new CommonResult();
//		result.successWithMessage("/");
//		response.getWriter().println(JSONObject.fromObject(result));
		return;
	}

	protected String determineTargetUrl(Authentication authentication) {
		boolean isUser = false;
		boolean isAdmin = false;
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().contains(RolesType.ROLE_WXUSER.getRoleName())) {
				isUser = true;
				break;
			} else if (grantedAuthority.getAuthority().contains(RolesType.ROLE_ADMIN.getRoleName())) {
				isAdmin = true;
				break;
			}
		}

		if (isUser) {
//			TODO
//			未定义微信登陆后跳转页面
			return "/";
		} else if (isAdmin) {
//			TODO
//			未定义管理员登陆后跳转页面
			return "/";
		} else {
			throw new IllegalStateException();
		}
	}

	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

}
