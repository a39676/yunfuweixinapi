package demo.base.user.mapper;

import demo.base.user.domain.param.mapperParam.DeleteUserRolesParam;
import demo.base.user.domain.param.mapperParam.InsertIgnoreUserRoleParam;
import demo.base.user.domain.po.UserRoles;

public interface UserRolesMapper {
    int insert(UserRoles record);

    int insertSelective(UserRoles record);
    
    int insertIgnore(InsertIgnoreUserRoleParam param);
    
    int deleteRolesByUserId(Long userId);
    
    int deleteUserRoles(DeleteUserRolesParam param);
}