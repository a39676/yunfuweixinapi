package demo.base.user.mapper;

import java.util.ArrayList;
import java.util.Date;

import org.apache.ibatis.annotations.Param;

import demo.base.user.domain.bo.UserCountBO;
import demo.base.user.domain.param.controllerParam.ChangeUserNameParam;
import demo.base.user.domain.param.mapperParam.ResetFailAttemptParam;
import demo.base.user.domain.param.mapperParam.UserAttemptQuerayParam;
import demo.base.user.domain.po.UserAttempts;
import demo.base.user.domain.po.Users;
import demo.base.user.domain.po.UsersDetail;

public interface UsersMapper {

	int insert(Users record);

	int insertSelective(Users record);

	int insertFailAttempts(@Param("userId")Long userId, @Param("userName")String userName);

	int resetFailAttempts(ResetFailAttemptParam param);

	ArrayList<UserAttempts> getUserAttempts(UserAttemptQuerayParam param);

	int isUserExists(String userName);
	
	int cleanAttempts(@Param("dateInput")Date dateInput);
	
	int resetPassword(@Param("pwd")String pwd, @Param("pwdd")String pwdd, @Param("userId")Long userId);

	int lockUserWithAttempts(String userName);

	Users findUserByUserName(String userName);

	int setLockeds(Users user);

	int insertNewUser(Users user);

	int eraserUsersAndRoles(Users user);

	Long getUserIdByUserName(String userName);
	
	UsersDetail getUserDetailByUserName(String userName);
	
	Users findUser(Long userId);
	
	int countAttempts(String userName);
	
	int matchUserPassword(@Param("userId")Long userId, @Param("pwd")String pwd);
	
	UserCountBO userCount();
	
	int deleteUser(Long id);
	
	int changeUserName(ChangeUserNameParam param);
}