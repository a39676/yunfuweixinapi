package demo.base.user.mapper;

import org.apache.ibatis.annotations.Param;

import demo.base.user.domain.param.controllerParam.ChangeUserNameParam;
import demo.base.user.domain.param.controllerParam.UpdateNickNameStaffNumberParam;
import demo.base.user.domain.po.UsersDetail;

public interface UsersDetailMapper {
	int insert(UsersDetail record);

	int insertSelective(UsersDetail record);

	UsersDetail findUserDetail(Long userId);

	int deleteUserDetail(Long userId);

	int updateOpenIdWithMobile(@Param("openId") String openId, @Param("mobile") String mobile);
	
	int updateMobile(ChangeUserNameParam param);
	
	int updateNickName(UpdateNickNameStaffNumberParam param);
}