package demo.base.user.mapper;

import demo.base.user.domain.param.mapperParam.UserIpDeleteParam;
import demo.base.user.domain.po.UserIp;

public interface UserIpMapper {
    int insert(UserIp record);

	int insertSelective(UserIp record);
	
	int deleteRecord(UserIpDeleteParam param);
}