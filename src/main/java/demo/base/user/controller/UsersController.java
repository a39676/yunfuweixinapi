package demo.base.user.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;

import demo.base.constant.BaseStatusCode;
import demo.base.constant.UrlConstant;
import demo.base.user.domain.constant.UsersUrlConstant;
import demo.base.user.domain.param.controllerParam.ChangeUserNameParam;
import demo.base.user.domain.param.controllerParam.ManagerBindParam;
import demo.base.user.domain.param.controllerParam.ManagerRegistParam;
import demo.base.user.domain.param.controllerParam.ResetPasswordByAdminParam;
import demo.base.user.domain.param.controllerParam.ResetPasswordByForgotParam;
import demo.base.user.domain.param.controllerParam.UserRegistParam;
import demo.base.user.domain.po.Users;
import demo.base.user.domain.result.CountUserResult;
import demo.base.user.domain.vo.UsersDetailVO;
import demo.base.user.service.UsersService;
import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.sms.controller.SmsController;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = UsersUrlConstant.root)
public class UsersController extends CommonController {
	
	@Autowired
	private SmsController smsController;
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private BaseUtilCustom baseUtilCustom;
	
	@RequestMapping(value = {UsersUrlConstant.userNameExistCheck})
	public void userNameExistCheck(
			@RequestBody String jsonStrInput, 
			HttpServletResponse response
			) throws IOException {
		// 建输出流
		PrintWriter out = response.getWriter();
		
		JSONObject jsonInput = JSONObject.fromObject(jsonStrInput);
		String userName = jsonInput.getString("userName");
		JSONObject json = new JSONObject();
		boolean result = usersService.isUserExists(userName);
		if(result) {
			json.put("result", BaseStatusCode.fail);
		} else {
			json.put("exception", "user name not exist");
		}
		
		out.print(json);
	}
	
	@PostMapping(value = UsersUrlConstant.userInfo)
	public ModelAndView userInfo() {
		ModelAndView view = new ModelAndView("userJSP/userInfo");
		
		if(!baseUtilCustom.isLoginUser()) {
			view.setViewName(UrlConstant.login);
			return view;
		}
		
		Long userId = baseUtilCustom.getUserId();
		UsersDetailVO ud = usersService.findUserDetail(userId);
		
		view.addObject("nickName", ud.getNickName());
		view.addObject("qq", ud.getQq());
		view.addObject("gender", ud.getGender());
		view.addObject("mobile", ud.getMobile());
		
		return view;
	}
	
	@GetMapping(value = UsersUrlConstant.userRegist)
	public ModelAndView userRegistView(HttpServletRequest request) {
		insertVisitIp(request, "registGet");
		ModelAndView view = new ModelAndView();
		view.setViewName("jsp/sign/sign");
		
		return view;
	}
	
	@PostMapping(value = UsersUrlConstant.userRegist)
	public void userRegistHandler(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) {
		insertVisitIp(request, "registPost");
		JSONObject json = getJson(data);
		UserRegistParam param = new UserRegistParam().fromJson(json);
		String ip = request.getHeader("X-FORWARDED-FOR");
		
		CommonResult result = usersService.newUserRegist(param, ip);
		
		JSONObject jsonOutput = JSONObject.fromObject(result);
		
		try {
			response.getWriter().println(jsonOutput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@PostMapping(value = UsersUrlConstant.newManagerRegist)
	public void managerRegistHandler(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) {
//		insertVisitIp(request, "registPost");
		JSONObject json = getJson(data);
		ManagerRegistParam param = new ManagerRegistParam().fromJson(json);
		String ip = request.getHeader("X-FORWARDED-FOR");
		
		CommonResult result = usersService.newManagerRegist(param, ip);
		
		JSONObject jsonOutput = JSONObject.fromObject(result);
		
		outputJson(response, jsonOutput);
		
	}
	
	public Long getCurrentUserId() {
		String userName = baseUtilCustom.getCurrentUserName();
		if(userName == null || userName.length() == 0) {
			return null;
		} 
		
		Users currentUser = usersService.getUserbyUserName(userName);
		if(currentUser == null) {
			return null;
		}
		return currentUser.getId();
	}

	@PostMapping(value = UsersUrlConstant.resetPassword)
	public void resetPassword(@RequestBody String data, HttpServletResponse response) {
		JSONObject jsonInput = getJson(data);
		CommonResult result = new CommonResult();
		
		if(!jsonInput.containsKey("newPassword") || !jsonInput.containsKey("newPasswordRepeat")) {
			result.fillWithResult(ResultTypeBase.nullParam);
			outputJson(response, JSONObject.fromObject(result));
			return;
		}
		
		if(!jsonInput.containsKey("oldPassword")) {
			result.fillWithResult(ResultTypeBase.nullParam);
			outputJson(response, JSONObject.fromObject(result));
			return;
		}
		result = usersService.resetPasswordByLoginUser(baseUtilCustom.getUserId(), jsonInput.getString("oldPassword"), jsonInput.getString("newPassword"), jsonInput.getString("newPasswordRepeat"));
		
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = UsersUrlConstant.resetPasswordByForgot)
	public void resetPasswordByForgot(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) throws ServerException, ClientException {
		ResetPasswordByForgotParam rpParam = new ResetPasswordByForgotParam().fromJson(getJson(data));
		CommonResult result = usersService.resetPasswordByForgot(rpParam);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = UsersUrlConstant.isLogin)
	public void isLogin(HttpServletResponse response) {
		CommonResult result = new CommonResult();
		if(baseUtilCustom.isLoginUser()) {
			result.setIsSuccess();
		}
		outputJson(response, JSONObject.fromObject(result));
	}

	@PostMapping(value = UsersUrlConstant.sendValidSms)
	public void sendValidSms(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) throws ServerException, ClientException {
		JSONObject json = getJson(data);
		CommonResult result = smsController.sendNewValidCode(json.getString("mobile"));
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = UsersUrlConstant.sendValidSms2)
	public void sendValidSms2(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) throws ServerException, ClientException {
		JSONObject json = getJson(data);
		CommonResult result = smsController.sendNewValidCode2(json.getString("mobile"));
		outputJson(response, JSONObject.fromObject(result));
	}

	public CommonResult changeUserRoleByRoleGroupId(Long userId, Long newGroupId) {
		return usersService.changeUserRoleByRoleGroupId(userId, newGroupId);
	}
	
	@PostMapping(value = UsersUrlConstant.getUserRoles)
	public void getUserRoles(HttpServletResponse response, HttpServletRequest request) {
		CommonResult result = usersService.getUserRoles();
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = UsersUrlConstant.countUser)
	public void countUser(HttpServletResponse response, HttpServletRequest request) {
		CountUserResult result = usersService.countUser();
		outputJson(response, JSONObject.fromObject(result));
	}

	@PostMapping(value = UsersUrlConstant.managerBind)
	public void managerBind(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) {
		ManagerBindParam param = new ManagerBindParam().fromJson(getJson(data));
		CommonResult result = usersService.managerBind(param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = UsersUrlConstant.changeUserName)
	public void changeUserName(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) throws Exception {
		ChangeUserNameParam param = new ChangeUserNameParam().fromJson(getJson(data));
		CommonResult result = usersService.changeUserName(param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = UsersUrlConstant.resetPasswordByAdmin)
	public void resetPasswordByAdmin(@RequestBody String data, HttpServletResponse response, HttpServletRequest request) throws Exception {
		ResetPasswordByAdminParam param = new ResetPasswordByAdminParam().fromJson(getJson(data));
		CommonResult result = usersService.resetPasswordByAdmin(param);
		outputJson(response, JSONObject.fromObject(result));
	}
}
