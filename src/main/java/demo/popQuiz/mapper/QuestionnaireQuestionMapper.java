package demo.popQuiz.mapper;

import demo.popQuiz.domain.param.mapperParam.BatchInsertQuestionnaireQuestionParam;
import demo.popQuiz.domain.po.QuestionnaireQuestion;

public interface QuestionnaireQuestionMapper {
    int insert(QuestionnaireQuestion record);

    int insertSelective(QuestionnaireQuestion record);

    int batchInsert(BatchInsertQuestionnaireQuestionParam param);

    int deleteByQuestionnaireId(Long questionnaireId);
}