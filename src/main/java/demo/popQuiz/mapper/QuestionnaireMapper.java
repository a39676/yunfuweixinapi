package demo.popQuiz.mapper;

import java.util.List;

import demo.popQuiz.domain.po.Questionnaire;

public interface QuestionnaireMapper {
    int insert(Questionnaire record);

    int insertSelective(Questionnaire record);
    
    List<Questionnaire> findList();
    
    int deleteById(Long id);
}