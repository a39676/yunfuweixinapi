package demo.popQuiz.mapper;

import java.util.List;

import demo.popQuiz.domain.param.mapperParam.FindQuestionPage;
import demo.popQuiz.domain.po.Question;

public interface QuestionMapper {
    int insert(Question record);

    int insertSelective(Question record);
    
    List<Question> findQuestionPage(FindQuestionPage p);
    
    Integer countQuestionPage(FindQuestionPage p);
    
    Question findOne(Long id);
    
    int deleteById(Long id);
}