package demo.popQuiz.mapper;

import java.util.List;

import demo.popQuiz.domain.param.mapperParam.FindQuestionnaireUserByQuestionnaireId;
import demo.popQuiz.domain.po.QuestionnaireUser;

public interface QuestionnaireUserMapper {
    int insert(QuestionnaireUser record);

    int insertSelective(QuestionnaireUser record);
    
    List<QuestionnaireUser> findByQuestionnaireId(FindQuestionnaireUserByQuestionnaireId p);
    Integer countByQuestionnaireId(FindQuestionnaireUserByQuestionnaireId p);
    
    int deleteByQuestionnaireId(Long questionnaireId);
}