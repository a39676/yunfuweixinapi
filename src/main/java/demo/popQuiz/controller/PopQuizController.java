package demo.popQuiz.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.popQuiz.domain.constant.PopQuizUrl;
import demo.popQuiz.domain.param.controllerParam.AddNewCollectUserParam;
import demo.popQuiz.domain.param.controllerParam.AddQuestionParam;
import demo.popQuiz.domain.param.controllerParam.BuildQuestionnaireParam;
import demo.popQuiz.domain.param.controllerParam.DeleteQuestionParam;
import demo.popQuiz.domain.param.controllerParam.DeleteQuestionnaireParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionPageParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionnaireUserByQuestionnaireIdParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionnaireUserByQuestionnaireIdResult;
import demo.popQuiz.domain.result.FindQuestionPageResult;
import demo.popQuiz.domain.result.FindQuestionResult;
import demo.popQuiz.domain.result.FindQuestionnaireResult;
import demo.popQuiz.service.PopQuizService;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = PopQuizUrl.root)
public class PopQuizController extends CommonController {

	@Autowired
	private PopQuizService popQuizService;
	@Autowired
	private BaseUtilCustom baseUtilCustom;

	@PostMapping(value = PopQuizUrl.addNewQuestion)
	public void addNewQuestion(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		AddQuestionParam param = new AddQuestionParam().fromJson(getJson(data));
		CommonResult serviceResult = popQuizService.addNewQuestion(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = PopQuizUrl.buildQuestionnaire)
	public void buildQuestionnaire(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		BuildQuestionnaireParam param = new BuildQuestionnaireParam().fromJson(getJson(data));
		CommonResult serviceResult = popQuizService.buildQuestionnaire(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = PopQuizUrl.findQuestionPage)
	public void findQuestionPage(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindQuestionPageParam param = new FindQuestionPageParam().fromJson(getJson(data));
		FindQuestionPageResult serviceResult = popQuizService.findQuestionPage(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = PopQuizUrl.addNewCollectUser)
	public void addNewCollectUser(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		AddNewCollectUserParam param = new AddNewCollectUserParam().fromJson(getJson(data));
		CommonResult serviceResult = popQuizService.addNewCollectUser(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = PopQuizUrl.findQuestion)
	public void findQuestion(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindQuestionParam param = new FindQuestionParam().fromJson(getJson(data));
		FindQuestionResult serviceResult = popQuizService.findQuestion(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = PopQuizUrl.findQuestionnaire)
	public void findQuestionnaire(HttpServletRequest request, HttpServletResponse response) {
		FindQuestionnaireResult serviceResult = popQuizService.findQuestionnaire();
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = PopQuizUrl.findQuestionnaireUserByQuestionnaireId)
	public void findQuestionnaireUserByQuestionnaireId(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindQuestionnaireUserByQuestionnaireIdParam param = new FindQuestionnaireUserByQuestionnaireIdParam().fromJson(getJson(data));
		FindQuestionnaireUserByQuestionnaireIdResult serviceResult = popQuizService.findQuestionnaireUserByQuestionnaireId(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@GetMapping(value = PopQuizUrl.findQuestionnaireListForApp)
	public ModelAndView findQuestionnaireListForApp(HttpServletRequest request, HttpServletResponse response) {
		if(!baseUtilCustom.isLoginUser()) {
			return new ModelAndView(new RedirectView("/weixinHtml/sign/sign.html"));
		} else {
			return new ModelAndView(new RedirectView("/weixinHtml/questionnaire/questionnaire.html"));
		}
	}
	
	@PostMapping(value = PopQuizUrl.deleteQuestionnaire)
	public void deleteQuestionnaire(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		DeleteQuestionnaireParam param = new DeleteQuestionnaireParam().fromJson(getJson(data));
		CommonResult serviceResult = popQuizService.deleteQuestionnaire(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = PopQuizUrl.deleteQuestion)
	public void deleteQuestion(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		DeleteQuestionParam param = new DeleteQuestionParam().fromJson(getJson(data));
		CommonResult serviceResult = popQuizService.deleteQuestion(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
}
