package demo.popQuiz.service;

import demo.common.domain.result.CommonResult;
import demo.popQuiz.domain.param.controllerParam.AddNewCollectUserParam;
import demo.popQuiz.domain.param.controllerParam.AddQuestionParam;
import demo.popQuiz.domain.param.controllerParam.BuildQuestionnaireParam;
import demo.popQuiz.domain.param.controllerParam.DeleteQuestionParam;
import demo.popQuiz.domain.param.controllerParam.DeleteQuestionnaireParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionPageParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionnaireUserByQuestionnaireIdParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionnaireUserByQuestionnaireIdResult;
import demo.popQuiz.domain.result.FindQuestionPageResult;
import demo.popQuiz.domain.result.FindQuestionResult;
import demo.popQuiz.domain.result.FindQuestionnaireResult;

public interface PopQuizService {

	CommonResult addNewQuestion(AddQuestionParam param);

	/** 新建问卷 */
	CommonResult buildQuestionnaire(BuildQuestionnaireParam param);

	FindQuestionPageResult findQuestionPage(Long userId, FindQuestionPageParam controllerParam);

	/** 记录答对问卷的用户 */
	CommonResult addNewCollectUser(Long userId, AddNewCollectUserParam param);

	FindQuestionResult findQuestion(FindQuestionParam p);

	/** 查找所有问卷 */
	FindQuestionnaireResult findQuestionnaire();

	/** 以问卷ID查找所有答对者 */
	FindQuestionnaireUserByQuestionnaireIdResult findQuestionnaireUserByQuestionnaireId(
			FindQuestionnaireUserByQuestionnaireIdParam p);

	/** 删除问题 */
	CommonResult deleteQuestion(DeleteQuestionParam p);

	/** 删除问卷,并删除对应的答对用户 */
	CommonResult deleteQuestionnaire(DeleteQuestionnaireParam p);

}
