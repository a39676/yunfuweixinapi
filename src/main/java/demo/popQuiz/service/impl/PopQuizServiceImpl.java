package demo.popQuiz.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.base.user.domain.vo.UsersDetailVO;
import demo.base.user.service.UsersService;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;
import demo.popQuiz.domain.param.controllerParam.AddNewCollectUserParam;
import demo.popQuiz.domain.param.controllerParam.AddQuestionParam;
import demo.popQuiz.domain.param.controllerParam.BuildQuestionnaireParam;
import demo.popQuiz.domain.param.controllerParam.DeleteQuestionParam;
import demo.popQuiz.domain.param.controllerParam.DeleteQuestionnaireParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionPageParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionnaireUserByQuestionnaireIdParam;
import demo.popQuiz.domain.param.controllerParam.FindQuestionnaireUserByQuestionnaireIdResult;
import demo.popQuiz.domain.param.mapperParam.BatchInsertQuestionnaireQuestionParam;
import demo.popQuiz.domain.param.mapperParam.FindQuestionPage;
import demo.popQuiz.domain.param.mapperParam.FindQuestionnaireUserByQuestionnaireId;
import demo.popQuiz.domain.po.Question;
import demo.popQuiz.domain.po.Questionnaire;
import demo.popQuiz.domain.po.QuestionnaireUser;
import demo.popQuiz.domain.result.FindQuestionPageResult;
import demo.popQuiz.domain.result.FindQuestionResult;
import demo.popQuiz.domain.result.FindQuestionnaireResult;
import demo.popQuiz.domain.vo.QuestionVO;
import demo.popQuiz.domain.vo.QuestionnaireUserVO;
import demo.popQuiz.domain.vo.QuestionnaireVO;
import demo.popQuiz.mapper.QuestionMapper;
import demo.popQuiz.mapper.QuestionnaireMapper;
import demo.popQuiz.mapper.QuestionnaireQuestionMapper;
import demo.popQuiz.mapper.QuestionnaireUserMapper;
import demo.popQuiz.service.PopQuizService;

@Service
public class PopQuizServiceImpl extends CommonService implements PopQuizService {

	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private QuestionnaireQuestionMapper questionnaireQuestionMapper;
	@Autowired
	private QuestionnaireMapper questionnaireMapper;
	@Autowired
	private QuestionnaireUserMapper questionnaireUserMapper;
	@Autowired
	private UsersService userService;
	
	@Override
	public CommonResult addNewQuestion(AddQuestionParam param) {
		Question q = new Question();
		q.setQuestion(param.getQuestion());
		q.setOption1(param.getOption1());
		q.setOption2(param.getOption2());
		q.setOption3(param.getOption3());
		q.setOption4(param.getOption4());

		questionMapper.insertSelective(q);

		return normalSuccess();
	}

	@Override
	public CommonResult buildQuestionnaire(BuildQuestionnaireParam controllerParam) {
		if (StringUtils.isBlank(controllerParam.getQuestionIds())) {
			return nullParam();
		}

		if (controllerParam.getQuestionnaireId() == null
				&& StringUtils.isBlank(controllerParam.getQuestionnaireName())) {
			return nullParam();
		}

		String questionIdStr = controllerParam.getQuestionIds();
		questionIdStr = questionIdStr.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "");
		String[] questionIdArr = questionIdStr.split(",");

		if (questionIdArr.length < 1) {
			return nullParam();
		}

		List<Long> questionIdList = new ArrayList<Long>();
		Long tmpId = null;
		for (String i : questionIdArr) {
			try {
				tmpId = Long.parseLong(i);
				questionIdList.add(tmpId);
			} catch (Exception e) {
			}
		}

		if (questionIdList.size() < 1) {
			return errorParam();
		}

		Long questionnaireId = null;
		if (StringUtils.isNotBlank(controllerParam.getQuestionnaireName())) {
			Questionnaire newQestionnaire = new Questionnaire();
			newQestionnaire.setQuestionnaireName(controllerParam.getQuestionnaireName());
			questionnaireMapper.insertSelective(newQestionnaire);

			if (newQestionnaire.getId() == null) {
				return serviceError();
			}
			questionnaireId = newQestionnaire.getId();
		} else {
			questionnaireId = controllerParam.getQuestionnaireId();
			questionnaireQuestionMapper.deleteByQuestionnaireId(questionnaireId);
		}

		BatchInsertQuestionnaireQuestionParam p = new BatchInsertQuestionnaireQuestionParam();
		p.setQuestionnaireId(questionnaireId);
		p.setQuesetionIdList(questionIdList);
		questionnaireQuestionMapper.batchInsert(p);

		return normalSuccess();
	}

	@Override
	public FindQuestionPageResult findQuestionPage(Long userId, FindQuestionPageParam controllerParam) {
		FindQuestionPageResult result = new FindQuestionPageResult();
		if(userId == null) {
			result.failWithMessage("请先登录");
			return result;
		}
		
		PageParam pp = setPageFromPageNo(controllerParam.getPageNo());

		FindQuestionPage p = new FindQuestionPage();
//		p.setPageSize(pp.getPageSize());
		p.setPageSize(9999);
		p.setPageStart(pp.getPageStart());
		if(controllerParam.getQuestionnaireId() != null) {
			p.setQuestionnaireId(controllerParam.getQuestionnaireId());
		}

		List<Question> questions = questionMapper.findQuestionPage(p);
		List<QuestionVO> questionPage = new ArrayList<QuestionVO>();
		questions.stream().forEach(q -> questionPage.add(buildQuestionVO(q)));
		Integer resultCount = questionMapper.countQuestionPage(p);

		result.setQuestions(questions);
		result.setQuestionList(questionPage);
		result.setResultCount(resultCount);
		result.setIsSuccess();

		return result;
	}

	@Override
	public CommonResult addNewCollectUser(Long userId, AddNewCollectUserParam param) {
		if(userId == null) {
			CommonResult result = new CommonResult();
			result.failWithMessage("请先登录");
			return result;
		}
		if(param.getQuestionnaireId() == null) {
			return nullParam();
		}
		
		UsersDetailVO userDetail = userService.findUserDetail(userId);
		if(userDetail == null || userDetail.getMobile() == null) {
			return serviceError();
		}
		
		QuestionnaireUser qu = new QuestionnaireUser();
		qu.setUserId(userId);
		qu.setQuestionnaireId(param.getQuestionnaireId());
		qu.setMobile(userDetail.getMobile().toString());
		qu.setNickName(userDetail.getNickName());
		
		try {
			questionnaireUserMapper.insertSelective(qu);
		} catch (Exception e) {
		}
		return normalSuccess();
	}

	@Override
	public FindQuestionResult findQuestion(FindQuestionParam p) {
		FindQuestionResult result = new FindQuestionResult();
		if(p.getId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		Question question = questionMapper.findOne(p.getId());
		if(question == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		QuestionVO vo = buildQuestionVO(question);
		
		result.setQuestion(vo);
		result.setIsSuccess();
		
		return result;
	}
	
	private QuestionVO buildQuestionVO(Question q) {
		QuestionVO vo = new QuestionVO();
		if(q == null) {
			return vo;
		}
		Integer collectIndex = new Random().nextInt(4) + 1;
		vo.setId(q.getId());
		vo.setQuestion(q.getQuestion());
		if(collectIndex == 1) {
			vo.setCollectOption(1);
			vo.setOption1(q.getOption1());
			vo.setOption2(q.getOption2());
			vo.setOption3(q.getOption3());
			vo.setOption4(q.getOption4());
		} else if(collectIndex == 2) {
			vo.setCollectOption(2);
			vo.setOption2(q.getOption1());
			vo.setOption1(q.getOption2());
			vo.setOption3(q.getOption3());
			vo.setOption4(q.getOption4());
		} else if(collectIndex == 3) {
			vo.setCollectOption(3);
			vo.setOption3(q.getOption1());
			vo.setOption2(q.getOption2());
			vo.setOption1(q.getOption3());
			vo.setOption4(q.getOption4());
		} else {
			vo.setCollectOption(4);
			vo.setOption4(q.getOption1());
			vo.setOption2(q.getOption2());
			vo.setOption3(q.getOption3());
			vo.setOption1(q.getOption4());
		}
		return vo;
	}

	@Override
	public FindQuestionnaireResult findQuestionnaire() {
		FindQuestionnaireResult result = new FindQuestionnaireResult();
		List<Questionnaire> poList = questionnaireMapper.findList();
		List<QuestionnaireVO> voList = new ArrayList<QuestionnaireVO>();
		QuestionnaireVO tmpVO = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		for(Questionnaire p : poList) {
			tmpVO = new QuestionnaireVO();
			tmpVO.setCreateTime(p.getCreateTime());
			tmpVO.setCreateTimeStr(sdf.format(p.getCreateTime()));
			tmpVO.setId(p.getId());
			tmpVO.setIsDelete(p.getIsDelete());
			tmpVO.setQuestionnaireName(p.getQuestionnaireName());
			voList.add(tmpVO);
		}
		result.setList(voList);
		result.setIsSuccess();
		return result;
	}
	
	@Override
	public FindQuestionnaireUserByQuestionnaireIdResult findQuestionnaireUserByQuestionnaireId(FindQuestionnaireUserByQuestionnaireIdParam p) {
		FindQuestionnaireUserByQuestionnaireIdResult result = new FindQuestionnaireUserByQuestionnaireIdResult();
		if(p.getId() == null) {
			return result;
		}
		
		FindQuestionnaireUserByQuestionnaireId mapperParam = new FindQuestionnaireUserByQuestionnaireId();
		PageParam pp = setPageFromPageNo(p.getPageNo(), p.getPageSize());
//		mapperParam.setPageSize(pp.getPageSize());
		mapperParam.setPageSize(99999);
		mapperParam.setPageStart(pp.getPageStart());
		mapperParam.setId(p.getId());
		
		List<QuestionnaireUser> poList = questionnaireUserMapper.findByQuestionnaireId(mapperParam);
		Integer resultCount = questionnaireUserMapper.countByQuestionnaireId(mapperParam);
		
		List<QuestionnaireUserVO> voList = new ArrayList<QuestionnaireUserVO>();
		QuestionnaireUserVO tmpVO = new QuestionnaireUserVO();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		for(QuestionnaireUser po : poList) {
			tmpVO = new QuestionnaireUserVO();
			tmpVO.setCreateTime(po.getCreateTime());
			tmpVO.setCreateTimeStr(sdf.format(po.getCreateTime()));
			tmpVO.setIsDelete(po.getIsDelete());
			tmpVO.setMobile(po.getMobile());
			tmpVO.setNickName(po.getNickName());
			tmpVO.setQuestionnaireId(po.getQuestionnaireId());
			tmpVO.setUserId(po.getUserId());
			voList.add(tmpVO);
		}
		
		result.setResultCount(resultCount);
		result.setList(voList);
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult deleteQuestion(DeleteQuestionParam p) {
		if(p.getId() == null) {
			return nullParam();
		}
		
		questionMapper.deleteById(p.getId());
		return normalSuccess();
	}
	
	@Override
	public CommonResult deleteQuestionnaire(DeleteQuestionnaireParam p) {
		if(p.getId() == null) {
			return nullParam();
		}
		
		questionnaireMapper.deleteById(p.getId());
		questionnaireQuestionMapper.deleteByQuestionnaireId(p.getId());
		questionnaireUserMapper.deleteByQuestionnaireId(p.getId());
		return normalSuccess();
	}
}