package demo.popQuiz.domain.result;

import demo.common.domain.result.CommonResult;
import demo.popQuiz.domain.vo.QuestionVO;

public class FindQuestionResult extends CommonResult {

	private QuestionVO question;

	public QuestionVO getQuestion() {
		return question;
	}

	public void setQuestion(QuestionVO question) {
		this.question = question;
	}

}
