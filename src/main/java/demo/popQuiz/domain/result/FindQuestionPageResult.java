package demo.popQuiz.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.popQuiz.domain.po.Question;
import demo.popQuiz.domain.vo.QuestionVO;

public class FindQuestionPageResult extends CommonResult {

	private List<Question> questions;
	private List<QuestionVO> questionList;
	private Integer resultCount;

	public List<QuestionVO> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<QuestionVO> questionList) {
		this.questionList = questionList;
	}

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

}
