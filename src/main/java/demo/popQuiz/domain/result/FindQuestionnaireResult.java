package demo.popQuiz.domain.result;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.popQuiz.domain.vo.QuestionnaireVO;

public class FindQuestionnaireResult extends CommonResult {

	private List<QuestionnaireVO> list;

	public List<QuestionnaireVO> getList() {
		return list;
	}

	public void setList(List<QuestionnaireVO> list) {
		this.list = list;
	}

}
