package demo.popQuiz.domain.constant;

public class PopQuizUrl {

	public static final String root = "/popQuiz";
	
	public static final String addNewQuestion = "/addNewQuestion";
	public static final String buildQuestionnaire = "/buildQuestionnaire";
	public static final String findQuestionPage = "/findQuestionPage";
	public static final String addNewCollectUser = "/addNewCollectUser";
	public static final String findQuestion = "/findQuestion";
	public static final String findQuestionnaire = "/findQuestionnaire";
	public static final String findQuestionnaireUserByQuestionnaireId = "/findQuestionnaireUserByQuestionnaireId";
	
	public static final String findQuestionnaireListForApp = "/findQuestionnaireListForApp";
	
	public static final String deleteQuestionnaire = "/deleteQuestionnaire";
	public static final String deleteQuestion = "/deleteQuestion";
}
