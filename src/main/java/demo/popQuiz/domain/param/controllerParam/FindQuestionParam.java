package demo.popQuiz.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindQuestionParam implements CommonControllerParam {
	@Override
	public FindQuestionParam fromJson(JSONObject j) {
		FindQuestionParam p = new FindQuestionParam();
		if (j.containsKey("id") && NumericUtilCustom.matchInteger(j.getString("id"))) {
			p.setId(j.getLong("id"));
		}
		return p;
	}

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
