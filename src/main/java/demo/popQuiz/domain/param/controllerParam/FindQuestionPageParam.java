package demo.popQuiz.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindQuestionPageParam implements CommonControllerParam {

	private Integer pageNo = 1;
	private Integer pageSize = 50;
	private Long questionnaireId;

	@Override
	public FindQuestionPageParam fromJson(JSONObject j) {
		FindQuestionPageParam p = new FindQuestionPageParam();
		if (j.containsKey("pageNo") && NumericUtilCustom.matchInteger(j.getString("pageNo"))) {
			p.setPageNo(j.getInt("pageNo"));
		}
		if (j.containsKey("questionnaireId") && NumericUtilCustom.matchInteger(j.getString("questionnaireId"))) {
			p.setQuestionnaireId(j.getLong("questionnaireId"));
		}
		return p;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Long getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Long questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

}
