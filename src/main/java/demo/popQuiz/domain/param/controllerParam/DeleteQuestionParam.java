package demo.popQuiz.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class DeleteQuestionParam implements CommonControllerParam {

	private Long id;

	@Override
	public DeleteQuestionParam fromJson(JSONObject j) {
		DeleteQuestionParam p = new DeleteQuestionParam();
		if (j.containsKey("id") && NumericUtilCustom.matchInteger(j.getString("id"))) {
			p.setId(j.getLong("id"));
		}
		return p;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeleteQuestionParam [id=" + id + "]";
	}

}
