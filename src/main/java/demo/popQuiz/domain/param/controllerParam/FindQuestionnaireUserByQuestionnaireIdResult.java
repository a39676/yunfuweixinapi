package demo.popQuiz.domain.param.controllerParam;

import java.util.List;

import demo.common.domain.result.CommonResult;
import demo.popQuiz.domain.vo.QuestionnaireUserVO;

public class FindQuestionnaireUserByQuestionnaireIdResult extends CommonResult {

	private List<QuestionnaireUserVO> list;

	private Integer resultCount = 0;

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	public List<QuestionnaireUserVO> getList() {
		return list;
	}

	public void setList(List<QuestionnaireUserVO> list) {
		this.list = list;
	}

}
