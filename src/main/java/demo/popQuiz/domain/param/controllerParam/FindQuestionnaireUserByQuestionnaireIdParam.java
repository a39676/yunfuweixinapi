package demo.popQuiz.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindQuestionnaireUserByQuestionnaireIdParam implements CommonControllerParam {
	@Override
	public FindQuestionnaireUserByQuestionnaireIdParam fromJson(JSONObject j) {
		FindQuestionnaireUserByQuestionnaireIdParam p = new FindQuestionnaireUserByQuestionnaireIdParam();
		if (j.containsKey("id") && NumericUtilCustom.matchInteger(j.getString("id"))) {
			p.setId(j.getLong("id"));
		}
		if (j.containsKey("pageSize") && NumericUtilCustom.matchInteger(j.getString("pageSize"))) {
			p.setPageSize(j.getInt("pageSize"));
		}
		if (j.containsKey("pageNo") && NumericUtilCustom.matchInteger(j.getString("pageNo"))) {
			p.setPageNo(j.getInt("pageNo"));
		}
		return p;
	}

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Integer pageNo = 1;
	private Integer pageSize = 10;

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
