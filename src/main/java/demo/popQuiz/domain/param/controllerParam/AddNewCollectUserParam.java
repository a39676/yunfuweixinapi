package demo.popQuiz.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class AddNewCollectUserParam implements CommonControllerParam {

	private Long questionnaireId;

	public Long getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Long questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	@Override
	public AddNewCollectUserParam fromJson(JSONObject j) {
		AddNewCollectUserParam p = new AddNewCollectUserParam();
		if(j.containsKey("questionnaireId") && NumericUtilCustom.matchInteger(j.getString("questionnaireId"))) {
			p.setQuestionnaireId(j.getLong("questionnaireId"));
		}
		return p;
	}

}
