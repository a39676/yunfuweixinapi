package demo.popQuiz.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class DeleteQuestionnaireParam implements CommonControllerParam {

	private Long id;

	@Override
	public DeleteQuestionnaireParam fromJson(JSONObject j) {
		DeleteQuestionnaireParam p = new DeleteQuestionnaireParam();
		if (j.containsKey("id") && NumericUtilCustom.matchInteger(j.getString("id"))) {
			p.setId(j.getLong("id"));
		}
		return p;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeleteQuestionParam [id=" + id + "]";
	}

}
