package demo.popQuiz.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class BuildQuestionnaireParam implements CommonControllerParam {

	private Long questionnaireId;
	private String questionnaireName;
	private String questionIds;

	@Override
	public BuildQuestionnaireParam fromJson(JSONObject j) {
		BuildQuestionnaireParam p = new BuildQuestionnaireParam();
		p.setQuestionnaireName(j.getString("questionnaireName"));
		p.setQuestionIds(j.getString("questionIds"));
		if (j.containsKey("questionnaireId") && NumericUtilCustom.matchInteger(j.getString("questionnaireId"))) {
			p.setQuestionnaireId(j.getLong("questionnaireId"));
		}
		return p;
	}

	public Long getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Long questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public String getQuestionnaireName() {
		return questionnaireName;
	}

	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	}

	public String getQuestionIds() {
		return questionIds;
	}

	public void setQuestionIds(String questionIds) {
		this.questionIds = questionIds;
	}

	@Override
	public String toString() {
		return "BuildQuestionnaireParam [questionnaireName=" + questionnaireName + ", questionIds=" + questionIds + "]";
	}

}
