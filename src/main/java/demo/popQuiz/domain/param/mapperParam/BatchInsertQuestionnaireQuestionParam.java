package demo.popQuiz.domain.param.mapperParam;

import java.util.List;

public class BatchInsertQuestionnaireQuestionParam {
	
	private Long questionnaireId;
	private List<Long> quesetionIdList;

	@Override
	public String toString() {
		return "BatchInsertQuestionnaireQuestionParam [questionnaireId=" + questionnaireId + "]";
	}

	public Long getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Long questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public List<Long> getQuesetionIdList() {
		return quesetionIdList;
	}

	public void setQuesetionIdList(List<Long> quesetionIdList) {
		this.quesetionIdList = quesetionIdList;
	}
	
	

}
