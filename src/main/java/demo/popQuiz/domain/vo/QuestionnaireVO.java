package demo.popQuiz.domain.vo;

import demo.popQuiz.domain.po.Questionnaire;

public class QuestionnaireVO extends Questionnaire {

	private String createTimeStr;

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

}
