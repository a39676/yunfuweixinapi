package demo.popQuiz.domain.vo;

import java.util.Date;

public class QuestionVO {
	private Long id;
	private Date createTime;
	private Boolean isDelete;
	private String question;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	private Integer collectOption;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question == null ? null : question.trim();
	}

	public String getOption4() {
		return option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4 == null ? null : option4.trim();
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2 == null ? null : option2.trim();
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3 == null ? null : option3.trim();
	}

	public Integer getCollectOption() {
		return collectOption;
	}

	public void setCollectOption(Integer collectOption) {
		this.collectOption = collectOption;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

}