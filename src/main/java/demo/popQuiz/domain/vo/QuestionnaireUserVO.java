package demo.popQuiz.domain.vo;

import demo.popQuiz.domain.po.QuestionnaireUser;

public class QuestionnaireUserVO extends QuestionnaireUser {

	private String createTimeStr;

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

}