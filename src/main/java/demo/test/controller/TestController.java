package demo.test.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import demo.common.controller.CommonController;
import demo.organizations.domain.param.controllerParam.DeleteOrgParam;
import demo.organizations.service.OrganizationsService;
import demo.util.BaseUtilCustom;
import demo.weixin.service.WeixinService;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = { "/test" })
public class TestController extends CommonController {

	// @Autowired
	// private TestMapper testMapper;
	@Autowired
	private BaseUtilCustom baseUtilCustom;
	@Autowired
	private OrganizationsService orgService;

	@GetMapping(value = { "/test" })
	public ModelAndView test(HttpServletRequest request) {
		ModelAndView view = new ModelAndView("jsp/testJSP/test");
		HashMap<String, Object> detail = baseUtilCustom.getAuthDetail();
		String code = null;
		if (detail != null) {
			code = String.valueOf(detail.get("code"));
		}
		view.addObject("message", code);
		return view;
	}

	@GetMapping(value = { "/testHtml" })
	public ModelAndView testHtml(HttpServletRequest request, HttpServletResponse response) throws IOException {
//		StringBuffer str = new StringBuffer();
//
//		Resource resource = new ClassPathResource("weixinHtml/sign/sign.html");
//		File file = resource.getFile();
//		FileUtilCustom io = new FileUtilCustom();
//		str.append(io.getStringFromFile(file.getAbsolutePath()));
//
//		return str.toString();
		return new ModelAndView(new RedirectView("/weixinHtml/sign/sign.html"));
	}

	@GetMapping(value = { "/test2" })
	public ModelAndView test2(HttpServletRequest request) {
		ModelAndView view = new ModelAndView("jsp/application/application");
		return view;
	}

	@PostMapping(value = { "/testJson" })
	public void testJson(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = JSONObject.fromObject("{\"someKey\":\"someValue\"}");
		outputJson(response, JSONObject.fromObject(json));
	}

	@GetMapping(value = { "/testException" })
	public ModelAndView testException(HttpServletRequest request) throws Exception {
		logger.debug("dateTime: " + new Date());
		throw new Exception();
	}

	@Autowired
	private WeixinService wxService;

	@GetMapping(value = { "/wxPush" })
	public void findNewValidCode() throws Exception {
		wxService.sendReservationApplication("oxWyP1iTqBojATZDQbebyR9zVd4E", "", "测试机构名", "", "预约时间", "", true);
	}

	// @GetMapping(value = "/testIgnoring")
	// public ModelAndView testIgnoring() {
	// return new ModelAndView(TestViewConstants.test01);
	// }
	
	@GetMapping(value = "/tmpTest")
	public void tmpTest() {
		DeleteOrgParam p = new DeleteOrgParam();
		p.setOrgId(1068L);
		orgService.deleteOrg(p);
	}

}
