package demo.config.costom_component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import demo.article.domain.constant.ArticleChannelDefault;
import demo.article.domain.po.ArticleChannels;
import demo.article.mapper.ArticleChannelsMapper;
import demo.base.base.domain.constant.RoleGroupDefault;
import demo.base.base.domain.param.mapperParam.RoleGroupsBatchInsertParam;
import demo.base.base.domain.po.Roles;
import demo.base.base.domain.type.RolesType;
import demo.base.base.mapper.RoleGroupsPOMapper;
import demo.base.base.mapper.RolesMapper;
import demo.base.user.domain.constant.UserDefault;
import demo.base.user.domain.param.controllerParam.ManagerRegistParam;
import demo.base.user.domain.po.Users;
import demo.base.user.mapper.UsersMapper;
import demo.base.user.service.UsersService;
import demo.geographical.domain.po.GeographicalAreaPO;
import demo.geographical.domain.type.GeographicalArea;
import demo.geographical.mapper.GeographicalAreaMapper;
import demo.organizations.domain.constant.AuthsDefault;
import demo.organizations.domain.constant.OrgDefault;
import demo.organizations.domain.param.controllerParam.CreateNewOrgParam;
import demo.organizations.domain.param.mapperParam.InitDefaultAuthsParam;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.type.OrganizationsType;
import demo.organizations.mapper.AuthsPOMapper;
import demo.organizations.mapper.OrganizationsPOMapper;
import demo.organizations.service.AuthsService;
import demo.organizations.service.OrganizationsService;

@Component
public class DatabaseFillerOnStartup implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private RolesMapper rolesMapper;
	@Autowired
	private RoleGroupsPOMapper roleGroupMapper;
	@Autowired
	private AuthsPOMapper authMapper;
	@Autowired
	private UsersMapper userMapper;
	@Autowired
	private OrganizationsPOMapper orgMapper;
	@Autowired
	private ArticleChannelsMapper articleChannelMapper;
	@Autowired
	private GeographicalAreaMapper geoMapper;
	@Autowired
	private UsersService userService;
	@Autowired
	private OrganizationsService orgService;
	@Autowired
	private AuthsService authsService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getDisplayName().equals("Root WebApplicationContext")) {
			initDefaultGeographicalArea();
			initDefaultRole();
			initDefaultRoleGroup();
			initDefaultAuths();
			initUnion();
			initDefaultArticleChannels();
		}
	}

	private void initDefaultRole() {
		Roles newRole = null;
		for (RolesType type : RolesType.values()) {
			newRole = new Roles();
			newRole.setRole(type.getRoleName());
			newRole.setRoleId(type.getId());
			newRole.setIsDelete(false);
			rolesMapper.insertOrUpdate(newRole);
		}
	}

	private void initDefaultRoleGroup() {
		roleGroupMapper.deleteByGroupId(RoleGroupDefault.ROLE_GROUP_BRANCH_ADMIN_ID);
		roleGroupMapper.deleteByGroupId(RoleGroupDefault.ROLE_GROUP_BUSINESS_HANDLER_ID);
		roleGroupMapper.deleteByGroupId(RoleGroupDefault.ROLE_GROUP_BRANCH_HANDLER_ID);
		roleGroupMapper.deleteByGroupId(RoleGroupDefault.ROLE_GROUP_UNION_ID);
		RoleGroupsBatchInsertParam roleGroupParam = new RoleGroupsBatchInsertParam();
		roleGroupParam.setRoleGroupList(RoleGroupDefault.roleGroupList);
		roleGroupMapper.batchInsert(roleGroupParam);
	}

	private void initDefaultAuths() {
		InitDefaultAuthsParam p = new InitDefaultAuthsParam();
		p.setAuthList(AuthsDefault.authsList);
		authMapper.initDefaultAuths(p);
	}

	private void initUnion() {
		OrganizationsPO orgUnion = orgMapper.findOrg(OrgDefault.orgUnionId);
		if (orgUnion == null) {
			CreateNewOrgParam createOrgParam = new CreateNewOrgParam();
			createOrgParam.setOrgType(OrganizationsType.union.getCode());
			createOrgParam.setOrgCode(OrgDefault.orgUnionId.toString());
			createOrgParam.setOrgName(OrgDefault.orgUnionName);
			createOrgParam.setTopHeadOrg(OrgDefault.orgUnionId);
			createOrgParam.setBelongTo(OrgDefault.orgUnionId);
			createOrgParam.setGeogralphicalId(GeographicalArea.g1.getAreaId());
			orgService.createNewOrg(createOrgParam, OrgDefault.orgUnionId);
			orgUnion = orgMapper.findOrg(OrgDefault.orgUnionId);
		}

		Users su = userMapper.findUserByUserName(UserDefault.sUserName);
		if (su == null) {
			ManagerRegistParam param = new ManagerRegistParam();
			param.setOrgId(OrgDefault.orgUnionId);
			param.setRouleGroupId(RoleGroupDefault.ROLE_GROUP_UNION_ID);
			param.setMobile("13800138000");
			param.setGender(1);
			param.setNickName(UserDefault.sUserName);
			param.setPwd("012345678");
			param.setPwdRepeat("012345678");
			param.setUserName(UserDefault.sUserName);
			userService.newManagerRegist(param, "0");
			su = userMapper.findUserByUserName(UserDefault.sUserName);
		}

		authsService.copyAuth(OrgDefault.orgUnionId, AuthsDefault.AUTH_BRANCH_ADMIN_ID);
	}

	private void initDefaultArticleChannels() {
		for (ArticleChannels channel : ArticleChannelDefault.defaultChannels) {
			channel.setIsDelete(false);
			articleChannelMapper.insertOrUpdate(channel);
		}
	}

	private void initDefaultGeographicalArea() {
		GeographicalAreaPO newGeo = null;
		for (GeographicalArea type : GeographicalArea.values()) {
			newGeo = new GeographicalAreaPO();
			newGeo.setId(type.getAreaId());
			// newGeo.setAreaName(StringEscapeUtils.escapeHtml(type.getAreaName()));
			newGeo.setAreaName(type.getAreaName());
			newGeo.setAreaType(type.getAreaType());
			newGeo.setBelongTo(type.getBelongTo());
			geoMapper.insertOrUpdate(newGeo);
		}
	}
}