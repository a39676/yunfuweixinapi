package demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import demo.base.constant.RoleBean;
import demo.util.BaseUtilCustom;

@EnableWebMvc // <mvc:annotation-driven />
@Configuration
@EnableScheduling // 开启定时任务支持
@ComponentScan({ "demo.controller", "demo.web.handler", "demo.*.controller", "demo.base.*.controller",
		"demo.config.costom_component" })
// @ComponentScan({"demo.service", "demo.*.service", "demo.base.*.service"})
public class SpringMvcConfig extends WebMvcConfigurerAdapter {

	// 添加 filter 需转至 SecurityConfig

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static_resources/**").addResourceLocations("classpath:/static_resources/");
		registry.addResourceHandler("/admin_resources/**").addResourceLocations("classpath:/static_resources/admin_resources/");
		registry.addResourceHandler("/index.html").addResourceLocations("classpath:/static_resources/index.html");
		registry.addResourceHandler("/login.html").addResourceLocations("classpath:/static_resources/login.html");
		registry.addResourceHandler("/weixinHtml/**").addResourceLocations("classpath:/weixinHtml/");
		registry.addResourceHandler("/view/**").addResourceLocations("classpath:/static_resources/view/");
		registry.addResourceHandler("/view/view/**").addResourceLocations("classpath:/static_resources/view/");
		registry.addResourceHandler("/view/admin_resources/**").addResourceLocations("classpath:/static_resources/admin_resources/");
		registry.addResourceHandler("/MP_verify_rQHLtzQw6LZznCcT.txt").addResourceLocations("classpath:/static_resources/mp/MP_verify_rQHLtzQw6LZznCcT.txt");
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver createMultiparResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setDefaultEncoding("utf-8");
		resolver.setMaxUploadSize(5L * 1024 * 1024); // 5m
		resolver.setMaxInMemorySize(40960);

		return resolver;
	}

	// mark
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
//			.allowedHeaders("Authorization", "Cache-Control", "Content-Type", "OPTIONS", "X-CSRF-TOKEN", "X-XSRF-TOKEN")
//			.allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH")
			.allowedHeaders("*")
			.allowedMethods("*")
			.allowedOrigins("*")
			.allowCredentials(false)
			;
	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(1);
		return viewResolver;
	}

	@Bean
	public RoleBean getRoleBean() {
		return new RoleBean();
	}

	@Bean
	public BaseUtilCustom getBaseUtilCustom() {
		return new BaseUtilCustom();
	}

}
