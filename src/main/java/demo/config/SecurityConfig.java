package demo.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.CharacterEncodingFilter;

import demo.base.constant.UrlConstant;
import demo.base.user.domain.constant.UsersUrlConstant;
import demo.base.user.service.impl.CustomAuthenticationFailHandler;
import demo.base.user.service.impl.CustomAuthenticationSuccessHandler;
import demo.base.user.service.impl.CustomUserDetailsService;
import demo.config.costom_component.CustomPasswordEncoder;
import demo.web.handler.LimitLoginAuthenticationProvider;
import demo.weixin.domain.constant.WXUrl;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

	@Autowired
	private CustomAuthenticationFailHandler customAuthenticationFailHandler;

	@Autowired
	@Qualifier("authenticationProvider")
	private AuthenticationProvider authenticationProvider;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider);
	}

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Bean
	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new LimitLoginAuthenticationProvider();
		authProvider.setUserDetailsService(customUserDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
			.authorizeRequests()
			.antMatchers("/welcome**").permitAll()
			.antMatchers(UrlConstant.login + "/**").permitAll()
			.antMatchers(UsersUrlConstant.root + "/**").permitAll()
			.antMatchers(WXUrl.root + "/**").permitAll()
			.antMatchers("/_csrf/**").permitAll()
			.antMatchers("/view/**").permitAll()
			.antMatchers("/static_resources/**").permitAll()
			.antMatchers("/admin/**").access("hasAnyRole('ROLE_ORG_MANAGE', 'ROLE_SUPER_ADMIN')")
			.antMatchers("/tool/**").access("hasRole('ROLE_SUPER_ADMIN')")
//			.antMatchers("/reservation/**").access("hasRole('ROLE_WXUSER')")
//			.antMatchers("/test/**").access("hasRole('ROLE_SUPER_ADMIN')")
			
			.and().formLogin()
				.loginPage("/login/login").failureUrl("/login/login?error").loginProcessingUrl("/auth/login_check")
				.successHandler(customAuthenticationSuccessHandler).failureHandler(customAuthenticationFailHandler)
				.usernameParameter("user_name").passwordParameter("pwd")
			.and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/login/logout"))
			.and().exceptionHandling()
				.accessDeniedPage("/403")
			.and().csrf()
				.disable()
				.cors()
			.and()
				.headers().frameOptions().disable()
			;

		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		encodingFilter.setForceEncoding(true);
		http.addFilterBefore(encodingFilter, CsrfFilter.class);
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/test/testIgnoring");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider());
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
		db.setDataSource(dataSource);
		return db;
	}

	@Bean
	public CustomPasswordEncoder passwordEncoder() {
		return new CustomPasswordEncoder();
	}

}
