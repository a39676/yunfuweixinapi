package demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ 
	SpringMvcConfig.class, 
	SecurityConfig.class, 
	MybatisConfig.class 
})
// 如果需要从 xml 文档导入配置则使用此注解
//@ImportResource("classpath:/xmlConfig/spring_mybatis.xml")
public class RootConfig {
	
	
	
}