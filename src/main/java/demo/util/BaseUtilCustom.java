package demo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import demo.base.base.domain.type.RolesType;

public class BaseUtilCustom {
	
	public UserDetails getCurrentUser() {
		if(isLoginUser()) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(auth != null && auth.getPrincipal() != null) {
				return (UserDetails) auth.getPrincipal();
			}
		}
		return null;
	}
	
	public String getCurrentUserName() {
		UserDetails userDetails = getCurrentUser();
		if(userDetails != null) {
			return userDetails.getUsername();
		} else {
			return null;
		}
	}
	
	public boolean isLoginUser() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		if(securityContext == null) {
			return false;
		}
		Authentication authentication = securityContext.getAuthentication();
		if(authentication == null || authentication instanceof AnonymousAuthenticationToken) {
			return false;
		}
		return true;
//		return !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken);
	}
	
	public List<String> getRoles() {
		List<String> roleList = new ArrayList<String>();
//		mark
		if(isLoginUser()) {
			
			if(getCurrentUser() == null) {
				return roleList;
			}
			
			Collection<? extends GrantedAuthority> userDetails = getCurrentUser().getAuthorities();
			if(userDetails != null) {
				for(Object ele : getCurrentUser().getAuthorities()) {
					roleList.add(String.valueOf(ele));
				}
			}
		}
//		List<RolesType> roles = Arrays.asList(RolesType.values());
//		roles.stream().forEach(r -> roleList.add(r.getRoleName()));
		
		return roleList;
	}
	
	public boolean hasAdminRole() {
		return hasThisRole(RolesType.ROLE_ADMIN.getRoleName());
	}
	
	public boolean hasThisRole(String roleName) {
		if(StringUtils.isBlank(roleName)) {
			return false;
		}
		
		List<String> roleList = getRoles();
		
		if(roleList == null || roleList.size() < 1 ) {
			return false;
		}
		
		for(int i = 0; i < roleList.size(); i++) {
			if(String.valueOf(roleList.get(i)).equals(roleName)) {
				return true;
			}
		}
		
		return false;
	}

	public boolean setAuthDetail(HashMap<String, Object> detailMap) {
		try {
			AbstractAuthenticationToken auth = (AbstractAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
			auth.setDetails(detailMap);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean modifyAuthDetail(String key, Object obj) {
		try {
			AbstractAuthenticationToken auth = (AbstractAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
			HashMap<String, Object> authDetailMap = getAuthDetail();
			authDetailMap.put(key, obj);
			auth.setDetails(authDetailMap);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean removeAushDetailAttrByKey(String key) {
		try {
			AbstractAuthenticationToken auth = (AbstractAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
			HashMap<String, Object> details = getAuthDetail();
			if(details.isEmpty() || !details.containsKey(key)) {
				return false;
			}
			auth.setDetails(details.remove(key));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> getAuthDetail() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		if(securityContext == null || securityContext.getAuthentication() == null || securityContext.getAuthentication().getDetails() == null) {
			return new HashMap<String, Object>();
		}
		Object obj = SecurityContextHolder.getContext().getAuthentication().getDetails();
		if(obj instanceof HashMap) {
			return (HashMap<String, Object>) obj;
		} else {
			return new HashMap<String, Object>();
		}
	}
	
	public Long getUserId() {
//		mark
		Object userId = getAuthDetail().get("userId");
		if(userId != null && userId instanceof Long) {
			return (Long) userId;
		} else {
			return null;
		}
//		return 1001L;
	}
}
