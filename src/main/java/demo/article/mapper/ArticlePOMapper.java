package demo.article.mapper;

import java.util.List;

import demo.article.domain.bo.ArticleBO;
import demo.article.domain.param.controllerParam.FindArticleByIdParam;
import demo.article.domain.param.controllerParam.FindArticleByProductTypeProductIdParam;
import demo.article.domain.param.mapperParam.FindArticleTitleByChannelParam;
import demo.article.domain.po.ArticlePO;

public interface ArticlePOMapper {
    int insert(ArticlePO record);

    int insertSelective(ArticlePO record);
    
    ArticleBO findArticleBOByProductId(FindArticleByProductTypeProductIdParam param);
    
    List<ArticleBO> findArticlePageByChannel(FindArticleTitleByChannelParam mapperParam);
    Integer countArticlePageByChannel(FindArticleTitleByChannelParam mapperParam);
    
    ArticlePO findByProductId(Long productId);
    
    ArticlePO findById(Long id);
    
    int updateInfo(ArticlePO po);
    
    int deleteById(Long id);
    
    ArticleBO findArticleBOById(FindArticleByIdParam param);
}