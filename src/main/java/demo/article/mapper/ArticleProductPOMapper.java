package demo.article.mapper;

import demo.article.domain.po.ArticleProductPO;

public interface ArticleProductPOMapper {
    int insert(ArticleProductPO record);

    int insertSelective(ArticleProductPO record);
}