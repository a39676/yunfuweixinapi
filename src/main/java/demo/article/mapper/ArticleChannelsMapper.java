package demo.article.mapper;

import java.util.List;

import demo.article.domain.po.ArticleChannels;

public interface ArticleChannelsMapper {
	int insert(ArticleChannels record);

	int insertSelective(ArticleChannels record);

	int insertOrUpdate(ArticleChannels channel);

	List<ArticleChannels> findChannelsByType(Integer channelType);
	
	int deleteChannel(Integer id);
	
	Integer existsChannelName(String channelName);
}