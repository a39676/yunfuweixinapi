package demo.article.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.article.domain.param.controllerParam.AddArticleChannelParam;
import demo.article.domain.param.controllerParam.DeleteArticleChannelParam;
import demo.article.domain.param.controllerParam.FindArticleChannelListParam;
import demo.article.domain.po.ArticleChannels;
import demo.article.domain.result.AddArticleChannelResult;
import demo.article.domain.result.FindArticleChannelListResult;
import demo.article.domain.type.ArticleChannelType;
import demo.article.domain.type.ResultTypeArticle;
import demo.article.mapper.ArticleChannelsMapper;
import demo.article.service.ArticleChannelService;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;

@Service
public class ArticleChannelServiceImpl extends CommonService implements ArticleChannelService {

	@Autowired
	private ArticleChannelsMapper channelMapper;
	
	@Override
	public FindArticleChannelListResult findArticleChannelList(FindArticleChannelListParam controllerParam) {
		FindArticleChannelListResult result = new FindArticleChannelListResult();
		if(controllerParam.getChannelType() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		ArticleChannelType channelType = ArticleChannelType.getType(controllerParam.getChannelType());
		if(channelType == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		List<ArticleChannels> channelList = channelMapper.findChannelsByType(controllerParam.getChannelType());
		result.setChannelList(channelList);
		result.setIsSuccess();
		return result;
	}
	
	@Override
	public FindArticleChannelListResult findFinancialKnowledgeChannelList() {
		FindArticleChannelListParam param = new FindArticleChannelListParam();
		param.setChannelType(ArticleChannelType.financialKnowledge.getCode());
		FindArticleChannelListResult result = findArticleChannelList(param);
		return result;
	}
	
	@Override
	public CommonResult deleteChannel(DeleteArticleChannelParam param) {
		CommonResult result = new CommonResult();
		if(param.getChannelId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		channelMapper.deleteChannel(param.getChannelId());
		result.setIsSuccess();
		return result;
	}
	
	@Override
	public AddArticleChannelResult addArticleChannel(AddArticleChannelParam param) {
		AddArticleChannelResult result = new AddArticleChannelResult();
		if(param.getChannelType() == null && StringUtils.isBlank(param.getChannelName())) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}
		
		ArticleChannelType channelType = ArticleChannelType.getType(param.getChannelType());
		if(channelType == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		
		Integer exists = channelMapper.existsChannelName(param.getChannelName());
		if(exists == null || !exists.equals(0)) {
			result.fillWithResult(ResultTypeArticle.articleChannelExists);
			return result;
		}
		
		ArticleChannels newChannel = new ArticleChannels();
		newChannel.setChannelName(param.getChannelName());
		newChannel.setChannelType(param.getChannelType());
		
		channelMapper.insertSelective(newChannel);
		result.setIsSuccess();
		return result;
	}
}
