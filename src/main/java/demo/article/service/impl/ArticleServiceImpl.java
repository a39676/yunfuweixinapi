package demo.article.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dateHandle.DateUtilCustom;
import demo.article.domain.bo.ArticleBO;
import demo.article.domain.param.controllerParam.CreatingArticleParam;
import demo.article.domain.param.controllerParam.CreatingArticleProductParam;
import demo.article.domain.param.controllerParam.DeleteArticleByIdParam;
import demo.article.domain.param.controllerParam.EditArticleParam;
import demo.article.domain.param.controllerParam.EditArticleProductParam;
import demo.article.domain.param.controllerParam.FindArticleByIdParam;
import demo.article.domain.param.controllerParam.FindArticleByProductTypeProductIdParam;
import demo.article.domain.param.controllerParam.FindArticlePageByChannelParam;
import demo.article.domain.param.mapperParam.AddNewArticleProductParam;
import demo.article.domain.param.mapperParam.FindArticleTitleByChannelParam;
import demo.article.domain.po.ArticlePO;
import demo.article.domain.po.ArticleProductPO;
import demo.article.domain.result.ArticleFileSaveResult;
import demo.article.domain.result.FindArticleLongResult;
import demo.article.domain.result.LoadArticleListResult;
import demo.article.domain.type.ArticleChannelType;
import demo.article.domain.vo.ArticleVO;
import demo.article.mapper.ArticlePOMapper;
import demo.article.mapper.ArticleProductPOMapper;
import demo.article.service.ArticleService;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;
import ioHandle.FileUtilCustom;

@Service
public class ArticleServiceImpl extends CommonService implements ArticleService {

	@Autowired
	private ArticlePOMapper articleMapper;
	@Autowired
	private ArticleProductPOMapper articleProductMapper;

	private static String storePrefixPath;

	static {
		if (isUnix()) {
			storePrefixPath = "/home/u1/articleStore/";
		} else {
			storePrefixPath = "d:/home/test/";
		}
	}

	@Override
	public ArticleFileSaveResult creatingArticle(CreatingArticleParam param) throws IOException {
		ArticleFileSaveResult result = new ArticleFileSaveResult();

		if (StringUtils.isBlank(param.getTitle()) || StringUtils.isBlank(param.getContent())) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		String title = String.valueOf(param.getTitle());

		if (storePrefixPath.length() < 1) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		ArticlePO newArticle = new ArticlePO();

		ArticleFileSaveResult saveArticleResult = saveArticleFile(param.getUserId(), param.getContent());
		if (!saveArticleResult.isSuccess()) {
			return saveArticleResult;
		}

		String filePath = saveArticleResult.getFilePath();

		newArticle.setArticleTitle(title);
		newArticle.setPath(filePath);
		newArticle.setUserId(param.getUserId());
		newArticle.setArticleChannel(param.getArticleChannel());

		articleMapper.insertSelective(newArticle);
		if (newArticle.getArticleId() == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		saveArticleResult.setArticleId(newArticle.getArticleId());

		return saveArticleResult;
	}

	@Override
	public ArticleFileSaveResult editArticle(EditArticleParam param) throws IOException {
		ArticleFileSaveResult result = new ArticleFileSaveResult();

		if (param.getArticleId() == null || StringUtils.isBlank(param.getTitle())
				|| StringUtils.isBlank(param.getContent())) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		if (storePrefixPath.length() < 1) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		ArticlePO article = articleMapper.findById(param.getArticleId());
		if(article == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		
		article.setArticleTitle(param.getTitle());

		ArticleFileSaveResult saveArticleResult = editArticleFile(article.getPath(), param.getContent());
		if (!saveArticleResult.isSuccess()) {
			return saveArticleResult;
		}

		articleMapper.updateInfo(article);

		saveArticleResult.setArticleId(article.getArticleId());

		return saveArticleResult;
	}

	private ArticleFileSaveResult saveArticleFile(Long userId, String content) throws IOException {
		String fileName = UUID.randomUUID().toString().substring(0, 8) + ".txt";
		if (userId != null) {
			fileName = userId.toString() + "L" + fileName;
		}
		String timeFolder = LocalDate.now().toString();
		File mainFolder = new File(storePrefixPath + timeFolder);
		String finalFilePath = storePrefixPath + timeFolder + "/" + fileName;
		ArticleFileSaveResult result = new ArticleFileSaveResult();

		if (!mainFolder.exists()) {
			if (!mainFolder.mkdirs()) {
				result.fillWithResult(ResultTypeBase.serviceError);
				return result;
			}
		}

		FileUtilCustom iou = new FileUtilCustom();
		String articleContentAfterTrim = content.trim();

		iou.byteToFile(articleContentAfterTrim.getBytes("utf8"), finalFilePath);

		result.setFilePath(finalFilePath);

		result.setIsSuccess();
		return result;
	}

	private ArticleFileSaveResult editArticleFile(String filePath, String content) throws UnsupportedEncodingException {
		File articleFile = new File(filePath);
		ArticleFileSaveResult result = new ArticleFileSaveResult();

		if (!articleFile.exists()) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}

		FileUtilCustom iou = new FileUtilCustom();
		String articleContentAfterTrim = content.trim();

		iou.byteToFile(articleContentAfterTrim.getBytes("utf8"), filePath);

		result.setIsSuccess();
		return result;
	}

	@Override
	public FindArticleLongResult findArticleByProductId(FindArticleByProductTypeProductIdParam param) {
		FindArticleLongResult result = new FindArticleLongResult();

		Long productId = param.getProductId();
		if (productId == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		result.setArticleId(productId);

		ArticleBO bo = articleMapper.findArticleBOByProductId(param);
		ArticleVO vo = null;
		if (bo == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			vo = new ArticleVO();
			vo.setContentLines(ResultTypeBase.serviceError.getName());
			result.setArticleLongVO(vo);
			return result;
		}
		vo = fillArticleContent(bo);
		result.setArticleLongVO(vo);
		result.setIsSuccess();
		return result;
	}

	private ArticleVO fillArticleContent(ArticleBO bo) {
		FileUtilCustom iou = new FileUtilCustom();
		String strContent = null;
		try {
			strContent = iou.getStringFromFile(bo.getPath());
		} catch (Exception e) {
			strContent = "";
		}
		List<String> strLines = Arrays.asList(strContent.split("\n"));
		StringBuffer outputLines = new StringBuffer();
		String line = "";
		for (int i = 0; i < strLines.size(); i++) {
			line = strLines.get(i);
			outputLines.append(line);
		}
		ArticleVO vo = new ArticleVO();
		vo.setCreateTime(bo.getCreateTime());
		if(bo.getCreateTime() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			vo.setCreateDateString(sdf.format(bo.getCreateTime()));
		}
		vo.setArticleTitle(bo.getArticleTitle());
		vo.setContentLines(outputLines.toString());

		return vo;
	}

	@Override
	public ArticleFileSaveResult addNewArticleProduct(CreatingArticleProductParam param) throws IOException {
		ArticleFileSaveResult result = new ArticleFileSaveResult();
		if (StringUtils.isBlank(param.getContent()) || param.getProductId() == null || param.getProductType() == null
				|| param.getUserId() == null || StringUtils.isBlank(param.getTitle())) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		CreatingArticleParam articlParam = new CreatingArticleParam();
		articlParam.setContent(param.getContent());
		articlParam.setTitle(param.getTitle());
		articlParam.setUserId(param.getUserId());
		result = creatingArticle(articlParam);
		if (!result.isSuccess()) {
			return result;
		}

		AddNewArticleProductParam relationParam = new AddNewArticleProductParam();
		relationParam.setArticleId(result.getArticleId());
		relationParam.setProductId(param.getProductId());
		relationParam.setProductType(param.getProductType());
		int insertCount = addNewArticleProduct(relationParam);
		if (insertCount < 1) {
			result.setIsFail();
		} else {
			result.setIsSuccess();
		}

		return result;
	}

	@Override
	public ArticleFileSaveResult editArticleProduct(EditArticleProductParam param) throws IOException {
		ArticleFileSaveResult result = new ArticleFileSaveResult();
		if (StringUtils.isBlank(param.getContent()) || param.getProductId() == null
				|| StringUtils.isBlank(param.getTitle())) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}
		ArticlePO article = articleMapper.findByProductId(param.getProductId());
		if (article == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}

		EditArticleParam subParam = new EditArticleParam();
		subParam.setArticleId(article.getArticleId());
		subParam.setTitle(param.getTitle());
		subParam.setContent(param.getContent());
		result = editArticle(subParam);
		return result;
	}
	
	private int addNewArticleProduct(AddNewArticleProductParam param) {
		if (param.getArticleId() == null || param.getProductId() == null || param.getProductType() == null) {
			return 0;
		}
		ArticleProductPO newPo = new ArticleProductPO();
		newPo.setProductId(param.getProductId());
		newPo.setProductType(param.getProductType());
		newPo.setArticleId(param.getArticleId());
		return articleProductMapper.insertSelective(newPo);
	}

	@Override
	public LoadArticleListResult findArticlePageByChannel(FindArticlePageByChannelParam controllerParam) {
		LoadArticleListResult result = new LoadArticleListResult();
		if (controllerParam.getChannelId() == null) {
			result.fillWithResult(ResultTypeBase.nullParam);
			return result;
		}

		ArticleChannelType ct = ArticleChannelType.getType(controllerParam.getChannelId());
		if (ct == null) {
			result.fillWithResult(ResultTypeBase.errorParam);
			return result;
		}

		PageParam pp = setPageFromPageNo(controllerParam.getPageNo(), controllerParam.getPageSize());

		FindArticleTitleByChannelParam mapperParam = new FindArticleTitleByChannelParam();
		if (ct.equals(ArticleChannelType.knowledge)) {
			List<Integer> channelIdList = new ArrayList<Integer>();
			channelIdList.add(ArticleChannelType.financialKnowledge.getCode());
			channelIdList.add(ArticleChannelType.insuranceKnowledge.getCode());
			channelIdList.add(ArticleChannelType.securitieslKnowledge.getCode());
			channelIdList.add(ArticleChannelType.ForeignExchangeKnowledge.getCode());
			channelIdList.add(ArticleChannelType.other.getCode());
			mapperParam.setChannelIdList(channelIdList);
		} else {
			mapperParam.setChannelId(controllerParam.getChannelId());
		}

		if (controllerParam.getPostDate() != null) {
			Date startTime = DateUtilCustom.atStartOfDay(controllerParam.getPostDate());
			Date endTime = DateUtilCustom.atEndOfDay(controllerParam.getPostDate());
			mapperParam.setStartTime(startTime);
			mapperParam.setEndTime(endTime);
		}

		mapperParam.setPageSize(pp.getPageSize());
		mapperParam.setPageStart(pp.getPageStart());
		List<ArticleBO> articleBOList = articleMapper.findArticlePageByChannel(mapperParam);
		Integer resultCount = articleMapper.countArticlePageByChannel(mapperParam);

		List<ArticleVO> articleVOList = new ArrayList<ArticleVO>();
		articleBOList.stream().forEach(b -> articleVOList.add(buildArticleVOFromBO(b)));

		result.setResultCount(resultCount);
		result.setArticleList(articleVOList);
		result.setIsSuccess();
		return result;
	}

	private ArticleVO buildArticleVOFromBO(ArticleBO bo) {
		ArticleVO vo = new ArticleVO();
		if (bo == null) {
			return vo;
		}
		vo.setId(bo.getId());
		vo.setCreateTime(bo.getCreateTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		vo.setCreateDateString(sdf.format(bo.getCreateTime()));
		vo.setArticleTitle(bo.getArticleTitle());
		vo.setArticleChannel(bo.getArticleChannel());
		ArticleChannelType channelType = ArticleChannelType.getType(bo.getArticleChannel());
		if (channelType != null) {
			vo.setChannelName(channelType.getName());
		}

		return vo;
	}
	
	@Override
	public CommonResult deleteById(DeleteArticleByIdParam param) {
		if(param.getId() == null) {
			return nullParam();
		}
		articleMapper.deleteById(param.getId());
		return normalSuccess();
	}
	
	@Override
	public FindArticleLongResult findArticleById(FindArticleByIdParam param) {
		FindArticleLongResult result = new FindArticleLongResult();
		
		result.setArticleId(param.getId());

		ArticleBO bo = articleMapper.findArticleBOById(param);
		ArticleVO vo = null;
		if (bo == null) {
			result.fillWithResult(ResultTypeBase.serviceError);
			vo = new ArticleVO();
			vo.setContentLines(ResultTypeBase.serviceError.getName());
			result.setArticleLongVO(vo);
			return result;
		}
		vo = fillArticleContent(bo);
		result.setArticleLongVO(vo);
		result.setIsSuccess();
		return result;
	}
}