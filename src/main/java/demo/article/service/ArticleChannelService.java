package demo.article.service;

import demo.article.domain.param.controllerParam.AddArticleChannelParam;
import demo.article.domain.param.controllerParam.DeleteArticleChannelParam;
import demo.article.domain.param.controllerParam.FindArticleChannelListParam;
import demo.article.domain.result.AddArticleChannelResult;
import demo.article.domain.result.FindArticleChannelListResult;
import demo.common.domain.result.CommonResult;

public interface ArticleChannelService {

	/** 按频道类型搜索频道列表 */
	FindArticleChannelListResult findArticleChannelList(FindArticleChannelListParam controllerParam);

	/** 查找金融知识类频道列表 */
	FindArticleChannelListResult findFinancialKnowledgeChannelList();

	CommonResult deleteChannel(DeleteArticleChannelParam param);

	AddArticleChannelResult addArticleChannel(AddArticleChannelParam param);

}
