package demo.article.service;

import java.io.IOException;

import demo.article.domain.param.controllerParam.CreatingArticleParam;
import demo.article.domain.param.controllerParam.CreatingArticleProductParam;
import demo.article.domain.param.controllerParam.DeleteArticleByIdParam;
import demo.article.domain.param.controllerParam.EditArticleParam;
import demo.article.domain.param.controllerParam.EditArticleProductParam;
import demo.article.domain.param.controllerParam.FindArticleByIdParam;
import demo.article.domain.param.controllerParam.FindArticleByProductTypeProductIdParam;
import demo.article.domain.param.controllerParam.FindArticlePageByChannelParam;
import demo.article.domain.result.ArticleFileSaveResult;
import demo.article.domain.result.FindArticleLongResult;
import demo.article.domain.result.LoadArticleListResult;
import demo.common.domain.result.CommonResult;

public interface ArticleService {

	/** 创建文章 */
	ArticleFileSaveResult creatingArticle(CreatingArticleParam param) throws IOException;

	/** 产品查找详细说明 */
	FindArticleLongResult findArticleByProductId(FindArticleByProductTypeProductIdParam param);

	/** 保存产品详细说明 */
	ArticleFileSaveResult addNewArticleProduct(CreatingArticleProductParam param) throws IOException;

	/** 根据文章频道加载文章标题列表 */
	LoadArticleListResult findArticlePageByChannel(FindArticlePageByChannelParam controllerParam);

	/** 编辑产品详细说明 */
	ArticleFileSaveResult editArticleProduct(EditArticleProductParam param) throws IOException;

	/** 编辑文章 */
	ArticleFileSaveResult editArticle(EditArticleParam param) throws IOException;

	CommonResult deleteById(DeleteArticleByIdParam param);

	/** 查找文章 */
	FindArticleLongResult findArticleById(FindArticleByIdParam param);

}
