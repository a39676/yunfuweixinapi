package demo.article.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import demo.article.domain.constant.ArticleUrl;
import demo.article.domain.param.controllerParam.CreatingArticleParam;
import demo.article.domain.param.controllerParam.CreatingArticleProductParam;
import demo.article.domain.param.controllerParam.DeleteArticleByIdParam;
import demo.article.domain.param.controllerParam.EditArticleParam;
import demo.article.domain.param.controllerParam.EditArticleProductParam;
import demo.article.domain.param.controllerParam.FindArticleByIdParam;
import demo.article.domain.param.controllerParam.FindArticleByProductTypeProductIdParam;
import demo.article.domain.param.controllerParam.FindArticlePageByChannelParam;
import demo.article.domain.result.ArticleFileSaveResult;
import demo.article.domain.result.FindArticleLongResult;
import demo.article.domain.result.LoadArticleListResult;
import demo.article.service.ArticleService;
import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = ArticleUrl.root)
public class ArticleController extends CommonController {

	@Autowired
	private ArticleService articleService;
	
	@PostMapping(value = ArticleUrl.creatingArticle)
	public void creatingArticle(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws IOException {
		CreatingArticleParam param = new CreatingArticleParam().fromJson(getJson(data));
		CommonResult serviceResult = articleService.creatingArticle(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = ArticleUrl.findArticleByProductId)
	public void findArticleByProductTypeProductId(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindArticleByProductTypeProductIdParam param = new FindArticleByProductTypeProductIdParam().fromJson(getJson(data));
		FindArticleLongResult serviceResult = articleService.findArticleByProductId(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	public ArticleFileSaveResult addNewArticleProduct(CreatingArticleProductParam param) throws IOException {
		return articleService.addNewArticleProduct(param);
	}
	
	public ArticleFileSaveResult editArticleProduct(EditArticleProductParam param) throws IOException {
		return articleService.editArticleProduct(param);
	}
	
	@PostMapping(value = ArticleUrl.findArticlePageByChannel)
	public void findArticlePageByChannel(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindArticlePageByChannelParam param = new FindArticlePageByChannelParam().fromJson(getJson(data));
		LoadArticleListResult serviceResult = articleService.findArticlePageByChannel(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@GetMapping( value = ArticleUrl.channel)
	public ModelAndView findNotice() {
		return new ModelAndView("jsp/notice/notice");
	}

	@PostMapping(value = ArticleUrl.editArticle)
	public void editArticle(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws IOException {
		EditArticleParam param = new EditArticleParam().fromJson(getJson(data));
		ArticleFileSaveResult serviceResult = articleService.editArticle(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
	
	@PostMapping(value = ArticleUrl.deleteArticle)
	public void deleteArticle(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws IOException {
		DeleteArticleByIdParam param = new DeleteArticleByIdParam().fromJson(getJson(data));
		CommonResult serviceResult = articleService.deleteById(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	@PostMapping(value = ArticleUrl.findArticleById)
	public void findArticleById(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindArticleByIdParam param = new FindArticleByIdParam().fromJson(getJson(data));
		FindArticleLongResult serviceResult = articleService.findArticleById(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}
}