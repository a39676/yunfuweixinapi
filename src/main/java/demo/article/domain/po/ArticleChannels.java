package demo.article.domain.po;

import java.util.Date;

import demo.article.domain.type.ArticleChannelType;

public class ArticleChannels {
	private Integer channelId;

	private String channelName;

	/** {@link ArticleChannelType} */
	private Integer channelType;

	private Date createTime;

	private Integer weights;

	private Boolean isDelete;

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName == null ? null : channelName.trim();
	}

	public Integer getChannelType() {
		return channelType;
	}

	public void setChannelType(Integer channelType) {
		this.channelType = channelType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getWeights() {
		return weights;
	}

	public void setWeights(Integer weights) {
		this.weights = weights;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "ArticleChannels [channelId=" + channelId + ", channelName=" + channelName + ", channelType="
				+ channelType + ", createTime=" + createTime + ", weights=" + weights + ", isDelete=" + isDelete + "]";
	}

}