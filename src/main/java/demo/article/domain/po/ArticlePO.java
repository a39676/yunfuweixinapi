package demo.article.domain.po;

import java.util.Date;

public class ArticlePO {
	private Long articleId;

	private Long userId;

	private Integer articleType;

	private String articleTitle;

	private String path;

	private Date createTime;

	private Date editTime;

	private Long editOf;

	private Integer editCount;

	private Boolean isDelete;

	private Integer articleChannel;

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getArticleType() {
		return articleType;
	}

	public void setArticleType(Integer articltType) {
		this.articleType = articltType;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle == null ? null : articleTitle.trim();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path == null ? null : path.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public Long getEditOf() {
		return editOf;
	}

	public void setEditOf(Long editOf) {
		this.editOf = editOf;
	}

	public Integer getEditCount() {
		return editCount;
	}

	public void setEditCount(Integer editCount) {
		this.editCount = editCount;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getArticleChannel() {
		return articleChannel;
	}

	public void setArticleChannel(Integer articleChannel) {
		this.articleChannel = articleChannel;
	}

	@Override
	public String toString() {
		return "ArticlePO [articleId=" + articleId + ", userId=" + userId + ", articleType=" + articleType
				+ ", articleTitle=" + articleTitle + ", path=" + path + ", createTime=" + createTime + ", editTime="
				+ editTime + ", editOf=" + editOf + ", editCount=" + editCount + ", isDelete=" + isDelete
				+ ", articleChannel=" + articleChannel + "]";
	}

}