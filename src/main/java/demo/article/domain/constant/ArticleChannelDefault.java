package demo.article.domain.constant;

import java.util.ArrayList;
import java.util.List;

import demo.article.domain.po.ArticleChannels;

public class ArticleChannelDefault {
	
	public static final int announcementChannelId = 1;
	
	public static final String announcementChannelName = "公告";

	public static final int announcementWeights = 99;
	
	public static final List<ArticleChannels> defaultChannels;
	
	static {
		defaultChannels = new ArrayList<ArticleChannels>();
		ArticleChannels tmpChannels = new ArticleChannels();
		tmpChannels.setChannelId(announcementChannelId);
		tmpChannels.setChannelName(announcementChannelName);
		tmpChannels.setWeights(announcementWeights);
		defaultChannels.add(tmpChannels);
		
	}
}
