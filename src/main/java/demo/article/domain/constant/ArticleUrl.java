package demo.article.domain.constant;

public class ArticleUrl {
	
	public static final String root = "/article";
	
	public static final String creatingArticle = "/creatingArticle";
	public static final String findArticleByProductId = "/findArticleByProductId";
	public static final String findArticlePageByChannel = "/findArticlePageByChannel";
	
	public static final String editArticle = "/editArticle";
	public static final String deleteArticle = "/deleteArticle";
	public static final String findArticleById = "/findArticleById";
	
	public static final String channel = "/channel";
}
