package demo.article.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class EditArticleParam implements CommonControllerParam {

	private Long articleId;
	private String title;
	private String content;

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "EditArticleParam [articleId=" + articleId + ", title=" + title + ", content=" + content + "]";
	}

	@Override
	public EditArticleParam fromJson(JSONObject json) {
		EditArticleParam param = new EditArticleParam();
		if (json.containsKey("articleId") && NumericUtilCustom.matchInteger(json.getString("articleId"))) {
			param.setArticleId(json.getLong("articleId"));
		}
		if (json.containsKey("title")) {
			param.setTitle(json.getString("title"));
		}
		if (json.containsKey("content")) {
			param.setContent(json.getString("content"));
		}
		return param;
	}

}
