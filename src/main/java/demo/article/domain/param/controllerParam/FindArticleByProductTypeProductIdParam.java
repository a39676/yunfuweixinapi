package demo.article.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindArticleByProductTypeProductIdParam implements CommonControllerParam {

	private Integer productType;
	private Long productId;
	private Long articleId;
	private Boolean isDelete = false;

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public FindArticleByProductTypeProductIdParam fromJson(JSONObject json) {
		FindArticleByProductTypeProductIdParam param = new FindArticleByProductTypeProductIdParam();

		if (json.containsKey("productType") && NumericUtilCustom.matchInteger(json.getString("productType"))) {
			param.setProductType(json.getInt("productType"));
		}
		if (json.containsKey("productId") && NumericUtilCustom.matchInteger(json.getString("productId"))) {
			param.setProductId(json.getLong("productId"));
		}
		return param;
	}

}
