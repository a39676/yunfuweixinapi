package demo.article.domain.param.controllerParam;

public class EditArticleProductParam {

	private String title;
	private String content;
	private Long productId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "EditArticleProductParam [title=" + title + ", content=" + content + ", productId=" + productId + "]";
	}

}
