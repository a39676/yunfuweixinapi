package demo.article.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindArticleByIdParam implements CommonControllerParam {

	private Long id;
	private Boolean isDelete = false;

	@Override
	public FindArticleByIdParam fromJson(JSONObject json) {
		FindArticleByIdParam param = new FindArticleByIdParam();

		if (json.containsKey("id") && NumericUtilCustom.matchInteger(json.getString("id"))) {
			param.setId(json.getLong("id"));
		}
		return param;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}
