package demo.article.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class DeleteArticleChannelParam implements CommonControllerParam {

	private Integer channelId;

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	@Override
	public String toString() {
		return "DeleteArticleChannelParam [channelId=" + channelId + "]";
	}

	@Override
	public DeleteArticleChannelParam fromJson(JSONObject json) {
		DeleteArticleChannelParam param = new DeleteArticleChannelParam();
		if (json.containsKey("channelId") && NumericUtilCustom.matchInteger("channelId")) {
			param.setChannelId(json.getInt("channelId"));
		}
		return param;
	}

}
