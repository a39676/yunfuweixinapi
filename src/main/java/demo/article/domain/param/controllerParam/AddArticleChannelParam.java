package demo.article.domain.param.controllerParam;

import demo.article.domain.type.ArticleChannelType;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class AddArticleChannelParam implements CommonControllerParam {

	private Integer channelType = ArticleChannelType.financialKnowledge.getCode();
	private String channelName;

	public Integer getChannelType() {
		return channelType;
	}

	public void setChannelType(Integer channelType) {
		this.channelType = channelType;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	@Override
	public String toString() {
		return "FindArticleChannelListParam [channelType=" + channelType + "]";
	}

	@Override
	public AddArticleChannelParam fromJson(JSONObject json) {
		AddArticleChannelParam param = new AddArticleChannelParam();
		if (json.containsKey("channelType") && NumericUtilCustom.matchInteger("channelType")) {
			param.setChannelType(json.getInt("channelType"));
		}
		if (json.containsKey("channelName")) {
			param.setChannelName(json.getString("channelName"));
		}
		return param;
	}

}
