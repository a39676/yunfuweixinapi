package demo.article.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class CreatingArticleParam implements CommonControllerParam {

	private Long userId;
	private String title;
	private String content;
	private Integer articleChannel;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getArticleChannel() {
		return articleChannel;
	}

	public void setArticleChannel(Integer articleChannel) {
		this.articleChannel = articleChannel;
	}

	@Override
	public String toString() {
		return "CreatingArticleParam [userId=" + userId + ", title=" + title + ", content=" + content
				+ ", articleChannel=" + articleChannel + "]";
	}

	@Override
	public CreatingArticleParam fromJson(JSONObject json) {
		CreatingArticleParam param = new CreatingArticleParam();
		if (json.containsKey("userId") && NumericUtilCustom.matchInteger(json.getString("userId"))) {
			param.setUserId(json.getLong("userId"));
		}
		if (json.containsKey("title")) {
			param.setTitle(json.getString("title"));
		}
		if (json.containsKey("content")) {
			param.setContent(json.getString("content"));
		}
		if (json.containsKey("articleChannel") && NumericUtilCustom.matchInteger(json.getString("articleChannel"))) {
			param.setArticleChannel(json.getInt("articleChannel"));
		}
		return param;
	}

}
