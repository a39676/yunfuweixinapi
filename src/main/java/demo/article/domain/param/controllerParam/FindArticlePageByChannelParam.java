package demo.article.domain.param.controllerParam;

import java.util.Date;

import dateHandle.DateUtilCustom;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindArticlePageByChannelParam implements CommonControllerParam {

	private Integer channelId;
	private Integer pageNo = 1;
	private Integer pageSize = 10;
	private Date postDate;

	@Override
	public FindArticlePageByChannelParam fromJson(JSONObject j) {
		FindArticlePageByChannelParam p = new FindArticlePageByChannelParam();
		if (j.containsKey("channelId") && NumericUtilCustom.matchInteger(j.getString("channelId"))) {
			p.setChannelId(j.getInt("channelId"));
		}
		if (j.containsKey("pageSize") && NumericUtilCustom.matchInteger(j.getString("pageSize"))) {
			p.setPageSize(j.getInt("pageSize"));
		}
		if (j.containsKey("pageNo") && NumericUtilCustom.matchInteger(j.getString("pageNo"))) {
			p.setPageNo(j.getInt("pageNo"));
		}
		if (j.containsKey("postDate") && DateUtilCustom.isDateValid(j.getString("postDate"))) {
			p.setPostDate(DateUtilCustom.stringToDateUnkonwFormat(j.getString("postDate")));
		}
		return p;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "FindArticlePageByChannelParam [channelId=" + channelId + ", pageNo=" + pageNo + ", pageSize=" + pageSize
				+ "]";
	}

}
