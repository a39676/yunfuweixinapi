package demo.article.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindArticleChannelListParam implements CommonControllerParam {

	private Integer channelType;

	public Integer getChannelType() {
		return channelType;
	}

	public void setChannelType(Integer channelType) {
		this.channelType = channelType;
	}

	@Override
	public String toString() {
		return "FindArticleChannelListParam [channelType=" + channelType + "]";
	}

	@Override
	public FindArticleChannelListParam fromJson(JSONObject json) {
		FindArticleChannelListParam param = new FindArticleChannelListParam();
		if(json.containsKey("channelType") && NumericUtilCustom.matchInteger("channelType")) {
			param.setChannelType(json.getInt("channelType"));
		}
		return param;
	}

}
