package demo.article.domain.param.controllerParam;

public class CreatingArticleProductParam {

	private Long userId;
	private String title;
	private String content;
	private Long productId;
	private Integer productType;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	@Override
	public String toString() {
		return "CreatingArticleProductParam [userId=" + userId + ", title=" + title + ", content=" + content
				+ ", productId=" + productId + ", productType=" + productType + "]";
	}


}
