package demo.article.domain.param.mapperParam;

import java.util.Date;
import java.util.List;

public class FindArticleTitleByChannelParam {

	private Integer channelId;
	private List<Integer> channelIdList;
	private Integer pageStart;
	private Integer pageSize = 10;
	private Date startTime;
	private Date endTime;
	private boolean isDelete = false;

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public List<Integer> getChannelIdList() {
		return channelIdList;
	}

	public void setChannelIdList(List<Integer> channelIdList) {
		this.channelIdList = channelIdList;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getPageStart() {
		return pageStart;
	}

	public void setPageStart(Integer pageStart) {
		this.pageStart = pageStart;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "FindArticleTitleByChannelParam [channelId=" + channelId + ", channelIdList=" + channelIdList
				+ ", pageStart=" + pageStart + ", pageSize=" + pageSize + ", startTime=" + startTime + ", endTime="
				+ endTime + "]";
	}

}
