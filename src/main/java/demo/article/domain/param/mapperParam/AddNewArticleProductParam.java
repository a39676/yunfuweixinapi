package demo.article.domain.param.mapperParam;

public class AddNewArticleProductParam {

	private Long articleId;
	private Integer productType;
	private Long productId;

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "AddNewArticleProductParam [articleId=" + articleId + ", productType=" + productType + ", productId="
				+ productId + "]";
	}

}
