package demo.article.domain.type;

public enum ResultTypeArticle {

	articleSaveError("文章保存异常", "-4-1"),
	articleChannelExists("同名频道已存在", "-7-1");
	;
		
	private String typeName;
	private String typeCode;
	
	ResultTypeArticle(String name, String code) {
		this.typeName = name;
		this.typeCode = code;
	}
	
	public String getName() {
		return this.typeName;
	}
	
	public String getCode() {
		return this.typeCode;
	}
}
