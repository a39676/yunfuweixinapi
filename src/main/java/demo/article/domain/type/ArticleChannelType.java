package demo.article.domain.type;

/**
 * 文章频道类型 
 */
public enum ArticleChannelType {
	
	/** 公告 */
	announcement("公告", 1), 
	/** 银行类 */
	financialKnowledge("银行类", 2),
	/** 保险类 */
	insuranceKnowledge("保险类", 3),
	/** 证券类 */
	securitieslKnowledge("证券类", 4),
	/** 外汇类 */
	ForeignExchangeKnowledge("外汇类", 5),
	/** 其他类 */
	other("其他类", 7),
	/** 知识 */
	knowledge("知识", 6),
	;
	
	private String typeName;
	private Integer typeCode;
	
	ArticleChannelType(String typeName, Integer typeCode) {
		this.typeName = typeName;
		this.typeCode = typeCode;
	}
	

	public String getName() {
		return typeName;
	}

	public Integer getCode() {
		return typeCode;
	}

	public static ArticleChannelType getType(String typeName) {
		for(ArticleChannelType t : ArticleChannelType.values()) {
			if(t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}
	
	public static ArticleChannelType getType(Integer typeCode) {
		for(ArticleChannelType t : ArticleChannelType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

}
