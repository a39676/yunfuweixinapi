package demo.article.domain.vo;

import java.util.Date;

public class ArticleVO {

	private Long id;

	private String articleTitle;

	private String nickName;

	private String contentLines;

	private String createDateString;

	private Date createTime;

	private Integer articleChannel;

	private String channelName;

	public Integer getArticleChannel() {
		return articleChannel;
	}

	public void setArticleChannel(Integer articleChannel) {
		this.articleChannel = articleChannel;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getContentLines() {
		return contentLines;
	}

	public void setContentLines(String contentLines) {
		this.contentLines = contentLines;
	}

	public String getCreateDateString() {
		return createDateString;
	}

	public void setCreateDateString(String createDateString) {
		this.createDateString = createDateString;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ArticleVO [id=" + id + ", articleTitle=" + articleTitle + ", nickName=" + nickName + ", contentLines="
				+ contentLines + ", createDateString=" + createDateString + ", createTime=" + createTime
				+ ", articleChannel=" + articleChannel + ", channelName=" + channelName + "]";
	}

}
