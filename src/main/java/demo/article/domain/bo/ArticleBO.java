package demo.article.domain.bo;

import java.util.Date;

public class ArticleBO {

	private Long id;
	private Long userId;
	private String articleTitle;
	private String path;
	private Date createTime;
	private String productName;
	private String productCode;
	private Integer articleChannel;

	public Integer getArticleChannel() {
		return articleChannel;
	}

	public void setArticleChannel(Integer articleChannel) {
		this.articleChannel = articleChannel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String article_title) {
		this.articleTitle = article_title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Override
	public String toString() {
		return "ArticleBO [id=" + id + ", userId=" + userId + ", articleTitle=" + articleTitle + ", path=" + path
				+ ", createTime=" + createTime + ", productName=" + productName + ", productCode=" + productCode
				+ ", articleChannel=" + articleChannel + "]";
	}

}
