package demo.article.domain.result;

import java.util.List;

import demo.article.domain.vo.ArticleVO;
import demo.common.domain.result.CommonResult;

public class LoadArticleListResult extends CommonResult {

	private List<ArticleVO> articleList;

	private Integer resultCount;

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	public List<ArticleVO> getArticleList() {
		return articleList;
	}

	public void setArticleList(List<ArticleVO> articleList) {
		this.articleList = articleList;
	}

	@Override
	public String toString() {
		return "LoadArticleListResult [articleList=" + articleList + ", resultCount=" + resultCount + "]";
	}

}
