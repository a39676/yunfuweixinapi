package demo.article.domain.result;

import java.util.List;

import demo.article.domain.po.ArticlePO;
import demo.common.domain.result.CommonResult;

public class FindArticlePageForAppResult extends CommonResult {

	private List<ArticlePO> articleList;

	public List<ArticlePO> getArticleList() {
		return articleList;
	}

	public void setArticleList(List<ArticlePO> articleList) {
		this.articleList = articleList;
	}

}
