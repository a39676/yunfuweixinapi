package demo.article.domain.result;

import demo.article.domain.type.ResultTypeArticle;
import demo.common.domain.result.CommonResult;

public class ArticleFileSaveResult extends CommonResult {

	private Long articleId;
	private String filePath;

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void fillWithResult(ResultTypeArticle resultType) {
		this.setResult(resultType.getCode());
		this.setMessage(resultType.getName());
	}
	
	@Override
	public String toString() {
		return "ArticleFileSaveResult [articleId=" + articleId + ", filePath=" + filePath + "]";
	}

}
