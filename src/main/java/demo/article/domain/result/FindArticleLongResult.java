package demo.article.domain.result;

import demo.article.domain.vo.ArticleVO;
import demo.common.domain.result.CommonResult;

public class FindArticleLongResult extends CommonResult {

	private ArticleVO articleLongVO;

	private Long articleId = -1L;

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public ArticleVO getArticleLongVO() {
		return articleLongVO;
	}

	public void setArticleLongVO(ArticleVO articleLongVO) {
		this.articleLongVO = articleLongVO;
	}

	@Override
	public String toString() {
		return "FindArticleLongResult [articleLongVO=" + articleLongVO + ", articleId=" + articleId + "]";
	}

}
