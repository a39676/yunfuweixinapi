package demo.article.domain.result;

import java.util.List;

import demo.article.domain.po.ArticleChannels;
import demo.common.domain.result.CommonResult;

public class FindArticleChannelListResult extends CommonResult {

	private List<ArticleChannels> channelList;

	public List<ArticleChannels> getChannelList() {
		return channelList;
	}

	public void setChannelList(List<ArticleChannels> channelList) {
		this.channelList = channelList;
	}

	@Override
	public String toString() {
		return "FindArticleChannelListResult [channelList=" + channelList + "]";
	}

}
