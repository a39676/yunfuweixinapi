package demo.article.domain.result;

import demo.article.domain.type.ResultTypeArticle;
import demo.common.domain.result.CommonResult;

public class AddArticleChannelResult extends CommonResult {
	
	public void fillWithResult(ResultTypeArticle resultTypeBase) {
		this.setResult(resultTypeBase.getCode());
		this.setMessage(resultTypeBase.getName());
	}

}
