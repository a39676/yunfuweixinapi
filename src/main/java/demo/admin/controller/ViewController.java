package demo.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

//	@GetMapping(value = "/view/{folder}/{fileName}")
//	public ModelAndView viewReturn(@PathVariable String folder, @PathVariable String fileName) {
//		return new ModelAndView("admin/view/" + folder + "/" + fileName);
//	}
//
//	@GetMapping(value = "/index")
//	public ModelAndView index() {
//		return new ModelAndView("admin/index");
//	}
//
//	@GetMapping(value = "/error")
//	public ModelAndView error() {
//		return new ModelAndView("admin/error");
//	}
//
//	@GetMapping(value = "/login")
//	public ModelAndView login() {
//		return new ModelAndView("admin/login");
//	}
//
//	@GetMapping(value = "/personCenter")
//	public ModelAndView personCenter() {
//		return new ModelAndView("admin/personCenter");
//	}

	@GetMapping(value = "/_csrf")
	public String _csrf() {
		return "/jsp/testJSP/_csrf";
	}
}
