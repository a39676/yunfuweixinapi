package demo.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import demo.admin.domain.constant.AdminUrl;
import demo.common.controller.CommonController;

@Controller
@RequestMapping(value = AdminUrl.root)
public class AdminController extends CommonController {

	@GetMapping(value = "/")
	public ModelAndView adminHome() {
		return new ModelAndView("admin/view/index");
	}
	
	
	
}
