package demo.common.service;

import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;

public abstract class CommonService {

	protected static final int normalPageSize = 5;
	protected static final int maxPageSize = 50;
	protected static final long theStartTime = 946656000000L;

	protected PageParam setPageFromPageNo(Integer pageNo) {
		return setPageFromPageNo(pageNo, maxPageSize);
	}

	protected PageParam setPageFromPageNo(Integer pageNo, Integer pageSize) {
		if (pageNo == null || pageNo <= 0) {
			pageNo = 1;
		}
		if (pageSize == null || pageSize <= 0) {
			pageSize = 1;
		}
		if (pageSize > maxPageSize) {
			pageSize = maxPageSize;
		}
		PageParam pp = new PageParam();
		if (pageNo == 1) {
			pp.setPageStart(0);
			pp.setPageEnd(pageSize);
		} else if (pageNo > 1) {
			pp.setPageStart(pageSize * (pageNo - 1));
			pp.setPageEnd(pp.getPageStart() + pageSize);
		}
		pp.setPageSize(pageSize);
		return pp;
	}

	protected CommonResult nullParam() {
		CommonResult result = new CommonResult();
		result.fillWithResult(ResultTypeBase.nullParam);
		return result;
	}
	
	protected CommonResult errorParam() {
		CommonResult result = new CommonResult();
		result.fillWithResult(ResultTypeBase.errorParam);
		return result;
	}
	
	protected CommonResult serviceError() {
		CommonResult result = new CommonResult();
		result.fillWithResult(ResultTypeBase.serviceError);
		return result;
	}
	
	protected CommonResult normalSuccess() {
		CommonResult result = new CommonResult();
		result.normalSuccess();
		return result;
	}
	
	protected CommonResult notLogin() {
		CommonResult result = new CommonResult();
		result.failWithMessage("请登录后操作");
		return result;
	}

	protected static boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		if(!os.contains("windows")) {
			return true;
		} else {
			return false;
		}
	}
}