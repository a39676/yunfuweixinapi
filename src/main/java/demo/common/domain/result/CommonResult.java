package demo.common.domain.result;

import demo.common.domain.type.ResultTypeBase;
import net.sf.json.JSONObject;

public class CommonResult {

	protected String result;

	protected String message;

	protected JSONObject json;

	public String getResult() {
		return result;
	}

	public CommonResult setResult(String result) {
		this.result = result;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public CommonResult setMessage(String message) {
		this.message = message;
		return this;
	}

	public CommonResult addMessage(String message) {
		this.message += message;
		return this;
	}

	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}

	public boolean isSuccess() {
		return ResultTypeBase.success.getCode().equals(this.getResult());
	}

	public void setIsSuccess() {
		this.setResult(ResultTypeBase.success.getCode());
	}

	public void setIsFail() {
		this.setResult(ResultTypeBase.fail.getCode());
	}

	public void normalSuccess() {
		this.setResult(ResultTypeBase.success.getCode());
	}

	public void normalFail() {
		this.setResult(ResultTypeBase.fail.getCode());
	}

	public void successWithMessage(String desc) {
		this.setResult(ResultTypeBase.success.getCode());
		this.setMessage(desc);
	}

	public void failWithMessage(String desc) {
		this.setResult(ResultTypeBase.fail.getCode());
		this.setMessage(desc);
	}

	public void fillWithResult(ResultTypeBase resultTypeBase) {
		this.setResult(resultTypeBase.getCode());
		this.setMessage(resultTypeBase.getName());
	}

	@Override
	public String toString() {
		return "CommonResult [result=" + result + ", message=" + message + ", json=" + json + "]";
	}

}