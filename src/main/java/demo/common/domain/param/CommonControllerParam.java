package demo.common.domain.param;

import net.sf.json.JSONObject;

public interface CommonControllerParam {

	public CommonControllerParam fromJson(JSONObject j);
	
}
