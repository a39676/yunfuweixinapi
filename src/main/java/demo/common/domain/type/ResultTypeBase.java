package demo.common.domain.type;

public enum ResultTypeBase {
	
	success	("success", "0"),
	resetPassword("已成功重置密码", "0"),
	
	fail ("fail", "-1"),
	nullParam ("参数为空", "-2"),
	errorParam ("参数异常", "-3"),
	serviceError("网络异常", "-4"),
	notLoginUser("请登录后操作", "-5"),
	loginFail("账户或密码错,请重新登录", "-6"),
//	logicError("逻辑错误限制", "-7"),
//	communicationError("网络通讯异常", "-8"),
	
	differentPassword("两次输入密码不同,请重新输入", "-3-1"),
	invalidPassword("请设定8~16位的密码", "-3-2"),
	wrongOldPassword("原密码错误", "-3-3"),
	
	communicationError("网络通讯异常", "-8-1"),
	;
	
	
	private String resultName;
	private String resultCode;
	
	ResultTypeBase(String name, String code) {
		this.resultName = name;
		this.resultCode = code;
	}

	public String getName() {
		return this.resultName;
	}
	
	public String getCode() {
		return this.resultCode;
	}
}
