package demo.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import demo.business.domain.constant.BusinessUrl;
import demo.business.domain.param.controllerParam.AddComplaintParam;
import demo.business.domain.param.controllerParam.FindComplaintPageParam;
import demo.business.domain.param.controllerParam.RejectComplaintParam;
import demo.business.domain.param.controllerParam.SendComplaintParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibOrgLimitParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibOrgUsedParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibUserLimitParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibUserUsedParam;
import demo.business.domain.result.FindComplaintPageResult;
import demo.business.domain.result.FindExchangeDibOrgLimitResult;
import demo.business.domain.result.FindExchangeDibUserLimitResult;
import demo.business.domain.result.UpdateExchangeDibUserUsedResult;
import demo.business.service.BusinessService;
import demo.common.controller.CommonController;
import demo.common.domain.result.CommonResult;
import demo.util.BaseUtilCustom;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

@Controller
@RequestMapping(value = BusinessUrl.root)
public class BusinessController extends CommonController {

	@Autowired
	private BusinessService bussinessService;
	@Autowired
	private BaseUtilCustom baseUtilCustom;

	@PostMapping(value = BusinessUrl.updateExchangeDibOrgLimit)
	public void updateExchangeDibOrgLimit(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		UpdateExchangeDibOrgLimitParam param = new UpdateExchangeDibOrgLimitParam().fromJson(getJson(data));
		CommonResult serviceResult = bussinessService.updateExchangeDibOrgLimit(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	public CommonResult updateExchangeDibOrgUsed(UpdateExchangeDibOrgUsedParam param) {
		CommonResult serviceResult = bussinessService.updateExchangeDibOrgUsed(param);
		return serviceResult;
	}

	@PostMapping(value = BusinessUrl.initOrUpdateDefaultUserLimit)
	public void initOrUpdateDefaultUserLimit(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		UpdateExchangeDibUserLimitParam param = new UpdateExchangeDibUserLimitParam().fromJson(getJson(data));
		CommonResult serviceResult = bussinessService.initOrUpdateDefaultUserLimit(param);
		outputJson(response, JSONObject.fromObject(serviceResult));
	}

	public UpdateExchangeDibUserUsedResult updateExchangeDibUserUsed(UpdateExchangeDibUserUsedParam param) {
		UpdateExchangeDibUserUsedResult serviceResult = bussinessService.updateExchangeDibUserUsed(param);
		return serviceResult;
	}

	@PostMapping(value = BusinessUrl.findExchangeDibOrgLimit)
	public void findExchangeDibOrgLimit(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		JSONObject json = getJson(data);
		FindExchangeDibOrgLimitResult result = bussinessService.findExchangeDibOrgLimit(json.getLong("orgId"));
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = BusinessUrl.findExchangeDibUserLimit)
	public void findUserLimit(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response) {
		JSONObject json = getJson(data);
		Long userId = null;
		if(json.containsKey("userId") && NumericUtilCustom.matchInteger(json.getString("userId"))) {
			userId = json.getLong("userId");
		}
		FindExchangeDibUserLimitResult result = bussinessService.findExchangeDibUserLimit(userId);
		outputJson(response, JSONObject.fromObject(result));
	}

	@GetMapping(value = BusinessUrl.addComplaint)
	public ModelAndView addComplaint() {
		if(!baseUtilCustom.isLoginUser()) {
			return new ModelAndView(new RedirectView("/weixinHtml/sign/sign.html"));
		} else {
			return new ModelAndView(new RedirectView("/weixinHtml/Complaint/Complaint.html"));
		}
	}

	@PostMapping(value = BusinessUrl.addComplaint)
	public void addComplaint(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
		AddComplaintParam param = new AddComplaintParam().fromJson(getJson(data));
		CommonResult result = bussinessService.addComplaint(baseUtilCustom.getUserId(), param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = BusinessUrl.rejectComplaint)
	public void rejectComplaint(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		RejectComplaintParam param = new RejectComplaintParam().fromJson(getJson(data));
		CommonResult result = bussinessService.rejectComplaint(param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = BusinessUrl.sendComplaint)
	public void sendComplaint(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		SendComplaintParam param = new SendComplaintParam().fromJson(getJson(data));
		CommonResult result = bussinessService.sendComplaint(param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
	@PostMapping(value = BusinessUrl.findComplaintPage)
	public void findComplaintPage(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {
		FindComplaintPageParam param = new FindComplaintPageParam().fromJson(getJson(data));
		FindComplaintPageResult result = bussinessService.findComplaintPage(param);
		outputJson(response, JSONObject.fromObject(result));
	}
	
}
