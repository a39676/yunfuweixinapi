package demo.business.service;

import demo.business.domain.param.controllerParam.AddComplaintParam;
import demo.business.domain.param.controllerParam.FindComplaintPageParam;
import demo.business.domain.param.controllerParam.RejectComplaintParam;
import demo.business.domain.param.controllerParam.SendComplaintParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibOrgLimitParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibOrgUsedParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibUserLimitParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibUserUsedParam;
import demo.business.domain.result.FindComplaintPageResult;
import demo.business.domain.result.FindExchangeDibOrgLimitResult;
import demo.business.domain.result.FindExchangeDibUserLimitResult;
import demo.business.domain.result.UpdateExchangeDibUserUsedResult;
import demo.common.domain.result.CommonResult;

public interface BusinessService {

	/** 机构更新零钞兑换每日限额 */
	CommonResult updateExchangeDibOrgLimit(UpdateExchangeDibOrgLimitParam controllerParam);

	/** 机构更新零钞兑换当日已用额度 */
	CommonResult updateExchangeDibOrgUsed(UpdateExchangeDibOrgUsedParam controllerParam);

	/** 初始化或更新用户(全局)每日零钞兑换额度 */
	CommonResult initOrUpdateDefaultUserLimit(UpdateExchangeDibUserLimitParam controllerParam);

	/** 更新指定用户零钞兑换限额 */
	UpdateExchangeDibUserUsedResult updateExchangeDibUserUsed(UpdateExchangeDibUserUsedParam controllerParam);

	/** 删除过期的用户零钞兑换限额记录 */
	Integer deleteExpiredExchangeDibRecord();

	/** 查找机构零钞兑换额度 */
	FindExchangeDibOrgLimitResult findExchangeDibOrgLimit(Long orgId);

	/** 添加投诉 
	 * @throws Exception */
	CommonResult addComplaint(Long userId, AddComplaintParam controllerParam) throws Exception;

	/** 委派投诉处理人 */
	CommonResult sendComplaint(SendComplaintParam controllerParam);

	/** 查询用户零钞兑换日限额(若用户id为空, 查询全局默认用户日限额) */
	FindExchangeDibUserLimitResult findExchangeDibUserLimit(Long userId);

	/** 后台获取投诉列表分页 */
	FindComplaintPageResult findComplaintPage(FindComplaintPageParam p);

	/** 拒絕投訴 */
	CommonResult rejectComplaint(RejectComplaintParam controllerParam);

}
