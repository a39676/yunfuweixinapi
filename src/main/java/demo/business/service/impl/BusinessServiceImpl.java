package demo.business.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dateHandle.DateUtilCustom;
import demo.base.user.domain.vo.UsersDetailVO;
import demo.base.user.service.UsersService;
import demo.business.domain.param.controllerParam.AddComplaintParam;
import demo.business.domain.param.controllerParam.FindComplaintPageParam;
import demo.business.domain.param.controllerParam.RejectComplaintParam;
import demo.business.domain.param.controllerParam.SendComplaintParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibOrgLimitParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibOrgUsedParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibUserLimitParam;
import demo.business.domain.param.controllerParam.UpdateExchangeDibUserUsedParam;
import demo.business.domain.param.mapperParam.FindComplaintPage;
import demo.business.domain.param.mapperParam.RejectComplaint;
import demo.business.domain.param.mapperParam.UpdateExchangeDibOrgLimit;
import demo.business.domain.param.mapperParam.UpdateExchangeDibOrgUsed;
import demo.business.domain.param.mapperParam.UpdateExchangeDibUserUsed;
import demo.business.domain.po.BusinessComplaintPO;
import demo.business.domain.po.BusinessExchangeDibPO;
import demo.business.domain.po.ExchangeDibOrganizationLimitPO;
import demo.business.domain.po.ExchangeDibUserLimitPO;
import demo.business.domain.result.FindComplaintPageResult;
import demo.business.domain.result.FindExchangeDibOrgLimitResult;
import demo.business.domain.result.FindExchangeDibUserLimitResult;
import demo.business.domain.result.UpdateExchangeDibUserUsedResult;
import demo.business.domain.type.ComplaintType;
import demo.business.domain.vo.BusinessComplaintVO;
import demo.business.mapper.BusinessComplaintPOMapper;
import demo.business.mapper.BusinessExchangeDibPOMapper;
import demo.business.mapper.ExchangeDibOrganizationLimitPOMapper;
import demo.business.mapper.ExchangeDibUserLimitPOMapper;
import demo.business.service.BusinessService;
import demo.common.domain.param.PageParam;
import demo.common.domain.result.CommonResult;
import demo.common.service.CommonService;
import demo.organizations.controller.OrganizationsController;
import demo.organizations.domain.constant.OrgDefault;
import demo.organizations.domain.param.controllerParam.FindStaffsByOrgIdParam;
import demo.organizations.domain.po.OrganizationsPO;
import demo.organizations.domain.result.FindStaffsByConditionResult;
import demo.organizations.service.StaffDetailService;
import demo.weixin.service.WeixinService;

@Service
public class BusinessServiceImpl extends CommonService implements BusinessService {

	@Autowired
	private UsersService userService;
	@Autowired
	private WeixinService wxService;
	@Autowired
	private StaffDetailService staffDetailService;
	
	@Autowired
	private ExchangeDibOrganizationLimitPOMapper exchangeDibOrgLimitMapper;
	@Autowired
	private ExchangeDibUserLimitPOMapper exchangeDibUserLimitMapper;
	@Autowired
	private BusinessExchangeDibPOMapper exchangeDibMapper;
	@Autowired
	private OrganizationsController orgContoller;
	@Autowired
	private BusinessComplaintPOMapper complaintMapper;

	@Override
	public CommonResult updateExchangeDibOrgUsed(UpdateExchangeDibOrgUsedParam controllerParam) {
		if (controllerParam.getOrgId() == null) {
			return nullParam();
		}

		if (!orgContoller.existOrgId(controllerParam.getOrgId()).isSuccess()) {
			return errorParam();
		}

		OrganizationsPO org = orgContoller.findOne(controllerParam.getOrgId());
		ExchangeDibOrganizationLimitPO limit = exchangeDibOrgLimitMapper
				.findExchangeDibOrganizationLimitByOrgId(org.getTopHeadOrg());
		CommonResult result = exchangeDibWithInOrgLimit(limit, controllerParam);
		if (!result.isSuccess()) {
			return result;
		}

		UpdateExchangeDibOrgUsed mapperParam = new UpdateExchangeDibOrgUsed();
		mapperParam.setOrgId(org.getTopHeadOrg());
		if (controllerParam.getCoin01Used() != null && controllerParam.getCoin01Used() > 0) {
			mapperParam.setCoin01Used(controllerParam.getCoin01Used());
		}
		if (controllerParam.getCoin05Used() != null && controllerParam.getCoin05Used() > 0) {
			mapperParam.setCoin05Used(controllerParam.getCoin05Used());
		}
		if (controllerParam.getCoin1Used() != null && controllerParam.getCoin1Used() > 0) {
			mapperParam.setCoin1Used(controllerParam.getCoin1Used());
		}
		if (controllerParam.getPaper1Used() != null && controllerParam.getPaper1Used() > 0) {
			mapperParam.setPaper1Used(controllerParam.getPaper1Used());
		}
		if (controllerParam.getPaper5Used() != null && controllerParam.getPaper5Used() > 0) {
			mapperParam.setPaper5Used(controllerParam.getPaper5Used());
		}
		if (controllerParam.getPaper10Used() != null && controllerParam.getPaper10Used() > 0) {
			mapperParam.setPaper10Used(controllerParam.getPaper10Used());
		}
		if (controllerParam.getPaper20Used() != null && controllerParam.getPaper20Used() > 0) {
			mapperParam.setPaper20Used(controllerParam.getPaper20Used());
		}
		if (controllerParam.getPaper50Used() != null && controllerParam.getPaper50Used() > 0) {
			mapperParam.setPaper50Used(controllerParam.getPaper50Used());
		}
		if (controllerParam.getPaper100Used() != null && controllerParam.getPaper100Used() > 0) {
			mapperParam.setPaper100Used(controllerParam.getPaper100Used());
		}

		exchangeDibOrgLimitMapper.updateUsed(mapperParam);

		return normalSuccess();
	}

	private CommonResult exchangeDibWithInOrgLimit(ExchangeDibOrganizationLimitPO orgLimit,
			UpdateExchangeDibOrgUsedParam orgParam) {
		CommonResult result = new CommonResult();
		if (orgLimit == null) {
			updateExchangeDibOrgLimit(new UpdateExchangeDibOrgLimitParam());
			result.setIsFail();
			result.setMessage("超出当日兑换限额");
			return result;
		}

		if (orgParam.getCoin01Used() != null
				&& orgParam.getCoin01Used() > (orgLimit.getCoin01Limit() - orgLimit.getCoin01Used())) {
			result.setIsFail();
			result.setMessage("硬币1角超出当日兑换限额");
			return result;
		}
		if (orgParam.getCoin05Used() != null
				&& orgParam.getCoin05Used() > (orgLimit.getCoin05Limit() - orgLimit.getCoin05Used())) {
			result.setIsFail();
			result.setMessage("硬币5角超出当日兑换限额");
			return result;
		}
		if (orgParam.getCoin1Used() != null
				&& orgParam.getCoin1Used() > (orgLimit.getCoin1Limit() - orgLimit.getCoin1Used())) {
			result.setIsFail();
			result.setMessage("硬币1元超出当日兑换限额");
			return result;
		}
		if (orgParam.getPaper1Used() != null
				&& orgParam.getPaper1Used() > (orgLimit.getPaper1Limit() - orgLimit.getPaper1Used())) {
			result.setIsFail();
			result.setMessage("纸币1元超出当日兑换限额");
			return result;
		}
		if (orgParam.getPaper5Used() != null
				&& orgParam.getPaper5Used() > (orgLimit.getPaper5Limit() - orgLimit.getPaper5Used())) {
			result.setIsFail();
			result.setMessage("纸币5元超出当日兑换限额");
			return result;
		}
		if (orgParam.getPaper10Used() != null
				&& orgParam.getPaper10Used() > (orgLimit.getPaper10Limit() - orgLimit.getPaper10Used())) {
			result.setIsFail();
			result.setMessage("纸币元10超出当日兑换限额");
			return result;
		}
		if (orgParam.getPaper20Used() != null
				&& orgParam.getPaper20Used() > (orgLimit.getPaper20Limit() - orgLimit.getPaper20Used())) {
			result.setIsFail();
			result.setMessage("纸币元20超出当日兑换限额");
			return result;
		}
		if (orgParam.getPaper50Used() != null
				&& orgParam.getPaper50Used() > (orgLimit.getPaper50Limit() - orgLimit.getPaper50Used())) {
			result.setIsFail();
			result.setMessage("纸币元50超出当日兑换限额");
			return result;
		}
		if (orgParam.getPaper100Used() != null
				&& orgParam.getPaper100Used() > (orgLimit.getPaper100Limit() - orgLimit.getPaper100Used())) {
			result.setIsFail();
			result.setMessage("纸币元100超出当日兑换限额");
			return result;
		}
		result.setIsSuccess();
		return result;
	}

	private CommonResult exchangeDibWithInOrgLimit(ExchangeDibOrganizationLimitPO orgLimit,
			UpdateExchangeDibUserUsedParam userParam) {
		CommonResult result = new CommonResult();
		if (orgLimit == null) {
			updateExchangeDibOrgLimit(new UpdateExchangeDibOrgLimitParam());
			result.setIsFail();
			result.setMessage("超出当日兑换限额");
			return result;
		}

		if (userParam.getCoin01Used() != null
				&& userParam.getCoin01Used() > (orgLimit.getCoin01Limit() - orgLimit.getCoin01Used())) {
			result.setIsFail();
			result.setMessage("硬币1角超出当日兑换限额");
			return result;
		}
		if (userParam.getCoin05Used() != null
				&& userParam.getCoin05Used() > (orgLimit.getCoin05Limit() - orgLimit.getCoin05Used())) {
			result.setIsFail();
			result.setMessage("硬币5角超出当日兑换限额");
			return result;
		}
		if (userParam.getCoin1Used() != null
				&& userParam.getCoin1Used() > (orgLimit.getCoin1Limit() - orgLimit.getCoin1Used())) {
			result.setIsFail();
			result.setMessage("硬币1元超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper1Used() != null
				&& userParam.getPaper1Used() > (orgLimit.getPaper1Limit() - orgLimit.getPaper1Used())) {
			result.setIsFail();
			result.setMessage("纸币1元超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper5Used() != null
				&& userParam.getPaper5Used() > (orgLimit.getPaper5Limit() - orgLimit.getPaper5Used())) {
			result.setIsFail();
			result.setMessage("纸币5元超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper10Used() != null
				&& userParam.getPaper10Used() > (orgLimit.getPaper10Limit() - orgLimit.getPaper10Used())) {
			result.setIsFail();
			result.setMessage("纸币元10超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper20Used() != null
				&& userParam.getPaper20Used() > (orgLimit.getPaper20Limit() - orgLimit.getPaper20Used())) {
			result.setIsFail();
			result.setMessage("纸币元20超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper50Used() != null
				&& userParam.getPaper50Used() > (orgLimit.getPaper50Limit() - orgLimit.getPaper50Used())) {
			result.setIsFail();
			result.setMessage("纸币元50超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper100Used() != null
				&& userParam.getPaper100Used() > (orgLimit.getPaper100Limit() - orgLimit.getPaper100Used())) {
			result.setIsFail();
			result.setMessage("纸币元100超出当日兑换限额");
			return result;
		}
		result.setIsSuccess();
		return result;
	}

	private CommonResult exchangeDibWithInUserLimit(ExchangeDibUserLimitPO userLimit,
			UpdateExchangeDibUserUsedParam userParam) {
		CommonResult result = new CommonResult();
		if (userLimit == null) {
			updateExchangeDibOrgLimit(new UpdateExchangeDibOrgLimitParam());
			result.setIsFail();
			result.setMessage("超出当日兑换限额");
			return result;
		}

		if (userParam.getCoin01Used() != null
				&& userParam.getCoin01Used() > (userLimit.getCoin01Limit() - userLimit.getCoin01Used())) {
			result.setIsFail();
			result.setMessage("硬币1角超出当日兑换限额");
			return result;
		}
		if (userParam.getCoin05Used() != null
				&& userParam.getCoin05Used() > (userLimit.getCoin05Limit() - userLimit.getCoin05Used())) {
			result.setIsFail();
			result.setMessage("硬币5角超出当日兑换限额");
			return result;
		}
		if (userParam.getCoin1Used() != null
				&& userParam.getCoin1Used() > (userLimit.getCoin1Limit() - userLimit.getCoin1Used())) {
			result.setIsFail();
			result.setMessage("硬币1元超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper1Used() != null
				&& userParam.getPaper1Used() > (userLimit.getPaper1Limit() - userLimit.getPaper1Used())) {
			result.setIsFail();
			result.setMessage("纸币1元超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper5Used() != null
				&& userParam.getPaper5Used() > (userLimit.getPaper5Limit() - userLimit.getPaper5Used())) {
			result.setIsFail();
			result.setMessage("纸币5元超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper10Used() != null
				&& userParam.getPaper10Used() > (userLimit.getPaper10Limit() - userLimit.getPaper10Used())) {
			result.setIsFail();
			result.setMessage("纸币元10超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper20Used() != null
				&& userParam.getPaper20Used() > (userLimit.getPaper20Limit() - userLimit.getPaper20Used())) {
			result.setIsFail();
			result.setMessage("纸币元20超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper50Used() != null
				&& userParam.getPaper50Used() > (userLimit.getPaper50Limit() - userLimit.getPaper50Used())) {
			result.setIsFail();
			result.setMessage("纸币元50超出当日兑换限额");
			return result;
		}
		if (userParam.getPaper100Used() != null
				&& userParam.getPaper100Used() > (userLimit.getPaper100Limit() - userLimit.getPaper100Used())) {
			result.setIsFail();
			result.setMessage("纸币元100超出当日兑换限额");
			return result;
		}
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult updateExchangeDibOrgLimit(UpdateExchangeDibOrgLimitParam controllerParam) {
		if (controllerParam.getOrgId() == null) {
			return nullParam();
		}

		UpdateExchangeDibOrgLimit mapperParam = new UpdateExchangeDibOrgLimit();
		
		OrganizationsPO org = orgContoller.findOne(controllerParam.getOrgId());
		
		mapperParam.setOrgId(org.getTopHeadOrg());
		if (controllerParam.getCoin01Limit() != null && controllerParam.getCoin01Limit() > 0) {
			mapperParam.setCoin01Limit(controllerParam.getCoin01Limit());
		}
		if (controllerParam.getCoin05Limit() != null && controllerParam.getCoin05Limit() > 0) {
			mapperParam.setCoin05Limit(controllerParam.getCoin05Limit());
		}
		if (controllerParam.getCoin1Limit() != null && controllerParam.getCoin1Limit() > 0) {
			mapperParam.setCoin1Limit(controllerParam.getCoin1Limit());
		}
		if (controllerParam.getPaper1Limit() != null && controllerParam.getPaper1Limit() > 0) {
			mapperParam.setPaper1Limit(controllerParam.getPaper1Limit());
		}
		if (controllerParam.getPaper5Limit() != null && controllerParam.getPaper5Limit() > 0) {
			mapperParam.setPaper5Limit(controllerParam.getPaper5Limit());
		}
		if (controllerParam.getPaper10Limit() != null && controllerParam.getPaper10Limit() > 0) {
			mapperParam.setPaper10Limit(controllerParam.getPaper10Limit());
		}
		if (controllerParam.getPaper20Limit() != null && controllerParam.getPaper20Limit() > 0) {
			mapperParam.setPaper20Limit(controllerParam.getPaper20Limit());
		}
		if (controllerParam.getPaper50Limit() != null && controllerParam.getPaper50Limit() > 0) {
			mapperParam.setPaper50Limit(controllerParam.getPaper50Limit());
		}
		if (controllerParam.getPaper100Limit() != null && controllerParam.getPaper100Limit() > 0) {
			mapperParam.setPaper100Limit(controllerParam.getPaper100Limit());
		}

		try {
			exchangeDibOrgLimitMapper.updateLimit(mapperParam);
		} catch (Exception e) {
			return serviceError();
		}

		return normalSuccess();
	}

	@Override
	public CommonResult initOrUpdateDefaultUserLimit(UpdateExchangeDibUserLimitParam controllerParam) {
		ExchangeDibUserLimitPO po = new ExchangeDibUserLimitPO();
		po.setUserId(1L);
		if (controllerParam.getCoin01Limit() != null && controllerParam.getCoin01Limit() > 0) {
			po.setCoin01Limit(controllerParam.getCoin01Limit());
		}
		if (controllerParam.getCoin05Limit() != null && controllerParam.getCoin05Limit() > 0) {
			po.setCoin05Limit(controllerParam.getCoin05Limit());
		}
		if (controllerParam.getCoin1Limit() != null && controllerParam.getCoin1Limit() > 0) {
			po.setCoin1Limit(controllerParam.getCoin1Limit());
		}
		if (controllerParam.getPaper1Limit() != null && controllerParam.getPaper1Limit() > 0) {
			po.setPaper1Limit(controllerParam.getPaper1Limit());
		}
		if (controllerParam.getPaper5Limit() != null && controllerParam.getPaper5Limit() > 0) {
			po.setPaper5Limit(controllerParam.getPaper5Limit());
		}
		if (controllerParam.getPaper10Limit() != null && controllerParam.getPaper10Limit() > 0) {
			po.setPaper10Limit(controllerParam.getPaper10Limit());
		}
		if (controllerParam.getPaper20Limit() != null && controllerParam.getPaper20Limit() > 0) {
			po.setPaper20Limit(controllerParam.getPaper20Limit());
		}
		if (controllerParam.getPaper50Limit() != null && controllerParam.getPaper50Limit() > 0) {
			po.setPaper50Limit(controllerParam.getPaper50Limit());
		}
		if (controllerParam.getPaper100Limit() != null && controllerParam.getPaper100Limit() > 0) {
			po.setPaper100Limit(controllerParam.getPaper100Limit());
		}

		try {
			exchangeDibUserLimitMapper.initOrUpdateDefaultUserLimit(po);
		} catch (Exception e) {
			return serviceError();
		}

		return normalSuccess();
	}

	@Override
	public UpdateExchangeDibUserUsedResult updateExchangeDibUserUsed(UpdateExchangeDibUserUsedParam controllerParam) {
		UpdateExchangeDibUserUsedResult result = new UpdateExchangeDibUserUsedResult();
		if (controllerParam.getUserId() == null) {
			result.failWithMessage("请先登录");
			return result;
		}

		ExchangeDibUserLimitPO userLimit = exchangeDibUserLimitMapper
				.findExchangeDibUserLimit(controllerParam.getUserId());
		OrganizationsPO targetOrg = orgContoller.findOne(controllerParam.getOrgId());
		ExchangeDibOrganizationLimitPO orgLimit = exchangeDibOrgLimitMapper
				.findExchangeDibOrganizationLimitByOrgId(targetOrg.getTopHeadOrg());
		if (userLimit == null) {
			userLimit = exchangeDibUserLimitMapper.findExchangeDibUserLimit(1L);
			if (userLimit != null) {
				userLimit.setUserId(controllerParam.getUserId());
				exchangeDibUserLimitMapper.insertSelective(userLimit);
			} else {
				initOrUpdateDefaultUserLimit(new UpdateExchangeDibUserLimitParam());
				result.setIsFail();
				result.setMessage("超出当日兑换限额");
				return result;
			}
		}
		CommonResult subResult = null;
		subResult = exchangeDibWithInOrgLimit(orgLimit, controllerParam);
		if (!subResult.isSuccess()) {
			result.failWithMessage(subResult.getMessage());
			return result;
		}
		subResult = exchangeDibWithInUserLimit(userLimit, controllerParam);
		if (!subResult.isSuccess()) {
			result.failWithMessage(subResult.getMessage());
			return result;
		}

		UpdateExchangeDibUserUsed mapperParam = new UpdateExchangeDibUserUsed();
		mapperParam.setUserId(controllerParam.getUserId());
		if (controllerParam.getCoin01Used() != null) {
			mapperParam.setCoin01Used(controllerParam.getCoin01Used() + userLimit.getCoin01Used());
		}
		if (controllerParam.getCoin05Used() != null) {
			mapperParam.setCoin05Used(controllerParam.getCoin05Used() + userLimit.getCoin05Used());
		}
		if (controllerParam.getCoin1Used() != null) {
			mapperParam.setCoin1Used(controllerParam.getCoin1Used() + userLimit.getCoin1Used());
		}
		if (controllerParam.getPaper1Used() != null) {
			mapperParam.setPaper1Used(controllerParam.getPaper1Used() + userLimit.getPaper1Used());
		}
		if (controllerParam.getPaper5Used() != null) {
			mapperParam.setPaper5Used(controllerParam.getPaper5Used() + userLimit.getPaper5Used());
		}
		if (controllerParam.getPaper10Used() != null) {
			mapperParam.setPaper10Used(controllerParam.getPaper10Used() + userLimit.getPaper10Used());
		}
		if (controllerParam.getPaper20Used() != null) {
			mapperParam.setPaper20Used(controllerParam.getPaper20Used() + userLimit.getPaper20Used());
		}
		if (controllerParam.getPaper50Used() != null) {
			mapperParam.setPaper50Used(controllerParam.getPaper50Used() + userLimit.getPaper50Used());
		}
		if (controllerParam.getPaper100Used() != null) {
			mapperParam.setPaper100Used(controllerParam.getPaper100Used() + userLimit.getPaper100Used());
		}

		mapperParam.setMoneyType(controllerParam.getMoneyType());

		exchangeDibUserLimitMapper.updateUsed(mapperParam);

		Long newBusinessId = __insertExchangeDib(controllerParam);
		result.setBusinessId(newBusinessId);
		result.setIsSuccess();

		return result;
	}

	@Override
	public Integer deleteExpiredExchangeDibRecord() {
		Date targetTime = DateUtilCustom.atStartOfDay(new Date());
		exchangeDibUserLimitMapper.deleteExpiredExchangeDibUserLimit(targetTime);
		exchangeDibMapper.deleteExpired(targetTime);
		return 0;
	}

	@Override
	public FindExchangeDibOrgLimitResult findExchangeDibOrgLimit(Long orgId) {
		FindExchangeDibOrgLimitResult result = new FindExchangeDibOrgLimitResult();
		if (orgId == null) {
			return result;
		}
		OrganizationsPO org = orgContoller.findOne(orgId);
		ExchangeDibOrganizationLimitPO po = exchangeDibOrgLimitMapper.findExchangeDibOrganizationLimitByOrgId(org.getTopHeadOrg());
		if (po == null) {
			return result;
		}

		result.setExchangeDibOrganizationLimit(po);
		result.setIsSuccess();
		return result;
	}

	@Override
	public FindExchangeDibUserLimitResult findExchangeDibUserLimit(Long userId) {
		FindExchangeDibUserLimitResult result = new FindExchangeDibUserLimitResult();
		if (userId == null) {
			userId = 1L;
		}
		ExchangeDibUserLimitPO po = exchangeDibUserLimitMapper.findExchangeDibUserLimit(userId);
		result.setUserLimit(po);
		result.setIsSuccess();
		return result;
	}

	private Long __insertExchangeDib(UpdateExchangeDibUserUsedParam controllerParam) {
		BusinessExchangeDibPO po = new BusinessExchangeDibPO();
		po.setMoneyType(controllerParam.getMoneyType());
		po.setCoin01(controllerParam.getCoin01Used());
		po.setCoin05(controllerParam.getCoin05Used());
		po.setCoin1(controllerParam.getCoin1Used());
		po.setPaper1(controllerParam.getPaper1Used());
		po.setPaper5(controllerParam.getPaper5Used());
		po.setPaper10(controllerParam.getPaper10Used());
		po.setPaper20(controllerParam.getPaper20Used());
		po.setPaper50(controllerParam.getPaper50Used());
		po.setPaper100(controllerParam.getPaper100Used());
		po.setExpirationTime(DateUtilCustom.atEndOfDay(new Date()));

		exchangeDibMapper.insertSelective(po);

		return po.getId();
	}

	@Override
	public CommonResult addComplaint(Long userId, AddComplaintParam controllerParam) throws Exception {
		CommonResult result = new CommonResult();
		if (userId == null || controllerParam == null || controllerParam.getComplaintType() == null
				|| controllerParam.getOrgId() == null) {
			return nullParam();
		}

		UsersDetailVO userVO = userService.findUserDetail(userId);
		if(userVO == null) {
			return errorParam();
		}
		
		if (StringUtils.isBlank(controllerParam.getContent())) {
			result.failWithMessage("请填写投诉内容");
			return result;
		}

		ComplaintType complaintType = ComplaintType.getType(controllerParam.getComplaintType());
		if (complaintType == null) {
			return errorParam();
		}

		BusinessComplaintPO newComplaint = new BusinessComplaintPO();
		newComplaint.setComplaintType(controllerParam.getComplaintType());
		newComplaint.setComplaintUserId(userId);
		newComplaint.setContent(controllerParam.getContent());
		newComplaint.setOrgId(controllerParam.getOrgId());
		complaintMapper.insertSelective(newComplaint);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String complaintTimeString = sdf.format(new Date());
		
		FindStaffsByOrgIdParam findStaffParam = new FindStaffsByOrgIdParam();
		findStaffParam.setOrgId(OrgDefault.orgUnionId);
		FindStaffsByConditionResult staffResult = staffDetailService.findStaffsByCondition(findStaffParam);
		if(staffResult != null && staffResult.getStaffList() != null && staffResult.getStaffList().size() > 0) {
			staffResult.getStaffList().stream().forEach(sd -> {
				if(StringUtils.isNotBlank(sd.getOpenId())) {
					try {
						wxService.addComplaint(sd.getOpenId(), "", userVO.getNickName(), userVO.getMobile().toString(), complaintTimeString, controllerParam.getContent(), "");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		

		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult sendComplaint(SendComplaintParam controllerParam) {
		if (controllerParam == null || controllerParam.getHandlerStaffId() == null || controllerParam.getId() == null) {
			return nullParam();
		}

		BusinessComplaintPO complaint = complaintMapper.findById(controllerParam.getId());
		if (complaint == null) {
			return errorParam();
		}

		SendComplaintParam mapperParam = new SendComplaintParam();
		mapperParam.setHandlerStaffId(controllerParam.getHandlerStaffId());
		mapperParam.setId(controllerParam.getId());
		complaintMapper.sendComplaint(mapperParam);

		CommonResult result = new CommonResult();
		result.setIsSuccess();
		return result;
	}

	@Override
	public CommonResult rejectComplaint(RejectComplaintParam controllerParam) {
		if (controllerParam == null || controllerParam.getId() == null
				|| StringUtils.isBlank(controllerParam.getRemark())) {
			return nullParam();
		}

		RejectComplaint param = new RejectComplaint();
		param.setId(controllerParam.getId());
		param.setRemark(controllerParam.getRemark());
		complaintMapper.rejectComplaint(param);

		CommonResult result = new CommonResult();
		result.setIsSuccess();
		return result;
	}

	@Override
	public FindComplaintPageResult findComplaintPage(FindComplaintPageParam p) {
		FindComplaintPageResult result = new FindComplaintPageResult();
		FindComplaintPage param = new FindComplaintPage();
		PageParam pp = setPageFromPageNo(p.getPageNo(), p.getPageSize());

		param.setPageSize(pp.getPageSize());
		param.setPageStart(pp.getPageStart());
		if (p.getComplaintStatus() != null) {
			if (p.getComplaintStatus() == 1) {
				param.setIsSend(true);
			} else if (p.getComplaintStatus() == 2) {
				param.setIsReject(true);
			}
		}

		if (p.getReceptionTime() != null) {
			Date startTime = DateUtilCustom.atStartOfDay(p.getReceptionTime());
			Date endTime = DateUtilCustom.atEndOfDay(p.getReceptionTime());
			param.setStartTime(startTime);
			param.setEndTime(endTime);
		}

		List<BusinessComplaintVO> complaints = complaintMapper.findComplaintPage(param);
		Integer resultcount = complaintMapper.countComplaintPage(param);

		List<BusinessComplaintVO> complaintList = new ArrayList<BusinessComplaintVO>();
		complaints.stream().forEach(v -> complaintList.add(buildBusinessComplaintVO(v)));

		result.setComplaintList(complaintList);
		result.setResultcount(resultcount);
		result.setIsSuccess();

		return result;
	}

	private BusinessComplaintVO buildBusinessComplaintVO(BusinessComplaintVO v) {
		if (v.getReceptionTime() == null) {
			v.setComplaintResultString("未受理");
			v.setComplaintResult(0);
		}
		if (v.getReceptionTime() != null && (v.getIsReject() == null || v.getIsReject() == false)) {
			v.setComplaintResultString("已指派");
			v.setComplaintResult(1);
		}
		if (v.getIsReject() != null && v.getIsReject() == true) {
			v.setComplaintResultString("已拒绝");
			v.setComplaintResult(2);
		}
		if (v.getComplaintType() != null) {
			ComplaintType ct = ComplaintType.getType(v.getComplaintType());
			if (ct != null) {
				v.setComplaintTypeString(ct.getName());
			}
		}
		if (v.getReceptionTime() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			v.setReceptionTimeString(sdf.format(v.getReceptionTime()));
		}
		return v;
	}
}
