package demo.business.mapper;

import java.util.List;

import demo.business.domain.param.controllerParam.SendComplaintParam;
import demo.business.domain.param.mapperParam.FindComplaintPage;
import demo.business.domain.param.mapperParam.RejectComplaint;
import demo.business.domain.po.BusinessComplaintPO;
import demo.business.domain.vo.BusinessComplaintVO;

public interface BusinessComplaintPOMapper {
    int insert(BusinessComplaintPO record);

    int insertSelective(BusinessComplaintPO record);

    BusinessComplaintPO findById(Long id);
    
    int sendComplaint(SendComplaintParam param);
    int rejectComplaint(RejectComplaint param);
    
    List<BusinessComplaintVO> findComplaintPage(FindComplaintPage param);
    Integer countComplaintPage(FindComplaintPage param);
}