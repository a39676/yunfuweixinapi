package demo.business.mapper;

import java.util.Date;

import demo.business.domain.po.BusinessExchangeDibPO;

public interface BusinessExchangeDibPOMapper {
    int insert(BusinessExchangeDibPO record);

    int insertSelective(BusinessExchangeDibPO record);
    
    int deleteExpired(Date targetTime);
}