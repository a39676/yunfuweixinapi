package demo.business.mapper;

import demo.business.domain.param.mapperParam.UpdateExchangeDibOrgLimit;
import demo.business.domain.param.mapperParam.UpdateExchangeDibOrgUsed;
import demo.business.domain.po.ExchangeDibOrganizationLimitPO;

public interface ExchangeDibOrganizationLimitPOMapper {
    int insert(ExchangeDibOrganizationLimitPO record);

    int insertSelective(ExchangeDibOrganizationLimitPO record);
    
    int updateLimit(UpdateExchangeDibOrgLimit param);
    
    int updateUsed(UpdateExchangeDibOrgUsed param);
    
    ExchangeDibOrganizationLimitPO findExchangeDibOrganizationLimitByOrgId(Long orgId);
    
}