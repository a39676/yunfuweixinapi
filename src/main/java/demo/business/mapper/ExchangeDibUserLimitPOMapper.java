package demo.business.mapper;

import java.util.Date;

import demo.business.domain.param.mapperParam.UpdateExchangeDibUserLimit;
import demo.business.domain.param.mapperParam.UpdateExchangeDibUserUsed;
import demo.business.domain.po.ExchangeDibUserLimitPO;

public interface ExchangeDibUserLimitPOMapper {
    int insert(ExchangeDibUserLimitPO record);

    int insertSelective(ExchangeDibUserLimitPO record);
    
    int updateLimit(UpdateExchangeDibUserLimit param);
    
    int updateUsed(UpdateExchangeDibUserUsed param);
    
    ExchangeDibUserLimitPO findExchangeDibUserLimit(Long userId);
    
    int initOrUpdateDefaultUserLimit(ExchangeDibUserLimitPO po);
    
    Integer deleteExpiredExchangeDibUserLimit(Date targetTime);
}