package demo.business.domain.vo;

import demo.business.domain.po.BusinessComplaintPO;

public class BusinessComplaintVO extends BusinessComplaintPO {

	private String orgName;
	private String complaintTypeString;
	private String complaintUserNickName;
	private String complaintUserMobile;
	private String complaintResultString;
    /** 0:未受理, 1:已受理, 2:已拒绝 */ 
	private Integer complaintResult;
	private String receptionTimeString;
	private String staffName;
	private String staffMobile;

	public Integer getComplaintResult() {
		return complaintResult;
	}

	public void setComplaintResult(Integer complaintResult) {
		this.complaintResult = complaintResult;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffMobile() {
		return staffMobile;
	}

	public void setStaffMobile(String staffMobile) {
		this.staffMobile = staffMobile;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getComplaintTypeString() {
		return complaintTypeString;
	}

	public void setComplaintTypeString(String complaintTypeString) {
		this.complaintTypeString = complaintTypeString;
	}

	public String getComplaintUserNickName() {
		return complaintUserNickName;
	}

	public void setComplaintUserNickName(String complaintUserNickName) {
		this.complaintUserNickName = complaintUserNickName;
	}

	public String getComplaintUserMobile() {
		return complaintUserMobile;
	}

	public void setComplaintUserMobile(String complaintUserMobile) {
		this.complaintUserMobile = complaintUserMobile;
	}

	public String getComplaintResultString() {
		return complaintResultString;
	}

	public void setComplaintResultString(String complaintResultString) {
		this.complaintResultString = complaintResultString;
	}

	public String getReceptionTimeString() {
		return receptionTimeString;
	}

	public void setReceptionTimeString(String receptionTimeString) {
		this.receptionTimeString = receptionTimeString;
	}

}