package demo.business.domain.param.mapperParam;

import java.util.Date;

public class UpdateExchangeDibUserLimit {

	private Long orgId;
	private Integer coin01Limit = 0;
	private Integer coin05Limit = 0;
	private Integer coin1Limit = 0;
	private Integer paper1Limit = 0;
	private Integer paper5Limit = 0;
	private Integer paper10Limit = 0;
	private Integer paper20Limit = 0;
	private Integer paper50Limit = 0;
	private Integer paper100Limit = 0;
	private Date updateTime;

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Integer getCoin01Limit() {
		return coin01Limit;
	}

	public void setCoin01Limit(Integer coin01Limit) {
		this.coin01Limit = coin01Limit;
	}

	public Integer getCoin05Limit() {
		return coin05Limit;
	}

	public void setCoin05Limit(Integer coin05Limit) {
		this.coin05Limit = coin05Limit;
	}

	public Integer getCoin1Limit() {
		return coin1Limit;
	}

	public void setCoin1Limit(Integer coin1Limit) {
		this.coin1Limit = coin1Limit;
	}

	public Integer getPaper1Limit() {
		return paper1Limit;
	}

	public void setPaper1Limit(Integer paper1Limit) {
		this.paper1Limit = paper1Limit;
	}

	public Integer getPaper5Limit() {
		return paper5Limit;
	}

	public void setPaper5Limit(Integer paper5Limit) {
		this.paper5Limit = paper5Limit;
	}

	public Integer getPaper10Limit() {
		return paper10Limit;
	}

	public void setPaper10Limit(Integer paper10Limit) {
		this.paper10Limit = paper10Limit;
	}

	public Integer getPaper20Limit() {
		return paper20Limit;
	}

	public void setPaper20Limit(Integer paper20Limit) {
		this.paper20Limit = paper20Limit;
	}

	public Integer getPaper50Limit() {
		return paper50Limit;
	}

	public void setPaper50Limit(Integer paper50Limit) {
		this.paper50Limit = paper50Limit;
	}

	public Integer getPaper100Limit() {
		return paper100Limit;
	}

	public void setPaper100Limit(Integer paper100Limit) {
		this.paper100Limit = paper100Limit;
	}

	@Override
	public String toString() {
		return "UpdateExchangeDibOrgLimit [orgId=" + orgId + ", coin01Limit=" + coin01Limit + ", coin05Limit="
				+ coin05Limit + ", coin1Limit=" + coin1Limit + ", paper1Limit=" + paper1Limit + ", paper5Limit="
				+ paper5Limit + ", paper10Limit=" + paper10Limit + ", paper20Limit=" + paper20Limit + ", paper50Limit="
				+ paper50Limit + ", paper100Limit=" + paper100Limit + "]";
	}

}
