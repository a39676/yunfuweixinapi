package demo.business.domain.param.mapperParam;

import java.util.Date;

import demo.business.domain.type.MoneyType;

public class UpdateExchangeDibUserUsed {

	private Long userId;
	private Integer coin01Used = 0;
	private Integer coin05Used = 0;
	private Integer coin1Used = 0;
	private Integer paper1Used = 0;
	private Integer paper5Used = 0;
	private Integer paper10Used = 0;
	private Integer paper20Used = 0;
	private Integer paper50Used = 0;
	private Integer paper100Used = 0;
	private Date updateTime;
	/** {@link MoneyType} */
	private Integer moneyType;

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getCoin01Used() {
		return coin01Used;
	}

	public void setCoin01Used(Integer coin01Used) {
		this.coin01Used = coin01Used;
	}

	public Integer getCoin05Used() {
		return coin05Used;
	}

	public void setCoin05Used(Integer coin05Used) {
		this.coin05Used = coin05Used;
	}

	public Integer getCoin1Used() {
		return coin1Used;
	}

	public void setCoin1Used(Integer coin1Used) {
		this.coin1Used = coin1Used;
	}

	public Integer getPaper1Used() {
		return paper1Used;
	}

	public void setPaper1Used(Integer paper1Used) {
		this.paper1Used = paper1Used;
	}

	public Integer getPaper5Used() {
		return paper5Used;
	}

	public void setPaper5Used(Integer paper5Used) {
		this.paper5Used = paper5Used;
	}

	public Integer getPaper10Used() {
		return paper10Used;
	}

	public void setPaper10Used(Integer paper10Used) {
		this.paper10Used = paper10Used;
	}

	public Integer getPaper20Used() {
		return paper20Used;
	}

	public void setPaper20Used(Integer paper20Used) {
		this.paper20Used = paper20Used;
	}

	public Integer getPaper50Used() {
		return paper50Used;
	}

	public void setPaper50Used(Integer paper50Used) {
		this.paper50Used = paper50Used;
	}

	public Integer getPaper100Used() {
		return paper100Used;
	}

	public void setPaper100Used(Integer paper100Used) {
		this.paper100Used = paper100Used;
	}

	public Integer getMoneyType() {
		return moneyType;
	}

	public void setMoneyType(Integer moneyType) {
		this.moneyType = moneyType;
	}

	@Override
	public String toString() {
		return "UpdateExchangeDibUserUsed [userId=" + userId + ", coin01Used=" + coin01Used + ", coin05Used="
				+ coin05Used + ", coin1Used=" + coin1Used + ", paper1Used=" + paper1Used + ", paper5Used=" + paper5Used
				+ ", paper10Used=" + paper10Used + ", paper20Used=" + paper20Used + ", paper50Used=" + paper50Used
				+ ", paper100Used=" + paper100Used + ", updateTime=" + updateTime + ", moneyType=" + moneyType + "]";
	}

}
