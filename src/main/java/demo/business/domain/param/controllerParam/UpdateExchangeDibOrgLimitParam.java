package demo.business.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class UpdateExchangeDibOrgLimitParam implements CommonControllerParam {

	private Long orgId;
	private Integer coin01Limit = 0;
	private Integer coin05Limit = 0;
	private Integer coin1Limit = 0;
	private Integer paper1Limit = 0;
	private Integer paper5Limit = 0;
	private Integer paper10Limit = 0;
	private Integer paper20Limit = 0;
	private Integer paper50Limit = 0;
	private Integer paper100Limit = 0;

	
	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Integer getCoin01Limit() {
		return coin01Limit;
	}

	public void setCoin01Limit(Integer coin01Limit) {
		this.coin01Limit = coin01Limit;
	}

	public Integer getCoin05Limit() {
		return coin05Limit;
	}

	public void setCoin05Limit(Integer coin05Limit) {
		this.coin05Limit = coin05Limit;
	}

	public Integer getCoin1Limit() {
		return coin1Limit;
	}

	public void setCoin1Limit(Integer coin1Limit) {
		this.coin1Limit = coin1Limit;
	}

	public Integer getPaper1Limit() {
		return paper1Limit;
	}

	public void setPaper1Limit(Integer paper1Limit) {
		this.paper1Limit = paper1Limit;
	}

	public Integer getPaper5Limit() {
		return paper5Limit;
	}

	public void setPaper5Limit(Integer paper5Limit) {
		this.paper5Limit = paper5Limit;
	}

	public Integer getPaper10Limit() {
		return paper10Limit;
	}

	public void setPaper10Limit(Integer paper10Limit) {
		this.paper10Limit = paper10Limit;
	}

	public Integer getPaper20Limit() {
		return paper20Limit;
	}

	public void setPaper20Limit(Integer paper20Limit) {
		this.paper20Limit = paper20Limit;
	}

	public Integer getPaper50Limit() {
		return paper50Limit;
	}

	public void setPaper50Limit(Integer paper50Limit) {
		this.paper50Limit = paper50Limit;
	}

	public Integer getPaper100Limit() {
		return paper100Limit;
	}

	public void setPaper100Limit(Integer paper100Limit) {
		this.paper100Limit = paper100Limit;
	}

	@Override
	public String toString() {
		return "UpdateExchangeDibOrgLimitParam [orgId=" + orgId + ", coin01Limit=" + coin01Limit + ", coin05Limit="
				+ coin05Limit + ", coin1Limit=" + coin1Limit + ", paper1Limit=" + paper1Limit + ", paper5Limit="
				+ paper5Limit + ", paper10Limit=" + paper10Limit + ", paper20Limit=" + paper20Limit + ", paper50Limit="
				+ paper50Limit + ", paper100Limit=" + paper100Limit + "]";
	}

	@Override
	public UpdateExchangeDibOrgLimitParam fromJson(JSONObject json) {
		UpdateExchangeDibOrgLimitParam param = new UpdateExchangeDibOrgLimitParam();
		if(json.containsKey("orgId") && NumericUtilCustom.matchInteger(json.getString("orgId"))) {
			param.setOrgId(json.getLong("orgId"));
		}
		if(json.containsKey("coin01Limit") && NumericUtilCustom.matchInteger(json.getString("coin01Limit"))) {
			param.setCoin01Limit(json.getInt("coin01Limit"));
		}
		if(json.containsKey("coin05Limit") && NumericUtilCustom.matchInteger(json.getString("coin05Limit"))) {
			param.setCoin05Limit(json.getInt("coin05Limit"));
		}
		if(json.containsKey("coin1Limit") && NumericUtilCustom.matchInteger(json.getString("coin1Limit"))) {
			param.setCoin1Limit(json.getInt("coin1Limit"));
		}
		if(json.containsKey("paper1Limit") && NumericUtilCustom.matchInteger(json.getString("paper1Limit"))) {
			param.setPaper1Limit(json.getInt("paper1Limit"));
		}
		if(json.containsKey("paper5Limit") && NumericUtilCustom.matchInteger(json.getString("paper5Limit"))) {
			param.setPaper5Limit(json.getInt("paper5Limit"));
		}
		if(json.containsKey("paper10Limit") && NumericUtilCustom.matchInteger(json.getString("paper10Limit"))) {
			param.setPaper10Limit(json.getInt("paper10Limit"));
		}
		if(json.containsKey("paper20Limit") && NumericUtilCustom.matchInteger(json.getString("paper20Limit"))) {
			param.setPaper20Limit(json.getInt("paper20Limit"));
		}
		if(json.containsKey("paper50Limit") && NumericUtilCustom.matchInteger(json.getString("paper50Limit"))) {
			param.setPaper50Limit(json.getInt("paper50Limit"));
		}
		if(json.containsKey("paper100Limit") && NumericUtilCustom.matchInteger(json.getString("paper100Limit"))) {
			param.setPaper100Limit(json.getInt("paper100Limit"));
		}
		return param;
	}

}
