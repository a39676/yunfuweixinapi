package demo.business.domain.param.controllerParam;

import demo.business.domain.type.ComplaintType;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class AddComplaintParam implements CommonControllerParam {

	private Long orgId;
	/** {@link ComplaintType}*/
	private Integer complaintType;
	private String content;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Integer getComplaintType() {
		return complaintType;
	}

	public void setComplaintType(Integer complaintType) {
		this.complaintType = complaintType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public AddComplaintParam fromJson(JSONObject json) {
		AddComplaintParam param = new AddComplaintParam();
		if (json.containsKey("orgId") && NumericUtilCustom.matchInteger(json.getString("orgId"))) {
			param.setOrgId(json.getLong("orgId"));
		}
		if (json.containsKey("complaintType") && NumericUtilCustom.matchInteger(json.getString("complaintType"))) {
			param.setComplaintType(json.getInt("complaintType"));
		}
		if (json.containsKey("content")) {
			param.setContent(json.getString("content"));
		}
		return param;
	}

}
