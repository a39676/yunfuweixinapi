package demo.business.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class SendComplaintParam implements CommonControllerParam {

	private Long id;
	private Long handlerStaffId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHandlerStaffId() {
		return handlerStaffId;
	}

	public void setHandlerStaffId(Long handlerStaffId) {
		this.handlerStaffId = handlerStaffId;
	}

	@Override
	public String toString() {
		return "SendComplaintParam [id=" + id + ", handlerStaffId=" + handlerStaffId + "]";
	}

	@Override
	public SendComplaintParam fromJson(JSONObject json) {
		SendComplaintParam param = new SendComplaintParam();
		if (json.containsKey("id") && NumericUtilCustom.matchInteger(json.getString("id"))) {
			param.setId(json.getLong("id"));
		}
		if (json.containsKey("handlerStaffId") && NumericUtilCustom.matchInteger(json.getString("handlerStaffId"))) {
			param.setHandlerStaffId(json.getLong("handlerStaffId"));
		}
		return param;
	}

}
