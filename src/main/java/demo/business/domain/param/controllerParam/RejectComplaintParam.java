package demo.business.domain.param.controllerParam;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class RejectComplaintParam implements CommonControllerParam {

	private Long id;
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "RejectComplaintParam [id=" + id + ", remark=" + remark + "]";
	}

	@Override
	public RejectComplaintParam fromJson(JSONObject json) {
		RejectComplaintParam param = new RejectComplaintParam();
		if (json.containsKey("id") && NumericUtilCustom.matchInteger(json.getString("id"))) {
			param.setId(json.getLong("id"));
		}
		if (json.containsKey("remark")) {
			param.setRemark(json.getString("remark"));
		}
		return param;
	}

}
