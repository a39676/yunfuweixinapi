package demo.business.domain.param.controllerParam;

import java.util.Date;

import dateHandle.DateUtilCustom;
import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;
import numericHandel.NumericUtilCustom;

public class FindComplaintPageParam implements CommonControllerParam {

	private Integer pageNo = 1;
	private Integer pageSize = 10;
	/** 0:全部, 1:已处理, 2:已拒绝 */
	private Integer complaintStatus = 0;
	private Date receptionTime;

	@Override
	public FindComplaintPageParam fromJson(JSONObject j) {
		FindComplaintPageParam p = new FindComplaintPageParam();
		if (j.containsKey("pageSize") && NumericUtilCustom.matchInteger(j.getString("pageSize"))) {
			p.setPageSize(j.getInt("pageSize"));
		}
		if (j.containsKey("pageNo") && NumericUtilCustom.matchInteger(j.getString("pageNo"))) {
			p.setPageNo(j.getInt("pageNo"));
		}
		if (j.containsKey("complaintStatus") && NumericUtilCustom.matchInteger(j.getString("complaintStatus"))) {
			p.setComplaintStatus(j.getInt("complaintStatus"));
		}
		if (j.containsKey("receptionTime") && DateUtilCustom.isDateValid(j.getString("receptionTime"))) {
			p.setReceptionTime(DateUtilCustom.stringToDateUnkonwFormat(j.getString("receptionTime")));
		}
		return p;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "FindComplaintPageParam [pageNo=" + pageNo + ", pageSize=" + pageSize + ", complaintStatus="
				+ complaintStatus + ", receptionTime=" + receptionTime + "]";
	}

	public Integer getComplaintStatus() {
		return complaintStatus;
	}

	public void setComplaintStatus(Integer complaintStatus) {
		this.complaintStatus = complaintStatus;
	}

	public Date getReceptionTime() {
		return receptionTime;
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

}
