package demo.business.domain.result;

import demo.business.domain.po.ExchangeDibOrganizationLimitPO;
import demo.common.domain.result.CommonResult;

public class FindExchangeDibOrgLimitResult extends CommonResult {

	private ExchangeDibOrganizationLimitPO exchangeDibOrganizationLimit;

	public ExchangeDibOrganizationLimitPO getExchangeDibOrganizationLimit() {
		return exchangeDibOrganizationLimit;
	}

	public void setExchangeDibOrganizationLimit(ExchangeDibOrganizationLimitPO exchangeDibOrganizationLimit) {
		this.exchangeDibOrganizationLimit = exchangeDibOrganizationLimit;
	}

}
