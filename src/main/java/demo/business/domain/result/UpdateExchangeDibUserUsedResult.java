package demo.business.domain.result;

import demo.common.domain.result.CommonResult;

public class UpdateExchangeDibUserUsedResult extends CommonResult {

	private Long businessId;

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

}
