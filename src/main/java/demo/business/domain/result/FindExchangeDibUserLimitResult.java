package demo.business.domain.result;

import demo.business.domain.po.ExchangeDibUserLimitPO;
import demo.common.domain.result.CommonResult;

public class FindExchangeDibUserLimitResult extends CommonResult {

	private ExchangeDibUserLimitPO userLimit;

	public ExchangeDibUserLimitPO getUserLimit() {
		return userLimit;
	}

	public void setUserLimit(ExchangeDibUserLimitPO userLimit) {
		this.userLimit = userLimit;
	}

}
