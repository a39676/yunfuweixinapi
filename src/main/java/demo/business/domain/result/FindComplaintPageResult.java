package demo.business.domain.result;

import java.util.List;

import demo.business.domain.vo.BusinessComplaintVO;
import demo.common.domain.result.CommonResult;

public class FindComplaintPageResult extends CommonResult {

	private List<BusinessComplaintVO> complaintList;
	private Integer resultcount = 0;

	public List<BusinessComplaintVO> getComplaintList() {
		return complaintList;
	}

	public void setComplaintList(List<BusinessComplaintVO> complaintList) {
		this.complaintList = complaintList;
	}

	public Integer getResultcount() {
		return resultcount;
	}

	public void setResultcount(Integer resultcount) {
		this.resultcount = resultcount;
	}

}
