package demo.business.domain.type;

public enum MoneyType {
	
	/** 纸币 */
	paper("纸币", 1),
	/** 硬币 */
	coin("硬币", 2),
	/** 残钞 */
	broken("残钞", 3),
	;
	
	
	private String typeName;
	private Integer typeCode;
	
	MoneyType(String name, Integer code) {
		this.typeName = name;
		this.typeCode = code;
	}

	public String getName() {
		return this.typeName;
	}
	
	public Integer getCode() {
		return this.typeCode;
	}
	
	public static MoneyType getType(Integer typeCode) {
		for(MoneyType t : MoneyType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}
}
