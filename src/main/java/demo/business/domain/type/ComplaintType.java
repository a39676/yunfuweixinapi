package demo.business.domain.type;

public enum ComplaintType {
	
	/** 金融消费侵权 */
	financialConsumptionInfringement("金融消费侵权", 1),
	/** 金融违法活动 */
	financialIllegalActivities("金融违法活动", 2),
	/** 非法金融广告 */
	illegalFinancialAdvertising("非法金融广告", 3),
	/** 其他 */
	other("其他", 4),
	;
	
	
	private String typeName;
	private Integer typeCode;
	
	ComplaintType(String name, Integer code) {
		this.typeName = name;
		this.typeCode = code;
	}

	public String getName() {
		return this.typeName;
	}
	
	public Integer getCode() {
		return this.typeCode;
	}
	
	public static ComplaintType getType(Integer typeCode) {
		for(ComplaintType t : ComplaintType.values()) {
			if(t.getCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}
}
