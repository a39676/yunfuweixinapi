package demo.business.domain.po;

import java.util.Date;

public class BusinessComplaintPO {
	private Long id;

	private Long complaintUserId;

	private Long orgId;

	private Integer complaintType;

	private String content;

	private Date createTime;

	private Boolean isReject;

	private Boolean isDelete;

	private Long handlerStaffId;

	private Date receptionTime;

	private String remark;

	public Date getReceptionTime() {
		return receptionTime;
	}

	public void setReceptionTime(Date receptionTime) {
		this.receptionTime = receptionTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getComplaintUserId() {
		return complaintUserId;
	}

	public void setComplaintUserId(Long complaintUserId) {
		this.complaintUserId = complaintUserId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Integer getComplaintType() {
		return complaintType;
	}

	public void setComplaintType(Integer complaintType) {
		this.complaintType = complaintType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Boolean getIsReject() {
		return isReject;
	}

	public void setIsReject(Boolean isReject) {
		this.isReject = isReject;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getHandlerStaffId() {
		return handlerStaffId;
	}

	public void setHandlerStaffId(Long handlerStaffId) {
		this.handlerStaffId = handlerStaffId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}