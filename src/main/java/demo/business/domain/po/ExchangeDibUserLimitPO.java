package demo.business.domain.po;

import java.util.Date;

public class ExchangeDibUserLimitPO {
    private Long userId;

    private Date createTime;

    private Date updateTime;

    private Integer coin01Limit = 0;

    private Integer coin01Used;

    private Integer coin05Limit = 0;

    private Integer coin05Used;

    private Integer coin1Limit = 0;

    private Integer coin1Used;

    private Integer paper1Limit = 0;

    private Integer paper1Used;

    private Integer paper5Limit = 0;

    private Integer paper5Used;

    private Integer paper10Limit = 0;

    private Integer paper10Used;

    private Integer paper20Limit = 0;

    private Integer paper20Used;

    private Integer paper50Limit = 0;

    private Integer paper50Used;

    private Integer paper100Limit = 0;

    private Integer paper100Used;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getCoin01Limit() {
		return coin01Limit;
	}

	public void setCoin01Limit(Integer coin01Limit) {
		this.coin01Limit = coin01Limit;
	}

	public Integer getCoin01Used() {
		return coin01Used;
	}

	public void setCoin01Used(Integer coin01Used) {
		this.coin01Used = coin01Used;
	}

	public Integer getCoin05Limit() {
		return coin05Limit;
	}

	public void setCoin05Limit(Integer coin05Limit) {
		this.coin05Limit = coin05Limit;
	}

	public Integer getCoin05Used() {
		return coin05Used;
	}

	public void setCoin05Used(Integer coin05Used) {
		this.coin05Used = coin05Used;
	}

	public Integer getCoin1Limit() {
		return coin1Limit;
	}

	public void setCoin1Limit(Integer coin1Limit) {
		this.coin1Limit = coin1Limit;
	}

	public Integer getCoin1Used() {
		return coin1Used;
	}

	public void setCoin1Used(Integer coin1Used) {
		this.coin1Used = coin1Used;
	}

	public Integer getPaper1Limit() {
		return paper1Limit;
	}

	public void setPaper1Limit(Integer paper1Limit) {
		this.paper1Limit = paper1Limit;
	}

	public Integer getPaper1Used() {
		return paper1Used;
	}

	public void setPaper1Used(Integer paper1Used) {
		this.paper1Used = paper1Used;
	}

	public Integer getPaper5Limit() {
		return paper5Limit;
	}

	public void setPaper5Limit(Integer paper5Limit) {
		this.paper5Limit = paper5Limit;
	}

	public Integer getPaper5Used() {
		return paper5Used;
	}

	public void setPaper5Used(Integer paper5Used) {
		this.paper5Used = paper5Used;
	}

	public Integer getPaper10Limit() {
		return paper10Limit;
	}

	public void setPaper10Limit(Integer paper10Limit) {
		this.paper10Limit = paper10Limit;
	}

	public Integer getPaper10Used() {
		return paper10Used;
	}

	public void setPaper10Used(Integer paper10Used) {
		this.paper10Used = paper10Used;
	}

	public Integer getPaper20Limit() {
		return paper20Limit;
	}

	public void setPaper20Limit(Integer paper20Limit) {
		this.paper20Limit = paper20Limit;
	}

	public Integer getPaper20Used() {
		return paper20Used;
	}

	public void setPaper20Used(Integer paper20Used) {
		this.paper20Used = paper20Used;
	}

	public Integer getPaper50Limit() {
		return paper50Limit;
	}

	public void setPaper50Limit(Integer paper50Limit) {
		this.paper50Limit = paper50Limit;
	}

	public Integer getPaper50Used() {
		return paper50Used;
	}

	public void setPaper50Used(Integer paper50Used) {
		this.paper50Used = paper50Used;
	}

	public Integer getPaper100Limit() {
		return paper100Limit;
	}

	public void setPaper100Limit(Integer paper100Limit) {
		this.paper100Limit = paper100Limit;
	}

	public Integer getPaper100Used() {
		return paper100Used;
	}

	public void setPaper100Used(Integer paper100Used) {
		this.paper100Used = paper100Used;
	}

    
}