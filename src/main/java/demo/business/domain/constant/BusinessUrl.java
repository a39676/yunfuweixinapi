package demo.business.domain.constant;

public class BusinessUrl {

	public static final String root = "/business";
	public static final String updateExchangeDibOrgLimit = "/updateExchangeDibOrgLimit";
//	public static final String updateExchangeDibOrgUsed = "/updateExchangeDibOrgUsed";
	public static final String initOrUpdateDefaultUserLimit = "/initOrUpdateDefaultUserLimit";
	public static final String findExchangeDibUserLimit = "/findExchangeDibUserLimit";
//	public static final String updateExchangeDibUserUsed = "/updateExchangeDibUserUsed";
	public static final String findExchangeDibOrgLimit = "/findExchangeDibOrgLimit";
	public static final String addComplaint = "/addComplaint";
	public static final String rejectComplaint = "/rejectComplaint";
	public static final String sendComplaint = "/sendComplaint";
	public static final String findComplaintPage = "/findComplaintPage";
	
}
