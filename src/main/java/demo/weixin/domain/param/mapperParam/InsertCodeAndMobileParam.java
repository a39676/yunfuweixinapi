package demo.weixin.domain.param.mapperParam;

public class InsertCodeAndMobileParam {

	private String wxCode;
	private String mobile;

	public String getWxCode() {
		return wxCode;
	}

	public void setWxCode(String wxCode) {
		this.wxCode = wxCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "InsertCodeAndMobileParam [wxCode=" + wxCode + ", mobile=" + mobile + "]";
	}

}
