package demo.weixin.domain.param.controllerParam;

public class MessageTempLateSend {

	private String openId;
	private String templateId;

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String toString() {
		return "MessageTempLateSend [openId=" + openId + ", templateId=" + templateId + "]";
	}

}
