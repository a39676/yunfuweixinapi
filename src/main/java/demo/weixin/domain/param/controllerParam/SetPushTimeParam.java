package demo.weixin.domain.param.controllerParam;

import java.text.SimpleDateFormat;
import java.util.Date;

import demo.common.domain.param.CommonControllerParam;
import net.sf.json.JSONObject;

public class SetPushTimeParam implements CommonControllerParam {

	private Date pushStartTime;

	private Date pushStopTime;

	public Date getPushStartTime() {
		return pushStartTime;
	}

	public void setPushStartTime(Date pushStartTime) {
		this.pushStartTime = pushStartTime;
	}

	public Date getPushStopTime() {
		return pushStopTime;
	}

	public void setPushStopTime(Date pushStopTime) {
		this.pushStopTime = pushStopTime;
	}

	@Override
	public SetPushTimeParam fromJson(JSONObject j) {
		SetPushTimeParam p = new SetPushTimeParam();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (j.containsKey("pushStartTime")) {
			try {
				p.setPushStartTime(sdf.parse(j.getString("pushStartTime")));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (j.containsKey("pushStopTime")) {
			try {
				p.setPushStopTime(sdf.parse(j.getString("pushStopTime")));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return p;
	}

}
