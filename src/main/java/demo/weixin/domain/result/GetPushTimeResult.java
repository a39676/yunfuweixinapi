package demo.weixin.domain.result;

import java.util.Date;

import demo.common.domain.result.CommonResult;

public class GetPushTimeResult extends CommonResult {

	private Date pushStartTime;
	private String pushStartTimeStr;
	private Date pushStopTime;
	private String pushStopTimeStr;

	public Date getPushStartTime() {
		return pushStartTime;
	}

	public void setPushStartTime(Date pushStartTime) {
		this.pushStartTime = pushStartTime;
	}

	public String getPushStartTimeStr() {
		return pushStartTimeStr;
	}

	public void setPushStartTimeStr(String pushStartTimeStr) {
		this.pushStartTimeStr = pushStartTimeStr;
	}

	public Date getPushStopTime() {
		return pushStopTime;
	}

	public void setPushStopTime(Date pushStopTime) {
		this.pushStopTime = pushStopTime;
	}

	public String getPushStopTimeStr() {
		return pushStopTimeStr;
	}

	public void setPushStopTimeStr(String pushStopTimeStr) {
		this.pushStopTimeStr = pushStopTimeStr;
	}

}