package demo.weixin.domain.result;

import demo.common.domain.result.CommonResult;
import demo.weixin.domain.type.WeixinResultType;

public class WeixinCommonResult extends CommonResult {

	public void fillWithResult(WeixinResultType resultType) {
		this.setResult(resultType.getCode());
		this.setMessage(resultType.getName());
	}
	
	protected WeixinCommonResult buildResult(WeixinCommonResult oldResult) {
		return oldResult;
	}
}
