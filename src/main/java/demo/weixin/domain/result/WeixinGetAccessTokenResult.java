package demo.weixin.domain.result;

public class WeixinGetAccessTokenResult extends WeixinCommonResult {

	private String accessToken;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	@Override
	public WeixinGetAccessTokenResult buildResult(WeixinCommonResult oldResult) {
		WeixinGetAccessTokenResult result = new WeixinGetAccessTokenResult();
		if(oldResult == null) {
			return result;
		}
		result.setJson(oldResult.getJson());
		result.setMessage(oldResult.getMessage());
		result.setResult(oldResult.getResult());
		return result;
	}

	@Override
	public String toString() {
		return "WeixinGetAccessTokenResult [accessToken=" + accessToken + "]";
	}

}
