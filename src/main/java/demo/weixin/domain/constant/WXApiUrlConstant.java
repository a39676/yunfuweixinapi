package demo.weixin.domain.constant;

public class WXApiUrlConstant {

	public static final String urlPrefix = "https://api.weixin.qq.com/";
	public static final String cgiBin = "cgi-bin/";
	
	public static final String getToken = urlPrefix + cgiBin + "token";
	public static final String messageTempLateSend = urlPrefix + cgiBin + "message/template/send";
	
	public static final String getOpenIdByCode = urlPrefix + "sns/oauth2/access_token";
}
