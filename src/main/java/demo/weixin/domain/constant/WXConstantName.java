package demo.weixin.domain.constant;

public class WXConstantName {

	public static final String appid = "appid";
	public static final String appsecret = "appsecret";
	public static final String grant_type = "client_credential";
	public static final String pushStartTime = "pushStartTime";
	public static final String pushStopTime = "pushStopTime";

}
