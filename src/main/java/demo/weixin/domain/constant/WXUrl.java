package demo.weixin.domain.constant;

public class WXUrl {
	
	public static final String root = "/weixin";
	public static final String tokenTest = "/tokenTest";
	public static final String weixin = "/weixin"; 
	public static final String setPushTime = "/setPushTime";
	public static final String getPushTime = "/getPushTime";
	
}
