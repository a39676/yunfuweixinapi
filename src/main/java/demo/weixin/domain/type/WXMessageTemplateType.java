package demo.weixin.domain.type;

/** 微信消息模板类型 */
public enum WXMessageTemplateType {

	/** 预约成功 */
	reservationSuccess("预约成功", "Xjw_YJnalScnHSmis_MxXcO5xWEu3TIzHZYpRnYQbNc"),
	/** 预约失败 */
	reservationFail("预约失败", "k5SRFrM5jRnXfwshdkDyTfC2mfLymriE1fvXlpaI3aU"),
	/** 投诉建议 */
	complaint("投诉建议", "hRwNQmMbGcaodKrdrWwiALRQt1VOImQ0Bf18hKalmnY"),
	/** 预约通知(发送至后台人员) */
	reservationNotiy("预约通知", "h7N1ItsAjyLfkeaRm_QPAA_dbU-cW4BSnfoYepKzq1A"),

	;

	private String name;
	private String code;

	WXMessageTemplateType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static WXEventType getType(String typeName) {
		for (WXEventType t : WXEventType.values()) {
			if (t.getName().equals(typeName)) {
				return t;
			}
		}
		return null;
	}

}
