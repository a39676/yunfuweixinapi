package demo.weixin.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import demo.weixin.domain.param.mapperParam.InsertCodeAndMobileParam;
import demo.weixin.domain.po.WXStateOid;

public interface WXStateOidMapper {
	int insert(WXStateOid record);

	int insertSelective(WXStateOid record);

	int insertCodeAndMobile(InsertCodeAndMobileParam param);

	WXStateOid findOne(Long id);

	int updateOpenIdByWXCode(@Param("WXCode") String WXCode, @Param("openId") String openId);

	String findOpenIdByMobile(@Param("mobile") Long mobile);

	int deleteOldRecord(@Param("targetTime") Date targetTime);
	
	List<String> findCodeWithoutOpenId();
	
	List<WXStateOid> findOpenIdAndMobile();
}