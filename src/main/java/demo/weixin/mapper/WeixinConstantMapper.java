package demo.weixin.mapper;

import org.apache.ibatis.annotations.Param;

import demo.weixin.domain.po.WeixinConstant;

public interface WeixinConstantMapper {
    int insert(WeixinConstant record);

    int insertSelective(WeixinConstant record);
    
    String getConstant(String constantName);
    
    int updateConstant(@Param("constantName")String constantName, @Param("constantValue")String constantValue);
}