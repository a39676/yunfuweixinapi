package demo.weixin.mapper;

import java.util.List;

import demo.weixin.domain.po.WXMessageLine;

public interface WXMessageLineMapper {
    int insert(WXMessageLine record);

    int insertSelective(WXMessageLine record);
    
    List<WXMessageLine> findNotSendYet();
    
    int batchUpdateDelete(List<Long> idList);
    
    int deleteByMark();
    
}