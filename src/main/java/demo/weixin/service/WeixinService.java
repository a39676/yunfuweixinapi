package demo.weixin.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import demo.common.domain.result.CommonResult;
import demo.weixin.domain.param.controllerParam.SetPushTimeParam;
import demo.weixin.domain.po.WXStateOid;
import demo.weixin.domain.result.GetPushTimeResult;
import demo.weixin.domain.result.WeixinGetAccessTokenResult;

public interface WeixinService {

	String weixinTokenTest(String signature, String timestamp, String nonce, String echostr);

	WeixinGetAccessTokenResult getNewToken() throws Exception;
	
	/** 刷新微信推送时间区段 */
	void reloadPushHour();

	/** 设置微信推送时间区段 */
	CommonResult setPushTime(SetPushTimeParam param);

	/** 查询微信推送时间区段 */
	void getNewWXMessage(HttpServletRequest request) throws IOException;

	/**
	 * 推送预约成功通知
	 * @param toUserOpenId
	 * @param title
	 * @param orgName 接收预约机构名称
	 * @param orgAddress 接收预约机构地址
	 * @param reservationTime 预约时间
	 * @param remark 备注(自定义内容)
	 * @return
	 * @throws Exception
	 */
	CommonResult sendReservationSecuss(String toUserOpenId, String title, String orgName, String orgAddress,
			String reservationTime, String remark) throws Exception;

	/**
	 * 推送预约失败通知 
	 * @param toUserOpenId
	 * @param title
	 * @param reservationTime 原预约时间
	 * @param rejectRemark 拒绝预约原因
	 * @param remark 备注(自定义内容)
	 * @return
	 * @throws Exception
	 */
	CommonResult sendReservationFail(String toUserOpenId, String title, String reservationTime, String rejectRemark,
			String remark) throws Exception;
	
	/**
	 *  推送预约申请通知
	 * @param toUserOpenId
	 * @param title
	 * @param orgName 机构名称
	 * @param orgAddress 机构地址
	 * @param reservationTime 预约时间
	 * @param remark 备注(自定义内容)
	 * @param pushToCustomer 是否发送至客户(发送至客户的微信消息模板, 不受推送时间范围制约)
	 * @return
	 * @throws Exception
	 */
	CommonResult sendReservationApplication(String toUserOpenId, String title, String orgName, String orgAddress,
			String reservationTime, String remark, boolean pushToCustomer) throws Exception;

	CommonResult insertCodeAndMobile(String wxCode, String mobile);

	void getOpenIdByCode();

	/** 查找已经返回openId的手机号 */
	List<WXStateOid> findOpenIdAndMobile();

	
	/** 删除过期无效记录 */
	void deleteOldRecord();

	GetPushTimeResult getPushTime();

	/** 是否推送时间 */
	boolean isPushTime();

	void sendMessageLine();

	void deleteByMark();

	/**
	 * 推送投诉信息至协会人员
	 * @param toUserOpenId
	 * @param title
	 * @param customerNickName
	 * @param customerMobile
	 * @param complaintTime
	 * @param content
	 * @param remark
	 * @return
	 * @throws Exception
	 */
	CommonResult addComplaint(String toUserOpenId, String title, String customerNickName, String customerMobile,
			String complaintTime, String content, String remark) throws Exception;

//	/**
//	 * 推送预约消息至后台工作人员
//	 * @param toUserOpenId
//	 * @param title
//	 * @param orgName
//	 * @param orgAddress
//	 * @param reservationTime
//	 * @param remark
//	 * @return
//	 * @throws Exception
//	 */
//	CommonResult sendReservationApplication(String toUserOpenId, String title, String orgName, String orgAddress,
//			String reservationTime, String remark) throws Exception;

}
