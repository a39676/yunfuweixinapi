package demo.weixin.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dateHandle.DateUtilCustom;
import demo.article.domain.po.ArticlePO;
import demo.article.mapper.ArticlePOMapper;
import demo.common.domain.result.CommonResult;
import demo.common.domain.type.ResultTypeBase;
import demo.common.service.CommonService;
import demo.weixin.domain.constant.WXApiUrlConstant;
import demo.weixin.domain.constant.WXConstantName;
import demo.weixin.domain.param.controllerParam.SetPushTimeParam;
import demo.weixin.domain.param.mapperParam.InsertCodeAndMobileParam;
import demo.weixin.domain.po.WXMessageLine;
import demo.weixin.domain.po.WXStateOid;
import demo.weixin.domain.po.WeixinAccessToken;
import demo.weixin.domain.result.GetPushTimeResult;
import demo.weixin.domain.result.WeixinCommonResult;
import demo.weixin.domain.result.WeixinGetAccessTokenResult;
import demo.weixin.domain.type.WXMessageTemplateType;
import demo.weixin.domain.type.WeixinResultType;
import demo.weixin.mapper.WXMessageLineMapper;
import demo.weixin.mapper.WXStateOidMapper;
import demo.weixin.mapper.WeixinAccessTokenMapper;
import demo.weixin.mapper.WeixinConstantMapper;
import demo.weixin.service.WeixinService;
import httpHandel.HttpUtil;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;
import numericHandel.NumericUtilCustom;

@Service
public class WeixinServiceImpl extends CommonService implements WeixinService {

	@Autowired
	private WeixinConstantMapper weixinConstantMapper;
	@Autowired
	private WeixinAccessTokenMapper weixinAccessTokenMapper;
	@Autowired
	private WXStateOidMapper wxStateOidMapper;
	private static String token = "weixinTestToken";
	private static LocalDateTime pushStartTime = null;
	private static LocalDateTime pushStopTime = null;

	@Autowired
	private ArticlePOMapper articleMapper;
	@Autowired
	private WXMessageLineMapper wxMessageLineMapper;
	
	static {
		LocalDateTime tmpLDT = LocalDateTime.now();
		tmpLDT.withHour(9).withMinute(0).withSecond(0);
		pushStartTime = tmpLDT;
		tmpLDT = LocalDateTime.now();
		tmpLDT.withHour(16).withMinute(59).withSecond(59);
		pushStopTime = tmpLDT;
	}

	@Override
	public void reloadPushHour() {
		// TODO
		String startTime = weixinConstantMapper.getConstant(WXConstantName.pushStartTime);
		String stopTime = weixinConstantMapper.getConstant(WXConstantName.pushStopTime);
		if (DateUtilCustom.isDateValid(startTime)) {
			pushStartTime = LocalDateTime.parse(startTime);
		}
		if (DateUtilCustom.isDateValid(stopTime)) {
			pushStopTime = LocalDateTime.parse(stopTime);
		}
	}

	public boolean isPushTime(Date dateInput) {
		if (pushStartTime == null || pushStopTime == null) {
			reloadPushHour();
			if (pushStartTime == null || pushStopTime == null) {
				return false;
			}
		}
		LocalDateTime localDT = DateUtilCustom.dateToLocalDateTime(dateInput);
		if (localDT.getHour() >= pushStartTime.getHour() && localDT.getMinute() >= pushStartTime.getMinute()
				&& localDT.getHour() <= pushStopTime.getHour() && localDT.getMinute() <= pushStopTime.getMinute()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public CommonResult setPushTime(SetPushTimeParam param) {
		if (param.getPushStartTime() == null || param.getPushStopTime() == null) {
			return nullParam();
		}

		if (param.getPushStartTime().after(param.getPushStopTime())) {
			return errorParam();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		weixinConstantMapper.updateConstant(WXConstantName.pushStartTime, sdf.format(param.getPushStartTime()));
		weixinConstantMapper.updateConstant(WXConstantName.pushStopTime, sdf.format(param.getPushStopTime()));

		return normalSuccess();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public GetPushTimeResult getPushTime() {
		GetPushTimeResult result = new GetPushTimeResult();
		String pushStartTimeStr = weixinConstantMapper.getConstant(WXConstantName.pushStartTime);
		String pushStopTimeStr = weixinConstantMapper.getConstant(WXConstantName.pushStopTime);
		
		Date startTime = null;
		Date stopTime = null;
		if(StringUtils.isBlank(pushStartTimeStr)) {
			pushStartTimeStr = pushStartTime.getHour() + ":" + pushStartTime.getMinute();
			startTime = DateUtilCustom.localDateTimeToDate(pushStartTime);
		} else {
			startTime = DateUtilCustom.stringToDateUnkonwFormat(pushStartTimeStr);
			String min = String.valueOf(startTime.getMinutes());
			if(min.length() < 2) {
				min = "0" + min;
			}
			pushStartTimeStr = startTime.getHours() + ":" + min;
		}
		
		
		if(StringUtils.isBlank(pushStopTimeStr)) {
			pushStopTimeStr = pushStopTime.getHour() + ":" + pushStopTime.getMinute();
			stopTime = DateUtilCustom.localDateTimeToDate(pushStopTime);
		} else {
			stopTime = DateUtilCustom.stringToDateUnkonwFormat(pushStopTimeStr);
			String min = String.valueOf(stopTime.getMinutes());
			if(min.length() < 2) {
				min = "0" + min;
			}
			pushStopTimeStr = stopTime.getHours() + ":" + min;
		}
		
		result.setPushStartTime(startTime);
		result.setPushStartTimeStr(pushStartTimeStr);
		result.setPushStopTime(stopTime);
		result.setPushStopTimeStr(pushStopTimeStr);
		result.setIsSuccess();
		
		return result;
	}
	
	@Override
	@SuppressWarnings("deprecation")
	public boolean isPushTime() {
		GetPushTimeResult pushTimeResult = getPushTime();
		Date pushStartTime = pushTimeResult.getPushStartTime();
		Date pushStopTime = pushTimeResult.getPushStopTime();
		
		Date now = new Date();
		if(pushStartTime.getHours() > now.getHours() || pushStopTime.getHours() < now.getHours()) {
			return false;
		}
		
		if(pushStartTime.getHours() == now.getHours() && pushStartTime.getMinutes() > now.getMinutes()) {
			return false;
		}
		
		if(pushStopTime.getHours() == now.getHours() && pushStopTime.getMinutes() < now.getMinutes()) {
			return false;
		}
		return true;
	}

	@Override
	public String weixinTokenTest(String signature, String timestamp, String nonce, String echostr) {
		StringBuffer sb = new StringBuffer("signature:" + signature + ", timestamp:" + timestamp + ", nonce:" + nonce
				+ ", echostr:" + echostr + ";\n");
		if (StringUtils.isAnyBlank(signature, timestamp, nonce, echostr)) {
			sb.append("fail: " + new Date() + "\n");
			return "false";
		}

		if (checkSignature(signature, timestamp, nonce)) {
			sb.append("success: " + new Date() + "\n");
			return echostr;
		} else {
			sb.append("fail: " + new Date() + "\n");
			return "false";
		}
	}

	private boolean checkSignature(String signature, String timestamp, String nonce) {
		// 拼接字符串
		String[] arr = new String[] { token, timestamp, nonce };
		// 排序
		Arrays.sort(arr);
		// 生成字符串
		StringBuffer content = new StringBuffer();
		for (int i = 0; i < arr.length; i++) {
			content.append(arr[i]);
		}
		// SHA1加密
		String tmp = SHA1Encrypt(content.toString());
		return tmp.equals(signature);
	}

	private String SHA1Encrypt(String decript) {
		try {
			MessageDigest digest = java.security.MessageDigest.getInstance("SHA-1");
			digest.update(decript.getBytes());
			byte messageDigest[] = digest.digest();
			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			// 字节数组转换为 十六进制 数
			for (int i = 0; i < messageDigest.length; i++) {
				String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
				if (shaHex.length() < 2) {
					hexString.append(0);
				}
				hexString.append(shaHex);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public WeixinGetAccessTokenResult getNewToken() throws Exception {
		WeixinGetAccessTokenResult errorResult = new WeixinGetAccessTokenResult();

		StringBuffer urlStr = new StringBuffer(WXApiUrlConstant.getToken);
		String appid = weixinConstantMapper.getConstant(WXConstantName.appid);
		String secret = weixinConstantMapper.getConstant(WXConstantName.appsecret);
		if (StringUtils.isAnyBlank(appid, secret)) {
			errorResult.fillWithResult(ResultTypeBase.serviceError);
			return errorResult;
		}

		urlStr.append("?appid=" + appid);
		urlStr.append("&secret=" + secret);
		urlStr.append("&grant_type=" + WXConstantName.grant_type);

		HttpUtil httpUtil = new HttpUtil();
		String httpResult = httpUtil.sendGet(urlStr.toString());

		WeixinGetAccessTokenResult result = new WeixinGetAccessTokenResult()
				.buildResult(createWeixinResultFromResponseString(httpResult));

		String accessToken = null;
		if (result.isSuccess()) {
			accessToken = result.getJson().getString("access_token");
			result.setAccessToken(accessToken);
		} else {
			return result;
		}

		if (StringUtils.isBlank(accessToken)) {
			errorResult.fillWithResult(ResultTypeBase.communicationError);
			return errorResult;
		} else {
			WeixinAccessToken accessTokenPO = new WeixinAccessToken();
			accessTokenPO.setCreateTime(new Date());
			accessTokenPO.setToken(result.getAccessToken());
			weixinAccessTokenMapper.insertSelective(accessTokenPO);
		}

		return result;
	}

	private WeixinCommonResult createWeixinResultFromResponseString(String httpResult) {
		WeixinCommonResult result = new WeixinCommonResult();

		if (StringUtils.isBlank(httpResult)) {
			result.fillWithResult(ResultTypeBase.communicationError);
			return result;
		}

		JSONObject tmpJson = null;
		try {
			tmpJson = JSONObject.fromObject(httpResult);
		} catch (Exception e) {
			result.fillWithResult(ResultTypeBase.serviceError);
			return result;
		}
		result.setJson(tmpJson);

		if (tmpJson.keySet().size() < 1) {
			result.fillWithResult(WeixinResultType.serviceError);
			return result;
		}

		if (tmpJson.containsKey("errmsg") || tmpJson.containsKey("errcode")) {
			result.setIsFail();
			result.setMessage(tmpJson.getString("errmsg"));
		} else {
			result.setIsSuccess();
		}

		return result;
	}

	@Override
	public void getNewWXMessage(HttpServletRequest request) throws IOException {
		// TODO
		StringBuilder sb = new StringBuilder();
		if (request.getCharacterEncoding() == null) {
			request.setCharacterEncoding("UTF-8");
		}
		BufferedReader reader = request.getReader();
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
		} finally {
			reader.close();
		}

		XMLSerializer xmlSerializer = new XMLSerializer();
		// TODO 
		ArticlePO a = new ArticlePO();
		a.setPath(sb.toString());
		articleMapper.insertSelective(a);
		JSON baseJson = xmlSerializer.read(sb.toString());
		a = new ArticlePO();
		a.setPath(baseJson.toString());
		articleMapper.insertSelective(a);

	}

	private String findToken() throws Exception {
		List<WeixinAccessToken> tokens = weixinAccessTokenMapper.getTokens();
		if (tokens == null || tokens.size() < 1) {
			WeixinGetAccessTokenResult getTokenResult = getNewToken();
			if (!getTokenResult.isSuccess()) {
				return null;
			} else {
				return getTokenResult.getAccessToken();
			}
		} else {
			if(System.currentTimeMillis() - tokens.get(0).getCreateTime().getTime() > 1000L * 60 * 110) {
				WeixinGetAccessTokenResult getTokenResult = getNewToken();
				if (!getTokenResult.isSuccess()) {
					return null;
				} else {
					return getTokenResult.getAccessToken();
				}
			}
			return tokens.get(0).getToken();
		}
	}

	@Override
	public CommonResult sendReservationSecuss(String toUserOpenId, String title, String orgName, String orgAddress,
			String reservationTime, String remark) throws Exception {
		String accessToken = findToken();
		if (StringUtils.isBlank(accessToken)) {
			return serviceError();
		}

		JSONObject jsonOutput = new JSONObject();
		jsonOutput.put("touser", toUserOpenId);
		jsonOutput.put("template_id", WXMessageTemplateType.reservationSuccess.getCode());
		JSONObject data = new JSONObject();
		JSONObject tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", title);
		data.put("first", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", orgName);
		data.put("keyword1", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", orgAddress);
		data.put("keyword2", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", reservationTime);
		data.put("keyword3", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", remark);
		data.put("remark", tmpSubDetail);
		jsonOutput.put("data", data);

		CommonResult sendMessageResult = messageTempLateSend(jsonOutput);
		return sendMessageResult;
	}

	@Override
	public CommonResult sendReservationFail(String toUserOpenId, String title, String reservationTime,
			String rejectRemark, String remark) throws Exception {
		String accessToken = findToken();
		if (StringUtils.isBlank(accessToken)) {
			return serviceError();
		}

		JSONObject jsonOutput = new JSONObject();
		jsonOutput.put("touser", toUserOpenId);
		jsonOutput.put("template_id", WXMessageTemplateType.reservationFail.getCode());
		JSONObject data = new JSONObject();
		JSONObject tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", title);
		data.put("first", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", reservationTime);
		data.put("keyword1", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", "");
		data.put("keyword2", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", rejectRemark);
		data.put("keyword3", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", remark);
		data.put("remark", tmpSubDetail);
		jsonOutput.put("data", data);

		CommonResult sendMessageResult = messageTempLateSend(jsonOutput);
		return sendMessageResult;
	}

	@Override
	public CommonResult sendReservationApplication(String toUserOpenId, String title, String orgName, String orgAddress,
			String reservationTime, String remark, boolean pushToCustomer) throws Exception {
		String accessToken = findToken();
		if (StringUtils.isBlank(accessToken)) {
			return serviceError();
		}

		JSONObject jsonOutput = new JSONObject();
		jsonOutput.put("touser", toUserOpenId);
		jsonOutput.put("template_id", WXMessageTemplateType.reservationNotiy.getCode());
		JSONObject data = new JSONObject();
		JSONObject tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", title);
		data.put("first", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", orgName);
		data.put("keyword1", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", orgAddress);
		data.put("keyword2", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", reservationTime);
		data.put("keyword3", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", remark);
		data.put("remark", tmpSubDetail);
		jsonOutput.put("data", data);
		
		CommonResult sendMessageResult = null;
		
		if(isPushTime() || pushToCustomer) {
			sendMessageResult = messageTempLateSend(jsonOutput);
		} else {
			saveToMessageLine(jsonOutput.toString());
			sendMessageResult = new CommonResult();
			sendMessageResult.failWithMessage("非发送时间,已经排入发送队列.");
		}

		return sendMessageResult;
	}
	
	@Override
	public CommonResult addComplaint(String toUserOpenId, String title, String customerNickName, String customerMobile,
			String complaintTime, String content, String remark) throws Exception {
		String accessToken = findToken();
		if (StringUtils.isBlank(accessToken)) {
			return serviceError();
		}

		JSONObject jsonOutput = new JSONObject();
		jsonOutput.put("touser", toUserOpenId);
		jsonOutput.put("template_id", WXMessageTemplateType.complaint.getCode());
		JSONObject data = new JSONObject();
		JSONObject tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", title);
		data.put("first", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", customerNickName);
		data.put("keyword1", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", customerMobile);
		data.put("keyword2", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", complaintTime);
		data.put("keyword3", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", content);
		data.put("keyword4", tmpSubDetail);
		tmpSubDetail = new JSONObject();
		tmpSubDetail.put("value", remark);
		data.put("remark", tmpSubDetail);
		jsonOutput.put("data", data);
		
		CommonResult sendMessageResult = null;
		if(isPushTime()) {
			sendMessageResult = messageTempLateSend(jsonOutput);
		} else {
			saveToMessageLine(jsonOutput.toString());
			sendMessageResult = new CommonResult();
			sendMessageResult.failWithMessage("非发送时间,已经排入发送队列.");
		}

		return sendMessageResult;
	}

	private CommonResult messageTempLateSend(JSONObject jsonOutput) throws Exception {
		String accessToken = findToken();
		if (StringUtils.isBlank(accessToken)) {
			return nullParam();
		}

		StringBuffer urlStr = new StringBuffer(WXApiUrlConstant.messageTempLateSend);
		urlStr.append("?access_token=" + accessToken);

		HttpUtil httpUtil = new HttpUtil();
		String httpResult = httpUtil.sendPost(urlStr.toString(), jsonOutput.toString());

		if (!wxResultSuccess(httpResult)) {
			CommonResult result = new CommonResult();
			result.failWithMessage(httpResult);
			return result;
		}

		return normalSuccess();
	}

	private boolean wxResultSuccess(String httpResult) {
		JSONObject json = null;
		try {
			json = JSONObject.fromObject(httpResult);
		} catch (Exception e) {
			return false;
		}
		if (!json.containsKey("errcode") && "0".equals(json.getString("errcode"))) {
			return true;
		}
		return false;
	}

	@Override
	public CommonResult insertCodeAndMobile(String wxCode, String mobile) {
		if (StringUtils.isBlank(wxCode) || StringUtils.isBlank(mobile)) {
			return nullParam();
		}

		if (!NumericUtilCustom.matchInteger(mobile)) {
			return errorParam();
		}

		InsertCodeAndMobileParam p = new InsertCodeAndMobileParam();
		p.setWxCode(wxCode);
		p.setMobile(mobile);
		wxStateOidMapper.insertCodeAndMobile(p);

		return normalSuccess();
	}

	@Override
	public void getOpenIdByCode() {
		List<String> wxCodeList = wxStateOidMapper.findCodeWithoutOpenId();
		if (wxCodeList == null || wxCodeList.size() < 1) {
			return;
		}

		wxCodeList.stream().forEach(c -> {
			try {
				getOpenIdByCode(c);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private boolean getOpenIdByCode(String wxCode) throws Exception {
		String appid = weixinConstantMapper.getConstant(WXConstantName.appid);
		String secret = weixinConstantMapper.getConstant(WXConstantName.appsecret);
		if (StringUtils.isAnyBlank(appid, secret)) {
			return false;
		}

		StringBuffer urlStr = new StringBuffer(WXApiUrlConstant.getOpenIdByCode);
		urlStr.append("?appid=" + appid);
		urlStr.append("&secret=" + secret);
		urlStr.append("&code=" + wxCode);
		urlStr.append("&grant_type=" + "authorization_code");

		HttpUtil httpUtil = new HttpUtil();
		String httpResult = httpUtil.sendGet(urlStr.toString());

		JSONObject json = null;
		try {
			json = JSONObject.fromObject(httpResult);
		} catch (Exception e) {
			return false;
		}

		if (json.containsKey("openid")) {
			String openId = json.getString("openid");
			wxStateOidMapper.updateOpenIdByWXCode(wxCode, openId);
			return true;
		}
		return false;
	}

	@Override
	public List<WXStateOid> findOpenIdAndMobile() {
		return wxStateOidMapper.findOpenIdAndMobile();
	}

	@Override
	public void deleteOldRecord() {
		LocalDateTime targetTime = LocalDateTime.now().minusMinutes(10);
		wxStateOidMapper.deleteOldRecord(DateUtilCustom.localDateTimeToDate(targetTime));
	}

	public void saveToMessageLine(String msg) {
		WXMessageLine message = new WXMessageLine();
		message.setContent(msg.getBytes());
		wxMessageLineMapper.insertSelective(message);
	}
	
	@Override
	public void sendMessageLine() {
		List<WXMessageLine> messageList = wxMessageLineMapper.findNotSendYet();
		if(messageList == null || messageList.size() < 1) {
			return;
		}
		List<Long> messageIdList = new ArrayList<Long>();
		
		messageList.stream().forEach(m -> {
			try {
				messageTempLateSend(JSONObject.fromObject(m.getContent()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			messageIdList.add(m.getId());
		});
		
		wxMessageLineMapper.batchUpdateDelete(messageIdList);
	}
	
	@Override
	public void deleteByMark() {
		wxMessageLineMapper.deleteByMark();
	}
}
