/**
 * 模块化加载
 * Layui中的模块
 * index.html页面中统一加载所需要用到的模块
 * 模块实例绑定在window对象上以做全局使用
 * 各个被load进来的页面中只需要调用相应组件的init,render等方法来进行元素渲染即可
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 系统配置模块
     * cookie模块
     * base模块
     * verify模块
     */
    require(["./module/config","./module/cookie","./module/base","./module/verify"],function (config, cookie, base, verify) {
        /**
         * 获取用户信息
         */
        $("#userName").html(cookie.get_userName());
        /**
         * 事件绑定
         * 给个人中心按钮绑定单击事件，弹出个人中心页面
         */
        $("#personCenter").on("click",function () {
            layer.open({
                type: 2,
                title: '个人中心',
                shade: [0.3, '#000'],
                area: ['470px', '290px'],
                anim: 0,
                resize: false,
                content: 'personCenter.html?limit=no'
            });
        });
        /**
         * 事件绑定
         * 给安全退出按钮绑定单击事件，实现注销功能
         */
        $("#logout").on("click",function () {
            EasyAjax.post_json({
                    url: config.api_host + "sysUser/logout"
                },
                function (data) {
                    cookie.del_user();
                    window.location.href = "login.html";
                });
        });
        /**
         * 事件绑定
         * 给控制导航菜单伸缩的按钮绑定单击事件，实现伸缩菜单效果
         */
        $("#nav-adjustable").on("click",function () {
            $("#layui-layout-admin").toggleClass("active");
        });
        /**
         * initConfig
         * 初始化配置
         */
        var initConfig = function() {
            /**
             * 默认加载
             * 登陆系统后，默认加载的子页面
             */
            var queryDefaultOpenPage = function () {
                var para = {};
                EasyAjax.post_json({
                        url: config.api_host + "sysFunction/selectSysFunctionDefaultOpenPage",
                        data: para
                    },
                    function (data) {
                        if (data.result && data.result.url){
                            var iframe = $("<iframe frameborder='0' width='100%' height='100%'></iframe>");
                            var src = data.result.url;
                            iframe.attr("src",src);
                            $("#layui-content").html(iframe);
                        }
                    });
            };
            queryDefaultOpenPage();
        };
        /**
         * init
         * 页面初始化
         */
        var init = function () {
            //点击树节点保持单一路径展开逻辑
            var curExpandNode = null;
            var singlePath = function(newNode) {
                if (newNode === curExpandNode) return;
                var zTree = $.fn.zTree.getZTreeObj("menuTree"),
                    rootNodes, tmpRoot, tmpTId, i, j, n;
                if (!curExpandNode) {
                    tmpRoot = newNode;
                    while (tmpRoot) {
                        tmpTId = tmpRoot.tId;
                        tmpRoot = tmpRoot.getParentNode();
                    }
                    rootNodes = zTree.getNodes();
                    for (i=0, j=rootNodes.length; i<j; i++) {
                        n = rootNodes[i];
                        if (n.tId != tmpTId) {
                            zTree.expandNode(n, false);
                        }
                    }
                } else if (curExpandNode && curExpandNode.open) {
                    if (newNode.parentTId === curExpandNode.parentTId) {
                        zTree.expandNode(curExpandNode, false);
                    } else {
                        var newParents = [];
                        while (newNode) {
                            newNode = newNode.getParentNode();
                            if (newNode === curExpandNode) {
                                newParents = null;
                                break;
                            } else if (newNode) {
                                newParents.push(newNode);
                            }
                        }
                        if (newParents!=null) {
                            var oldNode = curExpandNode;
                            var oldParents = [];
                            while (oldNode) {
                                oldNode = oldNode.getParentNode();
                                if (oldNode) {
                                    oldParents.push(oldNode);
                                }
                            }
                            if (newParents.length>0) {
                                zTree.expandNode(oldParents[Math.abs(oldParents.length-newParents.length)-1], false);
                            } else {
                                zTree.expandNode(oldParents[oldParents.length-1], false);
                            }
                        }
                    }
                }
                curExpandNode = newNode;
            };

            var setting = {
                data: {
                    simpleData: {
                        enable: true
                    }
                },
                view: {
                    showLine: false,
                    selectedMulti: false,
                    dblClickExpand: false
                },
                callback: {
                    beforeExpand: function (treeId, treeNode) {
                        var pNode = curExpandNode ? curExpandNode.getParentNode():null;
                        var treeNodeP = treeNode.parentTId ? treeNode.getParentNode():null;
                        var zTree = $.fn.zTree.getZTreeObj("menuTree");
                        for(var i=0, l=!treeNodeP ? 0:treeNodeP.children.length; i<l; i++ ) {
                            if (treeNode !== treeNodeP.children[i]) {
                                zTree.expandNode(treeNodeP.children[i], false);
                            }
                        }
                        while (pNode) {
                            if (pNode === treeNode) {
                                break;
                            }
                            pNode = pNode.getParentNode();
                        }
                        if (!pNode) {
                            singlePath(treeNode);
                        }
                    },
                    onExpand: function (event, treeId, treeNode) {
                        curExpandNode = treeNode;
                    },
                    onClick: function (event, treeId, treeNode) {

                        //点击树节点保持单一路径展开逻辑
                        var zTree = $.fn.zTree.getZTreeObj("menuTree");
                        zTree.expandNode(treeNode, null, null, null, true);

                        //单击树节点，如果单击的不是父节点则控制active的存在与否做到当前选择的节点高亮
                        var aDom = $(event.target).parents('li:first').children('a');
                        if (aDom.prev().hasClass('noline_docu')){
                            $("#menuTree a").removeClass('active');
                            aDom.addClass('active')
                        }

                        if (treeNode.pageUrl){
                            if ($("#layui-content>iframe").attr("src") !== treeNode.pageUrl){
                                var iframe = $("<iframe frameborder='0' width='100%' height='100%'></iframe>");
                                var src = treeNode.pageUrl;
                                iframe.attr("src",src);
                                $("#layui-content").html(iframe);
                            }
                        }
                    }
                }
            };

            var zNodes = [
                { id:4, pId:0, name:"首页",pageUrl:"view/home/home.html",icon:"images/ztree/nav_icon/nav_1.png",iconClose:"images/ztree/nav_icon/nav_1.png",iconOpen:"images/ztree/nav_icon/nav_1_hover.png"},

                { id:1, pId:0, name:"系统管理",pageUrl:"",icon:"images/ztree/nav_icon/nav_1.png",iconClose:"images/ztree/nav_icon/nav_1.png",iconOpen:"images/ztree/nav_icon/nav_1_hover.png"},
                { id:11, pId:1, name:"机构管理",pageUrl:"view/orgManage/orgManage.html",icon:"images/ztree/nav_icon/nav_2.png",iconClose:"images/ztree/nav_icon/nav_2.png",iconOpen:"images/ztree/nav_icon/nav_2_hover.png"},
                { id:12, pId:1, name:"机构组织管理",pageUrl:"view/orgTreeManage/orgTreeManage.html",icon:"images/ztree/nav_icon/nav_3.png",iconClose:"images/ztree/nav_icon/nav_3.png",iconOpen:"images/ztree/nav_icon/nav_3_hover.png"},
                { id:13, pId:1, name:"机构人员管理",pageUrl:"view/userManagement/userManagement.html",icon:"images/ztree/nav_icon/nav_4.png",iconClose:"images/ztree/nav_icon/nav_4.png",iconOpen:"images/ztree/nav_icon/nav_4_hover.png"},
                { id:14, pId:1, name:"信息推送管理",pageUrl:"view/msgPushManage/msgPushManage.html",icon:"images/ztree/nav_icon/nav_5.png",iconClose:"images/ztree/nav_icon/nav_5.png",iconOpen:"images/ztree/nav_icon/nav_5_hover.png"},
                { id:15, pId:1, name:"信息模板管理",pageUrl:"view/msgTplManage/msgTplManage.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},

                { id:2, pId:0, name:"金融服务管理",pageUrl:"",icon:"images/ztree/nav_icon/nav_1.png",iconClose:"images/ztree/nav_icon/nav_1.png",iconOpen:"images/ztree/nav_icon/nav_1_hover.png"},
                { id:21, pId:2, name:"融资产品发布",pageUrl:"view/finaRelease/finaRelease.html",icon:"images/ztree/nav_icon/nav_2.png",iconClose:"images/ztree/nav_icon/nav_2.png",iconOpen:"images/ztree/nav_icon/nav_2_hover.png"},
                { id:22, pId:2, name:"融资产品受理",pageUrl:"view/finaAccepting/finaAccepting.html",icon:"images/ztree/nav_icon/nav_3.png",iconClose:"images/ztree/nav_icon/nav_3.png",iconOpen:"images/ztree/nav_icon/nav_3_hover.png"},
                { id:23, pId:2, name:"理财产品发布",pageUrl:"view/finaCialRelease/finaCialRelease.html",icon:"images/ztree/nav_icon/nav_4.png",iconClose:"images/ztree/nav_icon/nav_4.png",iconOpen:"images/ztree/nav_icon/nav_4_hover.png"},
                { id:24, pId:2, name:"理财产品受理",pageUrl:"view/finaCialAccep/finaCialAccep.html",icon:"images/ztree/nav_icon/nav_5.png",iconClose:"images/ztree/nav_icon/nav_5.png",iconOpen:"images/ztree/nav_icon/nav_5_hover.png"},
                { id:25, pId:2, name:"保险产品发布",pageUrl:"view/finaInsuRelease/finaInsuRelease.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},
                { id:26, pId:2, name:"保险产品受理",pageUrl:"view/finaInsuAccep/finaInsuAccep.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},
                { id:27, pId:2, name:"证券产品发布",pageUrl:"view/finaSecuRelease/finaSecuRelease.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},
                { id:28, pId:2, name:"证券产品受理",pageUrl:"view/finaSecuAccep/finaSecuAccep.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},
                { id:29, pId:2, name:"零钞兑换限额管理",pageUrl:"view/finaExchaAdmin/finaExchaAdmin.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},
                { id:30, pId:2, name:"零钞兑换受理",pageUrl:"view/finaExchaAccep/finaExchaAccep.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},
                { id:31, pId:2, name:"互联网+信用三农产品发布",pageUrl:"view/finaInternetRelease/finaInternetRelease.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},
                { id:32, pId:2, name:"互联网+信用三农产品受理",pageUrl:"view/finaInternetAccep/finaInternetAccep.html",icon:"images/ztree/nav_icon/nav_6.png",iconClose:"images/ztree/nav_icon/nav_6.png",iconOpen:"images/ztree/nav_icon/nav_6_hover.png"},

                { id:3, pId:0, name:"其他管理",pageUrl:"",icon:"images/ztree/nav_icon/nav_15.png",iconClose:"images/ztree/nav_icon/nav_15.png",iconOpen:"images/ztree/nav_icon/nav_15_hover.png"},
                { id:33, pId:3, name:"投诉举报受理",pageUrl:"view/finaComplaAccep/finaComplaAccep.html",icon:"images/ztree/nav_icon/nav_18.png",iconClose:"images/ztree/nav_icon/nav_18.png",iconOpen:"images/ztree/nav_icon/nav_18_hover.png"},
                { id:34, pId:3, name:"知识宣传发布",pageUrl:"view/knowledgePromotion/knowledgePromotion.html",icon:"images/ztree/nav_icon/nav_19.png",iconClose:"images/ztree/nav_icon/nav_19.png",iconOpen:"images/ztree/nav_icon/nav_19_hover.png"},
                { id:35, pId:3, name:"公告发布",pageUrl:"view/announcement/announcement.html",icon:"images/ztree/nav_icon/nav_20.png",iconClose:"images/ztree/nav_icon/nav_20.png",iconOpen:"images/ztree/nav_icon/nav_20_hover.png"},
            ];

            $.fn.zTree.init($("#menuTree"), setting, zNodes);

            //给树节点中a标签绑定鼠标hover事件，为了控制其兄弟(哥哥)元素
            $(document).on("mouseover","#menuTree .noline_close+a",function () {
                var $this = $(this);
                $this.prev().css({
                    "background-image": "url(images/ztree/noline_close_hover.png)"
                });
            });
            $(document).on("mouseout","#menuTree .noline_close+a",function () {
                var $this = $(this);
                $this.prev().css({
                    "background-image": "url(images/ztree/noline_close.png)"
                });
            });

        };
        /**
         * 页面初始化
         */
        init(); //页面初始化
        // initConfig(); //初始化配置
    });
});
