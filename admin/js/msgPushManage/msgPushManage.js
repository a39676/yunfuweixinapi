/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();//每个页面都有
        form.render();//每个页面都有
        laydate.render({
            elem: '#startTime'
            ,type: 'datetime'
            ,theme: '#588fd0'
        });
        laydate.render({
            elem: '#endTime'
            ,type: 'datetime'
            ,theme: '#588fd0'
        });

        /****************************************静态数据开始****************************************/
        table.render({
            id : "dataTable",
            elem : "#dataTable",
            limit : config.maxPageSize,
            height : "300",
            cols : [[
                {checkbox : true},
                {align : "center",field : "a",title : "机构名称"}
            ]],
            data : [
                {
                    a : '云浮市金融消费权益保护协会'
                },
                {
                    a : '人行云浮市中心支行'
                }
            ],
            size : "sm"
        });
        /****************************************静态数据结束****************************************/

    });
});