/**
 * 系统配置
 * 项目中用到的公共配置信息
 */
define(['./base','./cookie'],function (base,cookie) {

    /**
     * api_project
     * 项目名称
     */
    // var pathName = window.location.pathname;
    var pathName = '/admin/ghjkl';
    var api_project = pathName.substring(1,pathName.substr(1).indexOf('/') + 1);
    /**
     * api_host
     * 项目基础路径
     */
    var api_host = window.location.protocol + "//" + window.location.host + "/" + api_project + "/api/";
    /**
     * 获取用户信息
     */
    var userName = cookie.get_userName();
    if (!userName && window.location.href.indexOf("/login.html") === -1){
        // window.location.href = window.location.protocol + "//" + window.location.host + "/" + api_project + "/login.html";
        $("#loading").remove();
        $("#layui-layout-admin").show();
    }else{

        $("#loading").remove();
        $("#layui-layout-admin").show();
        //针对layui框架中laypage模块bug统一处理
        $(document).on("change",".layui-laypage-skip .layui-input",function () {
            var $this = $(this),
                $count = parseInt($this.parent().prev().prev().text()),
                $curr = $this.val();
            if ($curr > $count){
                $this.val($count);
                return false;
            }
        });
        //针对layui框架中form模块统一限制input长度
        $(document).on("input",".layui-input",function () {
            var $this = $(this);
            if ($this.val().length > 120){
                $this.val($this.val().slice(0,120));
                return false;
            }
        });
        //针对layui框架中table模块点击单元td弹出详细信息，重新渲染表格时不关闭弹窗的bug
        $(document).on("click",".layui-table-view tr",function (e) {
            var event = e || window.event;
            event.stopPropagation();
        });
        $(document).on("click",function () {
            $(".layui-table-tips-c").click();
        });
        //自动给页面中所有input输入框增加autocomplete属性为了实现不缓存用户历史输入
        $("input[type='text'],input[type='number']").attr("autocomplete","off");
    }
    /**
     * pageSize
     * 系统统一数据表格每页显示条数
     */
    var pageSize = cookie.get_pageSize();
    /**
     * 系统统一查询分页接口默认传递一个最大值查询所有数据时的默认值
     */
    var maxPageSize = 999;
    /**
     * 树节点默认第一个选中延迟加载时间
     */
    var treeDefaultSelectTimeOut = 600;
    /**
     * 自动加载异步树延迟时间
     */
    var treeAutoLoadTimeOut = 800;
    /**
     * 查询默认页面延迟时间，为了确保多法人逻辑中加载的iframe页面加载完成
     */
    var queryDefaultOpenPageTimeOut = 600;

    /**
     * 登陆方式配置
     * account: 账号密码登陆方式[true:开启,false:关闭]
     * phone: 短信验证码登陆方式[true:开启,false:关闭]
     * concatenation: 账号密码短信验证码级联登陆方式[true:开启,false:关闭]
     */
    var loginType = {
        account : true,
        phone : false,
        concatenation : false
    };

    /**
     * 页面访问权限控制
     */
    var queryPageLimit = function () {
        if ($("#loadModel").html()){ //阻止加载的排序页面调用页面访问权限控制接口
            return false;
        }
        if (window.location.pathname.indexOf(".html") !== -1 && window.location.pathname.indexOf("/index.html") === -1 && window.location.pathname.indexOf("/login.html") === -1){
            var url = window.location.href;
            if (url.indexOf('limit=no') === -1){ //需要校验该页面是否有权限访问

                if (url.indexOf('sysFunctionId') !== -1){
                    var urlStr = url.split('sysFunctionId')[0];
                    url = urlStr.substr(0,urlStr.length-1)
                }

                // var para = {};
                // para.page = url;
                // EasyAjax.get_json({
                //         url: api_host + "system/pagePermissionCheck",
                //         data: para,
                //         async: false
                //     },
                //     function (data) {
                        $(".wrap").show();
                    // });
            }else{
                $(".wrap").show(); //不需要校验该页面是否有权限访问
            }
        }
    };

    /**
     * 查询系统配置信息
     */
    var querySystemConfig = function () {
        var para = {};
        EasyAjax.post_json({
                url: api_host + "sysConfig/selectSysConfigs",
                data: para
            },
            function (data) {
                for (var i = 0; i < data.result.length; i++){
                    if (data.result[i].configName === 'SYSTEM_TITLE'){ //系统名称
                        document.title = data.result[i].dataValue;
                        $("#systemTitle").html(data.result[i].dataValue);
                    }
                    if (data.result[i].configName === 'PASSWORD_COMPLEXITY'){ //密码格式校验
                        $("#equalPassword").data("passwordComplexity",data.result[i].dataValue);
                    }
                }
            });
    };

    /**
     * 查询页面功能按钮权限
     */
    var queryFunctionBtnLimit = function () {
        var sysFunctionId = base.getrequest('sysFunctionId');
        if (sysFunctionId){
            var para = {};
            para.sysFunctionId = sysFunctionId;
            EasyAjax.get_json({
                    url: api_host + "system/pageButtonPermissionCheck",
                    data: para
                },
                function (data) {
                    var limitStr = JSON.stringify(data.result);
                    $("[data-limit]").each(function (index,ele) {
                        var $this = $(this);
                        if (limitStr.indexOf('"buttonFlag":' + '"' + $this.data("limit") + '"') !== -1){
                            $this.css({
                                "visibility" : "visible"
                            });
                        }else{
                            $this.remove();
                        }
                    });
                });
        }
    };

    /**
     * 页面初始化
     */

    $.cookie("callbackLock",false,{path:"/"}); //页面加载时先进性给登陆超时锁解锁操作

    queryPageLimit(); //页面访问权限控制

    if (window.location.pathname.indexOf(".html") !== -1 && window.location.href.indexOf("/login.html") === -1 && window.location.href.indexOf("/index.html") === -1 && window.location.href.indexOf("/personCenter.html") === -1){
        // queryFunctionBtnLimit(); //查询页面功能按钮权限
    }else{
        // querySystemConfig(); //查询系统配置信息
    }

    return{
        api_project : api_project,
        api_host : api_host,
        pageSize : pageSize,
        maxPageSize : maxPageSize,
        treeDefaultSelectTimeOut : treeDefaultSelectTimeOut,
        treeAutoLoadTimeOut : treeAutoLoadTimeOut,
        queryDefaultOpenPageTimeOut : queryDefaultOpenPageTimeOut,
        loginType : loginType
    }
});