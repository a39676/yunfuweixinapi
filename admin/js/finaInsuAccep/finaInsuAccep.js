/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();//每个页面都有
        form.render();//每个页面都有
        laydate.render({
            elem: '#presentationTime'
            ,theme: '#588fd0'
            ,format: 'yyyy/MM/dd HH:mm:ss'
        });
        laydate.render({
            elem: '#EndTime'
            ,theme: '#588fd0'
            ,format: 'yyyy/MM/dd HH:mm:ss'
        });
        /**
         * 页面布局使用
         * 数据表格中计算高度部分的固定高度
         */
        var fixedHeight = $(".wrap>.layui-form:first").height() + 20 + 5 + 33;
        /**
         * 全局变量
         */
        var modelIndex = null; //标记模态框

        /****************************************静态数据开始****************************************/
        table.render({
            id : "dataTable",
            elem : "#dataTable",
            limit : config.pageSize,
            height : "full-" + fixedHeight,//全屏高度减去计算出的高度
            cols : [[
            	{type:'checkbox'},
                {align : "center",field : "a",title : "受理号"},
                {align : "center",field : "b",title : "用户姓名"},
                {align : "center",field : "c",title : "用户手机"},
                {align : "center",field : "d",title : "产品代码"},
                {align : "center",field : "e",title : "产品名称"},
                {align : "center",field : "f",title : "产品类别"},
                {align : "center",field : "g",title : "年利率"},
                {align : "center",field : "h",title : "申请周期"},
                {align : "center",field : "i",title : "受理结果"},
                {align : "center",field : "j",title : "受理网点"},
                {align : "center",field : "k",title : "受理时间"},
                {align : "center",field : "l",title : "受理人"},
                {align : "center",field : "m",title : "联系电话"},
                {align : "center",field : "n",title : "拒收理由"},
                {align : "center",title : "操作",width : 120,toolbar : "#dataTableToolBarTpl",fixed: 'right'}
            ]],
            data : [
                {
                    a : '427155',
                    b : '王五',
                    c : '1234567890',
                    d : '123456',
                    e : '产品2',
                    f : '人寿保险',
                    g : '8.0%',
                    h : '365天',
                    i : '未受理',
                    j : '',
                    k : '',
                    l : '',
                    m : '',
                    n : '',
                },
                {
                    a : '427154',
                    b : '张三',
                    c : '1234567890',
                    d : '12345',
                    e : '产品1',
                    f : '人寿保险',
                    g : '8.0%',
                    h : '365天',
                    i : '通过',
                    j : '云城区支行',
                    k : '2018-08-08',
                    l : '李四',
                    m : '0987654321',
                    n : '',
                }
            ],
            size : "sm"
        });
        laypage.render({
            elem: "dataPage",
            count: 12,
            limit: config.pageSize,
            curr: 1,
            prev: "<i class='layui-icon'>&#xe603;</i>",
            next: "<i class='layui-icon'>&#xe602;</i>",
            layout: ["prev", "page", "next", "skip", "count"],
            theme: "#588fd0",
            jump: function(obj,first){
                if(!first){

                }
            }
        });

        /**
         * 点击新增按钮
         */
        $("#addBtn").on("click",function () {
            modelIndex = layer.open({
                type: 1,
                title: "新增",
                shade: [0.3, '#000'],
                area: ['600px', 'auto'],
                anim: 0,
                resize: false,
                content: $('#editModel')
            });
        });

        /**
         * 工具栏操作
         */
        table.on('tool(dataTable)', function(obj){
            var data = obj.data;
            var layEvent = obj.event;
            if (layEvent === 'edit'){ //编辑
                modelIndex = layer.open({
                    type: 1,
                    title: "编辑",
                    shade: [0.3, '#000'],
                    area: ['600px', 'auto'],
                    anim: 0,
                    resize: false,
                    content: $('#editModel')
                });
            }else if(layEvent === 'del'){
                layer.confirm('确认删除', {anim: 6}, function(index){
                    layer.close(index);
                });
            }
        });

        /**
         * 点击保存按钮
         */
        form.on('submit(saveModelBtn)', function(data){
            layer.close(modelIndex);
        });

        /**
         * 取消模态框按钮
         */
        $("#cancelModelBtn").on("click",function () {
            layer.close(modelIndex);
        });

        /****************************************静态数据结束****************************************/

    });
});