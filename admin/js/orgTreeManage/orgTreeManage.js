/**
 * 模块化加载
 * Layui中的模块
 */
layui.use(['element', 'form','layer','laydate','table','laypage','tree'], function(){
    window.element = layui.element; //获取element模块
    window.form = layui.form; //获取form模块
    window.layer = layui.layer; //获取layer模块
    window.laydate = layui.laydate; //获取laydate模块
    window.table = layui.table; //获取table模块
    window.laypage = layui.laypage; //获取laypage模块
    window.tree = layui.tree; //获取tree模块
    /**
     * 表格全局默认参数设置
     */
    table.set({
        page : false, //是否开启分页
        even : true, //是否开启隔行换色
        cellMinWidth : 80 //单元格的最小宽度
    });
    /**
     * 模块化加载
     * 自定义模块
     */
    require(["../module/config","../module/cookie","../module/base","../module/verify"],function (config, cookie, base, verify){
        /**
         * element组件渲染
         * form组件渲染
         */
        element.init();
        form.render();
        /**
         * 全局变量
         */
        var treeObj = null; //节点树对象
        var treeSelected = null; //当前选中节点信息

        /**
         * 查询机构类型
         */
        var queryOrgType = function (currPage,orgTypeId,type) {
            var para = {};
            // para.pageSize = config.maxPageSize;
            // para.currPage = currPage;
            para.orgTypeId = orgTypeId;
            EasyAjax.post_json({
                    url: config.api_host + "sysOrgTypeRelationship/selectSysOrgTypeRelationshipListNotWol",
                    data: para,
                    async: false
                },
                function (data) {
                    if (data.result.length){
                        $("#orgTypeId").html('<option value=""></option>');
                        var tpl = type === 'add' ? 'organizationTypeTpl' : 'organizationTypeAllTpl';
                        base.renderTpl({
                            tplId : tpl,
                            arr : data.result,
                            type : "append",
                            containerId : "orgTypeId"
                        });
                    }else{
                        $("#orgTypeId").html('<option value=""></option>');
                    }
                });
        };

        /**
         * 页面初始化
         * 初始化组织机构树
         */
        var initTree = function () {
            var filter = function (treeId, parentNode, responseData) {
                if (!responseData.result) return [];
                for (var i=0, l=responseData.result.length; i<l; i++) {
                    responseData.result[i].isParent = responseData.result[i].isLeaf === 'Y' ? false : true;
                    delete responseData.result[i].icon;
                }
                return responseData.result;
            };
            var zTreeOnAsyncSuccess = function (event, treeId, treeNode, msg) {
                base.treeAsyncSuccessCallback(event, treeId, treeNode, msg);
            };
            var zTreeOnAsyncError = function(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
                base.treeHandleStatus(XMLHttpRequest.status);
            };
            var setting = {
                async: {
                    enable: true,
                    contentType: "application/json",
                    url: config.api_host + "sysOrg/selectSysOrgList",
                    type: "post",
                    dataType: "json",
                    autoParam: ["orgId"],
                    otherParam: {},
                    dataFilter: filter
                },
                data: {
                    key: {
                        name: "orgName",
                        url: "",
                        isParent: "isParent"
                    }
                },
                view: {
                    showLine: false,
                    selectedMulti: false
                },
                callback: {
                    onAsyncSuccess: zTreeOnAsyncSuccess,
                    onAsyncError: zTreeOnAsyncError,
                    onClick: function (event, treeId, treeNode) {
                        treeSelected = treeNode; //将当前选中节点数据存给全局变量，给增删改查提供数据
                        $("#saveBtn").data("type","edit");
                        $("#orgCode").val(treeNode.orgCode);
                        $("#orgName").val(treeNode.orgName);
                        $("#description").val(treeNode.description);
                        $("#postalCode").val(treeNode.postalCode);
                        $("#orgAddress").val(treeNode.orgAddress);
                        $("#isEnable").val(treeNode.isEnable);

                        //为组织机构类型下拉框赋值
                        if (treeNode.isLeaf === 'Y'){
                            queryOrgType(1,treeNode.pareOrgTypeId,'edit'); //查询机构类型
                            $("#orgTypeId").val(treeNode.orgTypeId);
                            if (treeNode.orgTypeName === '法人'){
                                $("#orgTypeId").prop("disabled",true);
                            }else{
                                $("#orgTypeId").prop("disabled",false);
                                $("#orgTypeId").find("option:contains('法人')").prop("disabled",true);
                            }
                        }else{
                            base.renderTpl({
                                tplId : "organizationTypeAllTpl",
                                arr : [
                                    {
                                        orgTypeId : treeNode.orgTypeId,
                                        orgTypeName : treeNode.orgTypeName
                                    }
                                ],
                                type : "html",
                                containerId : "orgTypeId"
                            });
                            $("#orgTypeId").val(treeNode.orgTypeId).prop("disabled",true);
                        }

                        $("#seq").val(treeNode.seq);
                        $("#externalCode").val(treeNode.externalCode);
                        form.render('select');
                    }
                }
            };
            treeObj = $.fn.zTree.init($("#dataTree"), setting);
        };

        /**
         * 点击新增按钮
         */
        $("#addBtn").on("click",function () {
            if (treeSelected === null){
                layer.msg("请选择机构");
                return;
            }

            queryOrgType(1,treeSelected ? treeSelected.orgTypeId : null,'add'); //查询机构类型
            $("#orgTypeId").prop("disabled",false);

            $("#saveBtn").data("type","add");
            $("#orgCode").val("");
            $("#orgName").val("");
            $("#description").val("");
            $("#postalCode").val("");
            $("#orgAddress").val("");
            $("#isEnable").val("Y");
            $("#orgTypeId").val("");
            $("#seq").val("");
            $("#externalCode").val("");
            form.render('select');
        });

        /**
         * 点击保存按钮
         */
        $("#saveBtn").on("click",function () {
            var type = $("#saveBtn").data("type");
            if (!type){
                return;
            }
            if (!$("#orgName").val()){
                layer.msg("请输入组织机构名称");
                return;
            }
            if (!$("#orgTypeId").val()){
                layer.msg("请选择组织机构类型");
                return;
            }
            if (!$("#orgAddress").val()){
                layer.msg("请输入组织机构地址");
                return;
            }
            if (!$("#isEnable").val()){
                layer.msg("请选择启停状态");
                return;
            }
            if (!base.isNumber($("#seq").val())){
                layer.msg("请输入数字类型的排序字段");
                return;
            }
            if (type === "add"){
                EasyAjax.get_json({
                        url: config.api_host + "sequence/id"
                    },
                    function (data) {
                        var para = {};
                        para.orgId = data.result;
                        para.pareOrgId = treeSelected ? treeSelected.orgId : null;
                        para.orgCode = treeSelected ? treeSelected.orgCode : null;
                        para.orgName = $("#orgName").val();
                        para.description = $("#description").val();
                        para.postalCode = $("#postalCode").val();
                        para.orgAddress = $("#orgAddress").val();
                        para.isEnable = $("#isEnable").val();
                        para.orgTypeId = $("#orgTypeId").val();
                        para.seq = $("#seq").val();
                        para.externalCode = $("#externalCode").val();
                        para.orgPath = (treeSelected ? base.treePath(treeObj,"add") : "") + para.orgName;
                        para.legalPersonOrgId = treeSelected ? treeSelected.legalPersonOrgId : null;
                        EasyAjax.post_json({
                                url: config.api_host + "sysOrg/addSysOrg",
                                data: para
                            },
                            function (data) {
                                layer.msg("保存成功");
                                base.treeAddNode(treeObj,"orgId","pareOrgId");
                            });
                    });
            }else if (type === "edit"){
                var para = {};
                para.orgId = treeSelected.orgId;
                para.pareOrgId = treeSelected.pareOrgId;
                para.orgCode = $("#orgCode").val();
                para.orgName = $("#orgName").val();
                para.description = $("#description").val();
                para.postalCode = $("#postalCode").val();
                para.orgAddress = $("#orgAddress").val();
                para.isEnable = $("#isEnable").val();
                para.orgTypeId = $("#orgTypeId").val();
                para.seq = $("#seq").val();
                para.externalCode = $("#externalCode").val();
                para.orgPath = base.treePath(treeObj,"edit") + para.orgName;
                para.legalPersonOrgId = treeSelected.legalPersonOrgId;
                EasyAjax.post_json({
                        url: config.api_host + "sysOrg/updateSysOrg",
                        data: para
                    },
                    function (data) {
                        layer.msg("保存成功");
                        base.treeUpdateNode(treeObj,"orgId","pareOrgId");
                    });
            }
        });

        /**
         * 点击删除按钮
         */
        $("#removeBtn").on("click",function () {
            if (treeSelected === null){
                layer.msg("请选择组织机构");
            }else if (treeSelected.isLeaf === 'N'){
                layer.msg("请先删除所有下级组织机构");
            }else{
                // var pNode = treeSelected.getParentNode();
                // if (pNode !== null){
                    layer.confirm('确认删除', {anim: 6}, function(index){
                        var para = {};
                        para.orgId = treeSelected.orgId;
                        para.pareOrgId = treeSelected.pareOrgId;
                        para.orgCode = treeSelected.orgCode;
                        para.orgName = treeSelected.orgName;
                        para.description = treeSelected.description;
                        para.postalCode = treeSelected.postalCode;
                        para.orgAddress = treeSelected.orgAddress;
                        para.isEnable = treeSelected.isEnable;
                        para.orgTypeId =treeSelected.orgTypeId;
                        para.seq = treeSelected.seq;
                        para.externalCode = treeSelected.externalCode;
                        para.orgPath = treeSelected.orgPath;
                        para.legalPersonOrgId = treeSelected.legalPersonOrgId;
                        para.isLeaf = treeSelected.isLeaf;
                        EasyAjax.post_json({
                                url: config.api_host + "sysOrg/deleteSysOrg",
                                data: para
                            },
                            function (data) {
                                layer.msg("删除成功");
                                layer.close(index);
                                base.treeDeleteNode(treeObj,"orgId","pareOrgId");

                                //重置初始状态
                                treeSelected = null;
                                $("#orgCode").val("");
                                $("#orgName").val("");
                                $("#description").val("");
                                $("#postalCode").val("");
                                $("#orgAddress").val("");
                                $("#isEnable").val("Y");
                                $("#orgTypeId").val("");
                                $("#seq").val("");
                                $("#externalCode").val("");
                                form.render('select');

                            });
                    });
                // }else{
                //     layer.msg("根节点不能删除");
                // }
            }
        });

        /**
         * 页面初始化
         */
        initTree(); //页面初始化
    });
});