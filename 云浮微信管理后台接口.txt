1.产品
产品类型
	1 理财产品
	2 融资产品
	3 保险产品
	4 证券产品
	5 互联网+信用三农

产品子类型
	理财产品
		deposit("存款", 1), 
		nationalDebt("国债", 2), 
		monetaryFund("货币基金", 3), 
		financial("理财", 4), 
		other("其他理财", 5), 
	融资产品
		smallBusiness("小微企业贷款", 1), 
		buyHouse("购房贷款", 2), 
		buyCar("购车贷款", 3), 
		personalConsumption("个人消费贷款", 4), 
		personalBusiness("个人经营性贷款", 5),
		other("其他贷款", 6),
	保险产品
		lifeInsurance("人寿保险", 1), 
		propertyInsurance("财产保险", 2), 
		medicalInsurance("医疗保险", 3), 
		other("其他保险服务", 4), 
	证券产品
		securities("证券", 1), 
		stock("股票", 2), 
		other("其他证券服务", 3), 
	互联网+信用三农
		fundraisingProducts("筹资产品", 1), 

利息类型
	/** 年化利率 */
	AnnualizedInterestRate("AnnualizedInterestRate", 1),
	/** 月利率 */
	MonthlyInterestRate("MonthlyInterestRate", 2),

产品详细
ProductsPO
	Long id
	Date createTime
	Date expirationTime 截止日期
	String productName
	String productCode
	Integer productType 产品类型
	Integer productSubType 产品子类型
	Integer interestType 利息类型
	BigDecimal interestRate 利息
	String productIntroduction 产品简介

ProductVO
	继承ProductsPO外
	BigDecimal insuranceAmountMin 保险产品特有字段,最小保额
	BigDecimal insuranceAmountMax 保险产品特有字段,最大保额

1.1 添加产品
/product/addNewProduct
http: post
请求参数:
	json
	Long orgId 机构ID
	String productCode 产品代码
	String productName 产品名称
	String productIntroduction 产品简介
	String article 产品详细介绍
	Integer productType 产品类型(见上)
	Integer productSubType 产品子类型(见上)
	String expirationTime 截止日期
	BigDecimal interestRate 利息
	Integer interestType 利息类型(见上)
	BigDecimal insuranceAmountMin 保险产品特有字段,最小保额
	BigDecimal insuranceAmountMax 保险产品特有字段,最大保额
返回:
	json
	String result (0:成功,其他为异常代码)
	String message 
	Long productId 新增的产品id


1.6 机构管理员查找产品分页
/product/findProductPageForAdmin
http: post
请求参数:
	json
	Integer productType  产品类型 (必填)
	Integer productSubType 产品子类型
	Long orgId  机构ID
	String expirationTime 截止日期 
	String productName 产品名称
	String productCode 产品代码
	Integer pageSize 
	Integer pageNo 
返回:
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<ProductsPO> productPage
	int resultCount

1.7 删除产品
/product/deleteProduct
http: post
请求参数:
	json
	Long id  产品id (必填)

返回:
	json
	String result (0:成功,其他为异常代码)
	String message 

1.8 编辑产品
/product/editProduct
http: post
请求参数:
	json
	Long productId 产品ID
	String productCode 产品代码
	String productName 产品名称
	String productIntroduction 产品简介
	String article 产品详细介绍
	Integer productType 产品类型(见上)
	Integer productSubType 产品子类型(见上)
	String expirationTime 截止日期
	BigDecimal interestRate 利息
	Integer interestType 利息类型(见上)
返回:
	json
	String result (0:成功,其他为异常代码)
	String message 
	
----------------------------------------------------------------

2. 预约

2.1 产品预约

2.1.1 接收产品预约申请
/reservation/acceptReservationProduct

http:post
请求参数:
	json
	Long staffId 受理人员工ID
	Long reservationId 预约ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

2.1.2 拒绝产品预约申请
/reservation/rejectReservationProduct

http:post
请求参数:
	json
	String remark 拒绝理由
	Long reservationId 预约ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

ReservationProductVO
	Long id
	String nickName 用户姓名/昵称
	String userName 用户手机号/用户名
	String interestType 利息类型
	BigDecimal interestRate 利息
	Date expirationTime 产品截止日期
	String productName 产品名称
	String productCode 产品代码
	String productTypeName 产品类型
	String orgName 受理机构名
	Long orgId 机构ID
	String staffName 受理人姓名
	String reservationStatuString 预约状态(名称)
	Integer reservationStatu 预约状态
	String remark 拒绝理由
	Date receptionTime 处理时间
	String receptionTimeString 处理时间(字符串)
	Date reservationTimeStart 客户申请预约开始时间
	Date reservationTimeEnd 客户申请预约结束时间
	BigDecimal amount 产品金额(信用三农特殊字段)

ReservationsBusinessVO
	Long id 受理号
	Date createTime 创建时间
	Date reservationTimeStart
	Date reservationTimeEnd
	String reservationTimeStartString
	String reservationTimeEndString
	Integer reservationStatus 预约状态(code)
	String reservationStatusString 预约状态(描述)
	Long userId 用户ID
	Long staffId 受理工作人员ID
	Long orgId 受理网点ID
	Long businessId 
	String remark 拒绝理由
	String staffName 受理人姓名
	String orgName 受理网点名(机构名)
	String nickName 用户姓名(昵称)
	String userName 用户账号(手机号)
	String businessContent 业务内容
	String moneyType
	Integer coin01
	Integer coin05
	Integer coin1
	Integer paper1
	Integer paper5
	Integer paper10
	Integer paper20
	Integer paper50
	Integer paper100
	Date receptionTime 受理时间
	String receptionTimeString 受理时间(字符串)
	

2.4 管理员查找产品预约分页
/reservation/findReservationPageForAdmin
http:post
请求参数:
	json
	Long orgId 机构id
	Long id 受理号
	Date receptionTime 受理时间
	String staffName 受理人员姓名
	String mobile 联系电话
	Integer pageSize
	Integer pageNo
	Integer productType
	Integer productSubType

返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	Integer resultCount 结果条数
	List<ReservationProductVO> reservationList (ReservationProductVO 参数见上)


2.4 管理员查找零钞兑换预约分页
/reservation/findReservationExchangeDibPageForAdmin
http:post
请求参数:
	json
	Long orgId 机构id
	Long id 受理号
	Date receptionTime 受理时间
	String staffName 受理人员姓名
	String mobile 联系电话
	Integer pageSize
	Integer pageNo

返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	Integer resultCount 结果条数
	List<ReservationsBusinessVO> reservationList (ReservationsBusinessVO 参数见上)

2.5 受理零钞兑换预约
/reservation/acceptReservationBusiness
http:post
请求参数:
	json
	Long staffId 受理人员id
	Long reservationId 受理号

返回
	json
	String result (0:成功,其他为异常代码)
	String message 

2.6 拒绝零钞兑换预约
/reservation/rejectReservationBusiness
http:post
请求参数:
	json
	String remark 拒绝理由
	Long reservationId 受理号

返回
	json
	String result (0:成功,其他为异常代码)
	String message 

2.7 服务预约统计
/reservation/getStatisticsForHomePage
http:post
请求参数:
	无

返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<ReservationStatisticsVO> statisticsResultList {
		String name 统计项目名
		Integer count 统计项目数量
	} 
	json{
		exchangeDib 零钞兑换
		financial 理财产品
		financing 融资产品
		insurance 保险
		securities  证券
		agriculturalCredit 信用三农
	}

----------------------------------------------------------------

3. 服务模块
3.1 服务管理
3.1.1 调整 机构 零钞兑换 每日受理限额
/business/updateExchangeDibOrgLimit
http: post
请求参数:
	json
	Long orgId 机构ID
	Integer coin01Limit 一角硬币(个)每日兑换上限
	Integer coin05Limit 五角硬币(个)每日兑换上限
	Integer coin1Limit 一元硬币(个)每日兑换上限
	Integer paper1Limit 一元纸币(张)每日兑换上限
	Integer paper5Limit 五元纸币(张)每日兑换上限
	Integer paper10Limit 十元纸币(张)每日兑换上限
	Integer paper20Limit 二十元纸币(张)每日兑换上限
	Integer paper50Limit 五十元纸币(张)每日兑换上限
	Integer paper100Limit 一百元纸币(张)每日兑换上限
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

3.1.2 查询 机构 零钞兑换 每日受理限额
/business/findExchangeDibOrgLimit
http: post
请求参数:
	json
	Long orgId 机构ID
返回
	json
	Long orgId
	Date createTime
	Date updateTime
	exchangeDibOrganizationLimit {
		Integer coin01Limit 当日限额
		Integer coin01Used 当日已用
		Integer coin05Limit
		Integer coin05Used
		Integer coin1Limit
		Integer coin1Used
		Integer paper1Limit
		Integer paper1Used
		Integer paper5Limit
		Integer paper5Used
		Integer paper10Limit
		Integer paper10Used
		Integer paper20Limit
		Integer paper20Used
		Integer paper50Limit
		Integer paper50Used
		Integer paper100Limit
		Integer paper100Used 
	}

3.1.3 调整 个人 零钞兑换 每日限额
/business/initOrUpdateDefaultUserLimit
http: post
请求参数:
	json
	Long userId 用户ID (若为空, 视为各个用户的每日上限)
	Integer coin01Limit 一角硬币(个)每日兑换上限
	Integer coin05Limit 五角硬币(个)每日兑换上限
	Integer coin1Limit 一元硬币(个)每日兑换上限
	Integer paper1Limit 一元纸币(张)每日兑换上限
	Integer paper5Limit 五元纸币(张)每日兑换上限
	Integer paper10Limit 十元纸币(张)每日兑换上限
	Integer paper20Limit 二十元纸币(张)每日兑换上限
	Integer paper50Limit 五十元纸币(张)每日兑换上限
	Integer paper100Limit 一百元纸币(张)每日兑换上限
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

3.1.4 查询 个人 零钞兑换 每日受理限额
/business/findExchangeDibUserLimit
http: post
请求参数:
	json
	Long userId 用户ID(可空,如空则返回全局用户初始限额)
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	ExchangeDibUserLimitPO userLimit {
		Date createTime
		Date updateTime
		Integer coin01Limit 当日限额
		Integer coin01Used 当日已用
		Integer coin05Limit
		Integer coin05Used
		Integer coin1Limit
		Integer coin1Used
		Integer paper1Limit
		Integer paper1Used
		Integer paper5Limit
		Integer paper5Used
		Integer paper10Limit
		Integer paper10Used
		Integer paper20Limit
		Integer paper20Used
		Integer paper50Limit
		Integer paper50Used
		Integer paper100Limit
		Integer paper100Used 
	}



----------------------------------------------------------------

4. 区域位置
areaType 区域类型 (1:乡/镇/街道, 2:区/县, 3:市)
GeographicalAreaPO 
	Long id
	Date createTime
	String areaName 区域名
	Integer areaType 区域类型(见上)
	Long belongTo 区域归属于(上一级区域的id)
	
4.1 增加区域
/geographical/addNewGeographicalArea
http: post
请求参数:
	json
	Integer areaType 区域类型
	String areaName 区域名称
	Long belongTo 区域归属于(上一级区域的id, 可空)
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

4.2 删除区域
/geographical/deleteGeographicalArea
http: post
请求参数:
	json
	Long id 
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

4.3 根据上一级区域ID查找归属区域
/geographical/findByBelongId
http: post
请求参数:
	json
	Long areaParentId 区域归属于(上一级区域的id, 若空, 返回最上一级的区域列表)
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<GeographicalAreaPO> areaList 区域列表

----------------------------------------------------------------

5. 机构模块

机构
OrganizationsPO
	Long id
	String orgName 机构名
	Integer orgType 机构类型 (见下 OrganizationsType)
	String orgCode 机构代码
	Long belongTo 隶属机构ID
	Long topHeadOrg 隶属机构顶端的机构ID
	Date createTime
	Date editTime
	Integer editCount
	Boolean isDelete

OrganizationsVO
	继承 OrganizationsPO 
	String staffName  员工姓名
	String staffNumber 员工工号(编号)
	String mobile 手机号
	String belongOrgName 上级机构名
	String areaName 区域名


机构类型
OrganizationsType
union("协会", 1), // 协会
bank("银行", 2), // 银行

StaffDetailVO
	Long id 
	Date createTime
	Long userId
	Integer positionType
	String staffsName 员工姓名
	String staffNumber  员工工号
	String mobile 手机号
	String authName 角色名
	Long authId 角色id
	Integer gender 性别(0:女, 1:男, -1:保密)

	

5.1 根据区域ID 查找对应机构
/organizations/findOrgByGeographicalId
http: post
请求参数:
	json
	Long geographicalId 区域id
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<OrganizationsPO> orgList 机构列表 (OrganizationsPO 机构参数见上)

5.3 分页查找机构
/organizations/findOrgPage
http: post
请求参数:
	json
	String orgCode 机构代码
	String orgName 机构名称
	Integer orgType 机构类型(OrganizationsType 见上)
	String staffNumber 员工号码
	String staffName 员工姓名
	Long mobile 手机号
	Long geographicalAreaId  地理区域ID
	Integer pageSize 每页数量
	Integer pageNo 页码
	Long topHeadOrg 顶级机构ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<OrganizationsVO> orgPage 机构列表 (OrganizationsVO 机构参数见上)
	resultCount 结果条数

5.3.1 根据上一级机构ID 获取其下直属机构
/organizations/findOrgByDirectBelongTo
http: post
请求参数:
	Long orgId 机构ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<OrganizationsVO> orgList 机构列表 (OrganizationsVO 机构参数见上)


5.3.2 分页查找机构
/organizations/findBranchOrgPage
http: post
请求参数:
	json
	Integer pageSize 每页数量
	Integer pageNo 页码
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<OrganizationsVO> orgPage 机构列表 (OrganizationsVO 机构参数见上)
	resultCount 结果条数


5.4 协会管理员新增机构
/organizations/createNewOrgByAdmin
http: post
请求参数:
	json
	Integer orgType 机构类型(OrganizationsType 见上)
	String orgCode 机构代码
	String orgName 机构名称
	Long geogralphicalId 区域ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

5.4.1 机构管理员新增分支机构
/organizations/createNewBranchOrg
http: post
请求参数:
	json
	String orgCode 新机构代码
	String orgName 新机构名字
	Long belongTo 上一级机构ID
	Long geogralphicalId 归属地理位置ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 


5.5 以机构ID,等条件查找对应工作人员
/organizations/findStaffsByCondition
http: post
请求参数:
	json
	Long orgId 机构id
	String staffNumber 工号
	String staffName 员工姓名
	String mobile 员工手机号
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<StaffDetailVO> staffList 人员列表(StaffDetailVO 具体字段见上)
	Integer resultCount

5.6 编辑机构信息
/organizations/updateOrgInfo
http: post
请求参数:
	json
	Long id  机构ID
	String orgCode 新机构代码
	String orgName 新机构名称
	String orgType 新机构类型
	Long topHeadOrgId 新归属顶级机构ID
	Long belongTo 新归属机构ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

5.7 删除机构
/organizations/deleteOrg
http: post
请求参数:
	json
	Long id  机构ID

返回
	json
	String result (0:成功,其他为异常代码)
	String message 

5.8 
/** 为已经登录管理员查找其下所有机构, 若登录账号为协会账号,则返回所有顶级机构 */
/organizations/findOrgForLoginManger
http: post
请求参数:
	无
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<OrganizationsPO> orgList 机构列表

5.9
/** 为已登录管理员查找其对应机构 */
/organizations/findTopOrgForLoginManger
http: post
请求参数:
	无
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	OrganizationsPO org 机构列表

5.10
/** 删除工作人员 */
/organizations/deleteStaff
http: post
请求参数:
	Long staffId 工作人员id
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

5.11
/** 编辑机构管理员工号,姓名 */
/organizations/changeStaffName
http: post
请求参数:
	json
	String nickName
	String mobile
	String staffNumber
返回
	json
	String result (0:成功,其他为异常代码)
	String message 


----------------------------------------------------------------

6 用户

authId (角色ID)
	机构管理员 1
	网点业务员 2
	网点经办员 3
	平台管理员 4

6.1 发送验证码
/user/sendValidSms
http: post
请求参数:
	json
	String mobile (手机号)
返回:
	json	
	String result (0:成功,其他为异常代码)
	String message


6.5 用户登录
/auth/login_check
请求参数:
	json
	String user_name
	String pwd

6.6 新增后台工作人员
/user/newManagerRegist
http: post
请求参数:
	json
	String userName  用户名(手机号)
	String nickName  昵称
	String pwd  密码
	String pwdRepeat 确认密码
	Integer gender 性别(0:女, 1:男, -1不指定)
	String qq (可空)
	String mobile 手机
	String staffNumber 工號
	Long orgId 机构id
	Integer authId 角色id(authId 见上, 若空, 指定为机构管理员(3))
返回:
	json	
	String result (0:成功,其他为异常代码)
	String message


6.7 为用户设定/更改角色
/auth/changeAuth
http: post
请求参数:
	json
	Long userId 用户ID
	Long newAuthId 角色ID
返回:
	json	
	String result (0:成功,其他为异常代码)
	String message

6.8 查询用户权限列表
/user/getUserRoles
http: post
请求参数:
	无
返回:
	json	
	String result (0:成功,其他为异常代码)
	String message
	String json (权限名称数组)

6.9 查询用户数量统计
/user/countUser
http: post
请求参数:
	无
返回:
	json	
	String result (0:成功,其他为异常代码)
	Long todayCount 今日注册用户数量 (查询异常也会返回0)
	Long allCount 用户总数 (查询异常也会返回0)

6.10 后台工作人员绑定
/user/managerBind
http: post
请求参数:
	json
	String wxCode
	String mobile
	Integer validCode
返回:
	json	
	String result (0:成功,其他为异常代码)

6.14 变更用户名
/user/changeUserName
http: post
请求参数:
	json
	String newUserName
	String oldUserName
	String pwd
	String pwdRepeat
返回:
	json	
	String result (0:成功,其他为异常代码)

6.15 管理员变更其他用户密码
/user/resetPasswordByAdmin
http: post
请求参数:
	json
	String mobile
	String pwd
	String pwdRepeat
返回:
	json	
	String result (0:成功,其他为异常代码)


----------------------------------------------------------------

7. 服务
ComplaintType
("金融消费侵权", 1),
("金融违法活动", 2),
("非法金融广告", 3),

7.3 投诉列表分页
/business/findComplaintPage
http:post
请求参数:
	json
	Integer pageNo
	Integer pageSize
	Integer complaintStatus /** 0:全部, 1:已处理, 2:已拒绝 */
	Date receptionTime 受理日期
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	Integer resultcount
	List<BusinessComplaintVO> complaintList
		Long id
    	Long complaintUserId 投诉者ID
    	Long orgId 被投诉机构ID
    	Integer complaintType 投诉类型
    	String content 投诉内容
    	Date createTime 
    	Boolean isReject 
    	Boolean isDelete
    	Long handlerStaffId 
    	String orgName 机构名
		String complaintTypeString 投诉类型名
		String complaintUserNickName 投诉者姓名
		String complaintUserMobile 投诉者手机
		String complaintResultString 投诉结果状态
		String receptionTimeString 受理时间

7.4 投诉拒绝
/business/rejectComplaint
http:post
请求参数:
	json
	Long id
	String remark 
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

7.5 委派拒绝
/business/sendComplaint
http:post
请求参数:
	json
	Long id
	Long handlerStaffId 受理人ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

----------------------------------------------------------------

8. 公告/通知/产品说明文档

ArticlePO
	Long articleId 文章id
	Long userId 发表者id
	Integer articleType 文章类型
	String articleTitle 文章标题
	String path 文章保存路径
	Date createTime 创建时间
	Date editTime 再次编辑时间
	Long editOf 编辑自(原文id)
	Integer editCount 编辑次数
	Boolean isDelete 
	Integer articleChannel 所属于文章频道

ArticleChannelType
	公告  1
	理财知识  2
	保险知识  3
	证券知识  4
	金融政策  5
	所有知识 6 (包括2, 3, 4, 5)

8.2 获取产品详情
/article/findArticleByProductId
http:post
请求参数:
	json
	Long productId 产品id
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	ArticleVO articleLongVO  产品详情(具体参数见下)

ArticleVO
	String articleTitle 产品标题
	List<String> contentLines 产品说明原文
	String createDateString 发表时间
	Date createTime 发表时间

8.3 添加通知/公告
/article/creatingArticle
http:post
请求参数:
	json
	String title 标题
	String content 内容
	Integer articleChannel 类型(ArticleChannelType 见上)
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

8.4 加载通知/公告分页
/article/findArticlePageByChannel
http:post
请求参数:
	json
	Integer channelId 频道ID
	Integer pageNo
	Integer pageSize
	private Date postDate 发布时间
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	List<ArticlePO> articleList (通知/公告列表)
	Integer resultCount

8.5 编辑通知/公告
/article/editArticle
http:post
请求参数:
	json
	Long articleId
	String title
	String content
返回
	json
	String result (0:成功,其他为异常代码)
	String message 

8.6 删除通知/公告
/article/deleteArticle
http:post
请求参数:
	json
	Long id
返回
	json
	String result (0:成功,其他为异常代码)
	String message 


8.7 以id查找通知/公告
/article/findArticleById
http:post
请求参数:
	json
	Long id
返回
	json
	String result (0:成功,其他为异常代码)
	String message 
	ArticleVO articleLongVO
	Long articleId


----------------------------------------------------------------

9. 微信
9.1 获取推送时间段
/weixin/getPushTime
http:post
请求参数:
	无
返回
	json
	String result (0:成功,其他为异常代码)
	String message
	Date pushStartTime
	String pushStartTimeStr
	Date pushStopTime
	String pushStopTimeStr

9.2 设置推送时间段
/weixin/setPushTime
http: post
请求参数:
	json
	Date pushStartTime
	Date pushStopTime
返回
	json
	String result (0:成功,其他为异常代码)
	String message
	Date pushStartTime

----------------------------------------------------------------

10. 有奖问答
Question
	Long id
    Date createTime
    Boolean isDelete
    String question 题目
    String option1 选项1
    String option2 选项2
    String option3 选项3
    String option4 选项4

10.1 添加问题
/popQuiz/addNewQuestion
http:post
请求参数:
	json
	String question 题目
	String option1 选项1
	String option2 选项2
	String option3 选项3
	String option4 选项4
返回
	json
	String result (0:成功,其他为异常代码)
	String message

10.2 搭建问卷
/popQuiz/buildQuestionnaire
http:post
请求参数:
	json
	Long questionnaireId 问卷ID
	String questionnaireName 问卷名
	String questionIds
	(如果 问卷名不为空, 即使有问卷ID, 视为创建新问卷.)
	(如果 有问卷ID, 视为更改原问卷题库.)
返回
	json
	String result (0:成功,其他为异常代码)
	String message

10.3 搜索问题分页
/popQuiz/findQuestionPage
http:post
请求参数:
	json
	Integer pageNo
	Integer pageSize
	Long questionnaireId 问卷ID(如空, 在所有题库中分页返回)
返回
	json
	String result (0:成功,其他为异常代码)
	String message
	List<Question> questions
	List<QuestionVO> questionList
		QuestionVO question
			Long id
			Date createTime
			Boolean isDelete
			String question 问题
			String option1 选项
			String option2
			String option3
			String option4
			Integer collectOption 正确选项
	Integer resultCount


10.4 添加全对答题者
/popQuiz/addNewCollectUser
http:post
请求参数:
	json
	Long questionnaireId 问卷ID(如空, 在所有题库中分页返回)
返回
	json
	String result (0:成功,其他为异常代码)
	String message

10.5 搜索问题(单个)
/popQuiz/findQuestion
http:post
请求参数:
	json
	Long id 问题ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message
	QuestionVO question
		Long id
		Date createTime
		Boolean isDelete
		String question 问题
		String option1 选项
		String option2
		String option3
		String option4
		Integer collectOption 正确选项

10.6 查找问卷(全部)
/popQuiz/findQuestionnaire
http:post
请求参数:
	json
	无
返回
	json
	String result (0:成功,其他为异常代码)
	String message
	List<Questionnaire> list
		Long id
    	Date createTime
    	Boolean isDelete
    	String questionnaireName


10.7 以问卷ID查找答对者(分页)
/popQuiz/findQuestionnaireUserByQuestionnaireId
http:post
请求参数:
	json
	Long id 问卷ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message
	Integer resultCount
	List<QuestionnaireUser> list
		Long questionnaireId
	   	Long userId
	   	String nickName
	   	String mobile
	   	Date createTime
	   	Boolean isDelete

10.8 删除问卷
/popQuiz/deleteQuestionnaire
http:post
请求参数:
	json
	Long id 问卷ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message

10.9 删除问题
/popQuiz/deleteQuestionnaire
http:post
请求参数:
	json
	Long id 问题ID
返回
	json
	String result (0:成功,其他为异常代码)
	String message
----------------------------------------------------------------